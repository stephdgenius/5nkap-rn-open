const nkapPackage = require("./package.json");

const IS_DEV = process.env.APP_ENV === "development";
const IS_ALPHA = process.env.APP_ENV === "staging";
const IS_BETA = process.env.APP_ENV === "beta";
const IS_PROD = process.env.APP_ENV === "production";
const APP_URL = process.env.APP_URL;

export default () => {
  return {
    name: IS_PROD
      ? `${nkapPackage.name}`
      : IS_BETA
      ? `${nkapPackage.name} (Beta)`
      : IS_ALPHA
      ? `${nkapPackage.name} (Alpha)`
      : `${nkapPackage.name} (Dev)`,
    slug: "5nkap",
    version: nkapPackage.version,
    orientation: "portrait",
    icon: "./assets/icon.png",
    userInterfaceStyle: "light",
    splash: {
      image: "./assets/splash.png",
      resizeMode: "contain",
      backgroundColor: "#ffffff",
    },
    assetBundlePatterns: ["**/*"],
    ios: {
      supportsTablet: false,
      buildNumber: "4",
      bundleIdentifier:
        IS_PROD || IS_BETA ? "com.fivenkap.app" : "com.fivenkap.app-alpha",
    },
    android: {
      versionCode: 4,
      adaptiveIcon: {
        foregroundImage: "./assets/adaptive-icon.png",
        backgroundColor: "#ffffff",
      },
      permissions: [
        "android.permission.INTERNET",
        "android.permission.WRITE_EXTERNAL_STORAGE",
        "android.permission.MODIFY_AUDIO_SETTINGS",
      ],
      package: "com.fivenkap.app",
    },
    web: {
      favicon: "./assets/favicon.png",
    },
    updates: {
      url: "https://u.expo.dev/ecadc701-bee6-4344-af01-d0ca0726efc2",
    },
    plugins: [
      [
        "expo-build-properties",
        {
          android: {
            usesCleartextTraffic: true,
          },
        },
      ],
      [
        "expo-camera",
        {
          cameraPermission: "Allow $(PRODUCT_NAME) to access your camera",
          microphonePermission:
            "Allow $(PRODUCT_NAME) to access your microphone",
          recordAudioAndroid: true,
        },
      ],
      [
        "expo-media-library",
        {
          photosPermission: "Allow $(PRODUCT_NAME) to access your photos.",
          savePhotosPermission: "Allow $(PRODUCT_NAME) to save photos.",
          isAccessMediaLocationEnabled: true,
        },
      ],
    ],
    runtimeVersion: {
      policy: "appVersion",
    },
    extra: {
      eas: {
        projectId: "",
      },
      APP_URL,
      IS_DEV,
      IS_ALPHA,
      IS_BETA,
      IS_PROD,
    },
    owner: "",
  };
};
