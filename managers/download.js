import { Share, Platform } from "react-native";
import * as FileSystem from "expo-file-system";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default class DownloadManager {
  nkapDir = FileSystem.cacheDirectory + "5nkap";
  downloadResumable;
  downloadStatus;
  toast;

  constructor(toastManager) {
    this.toast = toastManager;
    this.resumeLast();
  }

  // Checks if gif directory exists. If not, creates it
  ensureDirExists = async () => {
    const dirInfo = await FileSystem.getInfoAsync(gifDir);
    if (!dirInfo.exists) {
      console.log("Gif directory doesn't exist, creating…");
      await FileSystem.makeDirectoryAsync(gifDir, { intermediates: true });
    }
  };

  getFileExtension = (downloadUrl) => {
    return downloadUrl.slice(((downloadUrl.lastIndexOf(".") - 1) >>> 0) + 2);
  };

  generateUniqSerial = () => {
    return "xxxx-xxxx".replace(/[x]/g, (c) => {
      const r = Math.floor(Math.random() * 8);
      return r.toString(8);
    });
  };

  generateFilename(url) {
    let result;
    const rootName = "-list-by-5NKAP";

    switch (url) {
      case url.match("/produits/i"):
        result = `products-${rootName}`;
        break;

      case url.match("/client/i"):
        result = `customer${rootName}`;
        break;

      case url.match("/actionnaire/i"):
        result = `shareholder${rootName}`;
        break;

      case url.match("/creancier/i"):
        result = `creditor${rootName}`;
        break;

      case url.match("/debiteur/i"):
        result = `debtor${rootName}`;
        break;

      case url.match("/fournisseur/i"):
        result = `provider${rootName}`;
        break;

      case url.match("/subventionneur/i"):
        result = `funder${rootName}`;
        break;

      default:
        result = `${generateUniqSerial}-by-5NKAP`;
        break;
    }
    return result;
  }

  callback = (downloadProgress) => {
    const progress =
      downloadProgress.totalBytesWritten /
      downloadProgress.totalBytesExpectedToWrite;
    this.downloadStatus = progress;
  };

  shareFile = async (url) => {
    try {
      return Share.share({
        message: "Sélectionner la destination du fichier",
        url: url,
      });
    } catch (error) {
      return error;
    }
  };

  createTask = async (downloadUrl) => {
    console.log("downloadUrl: ", downloadUrl);
    await ensureDirExists();

    let filename = await generateFilename(downloadUrl);

    const fileUri = `${nkapDir}${filename}${getFileExtension(downloadUrl)}`;
    const fileInfo = await FileSystem.getInfoAsync(fileUri);

    if (!fileInfo.exists) {
      console.log(`${filename} File isn't exist. Downloading…`);
      downloadResumable = FileSystem.createDownloadResumable(
        downloadUrl,
        fileUri,
        {},
        this.callback
      );
      this.startTask();
      this.toast.hide();
      this.toast.show({
        type: "progress",
        title: `Téléchargement de ${filename}`,
        progress: this.downloadStatus / 100,
      });
    }
  };

  startTask = async () => {
    try {
      const { uri } = await downloadResumable.downloadAsync();
      console.log("Finished downloading to ", uri);
      this.toast.hide();
      this.toast.show({
        type: "success",
        title: "Téléchargement terminé",
        description: "Votre fichier a été téléchargé",
      });
      await this.shareFile(uri);
    } catch (e) {
      this.toast.hide();
      this.toast.show({
        type: "error",
        title: "Téléchargement",
        description: "Erreur lors du téléchargement",
        progress: this.downloadStatus / 100,
      });
      console.error(e);
    }
  };

  pauseTask = async () => {
    try {
      await this.downloadResumable.pauseAsync();
      console.log("Paused download operation, saving for future retrieval");
      AsyncStorage.setItem(
        "pausedDownload",
        JSON.stringify(this.downloadResumable.savable())
      );
      this.toast.show({
        type: "info",
        title: "Téléchargement",
        description: "Téléchargement en pause",
      });
    } catch (e) {
      this.toast.hide();
      this.toast.show({
        type: "warning",
        title: "Mise en pause",
        description: "Erreur de mise en pause du téléchargement",
      });
      console.error(e);
    }
  };

  resumeTask = async () => {
    try {
      this.toast.show({
        type: "progress",
        title: "Reprise du téléchargement",
        progress: this.downloadStatus / 100,
      });
      const { uri } = await this.downloadResumable.resumeAsync();
      console.log("Resume - Finished downloading to ", uri);
      await this.shareFile(uri);
    } catch (e) {
      this.toast.show({
        type: "error",
        time: 3000,
        title: "Téléchargement",
        description: "Erreur de reprise du téléchargement...",
        progress: this.downloadStatus / 100,
      });
      console.error(e);
    }
  };

  resumeLast = async () => {
    const downloadSnapshotJson = await AsyncStorage.getItem("pausedDownload");

    if (downloadSnapshotJson) {
      const downloadSnapshot = JSON.parse(downloadSnapshotJson);

      const fileInfo = await FileSystem.getInfoAsync(downloadSnapshot.fileUri);

      if (!fileInfo.exists) {
        this.downloadResumable = new FileSystem.DownloadResumable(
          downloadSnapshot.url,
          downloadSnapshot.fileUri,
          downloadSnapshot.options,
          this.callback,
          downloadSnapshot.resumeData
        );
        this.startTask();
        this.toast.hide();
        this.toast.show({
          type: "progress",
          title: `Téléchargement de fichier`,
          props: {
            progress: this.downloadStatus / 100,
          },
        });
      }
    }
  };

  on = (action, webview) => {
    if (action.name == "EXPORT_DOC") {
      start(action.url);
    }
    if (action.name == "EXPORT_FILE") {
      start(action.url);
    }
  };
}
