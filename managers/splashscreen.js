import * as SplashScreen from "expo-splash-screen";

export default class splashScreenManager {
  constructor() {
    SplashScreen.preventAutoHideAsync().catch(() => {
      /* reloading the app might trigger some race conditions, ignore them */
    });
  }

  hide = () => {
    SplashScreen.hideAsync();
  };

  on = (action) => {
    switch (action.name) {
      case "HIDE_SPLASH":
        console.log("Hidding splashscreen");
        SplashScreen.hideAsync();
        break;

      default:
        break;
    }
  };
}
