import { CameraType, useCameraPermissions } from "expo-camera";

export default class CameraManager {
  permissionAccess;
  requestAccess;

  constructor() {
    const [permission, requestPermission] = useCameraPermissions();
    permissionAccess = permission;
    requestAccess = requestPermission;
  }

  setPermissionToWebView = (webview) => {
    const toRun = `window.nativeCameraCheck = ${permissionAccess.granted};`;
    webview.injectJavaScript(toRun);
  };

  requestPermission = () => {
    if (!permissionAccess.granted) {
      requestAccess();
    } else {
      console.log("you have permission");
    }
  };

  changeCamera = () => {
    const [facing, setFacing] = useState < CameraType > "back";
    setFacing((current) => (current === "back" ? "front" : "back"));
  };

  on = (action, webview) => {
    switch (action.name) {
      case "CHECK_CAMERA":
        console.log("Checking camera");
        this.setPermissionToWebView(webview);
        break;

      case "REQUEST_CAMERA":
        console.log("Requesting camera permission");
        this.requestPermission();
        break;

      case "CHANGE_CAMERA":
        this.changeCamera();
        break;

      default:
        break;
    }
  };
}
