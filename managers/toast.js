import { StyleSheet, View, Text } from "react-native";
import Toast from "react-native-toast-message";
import * as Progress from "react-native-progress";

const infoStyles = StyleSheet.create({
  container: {
    height: 60,
    width: "90%",
    flex: 1,
    flexDirection: "column",
    padding: 20,
    paddingVertical: 10,
    paddingHorizontal: 16,
    backgroundColor: "#ffffff",
    borderWidth: 1,
    borderRadius: 14,
    borderColor: "rgba(0,0,0,0.1)",
  },
  title: { color: "#000", fontWeight: "bold" },
  description: { color: "#00000070" },
});

const successStyles = StyleSheet.create({
  container: {
    height: 60,
    width: "90%",
    flex: 1,
    flexDirection: "column",
    padding: 20,
    paddingVertical: 10,
    paddingHorizontal: 16,
    backgroundColor: "#0EBC48",
    borderWidth: 1,
    borderRadius: 14,
    borderColor: "rgba(0,0,0,0.08)",
  },
  title: { color: "#ffffff", fontWeight: "bold" },
  description: { color: "#FFFFFF70" },
});

const waningStyles = StyleSheet.create({
  container: {
    height: 60,
    width: "90%",
    flex: 1,
    flexDirection: "column",
    padding: 20,
    paddingVertical: 10,
    paddingHorizontal: 16,
    backgroundColor: "#DF9E11",
    borderWidth: 1,
    borderRadius: 14,
    borderColor: "rgba(0,0,0,0.1)",
  },
  title: { color: "#ffffff", fontWeight: "bold" },
  description: { color: "#FFFFFF70" },
});

const errorStyles = StyleSheet.create({
  container: {
    height: 60,
    width: "90%",
    flex: 1,
    flexDirection: "column",
    padding: 20,
    paddingVertical: 10,
    paddingHorizontal: 16,
    backgroundColor: "#E14D1B",
    borderWidth: 1,
    borderRadius: 14,
    borderColor: "rgba(0,0,0,0.1)",
  },
  title: { color: "#fff", fontWeight: "bold" },
  description: { color: "#FFFFFF70" },
});

const progressStyles = StyleSheet.create({
  container: {
    height: 60,
    width: "90%",
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
    paddingVertical: 10,
    paddingHorizontal: 16,
    backgroundColor: "#ffffff",
    borderWidth: 1,
    borderRadius: 14,
    borderColor: "rgba(0,0,0,0.1)",
  },
  title: { color: "#000000" },
  description: { color: "#000000" },
});

export default class ToastManager {
  constructor() {}

  config = {
    info: ({ text1, text2 }) => (
      <View style={infoStyles.container}>
        <Text style={infoStyles.title}>{text1}</Text>
        <Text style={infoStyles.description}>{text2}</Text>
      </View>
    ),

    success: ({ text1, text2 }) => (
      <View style={successStyles.container}>
        <Text style={successStyles.title}>{text1}</Text>
        <Text style={successStyles.description}>{text2}</Text>
      </View>
    ),

    warning: ({ text1, text2 }) => (
      <View style={waningStyles.container}>
        <Text style={waningStyles.title}>{text1}</Text>
        <Text style={waningStyles.description}>{text2}</Text>
      </View>
    ),

    error: ({ text1, text2 }) => (
      <View style={errorStyles.container}>
        <Text style={errorStyles.title}>{text1}</Text>
        <Text style={errorStyles.description}>{text2}</Text>
      </View>
    ),

    progress: ({ text1, props }) => (
      <View style={progressStyles.container}>
        <Text style={progressStyles.title}>{text1}</Text>
        <Progress.Bar
          animated={true}
          progress={props.progress}
          width={null}
          color="rgba(36, 157, 88, 1)"
          borderRadius={3}
          borderColor="rgba(0,0,0,0.05)"
        />
      </View>
    ),
  };

  show = (data) => {
    let { type, autoHide = true, title, description, progress } = data;

    switch (type) {
      case "info":
        Toast.show({
          type: "info",
          text1: title,
          text2: description,
          autoHide: autoHide,
        });
        break;

      case "success":
        Toast.show({
          type: "success",
          text1: title,
          text2: description,
          autoHide: autoHide,
        });
        break;

      case "warning":
        Toast.show({
          type: "warning",
          text1: title,
          text2: description,
          autoHide: autoHide,
        });
        break;

      case "error":
        Toast.show({
          type: "error",
          text1: title,
          text2: description,
          autoHide: autoHide,
        });
        break;

      case "progress":
        Toast.show({
          type: "progress",
          text1: title,
          autoHide: false,
          props: {
            progress: 0.3,
          },
        });
        break;

      default:
        Toast.show({
          type: "info",
          text1: title,
          text2: description,
        });
        break;
    }
  };

  hide = () => {
    Toast.hide();
  };
}

export const ToastOverlay = Toast;
