import { isJSONString, eventToJSON } from "../utils";

export default class EventsManager {
  constructor() {}

  subscribe = (event, managers, webview) => {
    if (isJSONString(event)) {
      const nkapEvent = eventToJSON(event);
      if ("api" in nkapEvent) {
        switch (nkapEvent.api) {
          case "splashcreen":
            managers.splashScreen.on(nkapEvent.action);
            break;

          case "camera":
            managers.camera.on(nkapEvent.action, webview);
            break;

          case "download":
            managers.download.on(nkapEvent.action, webview);
            break;

          case "logging":
            switch (nkapEvent.action.name) {
              case "LOG":
                console.log(nkapEvent.action.data);
                break;

              case "ERROR":
                console.error(nkapEvent.action.data);
                break;

              default:
                break;
            }
            break;

          default:
            console.log("Event API key not exist");
            break;
        }
      } else {
        console.log("Event API key not exist");
      }
    } else {
      console.log("we don't received event");
    }
  };
}
