import React, { Component, useEffect } from "react";
import { WebView } from "react-native-webview";
import StaticServer from "@dr.pogodin/react-native-static-server";
import Constants from "expo-constants";

export default class FivenkapWebview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      webView: null,
      webViewProps: {
        startInLoadingState: true,
        javaScriptEnabled: true,
        originWhitelist: ["*"],
        domStorageEnabled: true,
        mixedContentMode: "always",
        geolocationEnabled: true,
        allowFileAccess: true,
        allowFileAccessFromFileURL: true,
        allowUniversalAccessFromFileURL: true,
        allowsInlineMediaPlayback: true,
        allowsFullscreenVideo: true,
        mediaPlaybackRequiresUserAction: false,
        mediaCapturePermissionGrantType: "grant",
        allowsInlineMediaPlayback: true,
        scalesPageToFit: true,
        webviewDebuggingEnabled: Constants.expoConfig.extra.IS_DEV,
        cacheEnabled: true,
      },
      appLink: new StaticServer(5555, Constants.expoConfig.extra.NKAP, {
        localOnly: true,
        keepAlive: true,
      }),
      appRoute: "",
      firstScript: `
        true; // note: this is required, or you'll sometimes get silent failures
      `,
    };
  }

  handleWebViewNavigationStateChange = (newNavState) => {
    // newNavState looks something like this:
    // {
    //   url?: string;
    //   title?: string;
    //   loading?: boolean;
    //   canGoBack?: boolean;
    //   canGoForward?: boolean;
    // }
    const { url } = newNavState;
    console.log("url: ", url);

    // handle certain doctypes
    if (url.includes(".pdf")) {
      this.props.managers.download.createTask(downloadUrl);
      setTimeout(() => {
        this.state.webView.stopLoading();
        this.state.webView.goBack();
      }, 3000);
    }
    if (url.includes(".xls")) {
      this.props.managers.download.createTask(downloadUrl);
      setTimeout(() => {
        this.state.webView.stopLoading();
        this.state.webView.goBack();
      }, 3000);
    }
  };

  render() {
    const { managers } = this.props;
    const { webViewProps, firstScript, appLink } = this.state;

    useEffect(() => {
      appLink.start().then((route) => {
        console.log("App started");
        this.state.appRoute = route;
      });

      () => {
        // Stop the server
        appLink.stop();
        console.log("App stoped");
      };
    }, []);

    return (
      <WebView
        ref={(r) => (this.state.webView = r)}
        source={{
          uri: this.state.appRoute,
        }}
        onMessage={(event) => {
          managers.events.subscribe(
            event.nativeEvent.data,
            managers,
            this.state.webView
          );
        }}
        onFileDownload={({ nativeEvent: { downloadUrl } }) => {
          managers.download.createTask(downloadUrl);
        }}
        applicationNameForUserAgent={`${Constants.expoConfig.extra.name}/${Constants.expoConfig.extra.version}`}
        injectedJavaScriptBeforeContentLoaded={firstScript}
        onNavigationStateChange={this.handleWebViewNavigationStateChange}
        {...webViewProps}
      />
    );
  }
}
