import { StatusBar } from "expo-status-bar";
import React, { useEffect } from "react";
import { StyleSheet, View } from "react-native";
import FivenkapWebview from "./components/FivenkapWebview";

import SplashScreenManager from "./managers/splashscreen";
import CameraManager from "./managers/camera";
import DownloadManager from "./managers/download";
import EventsManager from "./managers/events";
import ToastManager, { ToastOverlay } from "./managers/toast";

export default function App(props) {
  // initializing managers
  const splashScreen = new SplashScreenManager();
  const camera = new CameraManager();
  const toast = new ToastManager();
  const download = new DownloadManager(toast);
  const events = new EventsManager();

  const managers = { splashScreen, camera, download, events, toast };

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#F2F2F2" />
      <FivenkapWebview managers={managers} />
      <ToastOverlay config={toast.config} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
  },
});
