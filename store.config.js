// Use the data from `eas metadata:pull`
const config = require("./store.config.json");

module.exports = async () => {
  const year = new Date().getFullYear();
  const info = await fetchLocalizations("...").then((response) =>
    response.json()
  );

  config.apple.copyright = `${year} GetReady, Inc.`;
  config.apple.info = info;

  return config;
};
