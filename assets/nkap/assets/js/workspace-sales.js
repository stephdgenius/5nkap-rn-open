(self["webpackChunk_5nkap"] = self["webpackChunk_5nkap"] || []).push([["workspace-sales"],{

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/sales/quotesAll.vue?vue&type=script&setup=true&lang=ts":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/sales/quotesAll.vue?vue&type=script&setup=true&lang=ts ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _composable_useExport__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/composable/useExport */ "./src/composable/useExport.ts");
/* harmony import */ var _composable_usePDFExport__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/composable/usePDFExport */ "./src/composable/usePDFExport.ts");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _composable_useNotyf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/composable/useNotyf */ "./src/composable/useNotyf.ts");
/* harmony import */ var _router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/router */ "./src/router/index.ts");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/api */ "./src/api/index.ts");
/* harmony import */ var _components_pages_quotes_and_invoices_validate_quote_formValidation_vue__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @/components/pages/quotes-and-invoices/validate-quote/formValidation.vue */ "./src/components/pages/quotes-and-invoices/validate-quote/formValidation.vue");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");












/**
 * Injects
 */
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'quotesAll',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_10__.u)({
      title: "Devis et Factures / Tous les Documents"
    });
    const updateAppHeaderAction = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("updateAppHeaderAction");
    /**
     * Variables
     */
    // Pagination
    const metaDataQuotes = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)();
    const windowWidth = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(window.innerWidth);
    const viewLeftContent = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    const pageHeight = (0,vue__WEBPACK_IMPORTED_MODULE_0__.computed)(() => {
      return window.innerWidth > 768 ? "calc(100vh - 18rem)" : "100vh";
    });
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("list");
    const activeView = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    const quoteSelected = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)();
    const isLoading = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    const notify = (0,_composable_useNotyf__WEBPACK_IMPORTED_MODULE_4__["default"])();
    const dataToSend = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)({
      label: "",
      end: "",
      paymentmethod: {},
      cash_pay: "",
      currency: "XAF",
      medias: [],
      taxes: []
    });
    /**
     * data
     */
    const quotes = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([]);
    /**
     * Watchers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_3__.userCurrentBranch.value.id, async newValue => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_9__["default"])("read", permissions)) {
        await getAllQuotes();
        if (quotes.value?.length > 0) quoteSelected.value = quotes.value[0];
      }
      updateSubView(1, "list");
    });
    /**
     * Functions
     */
    const selectQuote = id => {
      quoteSelected.value = quotes.value.find(item => id == item.id);
      viewLeftContent.value = !viewLeftContent.value;
    };
    const updateSubView = (view, action, state) => {
      activeView.value = view;
      actionType.value = action;
      if (state == "update-list") getAllQuotes();
    };
    const getAllQuotes = async (page, enableLoader = true) => {
      if (enableLoader) isLoading.value = true;
      if (page != null) {
        await (0,_api__WEBPACK_IMPORTED_MODULE_6__["default"])().quotes.getAll(page).then(response => {
          isLoading.value = false;
          metaDataQuotes.value = response.data;
          quotes.value = response.data.data;
        }).catch(error => {
          isLoading.value = false;
          console.log("Error when Guetting All Quotes", error);
        });
      } else {
        await (0,_api__WEBPACK_IMPORTED_MODULE_6__["default"])().quotes.getAll().then(response => {
          isLoading.value = false;
          metaDataQuotes.value = response.data;
          quotes.value = response.data.data;
        }).catch(error => {
          isLoading.value = false;
          console.log("Error when Guetting All Quotes", error);
        });
      }
    };
    const deleteQuote = async id => {
      isLoading.value = true;
      await (0,_api__WEBPACK_IMPORTED_MODULE_6__["default"])().quotes.delete(id).then(response => {
        isLoading.value = false;
        getAllQuotes();
        notify.success("Devis supprimé avec succès");
      }).catch(err => {
        isLoading.value = false;
        console.log("error when delete quote", err);
      });
    };
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_8__["default"])("devis").map(element => {
      return element.action;
    });
    /**
     * page life
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onMounted)(async () => {
      //
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onBeforeMount)(async () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_9__["default"])("read", permissions)) {
        await getAllQuotes();
        if (quotes.value?.length > 0) quoteSelected.value = quotes.value[0];
      }
    });
    /**
     * Providers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissions", permissions);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("updateSubView", updateSubView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("quotes", quotes);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("getAllQuotes", getAllQuotes);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("selectQuote", selectQuote);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("quoteSelected", quoteSelected);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("dataToSend", dataToSend);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("metaDataQuotes", metaDataQuotes);
    const __returned__ = {
      updateAppHeaderAction,
      metaDataQuotes,
      windowWidth,
      viewLeftContent,
      pageHeight,
      actionType,
      activeView,
      quoteSelected,
      isLoading,
      notify,
      dataToSend,
      quotes,
      selectQuote,
      updateSubView,
      getAllQuotes,
      deleteQuote,
      permissions,
      get filename() {
        return _composable_useExport__WEBPACK_IMPORTED_MODULE_1__.filename;
      },
      get exportQuoteToPDF() {
        return _composable_usePDFExport__WEBPACK_IMPORTED_MODULE_2__["default"];
      },
      get isOwnBranch() {
        return _state_api_userState__WEBPACK_IMPORTED_MODULE_3__.isOwnBranch;
      },
      get router() {
        return _router__WEBPACK_IMPORTED_MODULE_5__["default"];
      },
      FormValidation: _components_pages_quotes_and_invoices_validate_quote_formValidation_vue__WEBPACK_IMPORTED_MODULE_7__["default"],
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_9__["default"];
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/sales/quotesGenerateQuote.vue?vue&type=script&setup=true&lang=ts":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/sales/quotesGenerateQuote.vue?vue&type=script&setup=true&lang=ts ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _composable_useNotyf__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/composable/useNotyf */ "./src/composable/useNotyf.ts");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/utils */ "./src/utils/index.ts");
/* harmony import */ var _composable_useExport__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/composable/useExport */ "./src/composable/useExport.ts");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/state/pages/quotes/quote */ "./src/state/pages/quotes/quote.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");









/**
 * composable
 */


/**
 * Variables
 */
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'quotesGenerateQuote',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_8__.u)({
      title: "Devis et facture / Creation de dévis"
    });
    const notify = (0,_composable_useNotyf__WEBPACK_IMPORTED_MODULE_1__["default"])();
    let account = {
      name: "",
      address: "",
      tel: ""
    };
    const activeFilter = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("list");
    const viewRightContent = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    const windowWidth = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(window.innerWidth);
    /**
     * Functions
     */
    const updateSubView = action => {
      actionType.value = action;
    };
    /**
     * watchers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(_state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.categorieSelected, async newValue => {
      if (newValue) {
        _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.isLoading.value = true;
        await (0,_state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.getQuoteContent)();
        _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.isLoading.value = false;
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.quoteContent.value, newValue => {
      if (newValue) {
        _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.isLoading.value = true;
        (0,_state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.getQuoteContent)();
        _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.isLoading.value = false;
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.quoteContent.value, newValue => {
      if (newValue) {
        _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.quoteContent.selected = _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.quoteContent.data.find(item => item.key == newValue.key);
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.clients.value, newValue => {
      if (newValue) {
        _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.clients.selected = _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.clients.data.find(item => item.key == newValue.key);
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(_state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.taxe, async newValue => {
      if (_state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.taxe.suffix != null && _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.taxe.value != null && _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.taxe.type != null && _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.quoteContent.list?.length > 0) {
        await (0,_state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.getGlobalAmount)(_state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.quoteContent.list, [{
          ..._state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.taxe,
          libele: "taxe"
        }]);
      }
    }, {
      deep: true
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userCurrentBranch.value.id, newValue => {
      initOperation();
    });
    const initOperation = async () => {
      _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.isLoading.value = true;
      await (0,_state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.getClients)();
      await (0,_state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.getQuoteContent)();
      switch (_state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.user.type) {
        case "enterprise":
          account.name = _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.enterprise.nom_entreprise;
          account.address = _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.enterprise.ville;
          account.tel = _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.enterprise.phone;
          break;
        case "particular":
          account.name = _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.particular.nom;
          account.address = _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.particular.ville;
          account.tel = _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.particular.phone;
          break;
      }
      _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.isLoading.value = false;
    };
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_6__["default"])("depense_achats").map(element => {
      return element.action;
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("updateSubView", updateSubView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeView", _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.activeView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeFilter", activeFilter);
    /**
     *
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onBeforeMount)(async () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_7__["default"])('add', permissions)) {
        initOperation();
      }
      _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.label.value = "Devis ... " + Math.floor(Math.random() * 10000) + 1;
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onUnmounted)(() => {
      (0,_state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.resetError)();
      (0,_state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.resetThirdInfosError)();
      (0,_state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.reset)();
    });
    const __returned__ = {
      notify,
      get account() {
        return account;
      },
      set account(v) {
        account = v;
      },
      activeFilter,
      actionType,
      viewRightContent,
      windowWidth,
      updateSubView,
      initOperation,
      permissions,
      get formatDate() {
        return _utils__WEBPACK_IMPORTED_MODULE_2__.formatDate;
      },
      get filename() {
        return _composable_useExport__WEBPACK_IMPORTED_MODULE_3__.filename;
      },
      get error() {
        return _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.error;
      },
      get thirdInfosError() {
        return _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.thirdInfosError;
      },
      get thirdInfos() {
        return _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.thirdInfos;
      },
      get isLoading() {
        return _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.isLoading;
      },
      get validation() {
        return _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.validation;
      },
      get taxe() {
        return _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.taxe;
      },
      get label() {
        return _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.label;
      },
      get clients() {
        return _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.clients;
      },
      get categorieSelected() {
        return _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.categorieSelected;
      },
      get quoteContent() {
        return _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.quoteContent;
      },
      get quotePreviewContent() {
        return _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.quotePreviewContent;
      },
      get quoteTotalAmount() {
        return _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.quoteTotalAmount;
      },
      get endDate() {
        return _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.endDate;
      },
      get activeView() {
        return _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.activeView;
      },
      get back() {
        return _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.back;
      },
      get doAction() {
        return _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.doAction;
      },
      get tva() {
        return _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.tva;
      },
      get ir() {
        return _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.ir;
      },
      get toggle() {
        return _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.toggle;
      },
      get deleteQuoteContentElement() {
        return _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.deleteQuoteContentElement;
      },
      get add() {
        return _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.add;
      },
      get setDeadLine() {
        return _state_pages_quotes_quote__WEBPACK_IMPORTED_MODULE_5__.setDeadLine;
      },
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_7__["default"];
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/sales/quotesOverview.vue?vue&type=script&setup=true&lang=ts":
/*!************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/sales/quotesOverview.vue?vue&type=script&setup=true&lang=ts ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/api */ "./src/api/index.ts");
/* harmony import */ var _composable_useNotyf__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/composable/useNotyf */ "./src/composable/useNotyf.ts");
/* harmony import */ var _composable_useFormatMoney__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/composable/useFormatMoney */ "./src/composable/useFormatMoney.ts");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");









/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'quotesOverview',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_7__.u)({
      title: "Comptabilité / Vue d'ensemble"
    });
    /**
     * Composables
     */
    const notify = (0,_composable_useNotyf__WEBPACK_IMPORTED_MODULE_2__["default"])();
    /**
     * data
     */
    const isLoading = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    const overviewInfos = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)();
    /**
     * Injects
     */
    const goBack = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("goBack");
    const goNext = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("goNext");
    const reloadPage = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("reloadPage");
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userCurrentBranch.value.id, newValue => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_5__["default"])('read', permissions)) {
        getOverviewInfos();
      }
    });
    const getOverviewInfos = async () => {
      isLoading.value = true;
      await (0,_api__WEBPACK_IMPORTED_MODULE_1__["default"])().quotes.getOverviewInfos().then(response => {
        overviewInfos.value = response.data;
        isLoading.value = false;
      }).catch(err => {
        isLoading.value = false;
        notify.error(err.response.message);
      });
    };
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_6__["default"])("etatDevis").map(element => {
      return element.action;
    });
    /**
     * page life cycle hook
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onBeforeMount)(async () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_5__["default"])('read', permissions)) {
        getOverviewInfos();
      }
    });
    const __returned__ = {
      notify,
      isLoading,
      overviewInfos,
      goBack,
      goNext,
      reloadPage,
      getOverviewInfos,
      permissions,
      get orignalMoney() {
        return _composable_useFormatMoney__WEBPACK_IMPORTED_MODULE_3__["default"];
      },
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_5__["default"];
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/sales/quotesAll.vue?vue&type=template&id=138c24ce&scoped=true&ts=true":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/sales/quotesAll.vue?vue&type=template&id=138c24ce&scoped=true&ts=true ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var core_js_modules_es_array_push_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.push.js */ "./node_modules/core-js/modules/es.array.push.js");
/* harmony import */ var core_js_modules_es_array_push_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_push_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");


const _withScopeId = n => ((0,vue__WEBPACK_IMPORTED_MODULE_1__.pushScopeId)("data-v-138c24ce"), n = n(), (0,vue__WEBPACK_IMPORTED_MODULE_1__.popScopeId)(), n);
const _hoisted_1 = {
  class: "h-full pb-5 overflow-y-auto workspace-content md:pb-0"
};
const _hoisted_2 = {
  key: 0,
  class: "relative flex flex-col w-full h-full overflow-y-auto overflow-x-hidden transition-all duration-500 ease-in-out"
};
const _hoisted_3 = {
  key: 0,
  class: "flex flex-col md:flex-row w-full space-x-0 md:space-x-8",
  style: {
    "height": "calc(100vh - 18rem)"
  }
};
const _hoisted_4 = {
  class: "flex flex-row items-end justify-between md:justify-end w-full"
};
const _hoisted_5 = {
  class: "flex flex-row justify-start space-x-3 font-medium"
};
const _hoisted_6 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_1__.createElementVNode)("span", {
  class: ""
}, null, -1 /* HOISTED */));
const _hoisted_7 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_1__.createElementVNode)("span", {
  class: ""
}, null, -1 /* HOISTED */));
const _hoisted_8 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_1__.createElementVNode)("span", {
  class: ""
}, null, -1 /* HOISTED */));
const _hoisted_9 = {
  class: "flex flex-row justify-end space-x-4 font-medium"
};
const _hoisted_10 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_1__.createElementVNode)("span", {
  class: "hidden md:block"
}, "Valider le devis", -1 /* HOISTED */));
const _hoisted_11 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_1__.createElementVNode)("span", {
  class: "hidden md:block"
}, "Envoyer par mail", -1 /* HOISTED */));
const _hoisted_12 = {
  key: 1,
  class: "flex flex-row items-center justify-center h-full"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_WorkspaceHeader = (0,vue__WEBPACK_IMPORTED_MODULE_1__.resolveComponent)("WorkspaceHeader");
  const _component_QuotesList = (0,vue__WEBPACK_IMPORTED_MODULE_1__.resolveComponent)("QuotesList");
  const _component_QuoteDetails = (0,vue__WEBPACK_IMPORTED_MODULE_1__.resolveComponent)("QuoteDetails");
  const _component_iconButton = (0,vue__WEBPACK_IMPORTED_MODULE_1__.resolveComponent)("iconButton");
  const _component_EmptyDocument = (0,vue__WEBPACK_IMPORTED_MODULE_1__.resolveComponent)("EmptyDocument");
  const _component_SendQuoteThroughMail = (0,vue__WEBPACK_IMPORTED_MODULE_1__.resolveComponent)("SendQuoteThroughMail");
  const _component_Spinner = (0,vue__WEBPACK_IMPORTED_MODULE_1__.resolveComponent)("Spinner");
  return (0,vue__WEBPACK_IMPORTED_MODULE_1__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_1__.createVNode)(_component_WorkspaceHeader, {
    "active-type-view": 'list',
    "items-list": [],
    "file-name": $setup.filename('All Quotes'),
    "new-item": () => {
      $setup.router.push({
        name: 'workspace-quotes-generate-quote'
      });
    },
    "data-to-p-d-f": {
      data: [],
      which: 'Quotes'
    },
    "get-filtered": [],
    options: {
      titleBar: true,
      actionsBar: true,
      fabButtons: false,
      actions: {
        import: false,
        export: false,
        addItem: $setup.useAccess('add', $setup.permissions) && $setup.isOwnBranch,
        filter: false
      },
      fabButtonsActions: {
        addItem: $setup.useAccess('add', $setup.permissions) && $setup.isOwnBranch,
        moreMenu: false
      }
    }
  }, null, 8 /* PROPS */, ["file-name", "new-item", "options"]), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createElementVNode)("div", {
    class: "flex items-center justify-center w-full p-8 border border-gray-400 rounded-md border-opacity-40 bg-gray-50",
    style: (0,vue__WEBPACK_IMPORTED_MODULE_1__.normalizeStyle)({
      height: $setup.windowWidth > 768 ? 'calc(100vh - 5rem)' : '77vh'
    })
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_1__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_1__.Transition, {
    name: $setup.activeView == 2 ? 'translate-subview-right' : $setup.activeView == 1 || $setup.activeView == 3 ? 'translate-subview-left' : 'fade-slow'
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_1__.withCtx)(() => [$setup.activeView == 1 && $setup.isLoading != true ? ((0,vue__WEBPACK_IMPORTED_MODULE_1__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createElementBlock)("div", _hoisted_2, [$setup.quotes?.length > 0 && $setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_1__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createElementBlock)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_1__.createVNode)(_component_QuotesList), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_1__.Transition, {
      name: "zoom-fade"
    }, {
      default: (0,vue__WEBPACK_IMPORTED_MODULE_1__.withCtx)(() => [($setup.actionType == 'list' && $setup.windowWidth > 768 ? true : $setup.viewLeftContent) ? ((0,vue__WEBPACK_IMPORTED_MODULE_1__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createElementBlock)("div", {
        key: 0,
        class: (0,vue__WEBPACK_IMPORTED_MODULE_1__.normalizeClass)({
          'absolute lg:relative  flex flex-col flex-auto h-full space-y-8 z-20 lg:border-none border-gray-300 bg-gray-50 transform transition-all lg:translate-y-0 duration-300 md:duration-500 ease-linear': true,
          'opacity-100 w-full translate-y-0': $setup.viewLeftContent,
          'opacity-0 lg:opacity-100 w-auto translate-y-56': !$setup.viewLeftContent
        })
      }, [(0,vue__WEBPACK_IMPORTED_MODULE_1__.createVNode)(_component_QuoteDetails, {
        onCloseView: _cache[0] || (_cache[0] = status => $setup.viewLeftContent = status)
      }), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createCommentVNode)(" btn container "), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createElementVNode)("div", _hoisted_4, [(0,vue__WEBPACK_IMPORTED_MODULE_1__.createElementVNode)("div", _hoisted_5, [(0,vue__WEBPACK_IMPORTED_MODULE_1__.createCommentVNode)(" Supprimer "), $setup.useAccess('delete', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_1__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createElementBlock)("div", {
        key: 0,
        class: "flex flex-row items-center space-x-2 text-red-500 cursor-pointer hover:text-red-600",
        onClick: _cache[1] || (_cache[1] = $event => $setup.deleteQuote($setup.quoteSelected.id))
      }, [(0,vue__WEBPACK_IMPORTED_MODULE_1__.createVNode)(_component_iconButton, {
        icon: "/icons/remove-icon.svg"
      }), _hoisted_6])) : (0,vue__WEBPACK_IMPORTED_MODULE_1__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createCommentVNode)(" Imprimer "), $setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_1__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createElementBlock)("div", {
        key: 1,
        class: "flex flex-row items-center space-x-2 text-gray-500 cursor-pointer hover:text-gray-600",
        onClick: _cache[2] || (_cache[2] = $event => $setup.exportQuoteToPDF({
          data: $setup.quoteSelected,
          which: 'Quotes',
          isAutoPrint: true
        }, 'Devis'))
      }, [(0,vue__WEBPACK_IMPORTED_MODULE_1__.createVNode)(_component_iconButton, {
        icon: "/icons/printer-icon.svg",
        class: "opacity-80"
      }), _hoisted_7])) : (0,vue__WEBPACK_IMPORTED_MODULE_1__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createCommentVNode)(" Télécharger "), $setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_1__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createElementBlock)("div", {
        key: 2,
        class: "flex flex-row items-center space-x-2 text-gray-500 cursor-pointer hover:text-gray-600",
        onClick: _cache[3] || (_cache[3] = $event => $setup.exportQuoteToPDF({
          data: $setup.quoteSelected,
          which: 'Quotes'
        }, 'Devis'))
      }, [(0,vue__WEBPACK_IMPORTED_MODULE_1__.createVNode)(_component_iconButton, {
        icon: "/icons/download-2-icon.svg",
        class: "opacity-80"
      }), _hoisted_8])) : (0,vue__WEBPACK_IMPORTED_MODULE_1__.createCommentVNode)("v-if", true)]), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createElementVNode)("div", _hoisted_9, [(0,vue__WEBPACK_IMPORTED_MODULE_1__.createCommentVNode)(" Valider le devis "), $setup.quoteSelected?.state != 1 && $setup.useAccess('add', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_1__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createElementBlock)("div", {
        key: 0,
        class: "flex flex-row items-center space-x-2 text-blue-600 cursor-pointer hover:opacity-70",
        onClick: _cache[4] || (_cache[4] = $event => $setup.updateSubView(3, 'validateQuote'))
      }, [(0,vue__WEBPACK_IMPORTED_MODULE_1__.createVNode)(_component_iconButton, {
        icon: "/icons/success-reset-icon.svg",
        class: "opacity-90"
      }), _hoisted_10])) : (0,vue__WEBPACK_IMPORTED_MODULE_1__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createCommentVNode)(" Envoyer par mail "), $setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_1__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createElementBlock)("div", {
        key: 1,
        class: "flex flex-row items-center space-x-2 text-green-500 cursor-pointer hover:opacity-70",
        onClick: _cache[5] || (_cache[5] = $event => $setup.updateSubView(2, 'sendMail'))
      }, [(0,vue__WEBPACK_IMPORTED_MODULE_1__.createVNode)(_component_iconButton, {
        icon: "/icons/email-third-party-icon-c.svg",
        class: "opacity-80"
      }), _hoisted_11])) : (0,vue__WEBPACK_IMPORTED_MODULE_1__.createCommentVNode)("v-if", true)])])], 2 /* CLASS */)) : (0,vue__WEBPACK_IMPORTED_MODULE_1__.createCommentVNode)("v-if", true)]),
      _: 1 /* STABLE */
    })])) : ((0,vue__WEBPACK_IMPORTED_MODULE_1__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createElementBlock)("div", _hoisted_12, [(0,vue__WEBPACK_IMPORTED_MODULE_1__.createVNode)(_component_EmptyDocument, {
      callback: () => $setup.useAccess('read', $setup.permissions) ? $setup.router.push({
        name: 'workspace-quotes-generate-quote'
      }) : null,
      access: $setup.useAccess('read', $setup.permissions),
      text1: $setup.useAccess('read', $setup.permissions) ? 'Oups... Vous n\'avez aucun devis enregistré' : 'Oups... Vous n\'avez pas accès à cette page',
      text2: $setup.useAccess('read', $setup.permissions) ? 'Créer un nouveau devis ?' : ''
    }, null, 8 /* PROPS */, ["callback", "access", "text1", "text2"])]))])) : (0,vue__WEBPACK_IMPORTED_MODULE_1__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }, 8 /* PROPS */, ["name"]), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_1__.Transition, {
    name: $setup.activeView == 2 && $setup.isLoading == false ? 'translate-subview-right' : 'translate-subview-left'
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_1__.withCtx)(() => [$setup.activeView == 2 && $setup.isLoading != true ? ((0,vue__WEBPACK_IMPORTED_MODULE_1__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createBlock)(_component_SendQuoteThroughMail, {
      key: 0
    })) : (0,vue__WEBPACK_IMPORTED_MODULE_1__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }, 8 /* PROPS */, ["name"]), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_1__.Transition, {
    name: $setup.activeView == 3 ? 'translate-subview-left' : 'translate-subview-right'
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_1__.withCtx)(() => [$setup.activeView == 3 ? ((0,vue__WEBPACK_IMPORTED_MODULE_1__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createBlock)($setup["FormValidation"], {
      key: 0
    })) : (0,vue__WEBPACK_IMPORTED_MODULE_1__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }, 8 /* PROPS */, ["name"]), $setup.isLoading == true ? ((0,vue__WEBPACK_IMPORTED_MODULE_1__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_1__.createBlock)(_component_Spinner, {
    key: 0
  })) : (0,vue__WEBPACK_IMPORTED_MODULE_1__.createCommentVNode)("v-if", true)], 4 /* STYLE */)]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/sales/quotesGenerateQuote.vue?vue&type=template&id=5c215a02&ts=true":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/sales/quotesGenerateQuote.vue?vue&type=template&id=5c215a02&ts=true ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "h-full pb-5 overflow-y-auto workspace-content md:pb-0"
};
const _hoisted_2 = {
  class: "flex flex-row w-full px-4 pt-6 pb-4 border rounded-md bg-opacity-40 bg-gray-50",
  style: {
    "height": "calc(100vh - 13rem)"
  }
};
const _hoisted_3 = {
  key: 0,
  class: "flex flex-row items-center justify-center w-full space-x-4 h-5/6"
};
const _hoisted_4 = {
  key: 1,
  class: "relative flex flex-row justify-between w-full overflow-y-auto"
};
const _hoisted_5 = {
  class: "flex flex-col w-full pr-0 space-y-6 md:w-3/6 lg:w-2/5 md:pr-5"
};
const _hoisted_6 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/eye-icon.svg",
  alt: ""
}, null, -1 /* HOISTED */);
const _hoisted_7 = [_hoisted_6];
const _hoisted_8 = {
  class: "px-3 pt-2 overflow-y-auto",
  style: {
    "height": "calc(100vh - 20rem)"
  }
};
const _hoisted_9 = {
  key: 0,
  class: "w-full",
  style: {
    "flex": "0 0 auto"
  }
};
const _hoisted_10 = {
  class: "space-y-4"
};
const _hoisted_11 = {
  class: "w-full"
};
const _hoisted_12 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", {
  class: "mb-2"
}, "  Institulé du dévis", -1 /* HOISTED */);
const _hoisted_13 = ["state"];
const _hoisted_14 = {
  class: "w-full"
};
const _hoisted_15 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", null, "  Validité du dévis", -1 /* HOISTED */);
const _hoisted_16 = ["state"];
const _hoisted_17 = ["min"];
const _hoisted_18 = {
  class: "w-full mb-6"
};
const _hoisted_19 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", {
  class: "mb-2"
}, "  Nom du Client", -1 /* HOISTED */);
const _hoisted_20 = ["state", "options-data"];
const _hoisted_21 = {
  class: "flex flex-row"
};
const _hoisted_22 = {
  class: "w-full mb-6"
};
const _hoisted_23 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", {
  class: "mt-2 mb-1"
}, "  TVA à appliquer", -1 /* HOISTED */);
const _hoisted_24 = ["state"];
const _hoisted_25 = {
  class: "w-full mb-6"
};
const _hoisted_26 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", {
  class: "mt-2 mb-1"
}, "  IR à appliquer", -1 /* HOISTED */);
const _hoisted_27 = ["state"];
const _hoisted_28 = {
  class: "flex items-center space-x-2"
};
const _hoisted_29 = ["checked"];
const _hoisted_30 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
  for: "box"
}, "  Pas de clients ? Créer un nouveau client", -1 /* HOISTED */);
const _hoisted_31 = {
  key: 0,
  class: "flex flex-col space-y-4"
};
const _hoisted_32 = {
  class: "w-full"
};
const _hoisted_33 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", {
  class: "mb-2"
}, "  Nom du client", -1 /* HOISTED */);
const _hoisted_34 = ["state"];
const _hoisted_35 = {
  class: "w-full mb-6"
};
const _hoisted_36 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", {
  class: "mb-2"
}, "  Numéro de Téléphone", -1 /* HOISTED */);
const _hoisted_37 = ["state"];
const _hoisted_38 = {
  class: "w-full mb-6"
};
const _hoisted_39 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", {
  class: "mb-2"
}, "  Adresse Email", -1 /* HOISTED */);
const _hoisted_40 = ["state"];
const _hoisted_41 = {
  class: "w-full mb-6"
};
const _hoisted_42 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", {
  class: "mb-2"
}, "  Localisation", -1 /* HOISTED */);
const _hoisted_43 = {
  flex: "normal",
  placeholder: "Ex: Rue Charle de Gaulle, Cameroun",
  usage: "location"
};
const _hoisted_44 = {
  key: 0,
  class: "flex flex-col w-full"
};
const _hoisted_45 = {
  class: "flex flex-row items-center justify-between w-full mb-6"
};
const _hoisted_46 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "font-medium text-gray-500 text-md"
}, "Type de contenu", -1 /* HOISTED */);
const _hoisted_47 = {
  class: "flex flex-row items-center text-gray-400 cursor-pointer"
};
const _hoisted_48 = {
  class: "flex items-center justify-end px-2 py-1 bg-green-200 rounded-xl"
};
const _hoisted_49 = {
  class: "flex flex-row items-center justify-between w-full mb-3 space-x-3"
};
const _hoisted_50 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", {
  class: ""
}, "  Choix du contenu", -1 /* HOISTED */);
const _hoisted_51 = ["options-data"];
const _hoisted_52 = {
  key: 0,
  class: "flex flex-col items-center justify-center w-2/6"
};
const _hoisted_53 = {
  class: "font-medium text-green-700"
};
const _hoisted_54 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", null, "en stock", -1 /* HOISTED */);
const _hoisted_55 = {
  class: "flex flex-col justify-between w-full"
};
const _hoisted_56 = {
  key: 0,
  class: "w-full mb-4"
};
const _hoisted_57 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", {
  class: ""
}, "  Quantité", -1 /* HOISTED */);
const _hoisted_58 = {
  flex: "normal",
  usage: "number",
  placeholder: "Quantité "
};
const _hoisted_59 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-button", {
  flex: "normal",
  type: "link",
  url: "javascript:void(0)",
  text: "Ajouter",
  usage: "simple-text",
  "is-white-icon": "false",
  "text-color": "white",
  "textcolor-hover": "white",
  bgcolor: "gray-600",
  "bgcolor-hover": "gray-700",
  "border-color": "blue-200"
}, null, -1 /* HOISTED */);
const _hoisted_60 = [_hoisted_59];
const _hoisted_61 = {
  class: "w-full mt-4 mb-4"
};
const _hoisted_62 = {
  class: "font-medium text-gray-700"
};
const _hoisted_63 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-button", {
  flex: "normal",
  type: "link",
  url: "javascript:void(0)",
  text: "Retourner",
  usage: "back-with-text",
  "is-white-icon": "false",
  "text-color": "gray-900",
  "textcolor-hover": "gray-900",
  bgcolor: "gray-200",
  "bgcolor-hover": "gray-300",
  "border-color": "gray-200"
}, null, -1 /* HOISTED */);
const _hoisted_64 = [_hoisted_63];
const _hoisted_65 = ["text", "loading", "usage"];
const _hoisted_66 = {
  class: "hidden w-full mb-4 lg:flex"
};
const _hoisted_67 = {
  class: "flex w-full mb-4 md:hidden"
};
const _hoisted_68 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/header/windows/close-icon.svg",
  alt: ""
}, null, -1 /* HOISTED */);
const _hoisted_69 = [_hoisted_68];
const _hoisted_70 = {
  class: "space-y-6 overflow-y-scroll",
  style: {
    "height": "calc(100vh - 20rem)"
  }
};
const _hoisted_71 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-col items-center justify-center flex-shrink w-full h-4 p-4 py-5 space-y-4 text-center text-gray-400 bg-gray-100 border-2 border-gray-400 border-dashed cursor-pointer hover:border-green-700 border-opacity-30 hover:text-green-700 rounded-xl"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", null, "Zone d'entête")], -1 /* HOISTED */);
const _hoisted_72 = {
  class: "flex flex-row items-center justify-between"
};
const _hoisted_73 = {
  class: "flex flex-col"
};
const _hoisted_74 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-col"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h1", {
  class: "font-extrabold capitalize"
}, "Dévis")], -1 /* HOISTED */);
const _hoisted_75 = {
  key: 0,
  class: "flex flex-col"
};
const _hoisted_76 = {
  class: "flex flex-row items-center justify-between"
};
const _hoisted_77 = {
  class: "font-extrabold capitalize"
};
const _hoisted_78 = {
  class: "w-full table-auto"
};
const _hoisted_79 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("thead", null, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("tr", {
  class: "text-gray-800 bg-green-100 rounded-sm"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", {
  class: "p-2"
}, "No"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", {
  class: "p-2"
}, "Label"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", {
  class: "p-2"
}, "Qté"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", {
  class: "p-2"
}, "Prix U HT"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", {
  class: "p-2"
}, "Prix Total HT"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", {
  class: "p-2"
}, "TVA"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", {
  class: "p-2"
}, "IR"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", {
  class: "p-2"
}, "Prix Total TTC")])], -1 /* HOISTED */);
const _hoisted_80 = {
  class: "divide-y-2"
};
const _hoisted_81 = {
  class: "p-2"
};
const _hoisted_82 = {
  class: "p-2"
};
const _hoisted_83 = {
  class: "p-2"
};
const _hoisted_84 = {
  class: "p-2"
};
const _hoisted_85 = {
  class: "p-2"
};
const _hoisted_86 = {
  class: "p-2"
};
const _hoisted_87 = {
  class: "p-2"
};
const _hoisted_88 = {
  class: "p-2"
};
const _hoisted_89 = {
  class: "flex flex-col space-y-2"
};
const _hoisted_90 = {
  class: "flex flex-row justify-between space-x-10"
};
const _hoisted_91 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "font-bold"
}, "Total HT", -1 /* HOISTED */);
const _hoisted_92 = {
  class: ""
};
const _hoisted_93 = {
  class: "flex flex-row justify-between"
};
const _hoisted_94 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "font-bold"
}, "Taxe", -1 /* HOISTED */);
const _hoisted_95 = {
  class: "flex justify"
};
const _hoisted_96 = {
  class: "flex flex-row justify-between"
};
const _hoisted_97 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "font-bold"
}, "Total Net TTC", -1 /* HOISTED */);
const _hoisted_98 = {
  class: "flex justify"
};
const _hoisted_99 = {
  class: "flex flex-row justify-between"
};
const _hoisted_100 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "font-bold"
}, "Dévis Valable Jusqu'au", -1 /* HOISTED */);
const _hoisted_101 = {
  class: "flex justify"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_WorkspaceHeader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WorkspaceHeader");
  const _component_Spinner = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("Spinner");
  const _component_SlimTitleBar = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SlimTitleBar");
  const _component_iconButton = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("iconButton");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" eslint-disable vue/no-deprecated-slot-attribute "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_WorkspaceHeader, {
    "active-type-view": 'list',
    "items-list": [],
    "file-name": $setup.filename('Quotes'),
    "new-item": () => {
      //
    },
    "data-to-p-d-f": {
      data: [],
      which: 'Quotes'
    },
    "get-filtered": () => {},
    options: {
      titleBar: true,
      actionsBar: true,
      fabButtons: false,
      actions: {
        import: false,
        export: false,
        addItem: false,
        filter: false
      },
      fabButtonsActions: {
        addItem: false,
        moreMenu: false
      }
    }
  }, null, 8 /* PROPS */, ["file-name"]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Cadre "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" spinner pour le loading général "), $setup.isLoading ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_Spinner)])) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_4, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Section Left LG"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_5, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Sub Title "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SlimTitleBar, {
    title: $setup.activeView == 1 ? `Informations du devis` : `Contenu du dévis `,
    icon: "navigation-level-icon"
  }, {
    right: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
      class: "flex flex-row w-auto m-1 space-x-3 font-normal cursor-pointer md:hidden hover:opacity-70",
      onClick: _cache[0] || (_cache[0] = $event => $setup.viewRightContent = !$setup.viewRightContent)
    }, [..._hoisted_7])]),
    _: 1 /* STABLE */
  }, 8 /* PROPS */, ["title"]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_8, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" first part "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "translate-miniview-left"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.activeView == 1 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_9, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_10, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" label "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_11, [_hoisted_12, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-input", {
      flex: "normal",
      placeholder: "Ex : Devis Matériel de construction",
      "error-msg": 'Veuillez fournir l\'intitulé du devis',
      usage: "simple-text",
      state: $setup.error.label
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
      slot: "native-input",
      "onUpdate:modelValue": _cache[1] || (_cache[1] = $event => $setup.label = $event)
    }, null, 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, $setup.label]])], 8 /* PROPS */, _hoisted_13)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Input Component 2"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_14, [_hoisted_15, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-input", {
      flex: "normal",
      placeholder: "Adresse E-mail",
      usage: "date",
      "error-msg": 'Veuillez fournir la date de validité du devis',
      state: $setup.error.end
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
      slot: "native-input",
      type: "date",
      min: $setup.formatDate(new Date(), 1),
      onChange: _cache[2] || (_cache[2] =
      //@ts-ignore
      (...args) => $setup.setDeadLine && $setup.setDeadLine(...args))
    }, null, 40 /* PROPS, HYDRATE_EVENTS */, _hoisted_17)], 8 /* PROPS */, _hoisted_16)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" liste de clients "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_18, [_hoisted_19, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-select", {
      flex: "normal",
      placeholder: "Veuillez sélectionner le client",
      usage: "profile",
      "error-msg": 'Veuillez sélectionner le client',
      state: $setup.error.third,
      "options-data": JSON.stringify($setup.clients.options)
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
      slot: "native-input",
      onChange: _cache[3] || (_cache[3] = $event => $setup.clients.value = JSON.parse($event.target?.dataset.option))
    }, null, 32 /* HYDRATE_EVENTS */)], 8 /* PROPS */, _hoisted_20)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" TVA et IR "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_21, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" TVA "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_22, [_hoisted_23, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-input", {
      flex: "normal",
      usage: "number",
      placeholder: "Ex : 50%",
      state: $setup.error.tva,
      "error-msg": `Entrez une valeur entre 0 et 100`
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
      slot: "native-input",
      "onUpdate:modelValue": _cache[4] || (_cache[4] = $event => $setup.tva = $event),
      min: "0",
      step: "0.1",
      max: 100,
      onInput: _cache[5] || (_cache[5] = () => $setup.tva = Number($setup.tva))
    }, null, 544 /* HYDRATE_EVENTS, NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, $setup.tva]])], 8 /* PROPS */, _hoisted_24)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" IR "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_25, [_hoisted_26, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-input", {
      flex: "normal",
      usage: "number",
      placeholder: "Ex : 50%",
      state: $setup.error.ir,
      "error-msg": `Entrez une valeur entre 0 et 100`
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
      slot: "native-input",
      "onUpdate:modelValue": _cache[6] || (_cache[6] = $event => $setup.ir = $event),
      min: "0",
      step: "0.1",
      max: 100,
      onInput: _cache[7] || (_cache[7] = () => $setup.ir = Number($setup.ir))
    }, null, 544 /* HYDRATE_EVENTS, NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, $setup.ir]])], 8 /* PROPS */, _hoisted_27)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" chec box for new client "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_28, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
      id: "box",
      "onUpdate:modelValue": _cache[8] || (_cache[8] = $event => $setup.clients.isNew = $event),
      type: "checkbox",
      name: "box",
      checked: $setup.clients.isNew
    }, null, 8 /* PROPS */, _hoisted_29), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelCheckbox, $setup.clients.isNew]]), _hoisted_30]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" infos pour la création d'un nouveau client "), $setup.clients.isNew ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_31, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Input Component 1"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_32, [_hoisted_33, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-input", {
      flex: "normal",
      placeholder: "Ex : SIMO Valery",
      "error-msg": 'Nom invalide',
      usage: "profile",
      state: $setup.thirdInfosError.name
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
      slot: "native-input",
      "onUpdate:modelValue": _cache[9] || (_cache[9] = $event => $setup.thirdInfos.name = $event)
    }, null, 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, $setup.thirdInfos.name]])], 8 /* PROPS */, _hoisted_34)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Input Component 2"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_35, [_hoisted_36, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-input", {
      flex: "normal",
      placeholder: "Ex : +509-123-4567",
      "error-msg": 'Numéro incorrecte',
      usage: "tel",
      state: $setup.thirdInfosError.phone
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
      slot: "native-input",
      "onUpdate:modelValue": _cache[10] || (_cache[10] = $event => $setup.thirdInfos.phone = $event)
    }, null, 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, $setup.thirdInfos.phone]])], 8 /* PROPS */, _hoisted_37)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Input Component 3"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_38, [_hoisted_39, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-input", {
      flex: "normal",
      placeholder: "Ex : abc@example.com",
      usage: "email",
      "error-msg": 'Adresse E-mail incorrecte',
      state: $setup.thirdInfosError.email
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
      slot: "native-input",
      "onUpdate:modelValue": _cache[11] || (_cache[11] = $event => $setup.thirdInfos.email = $event)
    }, null, 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, $setup.thirdInfos.email]])], 8 /* PROPS */, _hoisted_40)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Input Component 4"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_41, [_hoisted_42, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-input", _hoisted_43, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
      slot: "native-input",
      "onUpdate:modelValue": _cache[12] || (_cache[12] = $event => $setup.thirdInfos.address = $event)
    }, null, 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, $setup.thirdInfos.address]])])])])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Second part "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "translate-miniview-right",
    mode: "out-in"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.activeView == 2 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_44, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_45, [_hoisted_46, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_47, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_48, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
      class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
        'flex items-center justify-center font-normal py-1 px-2 text-black rounded-md': true,
        'bg-green-600  font-medium text-white': $setup.categorieSelected === 'produits'
      }),
      onClick: _cache[13] || (_cache[13] = $event => $setup.toggle('produits'))
    }, " Produits ", 2 /* CLASS */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
      class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
        'flex items-center justify-center font-normal py-1 px-2 text-black rounded-md': true,
        'bg-green-600 font-medium text-white': $setup.categorieSelected === 'services'
      }),
      onClick: _cache[14] || (_cache[14] = $event => $setup.toggle('services'))
    }, " Services ", 2 /* CLASS */)])])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" select pour produits ou services "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_49, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
      class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
        'w-4/6': $setup.categorieSelected == 'produits',
        'w-full': $setup.categorieSelected == 'services' || $setup.quoteContent.value != null
      })
    }, [_hoisted_50, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-select", {
      flex: "normal",
      placeholder: 'Veuillez sélectionner un élément',
      usage: "box",
      "options-data": JSON.stringify($setup.quoteContent.options)
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
      slot: "native-input",
      onChange: _cache[15] || (_cache[15] = $event => $setup.quoteContent.value = JSON.parse($event.target?.dataset.option))
    }, null, 32 /* HYDRATE_EVENTS */)], 8 /* PROPS */, _hoisted_51)], 2 /* CLASS */), $setup.quoteContent.value != null && $setup.categorieSelected == 'produits' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_52, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_53, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.quoteContent.selected.stock), 1 /* TEXT */), _hoisted_54])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_55, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" quantité "), $setup.categorieSelected == 'produits' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_56, [_hoisted_57, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-input", _hoisted_58, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
      slot: "native-input",
      "onUpdate:modelValue": _cache[16] || (_cache[16] = $event => $setup.quoteContent.quantity = $event),
      type: "number",
      min: "1"
    }, null, 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, $setup.quoteContent.quantity]])])])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
      class: "w-full",
      onClick: _cache[17] || (_cache[17] =
      //@ts-ignore
      (...args) => $setup.add && $setup.add(...args))
    }, [..._hoisted_60])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_61, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.quoteContent.list, (item, index) => {
      return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
        key: index,
        class: "flex items-center justify-between w-full pb-3 mb-3 border-b border-gray-300"
      }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_62, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(`${item.label}`) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.categorieSelected == "produits" ? `(PU : ${item.selling_price}, qté : ${item.quantity})` : ""), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_iconButton, {
        icon: "/icons/header/trash-icon.svg",
        onClick: $event => $setup.deleteQuoteContentElement(index)
      }, null, 8 /* PROPS */, ["onClick"])]);
    }), 128 /* KEYED_FRAGMENT */))]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Title "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <SlimTitleBar :title=\"`Taxe`\" icon=\"navigation-level-icon\" /> "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <div class=\"w-full mt-3\">\n\t\t\t\t\t\t\t\t\t<p class=\"mt-2\">&nbsp; Type de taxe</p>\n\t\t\t\t\t\t\t\t\t<bb-select\n\t\t\t\t\t\t\t\t\t\tflex=\"normal\"\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Veuillez sélectionner le type de taxe\"\n\t\t\t\t\t\t\t\t\t\toptions-data='[\n\t\t\t\t\t\t\t\t\t\t\t\t{\"key\" : \"+\", \"value\" : \"Augmentation\"},\n\t\t\t\t\t\t\t\t\t\t\t\t{\"key\" : \"-\", \"value\" : \"Réduction\"}\n\t\t\t\t\t\t\t\t\t\t\t]'\n\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t\t<input\n\t\t\t\t\t\t\t\t\t\t\tslot=\"native-input\"\n\t\t\t\t\t\t\t\t\t\t\treadonly\n\t\t\t\t\t\t\t\t\t\t\t@change=\"taxe.type = JSON.parse($event.target?.dataset.option).key\"\n\t\t\t\t\t\t\t\t\t\t/>\n\t\t\t\t\t\t\t\t\t</bb-select>\n\t\t\t\t\t\t\t\t</div> "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Suffixe Taxe "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <div class=\"w-full mt-2\">\n\t\t\t\t\t\t\t\t\t<p class=\"mt-2\">&nbsp; Unité</p>\n\t\t\t\t\t\t\t\t\t<bb-select\n\t\t\t\t\t\t\t\t\t\tflex=\"normal\"\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Veuillez sélectionner l'unité\"\n\t\t\t\t\t\t\t\t\t\toptions-data='[\n\t\t\t\t\t\t\t\t\t\t\t\t{\"key\" : \"XAF\", \"value\" : \"XAF (Franc)\"},\n\t\t\t\t\t\t\t\t\t\t\t\t{\"key\" : \"%\", \"value\" : \"% (Pourcentage)\"}\n\t\t\t\t\t\t\t\t\t\t\t]'\n\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t\t<input\n\t\t\t\t\t\t\t\t\t\t\tslot=\"native-input\"\n\t\t\t\t\t\t\t\t\t\t\treadonly\n\t\t\t\t\t\t\t\t\t\t\t@change=\"taxe.suffix = JSON.parse($event.target?.dataset.option).key\"\n\t\t\t\t\t\t\t\t\t\t/>\n\t\t\t\t\t\t\t\t\t</bb-select>\n\t\t\t\t\t\t\t\t</div> "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Valeur Taxe "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <div class=\"w-full mt-4\">\n\t\t\t\t\t\t\t\t\t<p class=\"mt-2\">&nbsp; Valeur de la taxe</p>\n\t\t\t\t\t\t\t\t\t<bb-input flex=\"normal\" usage=\"unit-price\" placeholder=\"Ex : 1 200 XAf ou 5%\">\n\t\t\t\t\t\t\t\t\t\t<input\n\t\t\t\t\t\t\t\t\t\t\tslot=\"native-input\"\n\t\t\t\t\t\t\t\t\t\t\tv-model=\"taxe.value\"\n\t\t\t\t\t\t\t\t\t\t\tmin=\"0.01\"\n\t\t\t\t\t\t\t\t\t\t\tstep=\"0.01\"\n\t\t\t\t\t\t\t\t\t\t\t:max=\"taxe.suffix == '%' ? 100 : 100000000\"\n\t\t\t\t\t\t\t\t\t\t/>\n\t\t\t\t\t\t\t\t\t</bb-input>\n\t\t\t\t\t\t\t\t</div> "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Label "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <div class=\"w-full mt-4\">\n\t\t\t\t\t\t\t\t\t<p class=\"mt-2\">&nbsp; Intitulé de la Taxe</p>\n\t\t\t\t\t\t\t\t\t<bb-input flex=\"normal\" usage=\"default\" placeholder=\"Ex : Réduction, TVA\">\n\t\t\t\t\t\t\t\t\t\t<input slot=\"native-input\" v-model=\"taxe.label\" />\n\t\t\t\t\t\t\t\t\t</bb-input>\n\t\t\t\t\t\t\t\t</div> ")])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  })]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Groupe Button "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
      'flex w-full items-center': true,
      'justify-end': $setup.activeView === 1,
      ' justify-between': $setup.activeView === 2
    })
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Button Back "), $setup.activeView === 2 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
    key: 0,
    class: "w-auto cursor-pointer",
    onClick: _cache[18] || (_cache[18] =
    //@ts-ignore
    (...args) => $setup.back && $setup.back(...args))
  }, [..._hoisted_64])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Button Suivant And Valider "), $setup.useAccess('add', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
    key: 1,
    class: "w-auto",
    onClick: _cache[19] || (_cache[19] =
    //@ts-ignore
    (...args) => $setup.doAction && $setup.doAction(...args))
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-button", {
    flex: "normal",
    type: "link",
    url: "javascript:void(0)",
    text: $setup.activeView == 2 ? 'Valider' : 'Suivant',
    loading: $setup.validation,
    usage: $setup.activeView == 2 ? 'simple-text' : 'next-with-text',
    "text-color": "white",
    "textcolor-hover": "white",
    bgcolor: "green-800",
    "bgcolor-hover": "green-700",
    "border-color": "green-200"
  }, null, 8 /* PROPS */, _hoisted_65)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)], 2 /* CLASS */)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Section Right LG"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "translate-y-slim-plus"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [($setup.windowWidth > 425 ? true : $setup.viewRightContent) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
      key: 0,
      class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
        'flex flex-col absolute md:relative w-full md:w-3/6 lg:w-3/5 pl-0 md:pl-5 justify-self-end space-y-2 md:space-y-3 lg:space-y-6 lg:border-none border-gray-300 bg-opacity-100 md:bg-opacity-20 bg-layout md:bg-gray-50 z-20 transform transition-all lg:translate-y-0 duration-300 ease-linear': true,
        'opacity-100 translate-y-0': $setup.viewRightContent,
        'opacity-0 md:opacity-100 translate-y-56 md:translate-y-0': !$setup.viewRightContent
      })
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Title "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_66, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SlimTitleBar, {
      title: "Aperçu",
      icon: "navigation-level-icon"
    })]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_67, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SlimTitleBar, {
      title: "Aperçu",
      icon: "navigation-level-icon"
    }, {
      right: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
        class: "flex flex-row w-auto m-1 space-x-3 font-normal cursor-pointer md:hidden hover:opacity-70",
        onClick: _cache[20] || (_cache[20] = $event => $setup.viewRightContent = !$setup.viewRightContent)
      }, [..._hoisted_69])]),
      _: 1 /* STABLE */
    })]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_70, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Set Logo Enterprise "), _hoisted_71, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_72, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_73, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.account.name), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.account.address), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", null, "Tel " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.account.tel), 1 /* TEXT */)]), _hoisted_74, $setup.clients ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_75, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.clients.isNew ? $setup.thirdInfos.name : $setup.clients.selected?.name), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.clients.isNew ? $setup.thirdInfos.address : $setup.clients.selected?.address), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.clients.isNew ? $setup.thirdInfos.phone : $setup.clients.selected?.phone), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Intitulé du devis "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_76, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", _hoisted_77, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.label), 1 /* TEXT */)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Tableau "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("table", _hoisted_78, [_hoisted_79, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("tbody", _hoisted_80, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.quotePreviewContent, (item, index) => {
      return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("tr", {
        key: item.id,
        class: "cursor-pointer hover:bg-green-200"
      }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", _hoisted_81, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(index + 1), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", _hoisted_82, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.label), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", _hoisted_83, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.quantity), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", _hoisted_84, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.categorieSelected == "services" ? item.price : item.selling_price), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", _hoisted_85, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.totalAmountHT), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <td v-if=\"item.taxesproducts?.length != 0\" class=\"p-2\">\n\t\t\t\t\t\t\t\t\t\t\t<div\n\t\t\t\t\t\t\t\t\t\t\t\tv-for=\"(tax, index) in item.taxesproducts\"\n\t\t\t\t\t\t\t\t\t\t\t\t:key=\"index\"\n\t\t\t\t\t\t\t\t\t\t\t\tclass=\"flex flex-row items-end justify-between\"\n\t\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t\t\t\t<p>{{ `${tax.type}${tax.value}${tax.suffix}` }}</p>\n\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t<td v-else class=\"p-2\">--</td> "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", _hoisted_86, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.totalAugmentationXAF), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", _hoisted_87, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.totalReductionXAF), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("td", _hoisted_88, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.totalAmountTTC), 1 /* TEXT */)]);
    }), 128 /* KEYED_FRAGMENT */))])])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Resumé d'information "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_89, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_90, [_hoisted_91, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_92, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.quoteTotalAmount.HT) + " XAF", 1 /* TEXT */)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_93, [_hoisted_94, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_95, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.taxe.type) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.taxe.value) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.taxe.suffix), 1 /* TEXT */)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_96, [_hoisted_97, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_98, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.quoteTotalAmount.TTC) + " XAF", 1 /* TEXT */)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <div class=\"flex flex-row justify-between\">\n\t\t\t\t\t\t\t\t<div class=\"font-bold\">Acompte de 25 %</div>\n\t\t\t\t\t\t\t\t<div class=\"flex justify\"> 900 000 FCFA</div>\n\t\t\t\t\t\t\t</div> "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_99, [_hoisted_100, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_101, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.endDate), 1 /* TEXT */)])])], 2 /* CLASS */)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  })]))])])], 2112 /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/sales/quotesOverview.vue?vue&type=template&id=189c249e&ts=true":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/sales/quotesOverview.vue?vue&type=template&id=189c249e&ts=true ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "workspace-content h-full overflow-x-hidden pb-5 md:pb-0"
};
const _hoisted_2 = {
  class: "w-full relative"
};
const _hoisted_3 = {
  key: 0,
  class: "w-full"
};
const _hoisted_4 = {
  class: "grid sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 2xl:grid-cols-3 gap-6 w-full mt-10 group-cards"
};
const _hoisted_5 = {
  class: "w-full"
};
const _hoisted_6 = {
  class: "w-full"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_SkeletonSubOverview = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SkeletonSubOverview");
  const _component_SkeletonLoader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SkeletonLoader");
  const _component_CardBannerDashboardOverview = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("CardBannerDashboardOverview");
  const _component_StatCard2 = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("StatCard2");
  const _component_StatCard1 = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("StatCard1");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isLoading ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_SkeletonLoader, {
      key: 0
    }, {
      default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SkeletonSubOverview, {
        stats: 6
      })]),
      _: 1 /* STABLE */
    })) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), !$setup.isLoading ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_CardBannerDashboardOverview, {
    icon: "settings-banner-picture",
    "line-one": "Ventes et dévis",
    "line-two-a": "Dans cette section vous avez une vue d'ensemble des statistiques",
    "line-two-b": "",
    "line-three": "Lancer le tutoriel d'aide ",
    link: "workspace-settings-business-account"
  }), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_4, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_5, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_StatCard2, {
    "line-one": $setup.orignalMoney($setup.overviewInfos != undefined ? $setup.overviewInfos[0]?.value : 0).toString(),
    "line-two": $setup.overviewInfos != undefined ? $setup.overviewInfos[0]?.curency : '',
    "line-three": $setup.overviewInfos != undefined ? $setup.overviewInfos[0]?.title : 'Titre non accessible',
    link: "workspace-quotes-list-quotes",
    "route-params": {
      page: 'caisse'
    },
    access: $setup.useAccess('read', $setup.permissions)
  }, null, 8 /* PROPS */, ["line-one", "line-two", "line-three", "access"])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_6, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_StatCard1, {
    "line-one": $setup.overviewInfos != undefined ? $setup.overviewInfos[1].value.toString() : 0,
    "line-two": $setup.overviewInfos != undefined ? $setup.overviewInfos[1]?.title : 'Titre non accessible',
    link: "workspace-quotes-list-quotes",
    access: $setup.useAccess('read', $setup.permissions)
  }, null, 8 /* PROPS */, ["line-one", "line-two", "access"])])])])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])]);
}

/***/ }),

/***/ "./src/state/pages/quotes/quote.ts":
/*!*****************************************!*\
  !*** ./src/state/pages/quotes/quote.ts ***!
  \*****************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   activeView: function() { return /* binding */ activeView; },
/* harmony export */   add: function() { return /* binding */ add; },
/* harmony export */   back: function() { return /* binding */ back; },
/* harmony export */   calculate: function() { return /* binding */ calculate; },
/* harmony export */   categorieSelected: function() { return /* binding */ categorieSelected; },
/* harmony export */   clients: function() { return /* binding */ clients; },
/* harmony export */   deleteQuoteContentElement: function() { return /* binding */ deleteQuoteContentElement; },
/* harmony export */   doAction: function() { return /* binding */ doAction; },
/* harmony export */   endDate: function() { return /* binding */ endDate; },
/* harmony export */   error: function() { return /* binding */ error; },
/* harmony export */   getClients: function() { return /* binding */ getClients; },
/* harmony export */   getGlobalAmount: function() { return /* binding */ getGlobalAmount; },
/* harmony export */   getQuoteContent: function() { return /* binding */ getQuoteContent; },
/* harmony export */   ir: function() { return /* binding */ ir; },
/* harmony export */   isLoading: function() { return /* binding */ isLoading; },
/* harmony export */   label: function() { return /* binding */ label; },
/* harmony export */   next: function() { return /* binding */ next; },
/* harmony export */   quoteContent: function() { return /* binding */ quoteContent; },
/* harmony export */   quotePreviewContent: function() { return /* binding */ quotePreviewContent; },
/* harmony export */   quoteTotalAmount: function() { return /* binding */ quoteTotalAmount; },
/* harmony export */   reset: function() { return /* binding */ reset; },
/* harmony export */   resetError: function() { return /* binding */ resetError; },
/* harmony export */   resetThirdInfosError: function() { return /* binding */ resetThirdInfosError; },
/* harmony export */   setDeadLine: function() { return /* binding */ setDeadLine; },
/* harmony export */   taxe: function() { return /* binding */ taxe; },
/* harmony export */   thirdInfos: function() { return /* binding */ thirdInfos; },
/* harmony export */   thirdInfosError: function() { return /* binding */ thirdInfosError; },
/* harmony export */   thirdTosend: function() { return /* binding */ thirdTosend; },
/* harmony export */   toggle: function() { return /* binding */ toggle; },
/* harmony export */   tva: function() { return /* binding */ tva; },
/* harmony export */   validate: function() { return /* binding */ validate; },
/* harmony export */   validation: function() { return /* binding */ validation; }
/* harmony export */ });
/* harmony import */ var core_js_modules_es_array_push_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.push.js */ "./node_modules/core-js/modules/es.array.push.js");
/* harmony import */ var core_js_modules_es_array_push_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_push_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _composable_useNotyf__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/composable/useNotyf */ "./src/composable/useNotyf.ts");
/* harmony import */ var _composable_useRetrievingAll__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/composable/useRetrievingAll */ "./src/composable/useRetrievingAll.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/api */ "./src/api/index.ts");
/* harmony import */ var _router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/router */ "./src/router/index.ts");







/**
 * composable
 */
const notify = (0,_composable_useNotyf__WEBPACK_IMPORTED_MODULE_2__["default"])();
/**
 * var
 */
const activeView = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(1);
/**
 * loader
 */
const isLoading = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(false);
const validation = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(false);
/**
 * Data
 */
const thirdInfos = (0,vue__WEBPACK_IMPORTED_MODULE_1__.reactive)({
  name: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(""),
  phone: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(""),
  address: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(""),
  email: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)("")
});
const taxe = (0,vue__WEBPACK_IMPORTED_MODULE_1__.reactive)({
  value: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(null),
  suffix: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(null),
  type: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(null),
  label: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(null)
});
const label = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)("");
const tva = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(0);
const ir = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(0);
const clients = (0,vue__WEBPACK_IMPORTED_MODULE_1__.reactive)({
  value: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(),
  selected: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(),
  options: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(),
  list: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(),
  data: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(),
  isNew: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(false)
});
const categorieSelected = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)("produits");
const quoteContent = (0,vue__WEBPACK_IMPORTED_MODULE_1__.reactive)({
  data: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)({}),
  options: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)({}),
  value: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(null),
  list: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([]),
  selected: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(),
  products: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([]),
  services: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([]),
  quantity: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(null),
  price: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)()
});
const quotePreviewContent = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([]);
const quoteTotalAmount = (0,vue__WEBPACK_IMPORTED_MODULE_1__.reactive)({
  TTC: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(0),
  HT: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(0),
  totalAugmentationXAF: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(0),
  totalReductionXAF: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(0)
});
const endDate = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)("");
const thirdTosend = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(null);
/**
 * errors
 */
const error = (0,vue__WEBPACK_IMPORTED_MODULE_1__.reactive)({
  label: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(null),
  third: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(null),
  end: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(null),
  categorie: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(null),
  tva: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(null),
  ir: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(null)
});
const thirdInfosError = (0,vue__WEBPACK_IMPORTED_MODULE_1__.reactive)({
  name: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(null),
  email: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(null),
  phone: (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(null)
});
/**
 * functions
 */
const reset = () => {
  quoteContent.list = [];
  quoteContent.isNew = false;
  endDate.value = "";
  label.value = "";
  tva.value = 0;
  ir.value = 0;
  thirdInfos.name = "";
  thirdInfos.phone = "";
  thirdInfos.address = "";
  thirdInfos.email = "";
  quotePreviewContent.value = [];
  taxe.suffix = null;
  taxe.value = null;
  taxe.label = null;
  quoteContent.quantity = 1;
  quoteTotalAmount.TTC = 0;
  quoteTotalAmount.HT = 0;
};
const resetError = () => {
  error.label = null;
  error.third = null;
  error.end = null;
  error.categorie = null;
  error.tva = null;
  error.ir = null;
};
const resetThirdInfosError = () => {
  thirdInfosError.name = null;
  thirdInfosError.email = null;
  thirdInfosError.phone = null;
};
const next = () => {
  if (clients.isNew) {
    if (thirdInfos.name != "" && thirdInfos.email != "" && thirdInfos.phone != "" && label.value != "" && tva.value < 0 && tva.value > 100 && ir.value < 0 && ir.value > 100 && endDate.value != "") {
      resetError();
      resetThirdInfosError();
      activeView.value = 2;
    } else {
      notify.warning("Veuillez remplir tous les champs !");
      if (label.value.trim() == "") error.label = false;
      if (endDate.value == "") error.end = false;
      if (tva.value < 0 && tva.value > 0) error.tva = false;
      if (thirdInfos.name.trim() == "") thirdInfosError.name = false;
      if (thirdInfos.email.trim() == "") thirdInfosError.email = false;
      if (thirdInfos.phone.trim() == "") thirdInfosError.phone = false;
    }
  } else if (label.value != "" && endDate.value != "" && tva.value >= 0 && tva.value <= 100 && ir.value >= 0 && ir.value <= 100 && !lodash__WEBPACK_IMPORTED_MODULE_4__.isEmpty(clients.value)) {
    resetError();
    resetThirdInfosError();
    activeView.value = 2;
  } else {
    notify.warning("Veuillez remplir tous les champs SVP !");
    if (label.value.trim() == "") error.label = false;
    if (endDate.value == "") error.end = false;
    if (tva.value < 0 || tva.value >= 100) error.tva = false;
    if (ir.value < 0 || ir.value >= 100) error.ir = false;
    if (lodash__WEBPACK_IMPORTED_MODULE_4__.isEmpty(clients.value)) error.third = false;
  }
};
const createthird = async () => {
  const dataToSend = {
    name: thirdInfos.name,
    phone: thirdInfos.phone,
    email: thirdInfos.email,
    address: thirdInfos.address,
    type: "client"
  };
  // console.log("donnée", dataToSend);
  await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().thirds.create(dataToSend).then(response => {
    thirdTosend.value = response.data;
    notify.success("Nouveau client créé avec succès !");
  }).catch(error => {
    notify.error(error.data.response.message);
  });
};
const back = () => {
  activeView.value = 1;
};
const doAction = async () => {
  switch (activeView.value) {
    case 1:
      next();
      break;
    case 2:
      await validate();
      break;
    default:
      break;
  }
};
const validate = async () => {
  if (quoteContent.list.length > 0) {
    validation.value = true;
    // isLoading.value = true;
    if (clients.isNew) {
      await createthird();
    } else {
      thirdTosend.value = clients.selected;
    }
    const dataToSend = {
      label: label.value,
      third: thirdTosend.value,
      end: endDate.value,
      tva: tva.value,
      ir: ir.value,
      products: quoteContent.list,
      services: quoteContent.list,
      taxes: taxe.suffix != null && taxe.value != null && taxe.label && taxe.type != null && quoteContent.list?.length > 0 ? [taxe] : ""
    };
    if (categorieSelected.value == "produits") {
      delete dataToSend.services;
    } else {
      delete dataToSend.products;
    }
    // console.log("dataToSend: ", dataToSend);
    await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().quotes.create(dataToSend).then(response => {
      validation.value = false;
      notify.success("Devis créé avec succès !");
      // isLoading.value = false;
      _router__WEBPACK_IMPORTED_MODULE_6__["default"].push({
        name: 'workspace-quotes-list-quotes'
      });
    }).catch(error => {
      // isLoading.value = false;
      if (error.data.response) notify.error(error.data.response.message);else notify.error('Une erreur est survenue lors de la création du devis');
      validation.value = false;
    });
    validation.value = false;
    activeView.value = 1;
    reset();
  } else {
    notify.warning("Veuillez ajouter au moins un " + categorieSelected.value);
  }
};
const add = async () => {
  let QteError = true;
  if (categorieSelected.value != "services") {
    if (!isNaN(parseInt(quoteContent.quantity)) && quoteContent.quantity != null) {
      QteError = true; //quoteContent.selected.stock
    } else {
      QteError = false;
    }
  }
  if (!lodash__WEBPACK_IMPORTED_MODULE_4__.isEmpty(quoteContent.value) && QteError) {
    quoteContent.selected = quoteContent.data.find(item => item.key == quoteContent.value.key);
    quoteContent.selected.quantity = categorieSelected.value == "services" ? 1 : quoteContent.quantity;
    //liste des produits ou services affiché au nveau du formulaire
    quoteContent.list.push(quoteContent.selected);
    quoteContent.quantity = null;
    await calculate(quoteContent.selected);
    if (taxe.suffix != null && taxe.value != null && taxe.type != null && taxe.label != null && quoteContent.list?.length > 0) {
      await getGlobalAmount(quoteContent.list, [{
        taxe
      }]);
    } else {
      await getGlobalAmount(quoteContent.list, "");
    }
  } else if (lodash__WEBPACK_IMPORTED_MODULE_4__.isEmpty(quoteContent.value)) {
    notify.warning("Veuillez choisir un " + categorieSelected.value);
  } else {
    notify.warning(`Veuillez entrer une quantité pour le ${categorieSelected.value} sélectionné!`);
  }
  // console.log("quoteContent.list: ", quoteContent.list);
};

const toggle = value => {
  categorieSelected.value = value;
  quoteContent.list = [];
  quoteContent.selected = {};
  quoteContent.quantity = null;
  quotePreviewContent.value = [];
};
/**
 * Cette fonction calcule les montants spécifique au produit selectionné et ajouté dans le dévis
 *
 * @param data
 */
const calculate = async data => {
  switch (categorieSelected.value) {
    case "produits":
      await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().comptability.calculate.productsAmmount({
        products: [data],
        tva: tva.value,
        ir: ir.value
      }).then(response => {
        const data = response.data;
        const totalAmountTTC = data.total_price_NET;
        const totalAmountHT = data.total_price_HT;
        const totalAugmentationXAF = data.total_augmentation_XAF;
        const totalReductionXAF = data.total_reduction_XAF;
        quotePreviewContent.value.push({
          ...quoteContent.selected,
          totalAmountTTC,
          totalAmountHT,
          totalAugmentationXAF,
          totalReductionXAF
        });
      }).catch(error => {
        notify.error(error.data.response.message);
      });
      break;
    case "services":
      await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().comptability.calculate.servicesAmmount({
        services: [data],
        tva: tva.value,
        ir: ir.value
      }).then(response => {
        const data = response.data;
        const totalAmountTTC = data.total_price_NET;
        const totalAmountHT = data.total_price_HT;
        const totalAugmentationXAF = data.total_augmentation_XAF;
        const totalReductionXAF = data.total_reduction_XAF;
        quotePreviewContent.value.push({
          ...quoteContent.selected,
          totalAmountTTC,
          totalAmountHT,
          totalAugmentationXAF,
          totalReductionXAF
        });
      }).catch(error => {
        notify.error(error.data.response.message);
      });
      break;
  }
  // console.log("quotePreviewContent.value: ", quotePreviewContent.value);
};
/**
 * Cette fonction calcule le montant total en considerant l'ensemble des produits du devis
 *
 * @param data
 * @param taxe
 */
const getGlobalAmount = async (data, taxe) => {
  // console.log("{products: data, taxes: taxe}: ", {
  // 	products: data,
  // 	taxes: taxe,
  // });
  switch (categorieSelected.value) {
    case "produits":
      await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().comptability.calculate.productsAmmount({
        products: data,
        taxes: taxe,
        tva: tva.value,
        ir: ir.value
      }).then(response => {
        const data = response.data;
        const totalAmountTTC = data.total_price_NET;
        const totalAmountHT = data.total_price_HT;
        const totalAugmentationXAF = data.total_augmentation_XAF;
        const totalReductionXAF = data.total_reduction_XAF;
        quoteTotalAmount.TTC = totalAmountTTC;
        quoteTotalAmount.HT = totalAmountHT;
        quoteTotalAmount.totalAugmentationXAF = totalAugmentationXAF;
        quoteTotalAmount.totalReductionXAF = totalReductionXAF;
      }).catch(error => {
        notify.error(error.data.response.message);
      });
      break;
    case "services":
      await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().comptability.calculate.servicesAmmount({
        services: data,
        taxes: taxe,
        tva: tva,
        ir: ir
      }).then(response => {
        const data = response.data;
        const totalAmountTTC = data.total_price_NET;
        const totalAmountHT = data.total_price_HT;
        const totalAugmentationXAF = data.total_augmentation_XAF;
        const totalReductionXAF = data.total_reduction_XAF;
        quoteTotalAmount.TTC = totalAmountTTC;
        quoteTotalAmount.HT = totalAmountHT;
        quoteTotalAmount.totalAugmentationXAF = totalAugmentationXAF;
        quoteTotalAmount.totalReductionXAF = totalReductionXAF;
      }).catch(error => {
        notify.error(error.data.response.message);
      });
      break;
  }
  // console.log("quoteTotalAmount", quoteTotalAmount);
};

const getClients = async () => {
  await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().thirds.specificList("client").then(async response => {
    clients.data = response.data.data.map((third, index) => ({
      key: index.toString(),
      ...third
    }));
    const retrievedData = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(clients.data);
    if (response.data.last_page != 1) await (0,_composable_useRetrievingAll__WEBPACK_IMPORTED_MODULE_3__["default"])({
      which: 'Thirds',
      thirdType: "client",
      totalPages: response.data.last_page
    }, retrievedData);
    // console.log('retrievedData: ', retrievedData.value);
    // eslint-disable-next-line require-atomic-updates
    clients.data = retrievedData.value;
    // eslint-disable-next-line require-atomic-updates
    clients.options = clients.data.map(third => ({
      key: third.key,
      value: third.name
    }));
  }).catch(err => {
    // clients.value = "";
    console.log(err);
  });
};
const getQuoteContent = async () => {
  switch (categorieSelected.value) {
    case "produits":
      await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().stocks.product.getAll().then(async response => {
        quoteContent.data = response.data.data.map((content, index) => ({
          key: index.toString(),
          ...content
        })).filter(content => content.stock != 0);
        const retrievedData = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(quoteContent.data);
        if (response.data.last_page != 1) await (0,_composable_useRetrievingAll__WEBPACK_IMPORTED_MODULE_3__["default"])({
          which: 'Products-for-quote',
          totalPages: response.data.last_page
        }, retrievedData);
        // console.log('retrievedData: ', retrievedData.value);
        // eslint-disable-next-line require-atomic-updates
        quoteContent.data = retrievedData.value;
        // eslint-disable-next-line require-atomic-updates
        quoteContent.options = quoteContent.data.map(content => ({
          key: content.key,
          value: content.label
        }));
      }).catch(err => {
        console.error(err);
      });
      break;
    case "services":
      await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().stocks.services.getAll().then(async response => {
        quoteContent.data = response.data.data.map((content, index) => ({
          key: index.toString(),
          ...content
        }));
        const retrievedData = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(quoteContent.data);
        if (response.data.last_page != 1) await (0,_composable_useRetrievingAll__WEBPACK_IMPORTED_MODULE_3__["default"])({
          which: 'Services-for-quote',
          totalPages: response.data.last_page
        }, retrievedData);
        // console.log('retrievedData: ', retrievedData.value);
        // eslint-disable-next-line require-atomic-updates
        quoteContent.data = retrievedData.value;
        // eslint-disable-next-line require-atomic-updates
        quoteContent.options = quoteContent.data.map(content => ({
          key: content.key,
          value: content.label
        }));
      }).catch(err => {
        console.error(err);
      });
      break;
  }
};
const deleteQuoteContentElement = async index => {
  quoteContent.list.splice(index, 1);
  quotePreviewContent.value.splice(index, 1);
  if (quoteContent.list?.length > 0) {
    if (taxe.suffix != null && taxe.value != null && taxe.type != null && taxe.label != null && quoteContent.list?.length > 0) {
      await getGlobalAmount(quoteContent.list, [{
        taxe
      }]);
    } else {
      await getGlobalAmount(quoteContent.list, "");
    }
  } else {
    quoteTotalAmount.TTC = 0;
    quoteTotalAmount.HT = 0;
    quoteTotalAmount.totalReductionXAF = 0;
    quoteTotalAmount.totalAugmentationXAF = 0;
  }
};
const setDeadLine = event => {
  endDate.value = event.target.value;
};
/**
 * watchers
 */
(0,vue__WEBPACK_IMPORTED_MODULE_1__.watch)([label, clients, endDate, tva, ir], newValues => {
  if (newValues[0] != "") error.label = true;
  if (!lodash__WEBPACK_IMPORTED_MODULE_4__.isEmpty(newValues[1])) error.third = true;
  if (newValues[2] != "") error.end = true;
  if (newValues[3] >= 0 && newValues[3] <= 100) error.tva = true;
  if (newValues[4] >= 0 && newValues[4] <= 100) error.tva = true;
});
(0,vue__WEBPACK_IMPORTED_MODULE_1__.watch)([thirdInfos.name, thirdInfos.email, thirdInfos.phone], newsValue => {
  if (newsValue[0] != "") thirdInfosError.name = true;
  if (newsValue[1] != "") thirdInfosError.email = true;
  if (newsValue[2] != "") thirdInfosError.phone = true;
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/sales/quotesAll.vue?vue&type=style&index=0&id=138c24ce&lang=scss&scoped=true":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/sales/quotesAll.vue?vue&type=style&index=0&id=138c24ce&lang=scss&scoped=true ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/noSourceMaps.js */ "./node_modules/css-loader/dist/runtime/noSourceMaps.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, "[data-v-138c24ce] .p-paginator .p-paginator-current {\n  margin-left: auto;\n}\n[data-v-138c24ce] .p-progressbar {\n  height: 0.5rem;\n  background-color: #d8dadc;\n}\n[data-v-138c24ce] .p-progressbar .p-progressbar-value {\n  background-color: #607d8b;\n}\n[data-v-138c24ce] .p-datepicker {\n  min-width: 25rem;\n}\n[data-v-138c24ce] .p-datepicker td {\n  font-weight: 400;\n}\n[data-v-138c24ce] .p-datatable.p-datatable-5nkap .p-datatable-header {\n  padding: 1rem;\n  text-align: left;\n  font-size: 1.5rem;\n}\n[data-v-138c24ce] .p-datatable.p-datatable-5nkap .p-paginator {\n  padding: 0.3rem;\n}\n[data-v-138c24ce] .p-datatable.p-datatable-5nkap .p-datatable-thead > tr > th {\n  text-align: left;\n}\n[data-v-138c24ce] .p-datatable.p-datatable-5nkap .p-datatable-tbody > tr > td {\n  cursor: auto;\n}\n[data-v-138c24ce] .p-datatable.p-datatable-5nkap .p-dropdown-label:not(.p-placeholder) {\n  text-transform: uppercase;\n}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./src/pages/workspace/sales/quotesAll.vue":
/*!*************************************************!*\
  !*** ./src/pages/workspace/sales/quotesAll.vue ***!
  \*************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _quotesAll_vue_vue_type_template_id_138c24ce_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./quotesAll.vue?vue&type=template&id=138c24ce&scoped=true&ts=true */ "./src/pages/workspace/sales/quotesAll.vue?vue&type=template&id=138c24ce&scoped=true&ts=true");
/* harmony import */ var _quotesAll_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./quotesAll.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/sales/quotesAll.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _quotesAll_vue_vue_type_style_index_0_id_138c24ce_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./quotesAll.vue?vue&type=style&index=0&id=138c24ce&lang=scss&scoped=true */ "./src/pages/workspace/sales/quotesAll.vue?vue&type=style&index=0&id=138c24ce&lang=scss&scoped=true");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;


const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_3__["default"])(_quotesAll_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_quotesAll_vue_vue_type_template_id_138c24ce_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__scopeId',"data-v-138c24ce"],['__file',"src/pages/workspace/sales/quotesAll.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/sales/quotesGenerateQuote.vue":
/*!***********************************************************!*\
  !*** ./src/pages/workspace/sales/quotesGenerateQuote.vue ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _quotesGenerateQuote_vue_vue_type_template_id_5c215a02_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./quotesGenerateQuote.vue?vue&type=template&id=5c215a02&ts=true */ "./src/pages/workspace/sales/quotesGenerateQuote.vue?vue&type=template&id=5c215a02&ts=true");
/* harmony import */ var _quotesGenerateQuote_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./quotesGenerateQuote.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/sales/quotesGenerateQuote.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_quotesGenerateQuote_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_quotesGenerateQuote_vue_vue_type_template_id_5c215a02_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/sales/quotesGenerateQuote.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/sales/quotesOverview.vue":
/*!******************************************************!*\
  !*** ./src/pages/workspace/sales/quotesOverview.vue ***!
  \******************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _quotesOverview_vue_vue_type_template_id_189c249e_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./quotesOverview.vue?vue&type=template&id=189c249e&ts=true */ "./src/pages/workspace/sales/quotesOverview.vue?vue&type=template&id=189c249e&ts=true");
/* harmony import */ var _quotesOverview_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./quotesOverview.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/sales/quotesOverview.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_quotesOverview_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_quotesOverview_vue_vue_type_template_id_189c249e_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/sales/quotesOverview.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/sales/quotesAll.vue?vue&type=script&setup=true&lang=ts":
/*!************************************************************************************!*\
  !*** ./src/pages/workspace/sales/quotesAll.vue?vue&type=script&setup=true&lang=ts ***!
  \************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_quotesAll_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_quotesAll_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./quotesAll.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/sales/quotesAll.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/sales/quotesGenerateQuote.vue?vue&type=script&setup=true&lang=ts":
/*!**********************************************************************************************!*\
  !*** ./src/pages/workspace/sales/quotesGenerateQuote.vue?vue&type=script&setup=true&lang=ts ***!
  \**********************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_quotesGenerateQuote_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_quotesGenerateQuote_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./quotesGenerateQuote.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/sales/quotesGenerateQuote.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/sales/quotesOverview.vue?vue&type=script&setup=true&lang=ts":
/*!*****************************************************************************************!*\
  !*** ./src/pages/workspace/sales/quotesOverview.vue?vue&type=script&setup=true&lang=ts ***!
  \*****************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_quotesOverview_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_quotesOverview_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./quotesOverview.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/sales/quotesOverview.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/sales/quotesAll.vue?vue&type=template&id=138c24ce&scoped=true&ts=true":
/*!***************************************************************************************************!*\
  !*** ./src/pages/workspace/sales/quotesAll.vue?vue&type=template&id=138c24ce&scoped=true&ts=true ***!
  \***************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_quotesAll_vue_vue_type_template_id_138c24ce_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_quotesAll_vue_vue_type_template_id_138c24ce_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./quotesAll.vue?vue&type=template&id=138c24ce&scoped=true&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/sales/quotesAll.vue?vue&type=template&id=138c24ce&scoped=true&ts=true");


/***/ }),

/***/ "./src/pages/workspace/sales/quotesGenerateQuote.vue?vue&type=template&id=5c215a02&ts=true":
/*!*************************************************************************************************!*\
  !*** ./src/pages/workspace/sales/quotesGenerateQuote.vue?vue&type=template&id=5c215a02&ts=true ***!
  \*************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_quotesGenerateQuote_vue_vue_type_template_id_5c215a02_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_quotesGenerateQuote_vue_vue_type_template_id_5c215a02_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./quotesGenerateQuote.vue?vue&type=template&id=5c215a02&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/sales/quotesGenerateQuote.vue?vue&type=template&id=5c215a02&ts=true");


/***/ }),

/***/ "./src/pages/workspace/sales/quotesOverview.vue?vue&type=template&id=189c249e&ts=true":
/*!********************************************************************************************!*\
  !*** ./src/pages/workspace/sales/quotesOverview.vue?vue&type=template&id=189c249e&ts=true ***!
  \********************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_quotesOverview_vue_vue_type_template_id_189c249e_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_quotesOverview_vue_vue_type_template_id_189c249e_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./quotesOverview.vue?vue&type=template&id=189c249e&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/sales/quotesOverview.vue?vue&type=template&id=189c249e&ts=true");


/***/ }),

/***/ "./src/pages/workspace/sales/quotesAll.vue?vue&type=style&index=0&id=138c24ce&lang=scss&scoped=true":
/*!**********************************************************************************************************!*\
  !*** ./src/pages/workspace/sales/quotesAll.vue?vue&type=style&index=0&id=138c24ce&lang=scss&scoped=true ***!
  \**********************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_quotesAll_vue_vue_type_style_index_0_id_138c24ce_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!../../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../../node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./quotesAll.vue?vue&type=style&index=0&id=138c24ce&lang=scss&scoped=true */ "./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/sales/quotesAll.vue?vue&type=style&index=0&id=138c24ce&lang=scss&scoped=true");
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_quotesAll_vue_vue_type_style_index_0_id_138c24ce_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_quotesAll_vue_vue_type_style_index_0_id_138c24ce_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_quotesAll_vue_vue_type_style_index_0_id_138c24ce_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = function(key) { return _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_quotesAll_vue_vue_type_style_index_0_id_138c24ce_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__[key]; }.bind(0, __WEBPACK_IMPORT_KEY__)
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);


/***/ }),

/***/ "./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/sales/quotesAll.vue?vue&type=style&index=0&id=138c24ce&lang=scss&scoped=true":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/sales/quotesAll.vue?vue&type=style&index=0&id=138c24ce&lang=scss&scoped=true ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(/*! !!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!../../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../../node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./quotesAll.vue?vue&type=style&index=0&id=138c24ce&lang=scss&scoped=true */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/sales/quotesAll.vue?vue&type=style&index=0&id=138c24ce&lang=scss&scoped=true");
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(/*! !../../../../node_modules/vue-style-loader/lib/addStylesClient.js */ "./node_modules/vue-style-loader/lib/addStylesClient.js")["default"])
var update = add("1421e083", content, false, {"sourceMap":false,"shadowMode":false});
// Hot Module Replacement
if(false) {}

/***/ })

}]);
//# sourceMappingURL=workspace-sales.js.map