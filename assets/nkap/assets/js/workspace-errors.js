"use strict";
(self["webpackChunk_5nkap"] = self["webpackChunk_5nkap"] || []).push([["workspace-errors"],{

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/error/error.vue?vue&type=script&setup=true&lang=ts":
/*!*****************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/error/error.vue?vue&type=script&setup=true&lang=ts ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var vue_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-router */ "./node_modules/vue-router/dist/vue-router.mjs");




/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'error',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    const route = (0,vue_router__WEBPACK_IMPORTED_MODULE_1__.useRoute)();
    let errorContent = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)({});
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_2__.u)({
      title: "Error"
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onMounted)(() => {
      if (route.params.code != undefined) {
        const errorCode = route.params.code;
        switch (errorCode) {
          case "400":
            errorContent.value = {
              code: errorCode,
              message: "Mauvaise requête demandée"
            };
            break;
          case "401":
            errorContent.value = {
              code: errorCode,
              message: "Authentification requise"
            };
            break;
          case "403":
            errorContent.value = {
              code: errorCode,
              message: "Cette action est interdite"
            };
            break;
          case "403.2":
            errorContent.value = {
              code: errorCode,
              message: "Droits d'accès refusés"
            };
            break;
          case "404":
            errorContent.value = {
              code: errorCode,
              message: "Cette page n'existe pas"
            };
            break;
          case "500":
            errorContent.value = {
              code: errorCode,
              message: "Erreur système"
            };
            break;
          case "503":
            errorContent.value = {
              code: errorCode,
              message: "Erreur système"
            };
            break;
          default:
            errorContent.value = {
              code: "inconu",
              message: "Problème détecté"
            };
            break;
        }
      }
    });
    const __returned__ = {
      route,
      get errorContent() {
        return errorContent;
      },
      set errorContent(v) {
        errorContent = v;
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/error/error.vue?vue&type=template&id=5a3c03ed&ts=true":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/error/error.vue?vue&type=template&id=5a3c03ed&ts=true ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "error-container w-full h-full flex flex-row justify-center items-center"
};
const _hoisted_2 = {
  class: "error-wrapper w-1/2 flex flex-col items-center"
};
const _hoisted_3 = {
  class: "error-inner text-center mb-10"
};
const _hoisted_4 = {
  class: "text-9xl text-primary-800 transform scale-150 mb-8"
};
const _hoisted_5 = {
  class: "text-2xl mb-4"
};
const _hoisted_6 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", {
  class: "text-gray-400 px-28"
}, " Il semblerait qu'une erreur ce soit produite. Veuillez réessayer ou contacter nous si le problème persiste. ", -1 /* HOISTED */);
const _hoisted_7 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-button", {
  flex: "normal",
  type: "link",
  url: "javascript:void(0)",
  text: "Retour à la page précédente",
  usage: "simple-text",
  "is-white-icon": "false",
  "is-bgcolor": "false",
  "text-color": "gray-900",
  "textcolor-hover": "gray-600",
  "bgcolor-hover": "gray-900",
  "border-color": "border border-gray-900"
}, null, -1 /* HOISTED */);
const _hoisted_8 = [_hoisted_7];
function render(_ctx, _cache, $props, $setup, $data, $options) {
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_4, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.errorContent.code), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", _hoisted_5, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.errorContent.message), 1 /* TEXT */), _hoisted_6]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: "w-4/6",
    onClick: _cache[0] || (_cache[0] = (0,vue__WEBPACK_IMPORTED_MODULE_0__.withModifiers)($event => _ctx.$router.go(-1), ["prevent"]))
  }, [..._hoisted_8])])]);
}

/***/ }),

/***/ "./src/pages/error/error.vue":
/*!***********************************!*\
  !*** ./src/pages/error/error.vue ***!
  \***********************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _error_vue_vue_type_template_id_5a3c03ed_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./error.vue?vue&type=template&id=5a3c03ed&ts=true */ "./src/pages/error/error.vue?vue&type=template&id=5a3c03ed&ts=true");
/* harmony import */ var _error_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./error.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/error/error.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_error_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_error_vue_vue_type_template_id_5a3c03ed_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/error/error.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/error/error.vue?vue&type=script&setup=true&lang=ts":
/*!**********************************************************************!*\
  !*** ./src/pages/error/error.vue?vue&type=script&setup=true&lang=ts ***!
  \**********************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_error_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_error_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js!../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./error.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/error/error.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/error/error.vue?vue&type=template&id=5a3c03ed&ts=true":
/*!*************************************************************************!*\
  !*** ./src/pages/error/error.vue?vue&type=template&id=5a3c03ed&ts=true ***!
  \*************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_error_vue_vue_type_template_id_5a3c03ed_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_error_vue_vue_type_template_id_5a3c03ed_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js!../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./error.vue?vue&type=template&id=5a3c03ed&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/error/error.vue?vue&type=template&id=5a3c03ed&ts=true");


/***/ })

}]);
//# sourceMappingURL=workspace-errors.js.map