(self["webpackChunk_5nkap"] = self["webpackChunk_5nkap"] || []).push([["workspace-reports"],{

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsAccountingBalance.vue?vue&type=script&setup=true&lang=ts":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsAccountingBalance.vue?vue&type=script&setup=true&lang=ts ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useNotyf__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/composable/useNotyf */ "./src/composable/useNotyf.ts");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/api */ "./src/api/index.ts");




/**
 * Composables
 */

/**
 * API Calls
 */

/**
 * Variables
 */
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'reportsAccountingBalance',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("list");
    const activeFilter = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const activeView = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    const isLoading = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    const notify = (0,_composable_useNotyf__WEBPACK_IMPORTED_MODULE_2__["default"])();
    const balanceInfos = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)();
    const totals = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)();
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_1__["default"])("getBalance").map(element => {
      return element.action;
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onBeforeMount)(async () => {
      isLoading.value = true;
      await (0,_api__WEBPACK_IMPORTED_MODULE_3__["default"])().accounting.accountingBalance().then(response => {
        balanceInfos.value = response.data;
        totals.value = balanceInfos.value.splice(18, 2);
        console.log("totals.value: ", totals.value);
        isLoading.value = false;
      }).catch(error => {
        notify.error(error.data.response.message);
        isLoading.value = false;
      });
    });
    /**
     * Functions
     */
    const updateSubView = (view, action) => {
      activeView.value = view;
      actionType.value = action;
    };
    /**
     * Providers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissions", permissions);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("updateSubView", updateSubView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeFilter", activeFilter);
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_4__.u)({
      title: "Rapports / Solde comptable"
    });
    const __returned__ = {
      actionType,
      activeFilter,
      activeView,
      isLoading,
      notify,
      balanceInfos,
      totals,
      permissions,
      updateSubView
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsAccountingJournal.vue?vue&type=script&setup=true&lang=ts":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsAccountingJournal.vue?vue&type=script&setup=true&lang=ts ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");




/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'reportsAccountingJournal',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_2__.u)({
      title: "Rapports / Journal Comptable"
    });
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("list");
    const activeFilter = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const activeView = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    /**
     * Functions
     */
    const updateSubView = (view, action) => {
      activeView.value = view;
      actionType.value = action;
    };
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_1__["default"])("depense_achats").map(element => {
      return element.action;
    });
    /**
     * Providers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissions", permissions);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("updateSubView", updateSubView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeFilter", activeFilter);
    const __returned__ = {
      actionType,
      activeFilter,
      activeView,
      updateSubView,
      permissions
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsArchiving.vue?vue&type=script&setup=true&lang=ts":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsArchiving.vue?vue&type=script&setup=true&lang=ts ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var vue_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! vue-router */ "./node_modules/vue-router/dist/vue-router.mjs");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");
/* harmony import */ var _composable_useNotyf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/composable/useNotyf */ "./src/composable/useNotyf.ts");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/api */ "./src/api/index.ts");







/**
 * Composables
 */

/**
 * API Calls
 */

/**
 * Injects
 */
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'reportsArchiving',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_6__.u)({
      title: "Gestions / Données Archivées"
    });
    const updateAppHeaderAction = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("updateAppHeaderAction");
    /**
     * Variables
     */
    const router = (0,vue_router__WEBPACK_IMPORTED_MODULE_7__.useRouter)();
    const route = (0,vue_router__WEBPACK_IMPORTED_MODULE_7__.useRoute)();
    const notify = (0,_composable_useNotyf__WEBPACK_IMPORTED_MODULE_4__["default"])();
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("list");
    const activeFilter = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("Ventes");
    const activeView = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    // Pagination
    const metaDataAllArchives = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)({});
    // Datas
    const allArchives = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([]);
    const columns = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([]);
    const selected = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)({});
    const dateDebut = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const dateFin = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const others = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([{
      field: "libelle_operation",
      header: "Libellé Opération",
      sort: false,
      style: "min-width: 14rem"
    }, {
      field: "entite",
      header: "Entité",
      sort: true,
      style: "min-width: 10rem"
    }, {
      field: "type_reglement",
      header: "Paiement",
      sort: true,
      style: "min-width: 10rem"
    }, {
      field: "montant",
      header: "Montant",
      sort: true,
      style: "min-width: 10rem"
    }, {
      field: "montant_regle",
      header: "Montant réglé",
      sort: true,
      style: "min-width: 10rem"
    }, {
      field: "reste",
      header: "Reste",
      sort: true,
      style: "min-width: 10rem"
    }, {
      field: "reference",
      header: "Référence système",
      sort: true,
      style: "min-width: 10rem"
    }, {
      field: "currency",
      header: "Devise",
      sort: true,
      style: "min-width: 10rem"
    }, {
      field: "date_paye",
      header: "Date de paiment",
      sort: true,
      style: "min-width: 10rem"
    }, {
      field: "date_regle",
      header: "Date de règlement",
      sort: true,
      style: "min-width: 10rem"
    }, {
      field: "date",
      header: "Date",
      sort: true,
      style: "min-width: 10rem"
    }]);
    const purchases = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([{
      field: "label",
      header: "Intitulé de l'opération",
      sort: true,
      style: "min-width: 14rem;max-width: 16rem"
    }, {
      field: "start",
      header: "Date de l'achat",
      sort: true,
      style: "min-width: 12rem"
    }, {
      field: "created_at",
      header: "Date d'enregistrement'",
      sort: true,
      style: "min-width: 12rem"
    }, {
      field: "cost",
      header: "Montant facture",
      sort: true,
      style: "min-width: 10rem"
    }, {
      field: "third.name",
      header: "Fournisseur",
      sort: true,
      style: "min-width: 12rem"
    }]);
    const salesColumns = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([{
      field: "label",
      header: "Intitulé de l'opération",
      sort: false,
      style: "min-width: 18rem"
    }, {
      field: "third.name",
      header: "Client",
      sort: true,
      style: "min-width: 10rem"
    }, {
      field: "cost",
      header: "Montant facture",
      sort: true,
      style: "min-width: 12rem"
    }, {
      field: "created_at",
      header: "Date Opération",
      sort: true,
      style: "min-width: 10rem"
    }]);
    const expenseColumns = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([{
      field: "label",
      header: "Intitulé de l'opération",
      sort: true,
      style: "min-width: 10rem"
    }, {
      field: "typesdepense.libele",
      header: "Type",
      sort: true,
      style: actionType.value == "list" ? "min-width: 10rem" : "min-width: 8rem"
    }, {
      field: "sousdepense.label",
      header: "Sous Type",
      sort: true,
      style: actionType.value == "list" ? "min-width: 10rem" : "min-width: 6rem"
    }, {
      field: "cost",
      header: "Coût",
      sort: true,
      style: actionType.value == "list" ? "min-width: 10rem" : "min-width: 8rem"
    }, {
      field: "transaction.start",
      header: "Date Dépense",
      sort: true,
      style: actionType.value == "list" ? "min-width: 10rem" : "min-width: 10rem"
    }, {
      field: "created_at",
      header: "Date Enre.",
      sort: true,
      style: actionType.value == "list" ? "min-width: 10rem" : "min-width: 10rem"
    }]);
    const subventionsColums = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([{
      field: "label",
      header: "Intitulé de l'opération",
      sort: true,
      style: "min-width: 10rem"
    }, {
      field: "third.name",
      header: "Bailleur de fonds",
      sort: true,
      style: "min-width: 12rem"
    }, {
      field: "cost",
      header: "Montant",
      sort: true,
      style: "min-width: 10rem"
    }, {
      field: "start",
      header: "Date Subvention",
      sort: true,
      style: "min-width: 10rem"
    }, {
      field: "created_at",
      header: "Date Enre.",
      sort: true,
      style: "min-width: 10rem"
    }, {
      field: "branch",
      header: "Surcussale",
      sort: true,
      style: "min-width: 10rem"
    }]);
    const empruntsColumns = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([{
      field: "label",
      header: "Intitulé de l'opération",
      sort: true,
      style: "min-width: 10rem"
    }, {
      field: "cost",
      header: "Montant",
      sort: true,
      style: "min-width: 8rem"
    }, {
      field: "interet",
      header: "Interêt",
      sort: true,
      style: "min-width: 8rem"
    }, {
      field: "reste",
      header: "Montant payé",
      sort: true,
      style: "min-width: 10rem"
    }, {
      field: "value",
      header: "Interêt payé",
      sort: true,
      style: "min-width: 10rem"
    }, {
      field: "start",
      header: "Date Emprunt",
      sort: true,
      style: "min-width: 8rem"
    }, {
      field: "created_at",
      header: "Date Enre.",
      sort: true,
      style: "min-width: 10rem"
    }]);
    const ristournesColumns = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([{
      field: "label",
      header: "Intitulé de l'opération",
      sort: true,
      style: "min-width: 14rem"
    }, {
      field: "third.name",
      header: "Fournisseur",
      sort: true,
      style: "min-width: 12rem"
    }, {
      field: "start",
      header: "Date Réception",
      sort: true,
      style: "min-width: 10rem"
    }, {
      field: "created_at",
      header: "Date Enre.",
      sort: true,
      style: "min-width: 10rem"
    }, {
      field: "branch",
      header: "Surcussale",
      sort: true,
      style: "min-width: 8rem"
    }]);
    const avariesColumns = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([{
      field: "label",
      header: "Intitulé de l'opération",
      sort: true,
      style: "min-width: 14rem"
    }, {
      field: "quantity",
      header: "Quantité",
      sort: true,
      style: "max-width: 10rem"
    }, {
      field: "created_at",
      header: "Date de création",
      sort: true,
      style: "min-width: 14rem"
    }, {
      field: "start",
      header: "Date de Perte",
      sort: true,
      style: "min-width: 14rem"
    }, {
      field: "system_reference",
      header: "Référence Système",
      sort: true,
      style: "min-width: 10rem"
    }]);
    // User permissions
    const permissions = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([]);
    // Loaders
    const isLoading = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_1__.userCurrentBranch.value.id, async newValue => {
      getPermissions();
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])("read", permissions.value)) {
        await GetCurrentArchivesData();
        // await getAllArchives(); // TODO: delete
      }
    });

    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => activeFilter.value, () => {
      GetCurrentArchivesData();
    });
    /**
     * Functions
     */
    const updateSubView = (view, action) => {
      activeView.value = view;
      actionType.value = action;
    };
    async function GetCurrentArchivesData(page, enableLoader = true) {
      if (enableLoader) isLoading.value = true;
      switch (activeFilter.value) {
        case "Ventes":
          columns.value = salesColumns.value;
          if (page != null) {
            await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().archives.salesDeleted(page).then(response => {
              metaDataAllArchives.value = response.data;
              // console.log("response.data:, activeFilter ", response.data.data, activeFilter);
              allArchives.value = response.data.data;
              isLoading.value = false;
            }).catch(error => {
              isLoading.value = false;
              notify.error("Une erreur c'est produite lors de la récupération des données.");
            });
          } else {
            await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().archives.salesDeleted().then(response => {
              metaDataAllArchives.value = response.data;
              // console.log("response.data:, activeFilter ", response.data.data, activeFilter);
              allArchives.value = response.data.data;
              isLoading.value = false;
            }).catch(error => {
              isLoading.value = false;
              notify.error("Une erreur c'est produite lors de la récupération des données.");
            });
          }
          break;
        case "Achats":
          columns.value = purchases.value;
          if (page != null) {
            await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().archives.purchasesDeleted(page).then(response => {
              metaDataAllArchives.value = response.data;
              allArchives.value = response.data.data;
              isLoading.value = false;
            }).catch(error => {
              isLoading.value = false;
              notify.error("Une erreur c'est produite lors de la récupération des données.");
            });
          } else {
            await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().archives.purchasesDeleted().then(response => {
              metaDataAllArchives.value = response.data;
              // console.log('metaDataAllArchives.value : ', metaDataAllArchives.value);
              allArchives.value = response.data.data;
              isLoading.value = false;
            }).catch(error => {
              isLoading.value = false;
              notify.error("Une erreur c'est produite lors de la récupération des données.");
            });
          }
          break;
        case "Dépenses":
          columns.value = expenseColumns.value;
          if (page != null) {
            await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().archives.expenseDeleted(page).then(response => {
              metaDataAllArchives.value = response.data;
              allArchives.value = response.data.data;
              isLoading.value = false;
            }).catch(error => {
              isLoading.value = false;
              notify.error("Une erreur c'est produite lors de la récupération des données.");
            });
          } else {
            await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().archives.expenseDeleted().then(response => {
              metaDataAllArchives.value = response.data;
              // console.log('metaDataAllArchives.value : ', metaDataAllArchives.value);
              allArchives.value = response.data.data;
              isLoading.value = false;
            }).catch(error => {
              isLoading.value = false;
              notify.error("Une erreur c'est produite lors de la récupération des données.");
            });
          }
          break;
        case "Subventions":
          columns.value = subventionsColums.value;
          if (page != null) {
            await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().archives.subventionsDeleted(page).then(response => {
              metaDataAllArchives.value = response.data;
              allArchives.value = response.data.data;
              isLoading.value = false;
            }).catch(error => {
              isLoading.value = false;
              notify.error("Une erreur c'est produite lors de la récupération des données.");
            });
          } else {
            await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().archives.subventionsDeleted().then(response => {
              metaDataAllArchives.value = response.data;
              // console.log('metaDataAllArchives.value : ', metaDataAllArchives.value);
              allArchives.value = response.data.data;
              isLoading.value = false;
            }).catch(error => {
              isLoading.value = false;
              notify.error("Une erreur c'est produite lors de la récupération des données.");
            });
          }
          break;
        case "Emprunts":
          columns.value = empruntsColumns.value;
          if (page != null) {
            await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().archives.empruntsDeleted(page).then(response => {
              metaDataAllArchives.value = response.data;
              allArchives.value = response.data.data;
              isLoading.value = false;
            }).catch(error => {
              isLoading.value = false;
              notify.error("Une erreur c'est produite lors de la récupération des données.");
            });
          } else {
            await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().archives.empruntsDeleted().then(response => {
              metaDataAllArchives.value = response.data;
              // console.log('metaDataAllArchives.value : ', metaDataAllArchives.value);
              allArchives.value = response.data.data;
              isLoading.value = false;
            }).catch(error => {
              isLoading.value = false;
              notify.error("Une erreur c'est produite lors de la récupération des données.");
            });
          }
          break;
        case "Ristournes":
          columns.value = ristournesColumns.value;
          if (page != null) {
            await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().archives.ristournesDeleted(page).then(response => {
              metaDataAllArchives.value = response.data;
              allArchives.value = response.data.data;
              isLoading.value = false;
            }).catch(error => {
              isLoading.value = false;
              notify.error("Une erreur c'est produite lors de la récupération des données.");
            });
          } else {
            await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().archives.ristournesDeleted().then(response => {
              metaDataAllArchives.value = response.data;
              // console.log('metaDataAllArchives.value : ', metaDataAllArchives.value);
              allArchives.value = response.data.data;
              isLoading.value = false;
            }).catch(error => {
              isLoading.value = false;
              notify.error("Une erreur c'est produite lors de la récupération des données.");
            });
          }
          break;
        case "Avaries":
          columns.value = avariesColumns.value;
          if (page != null) {
            await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().archives.avariesDeleted(page).then(response => {
              metaDataAllArchives.value = response.data;
              allArchives.value = response.data.data;
              isLoading.value = false;
            }).catch(error => {
              isLoading.value = false;
              notify.error("Une erreur c'est produite lors de la récupération des données.");
            });
          } else {
            await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().archives.avariesDeleted().then(response => {
              metaDataAllArchives.value = response.data;
              // console.log('metaDataAllArchives.value : ', metaDataAllArchives.value);
              allArchives.value = response.data.data;
              isLoading.value = false;
            }).catch(error => {
              isLoading.value = false;
              notify.error("Une erreur c'est produite lors de la récupération des données.");
            });
          }
          break;
        case "Others":
          columns.value = others.value;
          if (page != null) {
            await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().archives.getAll(page).then(response => {
              metaDataAllArchives.value = response.data;
              allArchives.value = response.data.data;
              isLoading.value = false;
            }).catch(error => {
              isLoading.value = false;
              notify.error("Une erreur c'est produite lors de la récupération des données.");
            });
          } else {
            await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().archives.getAll().then(response => {
              metaDataAllArchives.value = response.data;
              // console.log('metaDataAllArchives.value : ', metaDataAllArchives.value);
              allArchives.value = response.data.data;
              isLoading.value = false;
            }).catch(error => {
              isLoading.value = false;
              notify.error("Une erreur c'est produite lors de la récupération des données.");
            });
          }
          break;
        default:
          break;
      }
    }
    const getPermissions = () => {
      switch (activeFilter.value) {
        case "Ventes":
          permissions.value = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_2__["default"])("vente_marchandises_delete").map(element => {
            return element.action;
          });
          break;
        case "Achats":
          permissions.value = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_2__["default"])("depense_achats_delete").map(element => {
            return element.action;
          });
          break;
        case "Dépenses":
          permissions.value = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_2__["default"])("depenses_delete").map(element => {
            return element.action;
          });
          break;
        case "Subventions":
          permissions.value = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_2__["default"])("subventions_delete").map(element => {
            return element.action;
          });
          break;
        case "Emprunts":
          permissions.value = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_2__["default"])("emprunts_delete").map(element => {
            return element.action;
          });
          break;
        case "Ristournes":
          permissions.value = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_2__["default"])("ristournes_delete").map(element => {
            return element.action;
          });
          break;
        case "Avaries":
          permissions.value = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_2__["default"])("avaries_delete").map(element => {
            return element.action;
          });
          break;
        case "Others":
          permissions.value = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_2__["default"])("archives").map(element => {
            return element.action;
          });
          break;
        default:
          //
          break;
      }
    };
    /**
     * Page Lifecycle
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onBeforeMount)(async () => {
      // getPermissions();
      // if (useAccess("read", permissions.value)) {
      // 	await GetCurrentArchivesData();
      // 	console.log("OnBefore activeFilter", activeFilter.value, allArchives.value);
      // 	// await getAllArchives();
      // }
    });
    /**
     * Providers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissions", permissions);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("updateSubView", updateSubView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeFilter", activeFilter);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("isLoading", isLoading);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("GetCurrentArchivesData", GetCurrentArchivesData);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("allArchives", allArchives);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("metaDataAllArchives", metaDataAllArchives);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("columns", columns);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("getPermissions", getPermissions);
    const __returned__ = {
      updateAppHeaderAction,
      router,
      route,
      notify,
      actionType,
      activeFilter,
      activeView,
      metaDataAllArchives,
      allArchives,
      columns,
      selected,
      dateDebut,
      dateFin,
      others,
      purchases,
      salesColumns,
      expenseColumns,
      subventionsColums,
      empruntsColumns,
      ristournesColumns,
      avariesColumns,
      permissions,
      isLoading,
      updateSubView,
      GetCurrentArchivesData,
      getPermissions,
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"];
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsBankAndFund.vue?vue&type=script&setup=true&lang=ts":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsBankAndFund.vue?vue&type=script&setup=true&lang=ts ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");
/* harmony import */ var _composable_useReports__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/composable/useReports */ "./src/composable/useReports.ts");






/**
 * Composables
 */

/**
 * Variables Model
 */
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'reportsBankAndFund',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("list");
    const activeFilter = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const activeView = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    let bankChart = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    let bankChartInstance = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    let fundChart = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    let fundChartInstance = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    let bank = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("6"),
      fund = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("6");
    // watchers
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => bank.value, () => {
      (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("banques", bank.value.toString(), bankChart.value, bankChartInstance);
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => fund.value, () => {
      (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("caisses", fund.value.toString(), fundChart.value, fundChartInstance);
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_1__.userCurrentBranch.value.id, newValue => {
      ChartGraph();
      (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("banques", bank.value.toString(), bankChart.value, bankChartInstance);
      (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("caisses", fund.value.toString(), fundChart.value, fundChartInstance);
    });
    const ChartGraph = () => {
      (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.setCharts)("banques", bank.value.toString(), bankChart.value, "Setting Graph Banks", bankChartInstance);
      (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.setCharts)("caisses", fund.value.toString(), fundChart.value, "Setting Graph Funds", fundChartInstance);
    };
    /**
     * Functions
     */
    const updateSubView = (view, action) => {
      activeView.value = view;
      actionType.value = action;
    };
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_5__.u)({
      title: "Rapports / Banque et Caisse"
    });
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_2__["default"])("graphes").map(element => {
      return element.action;
    });
    /**
     * Providers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissions", permissions);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("updateSubView", updateSubView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeFilter", activeFilter);
    /**
     * Page Lifecycle
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onMounted)(() => {
      ChartGraph();
    });
    const __returned__ = {
      actionType,
      activeFilter,
      activeView,
      get bankChart() {
        return bankChart;
      },
      set bankChart(v) {
        bankChart = v;
      },
      get bankChartInstance() {
        return bankChartInstance;
      },
      set bankChartInstance(v) {
        bankChartInstance = v;
      },
      get fundChart() {
        return fundChart;
      },
      set fundChart(v) {
        fundChart = v;
      },
      get fundChartInstance() {
        return fundChartInstance;
      },
      set fundChartInstance(v) {
        fundChartInstance = v;
      },
      get bank() {
        return bank;
      },
      set bank(v) {
        bank = v;
      },
      get fund() {
        return fund;
      },
      set fund(v) {
        fund = v;
      },
      ChartGraph,
      updateSubView,
      permissions,
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"];
      },
      get month_options() {
        return _composable_useReports__WEBPACK_IMPORTED_MODULE_4__.month_options;
      },
      get isLoadingChart() {
        return _composable_useReports__WEBPACK_IMPORTED_MODULE_4__.isLoadingChart;
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsBuyAndSelling.vue?vue&type=script&setup=true&lang=ts":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsBuyAndSelling.vue?vue&type=script&setup=true&lang=ts ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");
/* harmony import */ var _composable_useReports__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/composable/useReports */ "./src/composable/useReports.ts");






/**
 * Composables
 */

/**
 * Variables
 */
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'reportsBuyAndSelling',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("list");
    const activeFilter = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const activeView = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    let productPurchaseChart = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    let productPurchaseChartInstance = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    let productSaleChart = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    let productSaleChartInstance = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    let serviceSaleChart = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    let serviceSaleChartInstance = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    const productsP = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    const productsS = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    const services = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    /**
     * Variables Models
     */
    let productPurchase = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    let productSale = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    let serviceSale = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    // watchers
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => productPurchase.value, () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("achat_marchandises", productPurchase.value.toString(), productPurchaseChart.value, productPurchaseChartInstance);
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => productSale.value, () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("vente_marchandises", productSale.value.toString(), productSaleChart.value, productSaleChartInstance);
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => serviceSale.value, () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("vente_marchandises", serviceSale.value.toString(), serviceSaleChart.value, serviceSaleChartInstance);
      }
    });
    const ChartGraph = () => {
      (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.setCharts)("achat_marchandises", productPurchase.value.toString(), productPurchaseChart.value, "Graph achat marchandises", productPurchaseChartInstance);
      (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.setCharts)("vente_marchandises", productSale.value.toString(), productSaleChart.value, "Graph vente marchandises", productSaleChartInstance);
      (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.setCharts)("vente_services", serviceSale.value.toString(), serviceSaleChart.value, "Graph vente services", serviceSaleChartInstance);
    };
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_1__.userCurrentBranch.value.id, newValue => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        ChartGraph();
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("vente_marchandises", serviceSale.value.toString(), serviceSaleChart.value, serviceSaleChartInstance);
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("vente_marchandises", productSale.value.toString(), productSaleChart.value, productSaleChartInstance);
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("achat_marchandises", productPurchase.value.toString(), productPurchaseChart.value, productPurchaseChartInstance);
      }
    });
    /**
     * Functions
     */
    const updateSubView = (view, action) => {
      activeView.value = view;
      actionType.value = action;
    };
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_5__.u)({
      title: "Rapports / Achats et ventes"
    });
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_2__["default"])("graphes").map(element => {
      return element.action;
    });
    /**
     * Providers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissions", permissions);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("updateSubView", updateSubView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeFilter", activeFilter);
    /**
     * Page Lifecycle
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onMounted)(() => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        ChartGraph();
      }
    });
    const __returned__ = {
      actionType,
      activeFilter,
      activeView,
      get productPurchaseChart() {
        return productPurchaseChart;
      },
      set productPurchaseChart(v) {
        productPurchaseChart = v;
      },
      get productPurchaseChartInstance() {
        return productPurchaseChartInstance;
      },
      set productPurchaseChartInstance(v) {
        productPurchaseChartInstance = v;
      },
      get productSaleChart() {
        return productSaleChart;
      },
      set productSaleChart(v) {
        productSaleChart = v;
      },
      get productSaleChartInstance() {
        return productSaleChartInstance;
      },
      set productSaleChartInstance(v) {
        productSaleChartInstance = v;
      },
      get serviceSaleChart() {
        return serviceSaleChart;
      },
      set serviceSaleChart(v) {
        serviceSaleChart = v;
      },
      get serviceSaleChartInstance() {
        return serviceSaleChartInstance;
      },
      set serviceSaleChartInstance(v) {
        serviceSaleChartInstance = v;
      },
      productsP,
      productsS,
      services,
      get productPurchase() {
        return productPurchase;
      },
      set productPurchase(v) {
        productPurchase = v;
      },
      get productSale() {
        return productSale;
      },
      set productSale(v) {
        productSale = v;
      },
      get serviceSale() {
        return serviceSale;
      },
      set serviceSale(v) {
        serviceSale = v;
      },
      ChartGraph,
      updateSubView,
      permissions,
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"];
      },
      get isLoadingChart() {
        return _composable_useReports__WEBPACK_IMPORTED_MODULE_4__.isLoadingChart;
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsCreances.vue?vue&type=script&setup=true&lang=ts":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsCreances.vue?vue&type=script&setup=true&lang=ts ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");
/* harmony import */ var _composable_useReports__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/composable/useReports */ "./src/composable/useReports.ts");






/**
 * Composables
 */

/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'reportsCreances',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_5__.u)({
      title: "Rapports / Créances"
    });
    /**
     * Variables
     */
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("list");
    const activeFilter = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const activeView = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    let creancesChart = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null),
      creances = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("6"),
      creancesChartInstance = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    // watchers
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => creances.value, () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("creances", creances.value.toString(), creancesChart.value, creancesChartInstance);
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_1__.userCurrentBranch.value.id, newValue => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        ChartGraph();
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("creances", creances.value.toString(), creancesChart.value, creancesChartInstance);
      }
    });
    const ChartGraph = () => {
      (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.setCharts)("creances", creances.value, creancesChart.value, "Graph Creances", creancesChartInstance);
    };
    /**
     * Functions
     */
    const updateSubView = (view, action) => {
      activeView.value = view;
      actionType.value = action;
    };
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_2__["default"])("graphes").map(element => {
      return element.action;
    });
    /**
     * Providers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissions", permissions);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("updateSubView", updateSubView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeFilter", activeFilter);
    /**
     * Page Lifecycle
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onMounted)(() => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        ChartGraph();
      }
    });
    const __returned__ = {
      actionType,
      activeFilter,
      activeView,
      get creancesChart() {
        return creancesChart;
      },
      set creancesChart(v) {
        creancesChart = v;
      },
      get creances() {
        return creances;
      },
      set creances(v) {
        creances = v;
      },
      get creancesChartInstance() {
        return creancesChartInstance;
      },
      set creancesChartInstance(v) {
        creancesChartInstance = v;
      },
      ChartGraph,
      updateSubView,
      permissions,
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"];
      },
      get month_options() {
        return _composable_useReports__WEBPACK_IMPORTED_MODULE_4__.month_options;
      },
      get isLoadingChart() {
        return _composable_useReports__WEBPACK_IMPORTED_MODULE_4__.isLoadingChart;
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsDebts.vue?vue&type=script&setup=true&lang=ts":
/*!************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsDebts.vue?vue&type=script&setup=true&lang=ts ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");
/* harmony import */ var _composable_useReports__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/composable/useReports */ "./src/composable/useReports.ts");






/**
 * Composables
 */

// watchers
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'reportsDebts',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_5__.u)({
      title: "Rapports / Dettes"
    });
    /**
     * Variables
     */
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("list");
    const activeFilter = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const activeView = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    let debtsChart = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    let debts = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("6");
    let debtsChartInstance = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => debts.value, () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("dettes", debts.value.toString(), debtsChart.value, debtsChartInstance);
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_1__.userCurrentBranch.value.id, newValue => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        ChartGraph();
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("dettes", debts.value.toString(), debtsChart.value, debtsChartInstance);
      }
    });
    const ChartGraph = () => {
      (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.setCharts)("dettes", debts.value.toString(), debtsChart.value, "Graph Dettes", debtsChartInstance);
    };
    /**
     * Functions
     */
    const updateSubView = (view, action) => {
      activeView.value = view;
      actionType.value = action;
    };
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_2__["default"])("graphes").map(element => {
      return element.action;
    });
    /**
     * Providers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissions", permissions);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("updateSubView", updateSubView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeFilter", activeFilter);
    /**
     * Page Lifecycle
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onMounted)(() => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        ChartGraph();
      }
    });
    const __returned__ = {
      actionType,
      activeFilter,
      activeView,
      get debtsChart() {
        return debtsChart;
      },
      set debtsChart(v) {
        debtsChart = v;
      },
      get debts() {
        return debts;
      },
      set debts(v) {
        debts = v;
      },
      get debtsChartInstance() {
        return debtsChartInstance;
      },
      set debtsChartInstance(v) {
        debtsChartInstance = v;
      },
      ChartGraph,
      updateSubView,
      permissions,
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"];
      },
      get month_options() {
        return _composable_useReports__WEBPACK_IMPORTED_MODULE_4__.month_options;
      },
      get isLoadingChart() {
        return _composable_useReports__WEBPACK_IMPORTED_MODULE_4__.isLoadingChart;
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsExpenses.vue?vue&type=script&setup=true&lang=ts":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsExpenses.vue?vue&type=script&setup=true&lang=ts ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");
/* harmony import */ var _composable_useReports__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/composable/useReports */ "./src/composable/useReports.ts");






/**
 * Composables
 */

// watchers
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'reportsExpenses',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_5__.u)({
      title: "Rapports / Dépenses"
    });
    /**
     * Variables
     */
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("list");
    const activeFilter = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const activeView = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    const expensesChart = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    const expenses = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("6");
    const expensesChartInstance = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => expenses.value, () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("depenses", expenses.value.toString(), expensesChart.value, expensesChartInstance);
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_1__.userCurrentBranch.value.id, newValue => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        ChartGraph();
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("depenses", expenses.value.toString(), expensesChart.value, expensesChartInstance);
      }
    });
    const ChartGraph = () => {
      (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.setCharts)("depenses", expenses.value, expensesChart.value, "Graph Expenses | Dépenses", expensesChartInstance);
    };
    /**
     * Functions
     */
    const updateSubView = (view, action) => {
      activeView.value = view;
      actionType.value = action;
    };
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_2__["default"])("graphes").map(element => {
      return element.action;
    });
    /**
     * Providers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissions", permissions);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("updateSubView", updateSubView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeFilter", activeFilter);
    /**
     * Page Lifecycle
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onMounted)(() => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        ChartGraph();
      }
    });
    const __returned__ = {
      actionType,
      activeFilter,
      activeView,
      expensesChart,
      expenses,
      expensesChartInstance,
      ChartGraph,
      updateSubView,
      permissions,
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"];
      },
      get month_options() {
        return _composable_useReports__WEBPACK_IMPORTED_MODULE_4__.month_options;
      },
      get isLoadingChart() {
        return _composable_useReports__WEBPACK_IMPORTED_MODULE_4__.isLoadingChart;
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsImcomeStatement.vue?vue&type=script&setup=true&lang=ts":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsImcomeStatement.vue?vue&type=script&setup=true&lang=ts ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_push_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.push.js */ "./node_modules/core-js/modules/es.array.push.js");
/* harmony import */ var core_js_modules_es_array_push_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_push_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _composable_useFormatMoney__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/composable/useFormatMoney */ "./src/composable/useFormatMoney.ts");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");
/* harmony import */ var _composable_useExport__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/composable/useExport */ "./src/composable/useExport.ts");
/* harmony import */ var _composable_useNotyf__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @/composable/useNotyf */ "./src/composable/useNotyf.ts");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @/api */ "./src/api/index.ts");









// Compte de resultats
/**
 * Composables
 */

/**
 * API Calls
 */

/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_1__.defineComponent)({
  __name: 'reportsImcomeStatement',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_9__.u)({
      title: "Rapports / Comptes de résultat"
    });
    /**
     * variables
     */
    const currentYear = new Date(Date.now()).getFullYear();
    const lastAccountingYear = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([currentYear]);
    const notify = (0,_composable_useNotyf__WEBPACK_IMPORTED_MODULE_7__["default"])();
    const isLoading = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(false);
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)("list");
    const activeFilter = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)("");
    const activeView = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(1);
    const inputYear = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(currentYear);
    const year = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(currentYear);
    const selectedYear = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(0);
    const viewMore = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(false);
    const currentIcon = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)("/icons/arrow-right.svg");
    const currentIndex = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)();
    const allData = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([]);
    // Datas
    const totalRecipes = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)({});
    const recipes = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([]);
    const TotalExpenses = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([{}]);
    const expenses = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([]);
    const depreciations = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)({});
    const pay = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)({});
    const exerciseResult = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)({});
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.watch)(inputYear, newValue => {
      // console.log("typeof(newValue): ", typeof newValue);
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_3__.userCurrentBranch.value.id, async newValue => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_5__["default"])('read', permissions)) {
        await getIncomeStatement();
      }
    });
    /**
     * Functions
     */
    const updateSubView = (view, action) => {
      activeView.value = view;
      actionType.value = action;
    };
    async function getIncomeStatement(index) {
      if (inputYear.value.toString().length != 4 || isNaN(Number(inputYear.value))) {
        notify.warning("Veuillez entrer une année à 4 chiffres !");
      } else {
        isLoading.value = true;
        // console.log("index: ", index);
        if (!isNaN(index) && index != null) {
          inputYear.value = lastAccountingYear.value[index].toString();
          selectedYear.value = index;
        }
        await (0,_api__WEBPACK_IMPORTED_MODULE_8__["default"])().reports.incomeStatement(inputYear.value).then(response => {
          allData.value = response.data;
          const data = Object.assign([], response.data);
          // getting in mind that when splice table, it change its lenght !!!!
          totalRecipes.value = data.splice(2, 1);
          TotalExpenses.value = data.splice(9, 1);
          pay.value = data.splice(9, 1);
          depreciations.value = data.splice(9, 1);
          exerciseResult.value = data.splice(9, 1);
          recipes.value = data.splice(0, 2);
          expenses.value = data.splice(0, 7);
          year.value = inputYear.value;
          isLoading.value = false;
          console.log('allData.value: ', allData.value);
        }).catch(err => {
          console.log('err: ', err);
          isLoading.value = false;
          notify.error(`Erreur lors du chargement des informations sur le compte de résultat. Veuillez recharger la page`);
        });
      }
      viewMore.value = false;
    }
    const changeCurrentIcon = (state, index) => {
      currentIndex.value = index;
      currentIcon.value = state ? "/icons/arrow-right-icon-w.svg" : "/icons/arrow-right.svg";
    };
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_4__["default"])("getCompteResultat").map(element => {
      return element.action;
    });
    /**
     * Providers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("permissions", permissions);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("updateSubView", updateSubView);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("activeFilter", activeFilter);
    /**
     * page life cycle
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.onBeforeMount)(async () => {
      for (let index = 1; index < 7; index++) {
        lastAccountingYear.value.push(currentYear - index);
      }
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_5__["default"])('read', permissions)) {
        await getIncomeStatement();
      }
    });
    const __returned__ = {
      currentYear,
      lastAccountingYear,
      notify,
      isLoading,
      actionType,
      activeFilter,
      activeView,
      inputYear,
      year,
      selectedYear,
      viewMore,
      currentIcon,
      currentIndex,
      allData,
      totalRecipes,
      recipes,
      TotalExpenses,
      expenses,
      depreciations,
      pay,
      exerciseResult,
      updateSubView,
      getIncomeStatement,
      changeCurrentIcon,
      permissions,
      get orignalMoney() {
        return _composable_useFormatMoney__WEBPACK_IMPORTED_MODULE_2__["default"];
      },
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_5__["default"];
      },
      get exportIncomingStatementData() {
        return _composable_useExport__WEBPACK_IMPORTED_MODULE_6__.exportIncomingStatementData;
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsLoans.vue?vue&type=script&setup=true&lang=ts":
/*!************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsLoans.vue?vue&type=script&setup=true&lang=ts ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");
/* harmony import */ var _composable_useReports__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/composable/useReports */ "./src/composable/useReports.ts");






/**
 * Composables
 */

// watchers
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'reportsLoans',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_5__.u)({
      title: "Rapports / Emprunts"
    });
    /**
     * Variables
     */
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("list");
    const activeFilter = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const activeView = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    let loansChart = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null),
      loans = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("6"),
      loansChartInstance = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => loans.value, () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("emprunts", loans.value.toString(), loansChart.value, loansChartInstance);
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_1__.userCurrentBranch.value.id, newValue => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        ChartGraph();
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("emprunts", loans.value.toString(), loansChart.value, loansChartInstance);
      }
    });
    const ChartGraph = () => {
      (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.setCharts)("emprunts", loans.value.toString(), loansChart.value, "Graph Loans", loansChartInstance);
    };
    /**
     * Functions
     */
    const updateSubView = (view, action) => {
      activeView.value = view;
      actionType.value = action;
    };
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_2__["default"])("graphes").map(element => {
      return element.action;
    });
    /**
     * Providers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissions", permissions);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("updateSubView", updateSubView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeFilter", activeFilter);
    /**
     * Page Lifecycle
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onMounted)(() => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        ChartGraph();
      }
    });
    const __returned__ = {
      actionType,
      activeFilter,
      activeView,
      get loansChart() {
        return loansChart;
      },
      set loansChart(v) {
        loansChart = v;
      },
      get loans() {
        return loans;
      },
      set loans(v) {
        loans = v;
      },
      get loansChartInstance() {
        return loansChartInstance;
      },
      set loansChartInstance(v) {
        loansChartInstance = v;
      },
      ChartGraph,
      updateSubView,
      permissions,
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"];
      },
      get month_options() {
        return _composable_useReports__WEBPACK_IMPORTED_MODULE_4__.month_options;
      },
      get isLoadingChart() {
        return _composable_useReports__WEBPACK_IMPORTED_MODULE_4__.isLoadingChart;
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsOverview.vue?vue&type=script&setup=true&lang=ts":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsOverview.vue?vue&type=script&setup=true&lang=ts ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/api */ "./src/api/index.ts");
/* harmony import */ var _composable_useNotyf__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/composable/useNotyf */ "./src/composable/useNotyf.ts");
/* harmony import */ var _composable_useFormatMoney__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/composable/useFormatMoney */ "./src/composable/useFormatMoney.ts");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");









/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'reportsOverview',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_7__.u)({
      title: "Comptabilité / Vue d'ensemble"
    });
    /**
     * composables
     */
    const notify = (0,_composable_useNotyf__WEBPACK_IMPORTED_MODULE_2__["default"])();
    /**
     * Injects
     */
    const goBack = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("goBack");
    const goNext = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("goNext");
    const reloadPage = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("reloadPage");
    /**
     * variable
     */
    const overviewInfos = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([]);
    const isLoading = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("list");
    const activeFilter = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const activeView = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userCurrentBranch.value.id, newValue => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_6__["default"])('read', permissions)) {
        getReportsOverview();
      }
    });
    /**
     * Functions
     */
    const updateSubView = (view, action) => {
      activeView.value = view;
      actionType.value = action;
    };
    async function getReportsOverview() {
      isLoading.value = true;
      await (0,_api__WEBPACK_IMPORTED_MODULE_1__["default"])().overview.reports().then(response => {
        overviewInfos.value = response.data;
        // console.log("response.data: ", response.data);
        isLoading.value = false;
      }).catch(err => {
        isLoading.value = false;
        notify.error("Erreur lors du chargement des informations de la vue générale! Veuillez recharger la page.");
      });
    }
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_5__["default"])("etatRapports").map(element => {
      return element.action;
    });
    /**
     * Providers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissions", permissions);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("updateSubView", updateSubView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeFilter", activeFilter);
    /**
     * Page life cycle
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onBeforeMount)(async () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_6__["default"])('read', permissions)) {
        getReportsOverview();
      }
    });
    const __returned__ = {
      notify,
      goBack,
      goNext,
      reloadPage,
      overviewInfos,
      isLoading,
      actionType,
      activeFilter,
      activeView,
      updateSubView,
      getReportsOverview,
      permissions,
      get orignalMoney() {
        return _composable_useFormatMoney__WEBPACK_IMPORTED_MODULE_3__["default"];
      },
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_6__["default"];
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsStocks.vue?vue&type=script&setup=true&lang=ts":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsStocks.vue?vue&type=script&setup=true&lang=ts ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");
/* harmony import */ var _composable_useReports__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/composable/useReports */ "./src/composable/useReports.ts");






/**
 * Composables
 */

/**
 * Variables
 */
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'reportsStocks',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_5__.u)({
      title: "Rapports / Stocks"
    });
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("list");
    const activeFilter = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const activeView = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    let categoriesChart = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    let categoriesChartInstance = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    let categories = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    let productsChart = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    let productsChartInstance = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    let products = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    let servicesChart = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    let servicesChartInstance = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    let services = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    // watchers
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => categories.value, () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("categories", categories.value.toString(), categoriesChart.value, categoriesChartInstance);
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => products.value, () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("produits", products.value.toString(), productsChart.value, productsChartInstance);
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => services.value, () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("services", services.value.toString(), servicesChart.value, servicesChartInstance);
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_1__.userCurrentBranch.value.id, newValue => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        ChartGraph();
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("categories", categories.value.toString(), categoriesChart.value, categoriesChartInstance);
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("produits", products.value.toString(), productsChart.value, productsChartInstance);
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("services", services.value.toString(), servicesChart.value, servicesChartInstance);
      }
    });
    /**
     * Functions
     */
    const updateSubView = (view, action) => {
      activeView.value = view;
      actionType.value = action;
    };
    const ChartGraph = () => {
      (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.setCharts)("categories", categories.value, categoriesChart.value, "Graph categories", categoriesChartInstance);
      (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.setCharts)("produits", products.value, productsChart.value, "produits", productsChartInstance);
      (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.setCharts)("services", services.value, servicesChart.value, "services", servicesChartInstance);
    };
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_2__["default"])("graphes").map(element => {
      return element.action;
    });
    /**
     * Providers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissions", permissions);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("updateSubView", updateSubView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeFilter", activeFilter);
    /**
     * Page Lifecycle
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onMounted)(() => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        ChartGraph();
      }
    });
    const __returned__ = {
      actionType,
      activeFilter,
      activeView,
      get categoriesChart() {
        return categoriesChart;
      },
      set categoriesChart(v) {
        categoriesChart = v;
      },
      get categoriesChartInstance() {
        return categoriesChartInstance;
      },
      set categoriesChartInstance(v) {
        categoriesChartInstance = v;
      },
      get categories() {
        return categories;
      },
      set categories(v) {
        categories = v;
      },
      get productsChart() {
        return productsChart;
      },
      set productsChart(v) {
        productsChart = v;
      },
      get productsChartInstance() {
        return productsChartInstance;
      },
      set productsChartInstance(v) {
        productsChartInstance = v;
      },
      get products() {
        return products;
      },
      set products(v) {
        products = v;
      },
      get servicesChart() {
        return servicesChart;
      },
      set servicesChart(v) {
        servicesChart = v;
      },
      get servicesChartInstance() {
        return servicesChartInstance;
      },
      set servicesChartInstance(v) {
        servicesChartInstance = v;
      },
      get services() {
        return services;
      },
      set services(v) {
        services = v;
      },
      updateSubView,
      ChartGraph,
      permissions,
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"];
      },
      get month_options() {
        return _composable_useReports__WEBPACK_IMPORTED_MODULE_4__.month_options;
      },
      get isLoadingChart() {
        return _composable_useReports__WEBPACK_IMPORTED_MODULE_4__.isLoadingChart;
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsSubventions.vue?vue&type=script&setup=true&lang=ts":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsSubventions.vue?vue&type=script&setup=true&lang=ts ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");
/* harmony import */ var _composable_useReports__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/composable/useReports */ "./src/composable/useReports.ts");






/**
 * Composables
 */

// watchers
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'reportsSubventions',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_5__.u)({
      title: "Rapports / Subventions d'exploitation"
    });
    /**
     * Variables
     */
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("list");
    const activeFilter = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const activeView = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    let subventionsChart = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null),
      subventions = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(6),
      subventionsChartInstance = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => subventions.value, () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("subventions", subventions.value.toString(), subventionsChart.value, subventionsChartInstance);
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_1__.userCurrentBranch.value.id, newValue => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        ChartGraph();
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("subventions", subventions.value.toString(), subventionsChart.value, subventionsChartInstance);
      }
    });
    const ChartGraph = () => {
      (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.setCharts)("subventions", subventions.value.toString(), subventionsChart.value, "Graph Subvention", subventionsChartInstance);
    };
    /**
     * Functions
     */
    const updateSubView = (view, action) => {
      activeView.value = view;
      actionType.value = action;
    };
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_2__["default"])("graphes").map(element => {
      return element.action;
    });
    /**
     * Providers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissions", permissions);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("updateSubView", updateSubView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeFilter", activeFilter);
    /**
     * Page Lifecycle
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onMounted)(() => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        ChartGraph();
      }
    });
    const __returned__ = {
      actionType,
      activeFilter,
      activeView,
      get subventionsChart() {
        return subventionsChart;
      },
      set subventionsChart(v) {
        subventionsChart = v;
      },
      get subventions() {
        return subventions;
      },
      set subventions(v) {
        subventions = v;
      },
      get subventionsChartInstance() {
        return subventionsChartInstance;
      },
      set subventionsChartInstance(v) {
        subventionsChartInstance = v;
      },
      ChartGraph,
      updateSubView,
      permissions,
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"];
      },
      get month_options() {
        return _composable_useReports__WEBPACK_IMPORTED_MODULE_4__.month_options;
      },
      get isLoadingChart() {
        return _composable_useReports__WEBPACK_IMPORTED_MODULE_4__.isLoadingChart;
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsThirdParties.vue?vue&type=script&setup=true&lang=ts":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsThirdParties.vue?vue&type=script&setup=true&lang=ts ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");
/* harmony import */ var _composable_useReports__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/composable/useReports */ "./src/composable/useReports.ts");






/**
 * Composables
 */

/**
 * Variables Model
 */
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'reportsThirdParties',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("list");
    const activeFilter = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const activeView = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    let clientsChart = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    let clientsChartInstance = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    let providerChart = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    let providerChartInstance = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    let creditorsChart = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    let creditorsChartInstance = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    let financialBackerChart = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    let financialBackerChartInstance = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    let clients = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1),
      provider = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(2),
      creditor = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(2),
      financialBacker = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(2);
    // Watchers
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => clients.value, () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("clients", clients.value.toString(), clientsChart.value, clientsChartInstance);
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => provider.value, () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("fournisseurs", provider.value.toString(), providerChart.value, providerChartInstance);
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => creditor.value, () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("creanciers", creditor.value.toString(), creditorsChart.value, creditorsChartInstance);
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => financialBacker.value, () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.refreshChart)("bailleur_fond", financialBacker.value.toString(), financialBackerChart.value, financialBackerChartInstance);
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_1__.userCurrentBranch.value.id, newValue => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        ChartGraph();
      }
    });
    /**
     * Functions
     */
    const updateSubView = (view, action) => {
      activeView.value = view;
      actionType.value = action;
    };
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_5__.u)({
      title: "Rapports / Les tiers"
    });
    const ChartGraph = () => {
      (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.setCharts)("clients", clients.value.toString(), clientsChart.value, "Report Graph Clients", clientsChartInstance);
      (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.setCharts)("fournisseurs", provider.value.toString(), providerChart.value, "Report Graph Providers", providerChartInstance);
      (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.setCharts)("creanciers", creditor.value.toString(), creditorsChart.value, "Report Graph Creditors", creditorsChartInstance);
      (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_4__.setCharts)("bailleur_fond", financialBacker.value.toString(), financialBackerChart.value, "Report Graph Creditors", financialBackerChartInstance);
    };
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_2__["default"])("graphes").map(element => {
      return element.action;
    });
    /**
     * Providers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissions", permissions);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("updateSubView", updateSubView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeFilter", activeFilter);
    /**
     * Page Lifecycle
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onMounted)(() => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"])('read', permissions)) {
        ChartGraph();
      }
    });
    const __returned__ = {
      actionType,
      activeFilter,
      activeView,
      get clientsChart() {
        return clientsChart;
      },
      set clientsChart(v) {
        clientsChart = v;
      },
      get clientsChartInstance() {
        return clientsChartInstance;
      },
      set clientsChartInstance(v) {
        clientsChartInstance = v;
      },
      get providerChart() {
        return providerChart;
      },
      set providerChart(v) {
        providerChart = v;
      },
      get providerChartInstance() {
        return providerChartInstance;
      },
      set providerChartInstance(v) {
        providerChartInstance = v;
      },
      get creditorsChart() {
        return creditorsChart;
      },
      set creditorsChart(v) {
        creditorsChart = v;
      },
      get creditorsChartInstance() {
        return creditorsChartInstance;
      },
      set creditorsChartInstance(v) {
        creditorsChartInstance = v;
      },
      get financialBackerChart() {
        return financialBackerChart;
      },
      set financialBackerChart(v) {
        financialBackerChart = v;
      },
      get financialBackerChartInstance() {
        return financialBackerChartInstance;
      },
      set financialBackerChartInstance(v) {
        financialBackerChartInstance = v;
      },
      get clients() {
        return clients;
      },
      set clients(v) {
        clients = v;
      },
      get provider() {
        return provider;
      },
      set provider(v) {
        provider = v;
      },
      get creditor() {
        return creditor;
      },
      set creditor(v) {
        creditor = v;
      },
      get financialBacker() {
        return financialBacker;
      },
      set financialBacker(v) {
        financialBacker = v;
      },
      updateSubView,
      ChartGraph,
      permissions,
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"];
      },
      get isLoadingChart() {
        return _composable_useReports__WEBPACK_IMPORTED_MODULE_4__.isLoadingChart;
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsAccountingBalance.vue?vue&type=template&id=2e5d8b96&ts=true":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsAccountingBalance.vue?vue&type=template&id=2e5d8b96&ts=true ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "workspace-content h-full overflow-y-auto pb-5 md:pb-0"
};
const _hoisted_2 = {
  class: "w-full overflow-y-auto flex flex-col space-y-8 mt-4 transition-all duration-500 ease-in-out",
  style: {
    "height": "calc(100% - 5rem)"
  }
};
const _hoisted_3 = {
  class: "w-full flex flex-col"
};
const _hoisted_4 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full flex flex-row text-md font-norlmal mt-3 text-xl"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "w-6/12 bg-green-800 rounded-tl-xl py-4 pl-4 text-white"
}, " Rubriques "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "bg-green-700 border-r border-gray-300 w-3/12 py-4 text-center text-white"
}, "Débit"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "bg-green-600 w-3/12 py-4 text-center rounded-tr-xl font-normal text-white"
}, "Crédit")], -1 /* HOISTED */);
const _hoisted_5 = {
  class: "w-6/12 py-3 bg-opacity-60"
};
const _hoisted_6 = {
  class: "list-disc"
};
const _hoisted_7 = {
  class: "ml-9"
};
const _hoisted_8 = {
  class: "w-3/12 justify-center flex border-r border-gray-300 flex-row py-3 bg-opacity-70"
};
const _hoisted_9 = {
  class: "w-3/12 justify-center flex flex-row border-t py-3 bg-opacity-70"
};
const _hoisted_10 = {
  class: "w-full flex flex-col"
};
const _hoisted_11 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full flex flex-row text-md font-norlmal mt-3 text-xl"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "w-6/12 bg-green-800 rounded-tl-xl py-4 pl-4 text-white"
}, " Rubriques "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "bg-green-700 border-r border-gray-300 w-3/12 py-4 text-center text-white"
}, "Débit"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "bg-green-600 w-3/12 py-4 text-center rounded-tr-xl font-normal text-white"
}, "Crédit")], -1 /* HOISTED */);
const _hoisted_12 = {
  class: "w-6/12 py-3 bg-opacity-60"
};
const _hoisted_13 = {
  class: "list-disc"
};
const _hoisted_14 = {
  class: "ml-9"
};
const _hoisted_15 = {
  class: "w-3/12 justify-center flex border-r border-gray-300 flex-row py-3 bg-opacity-70"
};
const _hoisted_16 = {
  class: "w-3/12 justify-center flex flex-row border-t py-3 bg-opacity-70"
};
const _hoisted_17 = {
  class: "w-full flex flex-col"
};
const _hoisted_18 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full flex flex-row text-md font-norlmal mt-3 text-xl"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "w-6/12 bg-green-800 rounded-tl-xl py-4 pl-4 text-white"
}, " Rubriques "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "bg-green-700 border-r border-gray-300 w-3/12 py-4 text-center text-white"
}, "Débit"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "bg-green-600 w-3/12 py-4 text-center rounded-tr-xl font-normal text-white"
}, "Crédit")], -1 /* HOISTED */);
const _hoisted_19 = {
  class: "w-6/12 py-3 bg-opacity-60"
};
const _hoisted_20 = {
  class: "list-disc"
};
const _hoisted_21 = {
  class: "ml-9"
};
const _hoisted_22 = {
  class: "w-3/12 justify-center flex border-r border-gray-300 flex-row py-3 bg-opacity-70"
};
const _hoisted_23 = {
  class: "w-3/12 justify-center flex flex-row border-t py-3 bg-opacity-70"
};
const _hoisted_24 = {
  class: "w-6/12 py-3 bg-opacity-60"
};
const _hoisted_25 = {
  class: "list-disc"
};
const _hoisted_26 = {
  class: "ml-9"
};
const _hoisted_27 = {
  class: "w-3/12 justify-center text-green-700 flex border-r border-gray-300 flex-row py-3 bg-opacity-70"
};
const _hoisted_28 = {
  class: "w-3/12 justify-center text-green-700 flex flex-row border-t py-3 bg-opacity-70"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_WorkspaceHeader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WorkspaceHeader");
  const _component_SlimTitleBar = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SlimTitleBar");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_WorkspaceHeader, {
    options: {
      titleBar: true,
      actionsBar: false,
      actions: {
        import: false,
        export: true,
        addItem: false,
        filter: false
      }
    }
  }), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" section Solde Ouverture"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SlimTitleBar, {
    title: "Solde d'ouverture"
  }), _hoisted_4, ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.balanceInfos, (info, index) => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
      key: index,
      class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
        'w-full flex flex-row border-b border-gray-300 border-opacity-50': true,
        'bg-gray-200': index % 2 === 0
      })
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_5, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("ul", _hoisted_6, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("li", _hoisted_7, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(info.title), 1 /* TEXT */)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_8, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(isNaN(info.value1) ? info.value1 : `${_ctx.$filters.moneyFormat(info.value1.toString())} ${info.currency}`), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_9, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(isNaN(info.value2) ? info.value2 : `${_ctx.$filters.moneyFormat(info.value2.toString())} ${info.currency}`), 1 /* TEXT */)], 2 /* CLASS */);
  }), 128 /* KEYED_FRAGMENT */))]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" section Mouvement"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_10, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SlimTitleBar, {
    title: "Mouvements"
  }), _hoisted_11, ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.balanceInfos, (info, index) => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
      key: index,
      class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
        'w-full flex flex-row border-b border-gray-300 border-opacity-50': true,
        'bg-gray-200': index % 2 === 0
      })
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_12, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("ul", _hoisted_13, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("li", _hoisted_14, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(info.title), 1 /* TEXT */)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_15, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(isNaN(info.value3) ? info.value3 : `${_ctx.$filters.moneyFormat(info.value3.toString())} ${info.currency}`), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_16, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(isNaN(info.value4) ? info.value4 : `${_ctx.$filters.moneyFormat(info.value4.toString())} ${info.currency}`), 1 /* TEXT */)], 2 /* CLASS */);
  }), 128 /* KEYED_FRAGMENT */))]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Section Solde Cloture "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_17, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SlimTitleBar, {
    title: "Solde de clôture"
  }), _hoisted_18, ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.balanceInfos, (info, index) => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
      key: index,
      class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
        'w-full flex flex-row border-b border-gray-300 border-opacity-50': true,
        'bg-gray-200': index % 2 === 0
      })
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_19, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("ul", _hoisted_20, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("li", _hoisted_21, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(info.title), 1 /* TEXT */)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_22, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(isNaN(info.value5) ? info.value5 : `${_ctx.$filters.moneyFormat(info.value5.toString())} ${info.currency}`), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_23, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(isNaN(info.value6) ? info.value6 : `${_ctx.$filters.moneyFormat(info.value6.toString())} ${info.currency}`), 1 /* TEXT */)], 2 /* CLASS */);
  }), 128 /* KEYED_FRAGMENT */)), ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.totals, (info, index) => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
      key: index,
      class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
        'w-full flex flex-row bg-gray-400 bg-opacity-50 border-t font-medium border-b text-green-700 border-gray-300 border-opacity-50': true
      })
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_24, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("ul", _hoisted_25, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("li", _hoisted_26, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(info.title), 1 /* TEXT */)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_27, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(isNaN(info.value1) ? info.value1 : `${_ctx.$filters.moneyFormat(info.value1.toString())} ${info.currency}`), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_28, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(isNaN(info.value2) ? info.value2 : `${_ctx.$filters.moneyFormat(info.value2.toString())} ${info.currency}`), 1 /* TEXT */)]);
  }), 128 /* KEYED_FRAGMENT */))])])]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsAccountingJournal.vue?vue&type=template&id=4adc2671&ts=true":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsAccountingJournal.vue?vue&type=template&id=4adc2671&ts=true ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "workspace-content h-full overflow-y-auto pb-5 md:pb-0"
};
const _hoisted_2 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createStaticVNode)("<div class=\"w-full flex flex-col p-5 mt-4 transition-all duration-500 ease-in-out\"><!-- header --><div class=\"w-full flex flex-row\"><span class=\"text-xl text-center font-medium w-2/12 bg-primary-200 rounded-tl-xl py-4 pl-4\">AC NO</span><span class=\"text-xl text-center font-medium w-6/12 bg-primary-100 py-4 pl-4\">Label of the operation</span><span class=\"text-xl text-center font-medium w-2/12 bg-primary-200 py-4 pl-4\">Debit</span><span class=\"text-xl text-center font-medium w-2/12 bg-primary-100 rounded-tr-xl py-4 pl-4\">Credit</span></div><!-- body --><div class=\"flex flex-row w-full border-b border-gray-400 border-opacity-30 text-gray-400 font-normal\"><div class=\"w-2/12 flex flex-row justify-between bg-gray-300 bg-opacity-50\"><span class=\"w-1/2 p-3 h-full text-center border-r border-gray-400 border-opacity-30\"> 5120 </span><span class=\"w-1/2 p-3 text-center self-end\"> 5120 </span></div><div class=\"w-6/12 p-3 bg-gray-200 bg-opacity-30\"><span class=\"text-green-500\">12/08/2021 </span> - achat de marchnadises chez F <ul class=\"ml-6 list-disc block\"><li>Achat de Marchandises</li><li>Fournisseur F</li></ul></div><div class=\"w-2/12 p-3 bg-gray-300 bg-opacity-50 text-center border-r border-gray-400 border-opacity-30\"> 2, 058, 450 XAF </div><div class=\"w-2/12 p-3 bg-gray-300 bg-opacity-50 text-center flex\"><span class=\"w-full flex-auto self-end\">2, 058, 450 XAF</span></div></div><div class=\"flex flex-row w-full border-b border-gray-400 border-opacity-30 text-gray-400 font-normal\"><div class=\"w-2/12 flex flex-row justify-between bg-gray-300 bg-opacity-50\"><span class=\"w-1/2 p-3 h-full text-center border-r border-gray-400 border-opacity-30\"> 5120 </span><span class=\"w-1/2 p-3 text-center self-end\"> 5120 </span></div><div class=\"w-6/12 p-3 bg-gray-200 bg-opacity-30\"><span class=\"text-green-500\">12/08/2021 </span> - achat de marchnadises chez F <ul class=\"ml-6 list-disc block\"><li>Achat de Marchandises</li><li>Fournisseur F</li></ul></div><div class=\"w-2/12 p-3 bg-gray-300 bg-opacity-50 text-center border-r border-gray-400 border-opacity-30\"> 2, 058, 450 XAF </div><div class=\"w-2/12 p-3 bg-gray-300 bg-opacity-50 text-center flex\"><span class=\"w-full flex-auto self-end\">2, 058, 450 XAF</span></div></div><div class=\"flex flex-row w-full border-b border-gray-400 border-opacity-30 text-gray-400 font-normal\"><div class=\"w-2/12 flex flex-row justify-between bg-gray-300 bg-opacity-50\"><span class=\"w-1/2 p-3 h-full text-center border-r border-gray-400 border-opacity-30\"> 5120 </span><span class=\"w-1/2 p-3 text-center self-end\"> 5120 </span></div><div class=\"w-6/12 p-3 bg-gray-200 bg-opacity-30\"><span class=\"text-green-500\">12/08/2021 </span> - achat de marchnadises chez F <ul class=\"ml-6 list-disc block\"><li>Achat de Marchandises</li><li>Fournisseur F</li></ul></div><div class=\"w-2/12 p-3 bg-gray-300 bg-opacity-50 text-center border-r border-gray-400 border-opacity-30\"> 2, 058, 450 XAF </div><div class=\"w-2/12 p-3 bg-gray-300 bg-opacity-50 text-center flex\"><span class=\"w-full flex-auto self-end\">2, 058, 450 XAF</span></div></div><div class=\"flex flex-row w-full border-b border-gray-400 border-opacity-30 text-gray-400 font-normal\"><div class=\"w-2/12 flex flex-row justify-between bg-gray-300 bg-opacity-50\"><span class=\"w-1/2 p-3 h-full text-center border-r border-gray-400 border-opacity-30\"> 5120 </span><span class=\"w-1/2 p-3 text-center self-end\"> 5120 </span></div><div class=\"w-6/12 p-3 bg-gray-200 bg-opacity-30\"><span class=\"text-green-500\">12/08/2021 </span> - achat de marchnadises chez F <ul class=\"ml-6 list-disc block\"><li>Achat de Marchandises</li><li>Fournisseur F</li></ul></div><div class=\"w-2/12 p-3 bg-gray-300 bg-opacity-50 text-center border-r border-gray-400 border-opacity-30\"> 2, 058, 450 XAF </div><div class=\"w-2/12 p-3 bg-gray-300 bg-opacity-50 text-center flex\"><span class=\"w-full flex-auto self-end\">2, 058, 450 XAF</span></div></div></div>", 1);
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_WorkspaceHeader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WorkspaceHeader");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_WorkspaceHeader, {
    options: {
      titleBar: true,
      actionsBar: false,
      actions: {
        import: false,
        export: true,
        addItem: false,
        filter: false
      }
    }
  }), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" main content "), _hoisted_2]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsArchiving.vue?vue&type=template&id=449eade4&scoped=true&ts=true":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsArchiving.vue?vue&type=template&id=449eade4&scoped=true&ts=true ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _withScopeId = n => ((0,vue__WEBPACK_IMPORTED_MODULE_0__.pushScopeId)("data-v-449eade4"), n = n(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.popScopeId)(), n);
const _hoisted_1 = {
  class: "h-full overflow-y-auto workspace-content md:pb-0"
};
const _hoisted_2 = {
  class: "flex w-full transition-all duration-500 ease-in-out"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_WorkspaceHeader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WorkspaceHeader");
  const _component_ArchivesDatatable = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("ArchivesDatatable");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_WorkspaceHeader, {
    "breadcums-nav-list": ['Ventes', 'Achats', 'Dépenses', 'Subventions', 'Emprunts', 'Ristournes', 'Avaries', 'Others'],
    options: {
      titleBar: true,
      actionsBar: true,
      actions: {
        import: false,
        export: false,
        addItem: false,
        filter: $setup.useAccess('read', $setup.permissions)
      }
    }
  }, null, 8 /* PROPS */, ["options"]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_ArchivesDatatable)])]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsBankAndFund.vue?vue&type=template&id=2f24fd2f&ts=true":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsBankAndFund.vue?vue&type=template&id=2f24fd2f&ts=true ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "workspace-content h-full overflow-x-hidden pb-6 md:pb-0"
};
const _hoisted_2 = {
  class: "stats-content w-full h-auto grid gap-6 sm:grid-cols-1"
};
const _hoisted_3 = {
  class: "w-full h-96"
};
const _hoisted_4 = {
  class: "graph-card w-full h-full p-8 bg-white rounded-2xl"
};
const _hoisted_5 = {
  class: "card-header flex justify-between items-center"
};
const _hoisted_6 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "text-lg flex items-center"
}, "Banque", -1 /* HOISTED */);
const _hoisted_7 = ["value"];
const _hoisted_8 = {
  key: 0,
  class: "flex flex-row"
};
const _hoisted_9 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/header/rotate-right-icon.svg",
  class: "z-0 mr-4 animate-spin"
}, null, -1 /* HOISTED */);
const _hoisted_10 = {
  key: 0,
  id: "bankChart",
  ref: "bankChart",
  width: "100%",
  height: "100%"
};
const _hoisted_11 = ["src"];
const _hoisted_12 = {
  class: "w-full h-96"
};
const _hoisted_13 = {
  class: "graph-card w-full h-full p-8 bg-white rounded-2xl"
};
const _hoisted_14 = {
  class: "card-header flex justify-between items-center"
};
const _hoisted_15 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "text-lg flex items-center"
}, "Caisse", -1 /* HOISTED */);
const _hoisted_16 = ["value"];
const _hoisted_17 = {
  key: 0,
  class: "flex flex-row"
};
const _hoisted_18 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/header/rotate-right-icon.svg",
  class: "z-0 mr-4 animate-spin"
}, null, -1 /* HOISTED */);
const _hoisted_19 = {
  key: 0,
  id: "fundChart",
  ref: "fundChart",
  width: "100%",
  height: "100%"
};
const _hoisted_20 = ["src"];
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_WorkspaceHeader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WorkspaceHeader");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_WorkspaceHeader, {
    options: {
      titleBar: true,
      actionsBar: false,
      actions: {
        import: false,
        export: true,
        addItem: false,
        filter: false
      }
    }
  }), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <div class=\"w-1/2 h-1/2 pr-3\">\n\t\t\t\t<div class=\"graph-card w-full h-full p-8 bg-white rounded-2xl\">\n\t\t\t\t\t<div class=\"card-header flex justify-between items-center\">\n\t\t\t\t\t\t<h4 class=\"text-lg flex items-center\"> Banque -> Caisse </h4>\n\t\t\t\t\t\t<select\n\t\t\t\t\t\t\tclass=\"custom-select text-xl text-right font-light pr-4 active:overflow-none focus:overflow-none\"\n\t\t\t\t\t\t>\n\t\t\t\t\t\t\t<option\n\t\t\t\t\t\t\t\tv-for=\"option of month_options\"\n\t\t\t\t\t\t\t\tv-bind:key=\"option.key\"\n\t\t\t\t\t\t\t\t:value=\"option.key\"\n\t\t\t\t\t\t\t\t>{{ option.value }}</option\n\t\t\t\t\t\t\t>\n\t\t\t\t\t\t</select>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div :class=\"`${!useAccess('read', permissions) ? 'flex justify-center items-center' : ''} card-graph w-full h-4/5 mt-5`\">\n\t\t\t\t\t\t<canvas\n\t\t\t\t\t\t\tref=\"bankTofundChart\"\n\t\t\t\t\t\t\tid=\"bankTofundChart\"\n\t\t\t\t\t\t\twidth=\"100%\"\n\t\t\t\t\t\t\theight=\"100%\"\n\t\t\t\t\t\t\tv-if=\"useAccess('read', permissions)\"\n\t\t\t\t\t\t></canvas>\n\t\t\t\t\t\t<img v-else :src=\"'/icons/no-access-icon-c.svg'\"  style=\"width: 100px;\"/>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"w-1/2 h-1/2 pl-3\">\n\t\t\t\t<div class=\"graph-card w-full h-full p-8 bg-white rounded-2xl\">\n\t\t\t\t\t<div class=\"card-header flex justify-between items-center\">\n\t\t\t\t\t\t<h4 class=\"text-lg flex items-center\"> Caisse -> Banque </h4>\n\t\t\t\t\t\t<select\n\t\t\t\t\t\t\tclass=\"custom-select text-xl text-right font-light pr-4 active:overflow-none focus:overflow-none\"\n\t\t\t\t\t\t>\n\t\t\t\t\t\t\t<option\n\t\t\t\t\t\t\t\tv-for=\"option of month_options\"\n\t\t\t\t\t\t\t\tv-bind:key=\"option.key\"\n\t\t\t\t\t\t\t\t:value=\"option.key\"\n\t\t\t\t\t\t\t\t>{{ option.value }}</option\n\t\t\t\t\t\t\t>\n\t\t\t\t\t\t</select>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div :class=\"`${!useAccess('read', permissions) ? 'flex justify-center items-center' : ''} card-graph w-full h-4/5 mt-5`\">\n\t\t\t\t\t\t<canvas\n\t\t\t\t\t\t\tref=\"fundToBankChart\"\n\t\t\t\t\t\t\tid=\"fundToBankChart\"\n\t\t\t\t\t\t\twidth=\"100%\"\n\t\t\t\t\t\t\theight=\"100%\"\n\t\t\t\t\t\t\tv-if=\"useAccess('read', permissions)\"\n\t\t\t\t\t\t></canvas>\n\t\t\t\t\t\t<img v-else :src=\"'/icons/no-access-icon-c.svg'\"  style=\"width: 100px;\"/>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div> "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_4, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_5, [_hoisted_6, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("select", {
    "onUpdate:modelValue": _cache[0] || (_cache[0] = $event => $setup.bank = $event),
    class: "custom-select text-xl text-right font-light pr-4 active:overflow-none focus:overflow-none"
  }, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.month_options, option => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("option", {
      key: option.key,
      value: option.key
    }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(option.value), 9 /* TEXT, PROPS */, _hoisted_7);
  }), 128 /* KEYED_FRAGMENT */))], 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelSelect, $setup.bank]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)(`${!$setup.useAccess('read', $setup.permissions) ? 'flex justify-center items-center' : ''} card-graph w-full h-5/6 mt-5`)
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isLoadingChart ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_8, [_hoisted_9, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Chargement en cours.. ")])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), $setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("canvas", _hoisted_10, null, 512 /* NEED_PATCH */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "100px"
    }
  }, null, 8 /* PROPS */, _hoisted_11))], 2 /* CLASS */)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_12, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_13, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_14, [_hoisted_15, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("select", {
    "onUpdate:modelValue": _cache[1] || (_cache[1] = $event => $setup.fund = $event),
    class: "custom-select text-xl text-right font-light pr-4 active:overflow-none focus:overflow-none"
  }, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.month_options, option => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("option", {
      key: option.key,
      value: option.key
    }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(option.value), 9 /* TEXT, PROPS */, _hoisted_16);
  }), 128 /* KEYED_FRAGMENT */))], 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelSelect, $setup.fund]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)(`${!$setup.useAccess('read', $setup.permissions) ? 'flex justify-center items-center' : ''} card-graph w-full h-5/6 mt-5`)
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isLoadingChart ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_17, [_hoisted_18, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Chargement en cours.. ")])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), $setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("canvas", _hoisted_19, null, 512 /* NEED_PATCH */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "100px"
    }
  }, null, 8 /* PROPS */, _hoisted_20))], 2 /* CLASS */)])])])]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsBuyAndSelling.vue?vue&type=template&id=27ae792e&ts=true":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsBuyAndSelling.vue?vue&type=template&id=27ae792e&ts=true ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "workspace-content h-full overflow-x-hidden pb-6 md:pb-0"
};
const _hoisted_2 = {
  class: "stats-content w-full h-auto grid gap-6 sm:grid-cols-1 lg:grid-cols-2 2xl:grid-cols-3"
};
const _hoisted_3 = {
  class: "w-full h-72"
};
const _hoisted_4 = {
  class: "graph-card w-full h-full p-8 bg-white rounded-2xl"
};
const _hoisted_5 = {
  class: "card-header flex justify-between items-center"
};
const _hoisted_6 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "text-lg flex items-center"
}, "Achat de produits", -1 /* HOISTED */);
const _hoisted_7 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("option", {
  value: "2"
}, "Ce mois", -1 /* HOISTED */);
const _hoisted_8 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("option", {
  value: "6"
}, "6 Derniers mois", -1 /* HOISTED */);
const _hoisted_9 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("option", {
  value: "24"
}, "1 an avant", -1 /* HOISTED */);
const _hoisted_10 = [_hoisted_7, _hoisted_8, _hoisted_9];
const _hoisted_11 = {
  key: 0,
  class: "flex flex-row"
};
const _hoisted_12 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/header/rotate-right-icon.svg",
  class: "z-0 mr-4 animate-spin"
}, null, -1 /* HOISTED */);
const _hoisted_13 = {
  key: 0,
  id: "productPurchaseChart",
  ref: "productPurchaseChart",
  width: "100%",
  height: "100%"
};
const _hoisted_14 = ["src"];
const _hoisted_15 = {
  class: "w-full h-72"
};
const _hoisted_16 = {
  class: "graph-card w-full h-full p-8 bg-white rounded-2xl"
};
const _hoisted_17 = {
  class: "card-header flex justify-between items-center"
};
const _hoisted_18 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "text-lg flex items-center"
}, "Ventes de produits", -1 /* HOISTED */);
const _hoisted_19 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("option", {
  value: "2"
}, "Ce mois", -1 /* HOISTED */);
const _hoisted_20 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("option", {
  value: "6"
}, "6 Derniers mois", -1 /* HOISTED */);
const _hoisted_21 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("option", {
  value: "24"
}, "1 an avant", -1 /* HOISTED */);
const _hoisted_22 = [_hoisted_19, _hoisted_20, _hoisted_21];
const _hoisted_23 = {
  key: 0,
  class: "flex flex-row"
};
const _hoisted_24 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/header/rotate-right-icon.svg",
  class: "z-0 mr-4 animate-spin"
}, null, -1 /* HOISTED */);
const _hoisted_25 = {
  key: 0,
  id: "productSaleChart",
  ref: "productSaleChart",
  width: "100%",
  height: "100%"
};
const _hoisted_26 = ["src"];
const _hoisted_27 = {
  class: "w-full h-72"
};
const _hoisted_28 = {
  class: "graph-card w-full h-full p-8 bg-white rounded-2xl"
};
const _hoisted_29 = {
  class: "card-header flex justify-between items-center"
};
const _hoisted_30 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "text-lg flex items-center"
}, "Ventes de services", -1 /* HOISTED */);
const _hoisted_31 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("option", {
  value: "2"
}, "Ce mois", -1 /* HOISTED */);
const _hoisted_32 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("option", {
  value: "6"
}, "6 Derniers mois", -1 /* HOISTED */);
const _hoisted_33 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("option", {
  value: "24"
}, "1 an avant", -1 /* HOISTED */);
const _hoisted_34 = [_hoisted_31, _hoisted_32, _hoisted_33];
const _hoisted_35 = {
  key: 0,
  class: "flex flex-row"
};
const _hoisted_36 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/header/rotate-right-icon.svg",
  class: "z-0 mr-4 animate-spin"
}, null, -1 /* HOISTED */);
const _hoisted_37 = {
  key: 0,
  id: "serviceSaleChart",
  ref: "serviceSaleChart",
  width: "100%",
  height: "100%"
};
const _hoisted_38 = ["src"];
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_WorkspaceHeader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WorkspaceHeader");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_WorkspaceHeader, {
    options: {
      titleBar: true,
      actionsBar: false,
      actions: {
        import: false,
        export: true,
        addItem: false,
        filter: false
      }
    }
  }), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_4, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_5, [_hoisted_6, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("select", {
    "onUpdate:modelValue": _cache[0] || (_cache[0] = $event => $setup.productPurchase = $event),
    class: "custom-select text-xl text-right font-light pr-4 active:overflow-none focus:overflow-none"
  }, [..._hoisted_10], 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelSelect, $setup.productPurchase]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)(`${!$setup.useAccess('read', $setup.permissions) ? 'flex justify-center items-center' : ''} card-graph w-full h-4/5 mt-5`)
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isLoadingChart ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_11, [_hoisted_12, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Chargement en cours.. ")])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), $setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("canvas", _hoisted_13, null, 512 /* NEED_PATCH */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "100px"
    }
  }, null, 8 /* PROPS */, _hoisted_14))], 2 /* CLASS */)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_15, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_16, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_17, [_hoisted_18, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("select", {
    "onUpdate:modelValue": _cache[1] || (_cache[1] = $event => $setup.productSale = $event),
    class: "custom-select text-xl text-right font-light pr-4 active:overflow-none focus:overflow-none"
  }, [..._hoisted_22], 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelSelect, $setup.productSale]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)(`${!$setup.useAccess('read', $setup.permissions) ? 'flex justify-center items-center' : ''} card-graph w-full h-4/5 mt-5`)
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isLoadingChart ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_23, [_hoisted_24, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Chargement en cours.. ")])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), $setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("canvas", _hoisted_25, null, 512 /* NEED_PATCH */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "100px"
    }
  }, null, 8 /* PROPS */, _hoisted_26))], 2 /* CLASS */)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_27, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_28, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_29, [_hoisted_30, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("select", {
    "onUpdate:modelValue": _cache[2] || (_cache[2] = $event => $setup.serviceSale = $event),
    class: "custom-select text-xl text-right font-light pr-4 active:overflow-none focus:overflow-none"
  }, [..._hoisted_34], 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelSelect, $setup.serviceSale]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)(`${!$setup.useAccess('read', $setup.permissions) ? 'flex justify-center items-center' : ''} card-graph w-full h-4/5 mt-5`)
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isLoadingChart ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_35, [_hoisted_36, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Chargement en cours.. ")])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), $setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("canvas", _hoisted_37, null, 512 /* NEED_PATCH */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "100px"
    }
  }, null, 8 /* PROPS */, _hoisted_38))], 2 /* CLASS */)])])])]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsCreances.vue?vue&type=template&id=3673f4af&ts=true":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsCreances.vue?vue&type=template&id=3673f4af&ts=true ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "workspace-content h-full overflow-x-hidden pb-6 md:pb-0"
};
const _hoisted_2 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "workspace-header w-full h-16 flex flex-row justify-between items-center"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", {
  class: "text-lg text-gray-500"
}, "Rapports > Créances"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "workspace-actions h-full flex flex-row items-center"
})], -1 /* HOISTED */);
const _hoisted_3 = {
  class: "w-full h-3/6 lg:h-5/6"
};
const _hoisted_4 = {
  class: "graph-card w-full h-full p-8 bg-white rounded-2xl"
};
const _hoisted_5 = {
  class: "card-header flex justify-between items-center"
};
const _hoisted_6 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "text-lg flex items-center"
}, "Statistiques", -1 /* HOISTED */);
const _hoisted_7 = ["value"];
const _hoisted_8 = {
  key: 0,
  class: "flex flex-row"
};
const _hoisted_9 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/header/rotate-right-icon.svg",
  class: "z-0 mr-4 animate-spin"
}, null, -1 /* HOISTED */);
const _hoisted_10 = {
  key: 0,
  id: "creancesChart",
  ref: "creancesChart",
  width: "100%",
  height: "100%"
};
const _hoisted_11 = ["src"];
function render(_ctx, _cache, $props, $setup, $data, $options) {
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [_hoisted_2, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_4, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_5, [_hoisted_6, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("select", {
    "onUpdate:modelValue": _cache[0] || (_cache[0] = $event => $setup.creances = $event),
    class: "custom-select text-xl text-right font-light pr-4 active:overflow-none focus:overflow-none"
  }, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.month_options, option => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("option", {
      key: option.key,
      value: option.key
    }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(option.value), 9 /* TEXT, PROPS */, _hoisted_7);
  }), 128 /* KEYED_FRAGMENT */))], 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelSelect, $setup.creances]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)(`${!$setup.useAccess('read', $setup.permissions) ? 'flex justify-center items-center' : ''}card-graph w-full h-5/6 mt-10`)
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isLoadingChart ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_8, [_hoisted_9, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Chargement en cours.. ")])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), $setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("canvas", _hoisted_10, null, 512 /* NEED_PATCH */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "100px"
    }
  }, null, 8 /* PROPS */, _hoisted_11))], 2 /* CLASS */)])])]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsDebts.vue?vue&type=template&id=7231f54f&ts=true":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsDebts.vue?vue&type=template&id=7231f54f&ts=true ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "workspace-content h-full overflow-x-hidden pb-6 md:pb-0"
};
const _hoisted_2 = {
  class: "w-full h-3/6 lg:h-5/6"
};
const _hoisted_3 = {
  class: "graph-card w-full h-full p-8 bg-white rounded-2xl"
};
const _hoisted_4 = {
  class: "card-header flex justify-between items-center"
};
const _hoisted_5 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "text-lg flex items-center"
}, "Statistiques", -1 /* HOISTED */);
const _hoisted_6 = ["value"];
const _hoisted_7 = {
  key: 0,
  class: "flex flex-row"
};
const _hoisted_8 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/header/rotate-right-icon.svg",
  class: "z-0 mr-4 animate-spin"
}, null, -1 /* HOISTED */);
const _hoisted_9 = {
  key: 0,
  id: "debtsChart",
  ref: "debtsChart",
  width: "100%",
  height: "100%"
};
const _hoisted_10 = ["src"];
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_WorkspaceHeader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WorkspaceHeader");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_WorkspaceHeader, {
    options: {
      titleBar: true,
      actionsBar: false,
      actions: {
        import: false,
        export: true,
        addItem: false,
        filter: false
      }
    }
  }), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_4, [_hoisted_5, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("select", {
    "onUpdate:modelValue": _cache[0] || (_cache[0] = $event => $setup.debts = $event),
    class: "custom-select text-xl text-right font-light pr-4 active:overflow-none focus:overflow-none"
  }, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.month_options, option => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("option", {
      key: option.key,
      value: option.key
    }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(option.value), 9 /* TEXT, PROPS */, _hoisted_6);
  }), 128 /* KEYED_FRAGMENT */))], 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelSelect, $setup.debts]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)(`${!$setup.useAccess('read', $setup.permissions) ? 'flex justify-center items-center' : ''} card-graph w-full h-5/6 mt-10`)
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isLoadingChart ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_7, [_hoisted_8, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Chargement en cours.. ")])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), $setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("canvas", _hoisted_9, null, 512 /* NEED_PATCH */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "100px"
    }
  }, null, 8 /* PROPS */, _hoisted_10))], 2 /* CLASS */)])])]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsExpenses.vue?vue&type=template&id=3c5e8748&ts=true":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsExpenses.vue?vue&type=template&id=3c5e8748&ts=true ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "workspace-content h-full overflow-x-hidden pb-6 md:pb-0"
};
const _hoisted_2 = {
  class: "w-full h-3/6 lg:h-5/6"
};
const _hoisted_3 = {
  class: "graph-card w-full h-full p-8 bg-white rounded-2xl"
};
const _hoisted_4 = {
  class: "card-header flex justify-between items-center"
};
const _hoisted_5 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "text-lg flex items-center"
}, "Statistiques", -1 /* HOISTED */);
const _hoisted_6 = ["value"];
const _hoisted_7 = {
  key: 0,
  class: "flex flex-row"
};
const _hoisted_8 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/header/rotate-right-icon.svg",
  class: "z-0 mr-4 animate-spin"
}, null, -1 /* HOISTED */);
const _hoisted_9 = {
  key: 0,
  id: "expensesChart",
  ref: "expensesChart",
  width: "100%",
  height: "100%"
};
const _hoisted_10 = ["src"];
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_WorkspaceHeader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WorkspaceHeader");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_WorkspaceHeader, {
    options: {
      titleBar: true,
      actionsBar: false,
      actions: {
        import: false,
        export: true,
        addItem: false,
        filter: false
      }
    }
  }), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_4, [_hoisted_5, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("select", {
    "onUpdate:modelValue": _cache[0] || (_cache[0] = $event => $setup.expenses = $event),
    class: "custom-select text-xl text-right font-light pr-4 active:overflow-none focus:overflow-none"
  }, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.month_options, option => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("option", {
      key: option.key,
      value: option.key
    }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(option.value), 9 /* TEXT, PROPS */, _hoisted_6);
  }), 128 /* KEYED_FRAGMENT */))], 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelSelect, $setup.expenses]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)(`${!$setup.useAccess('read', $setup.permissions) ? 'flex justify-center items-center' : ''} card-graph w-full h-5/6 mt-10`)
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isLoadingChart ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_7, [_hoisted_8, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Chargement en cours.. ")])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), $setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("canvas", _hoisted_9, null, 512 /* NEED_PATCH */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "100px"
    }
  }, null, 8 /* PROPS */, _hoisted_10))], 2 /* CLASS */)])])]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsImcomeStatement.vue?vue&type=template&id=e66e5214&ts=true":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsImcomeStatement.vue?vue&type=template&id=e66e5214&ts=true ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "relative h-full pb-6 overflow-x-hidden workspace-content md:pb-0"
};
const _hoisted_2 = {
  key: 0,
  class: "flex flex-col w-full mt-4 transition-all duration-500 ease-in-out lg:flex-row"
};
const _hoisted_3 = {
  class: "w-full mr-8 lg:w-3/12"
};
const _hoisted_4 = {
  class: "flex flex-row items-center w-full mt-0 space-x-3 space-y-0 overflow-x-auto text-gray-600 lg:flex-col flex-nowrap lg:flex-wrap lg:items-start lg:space-x-0 lg:space-y-5 lg:mt-5"
};
const _hoisted_5 = ["onMouseover", "onMouseleave", "onClick"];
const _hoisted_6 = ["src"];
const _hoisted_7 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", null, "Voir plus", -1 /* HOISTED */);
const _hoisted_8 = {
  class: "w-full mb-4 space-y-4"
};
const _hoisted_9 = {
  key: 0,
  class: "w-full"
};
const _hoisted_10 = ["placeholder"];
const _hoisted_11 = ["loading", "text"];
const _hoisted_12 = {
  key: 0,
  class: "relative flex flex-col w-full font-normal lg:w-9/12"
};
const _hoisted_13 = {
  class: "flex flex-row w-full"
};
const _hoisted_14 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "flex items-center justify-center w-1/2 h-auto py-4 pl-4 text-xl font-medium text-white bg-green-800 lg:w-3/5 opacity-90 rounded-tl-2xl"
}, "Rubriques", -1 /* HOISTED */);
const _hoisted_15 = {
  class: "flex flex-col w-1/2 text-xl font-medium text-center text-white bg-green-900 lg:w-2/5 rounded-tr-2xl"
};
const _hoisted_16 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "w-full py-4 text-center"
}, "Montants", -1 /* HOISTED */);
const _hoisted_17 = {
  class: "flex flex-row w-full font-normal border-t border-gray-400 text-md"
};
const _hoisted_18 = {
  class: "w-1/2 py-3 text-sm text-center border-r border-gray-300 lg:text-lg"
};
const _hoisted_19 = {
  class: "w-1/2 py-3 text-sm text-center lg:text-lg"
};
const _hoisted_20 = {
  class: "w-1/2 py-3 bg-gray-200 lg:w-3/5 bg-opacity-60"
};
const _hoisted_21 = {
  class: "list-disc"
};
const _hoisted_22 = {
  class: "ml-9"
};
const _hoisted_23 = {
  class: "flex flex-row w-1/2 text-center bg-gray-300 bg-opacity-50 border-t lg:w-2/5"
};
const _hoisted_24 = {
  class: "w-1/2 py-3 text-center border-r border-gray-300"
};
const _hoisted_25 = {
  class: "w-1/2 py-3 text-center"
};
const _hoisted_26 = {
  class: "w-1/2 py-3 pl-4 font-medium text-green-700 bg-gray-400 border-t border-b lg:w-3/5 bg-opacity-30"
};
const _hoisted_27 = {
  class: "flex flex-row w-1/2 font-medium text-green-700 bg-gray-400 bg-opacity-50 border-t border-b lg:w-2/5"
};
const _hoisted_28 = {
  class: "w-1/2 py-3 text-center border-r border-gray-400"
};
const _hoisted_29 = {
  class: "w-1/2 py-3 text-center"
};
const _hoisted_30 = {
  class: "w-1/2 py-3 bg-gray-200 lg:w-3/5 bg-opacity-60"
};
const _hoisted_31 = {
  class: "list-disc"
};
const _hoisted_32 = {
  class: "ml-9"
};
const _hoisted_33 = {
  class: "flex flex-row w-1/2 text-center bg-gray-300 bg-opacity-50 border-t lg:w-2/5"
};
const _hoisted_34 = {
  class: "w-1/2 py-3 text-center border-r border-gray-300"
};
const _hoisted_35 = {
  class: "w-1/2 py-3 text-center"
};
const _hoisted_36 = {
  class: "w-1/2 py-3 pl-4 font-medium text-green-700 bg-gray-400 border-t border-b lg:w-3/5 bg-opacity-30"
};
const _hoisted_37 = {
  class: "flex flex-row w-1/2 font-medium text-green-700 bg-gray-400 bg-opacity-50 border-t border-b lg:w-2/5"
};
const _hoisted_38 = {
  class: "w-1/2 py-3 text-center border-r border-gray-400"
};
const _hoisted_39 = {
  class: "w-1/2 py-3 text-center"
};
const _hoisted_40 = {
  class: "w-1/2 py-3 pl-4 font-medium text-green-700 bg-gray-400 border-t border-b lg:w-3/5 bg-opacity-30"
};
const _hoisted_41 = {
  class: "flex flex-row w-1/2 font-medium text-green-700 bg-gray-400 bg-opacity-50 border-t border-b lg:w-2/5"
};
const _hoisted_42 = {
  class: "w-1/2 py-3 text-center border-r border-gray-400"
};
const _hoisted_43 = {
  class: "w-1/2 py-3 text-center"
};
const _hoisted_44 = {
  class: "w-1/2 py-3 pl-4 font-medium text-green-700 bg-gray-400 border-t border-b lg:w-3/5 bg-opacity-30 rounded-bl-2xl"
};
const _hoisted_45 = {
  class: "flex flex-row w-1/2 font-medium text-green-700 bg-gray-400 bg-opacity-50 border-t border-b lg:w-2/5 rounded-br-2xl"
};
const _hoisted_46 = {
  class: "w-1/2 py-3 text-center border-r border-gray-400"
};
const _hoisted_47 = {
  class: "w-1/2 py-3 text-center"
};
const _hoisted_48 = {
  key: 1,
  class: "relative flex flex-col w-full font-normal lg:w-9/12"
};
const _hoisted_49 = {
  key: 1,
  class: "absolute mt-12 transform -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2 z-100"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_WorkspaceHeader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WorkspaceHeader");
  const _component_SlimTitleBar = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SlimTitleBar");
  const _component_SkeletonIncomeStateWithoutPeriodSection = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SkeletonIncomeStateWithoutPeriodSection");
  const _component_SkeletonLoader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SkeletonLoader");
  const _component_EmptyContent = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("EmptyContent");
  const _component_SkeletonIncomeState = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SkeletonIncomeState");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" eslint-disable vue/no-deprecated-slot-attribute "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_WorkspaceHeader, {
    "items-list": $setup.allData,
    "data-to-p-d-f": {
      data: $setup.allData,
      which: 'IncomeStatement',
      year: $setup.inputYear
    },
    "data-to-export": $setup.exportIncomingStatementData($setup.allData, $setup.inputYear),
    options: {
      titleBar: true,
      actionsBar: true,
      actions: {
        import: false,
        export: $setup.isLoading ? false : true,
        addItem: false,
        filter: false
      }
    }
  }, null, 8 /* PROPS */, ["items-list", "data-to-p-d-f", "data-to-export", "options"]), $setup.recipes.length != 0 && $setup.expenses.length != 0 && $setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SlimTitleBar, {
    title: "Année d'exercice"
  }), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" accounting period section "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_4, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Date buttons list "), ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.lastAccountingYear, (year, index) => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
      key: index,
      class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
        'w-4/6 lg:w-full flex flex-row justify-start lg:justify-between cursor-pointer opacity-90  hover:text-white items-center p-4 bg-gray-300 rounded-2xl hover:bg-green-700': true,
        'text-gray-800': $setup.selectedYear != index,
        'bg-green-800 text-white': $setup.selectedYear == index
      }),
      onMouseover: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withModifiers)($event => $setup.changeCurrentIcon(true, index), ["self"]),
      onMouseleave: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withModifiers)($event => $setup.changeCurrentIcon(false, index), ["self"]),
      onClick: $event => $setup.getIncomeStatement(index)
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(year), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
      src: $setup.currentIndex == index ? $setup.currentIcon : $setup.selectedYear == index ? '/icons/arrow-right-icon-w.svg' : '/icons/arrow-right.svg',
      alt: "aArrow Icon",
      class: "hidden w-4 h-4 lg:flex"
    }, null, 8 /* PROPS */, _hoisted_6)], 42 /* CLASS, PROPS, HYDRATE_EVENTS */, _hoisted_5);
  }), 128 /* KEYED_FRAGMENT */)), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" View more button "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: "flex items-center justify-start w-4/6 p-4 pt-4 font-medium text-green-700 cursor-pointer lg:w-full lg:justify-between lg:pt-0",
    onClick: _cache[0] || (_cache[0] = () => {
      $setup.viewMore = !$setup.viewMore;
    })
  }, [_hoisted_7, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
    src: "/icons/arrow-right-icon-c.svg",
    alt: "",
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
      'w-4 h-4': true,
      'rotate-45': $setup.viewMore == true
    })
  }, null, 2 /* CLASS */)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Input for date "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_8, [$setup.viewMore ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_9, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-input", {
    flex: "normal",
    placeholder: $setup.currentYear
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    slot: "native-input",
    "onUpdate:modelValue": _cache[1] || (_cache[1] = $event => $setup.inputYear = $event)
  }, null, 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, $setup.inputYear]])], 8 /* PROPS */, _hoisted_10)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.viewMore ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
    key: 1,
    class: "w-full cursor-pointer",
    onClick: $setup.getIncomeStatement
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-button", {
    flex: "normal",
    type: "link",
    url: "javascript:void(0)",
    loading: $setup.isLoading,
    text: $setup.isLoading ? 'chargement...' : 'Valider',
    usage: "simple-text",
    "text-color": "white",
    class: "font-light"
  }, null, 8 /* PROPS */, _hoisted_11)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Income statement "), !$setup.isLoading ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_12, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_13, [_hoisted_14, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_15, [_hoisted_16, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_17, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_18, "Année " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.year), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_19, "Année " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.year - 1), 1 /* TEXT */)])])]), ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.recipes, recipe => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
      key: recipe.id,
      class: "flex flex-row w-full border-b border-gray-400 border-opacity-50"
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_20, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("ul", _hoisted_21, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("li", _hoisted_22, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(recipe.title), 1 /* TEXT */)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_23, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_24, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.orignalMoney(recipe.value)) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(recipe.curency), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_25, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.orignalMoney(recipe.value2)) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(recipe.curency), 1 /* TEXT */)])]);
  }), 128 /* KEYED_FRAGMENT */)), ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.totalRecipes, total => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
      key: total.id,
      class: "flex flex-row w-full"
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_26, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(total.title), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_27, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_28, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.orignalMoney(total.value)) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(total.curency), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_29, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.orignalMoney(total.value2)) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(total.curency), 1 /* TEXT */)])]);
  }), 128 /* KEYED_FRAGMENT */)), ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.expenses, expense => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
      key: expense.id,
      class: "flex flex-row w-full border-b border-gray-400 border-opacity-50"
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_30, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("ul", _hoisted_31, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("li", _hoisted_32, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(expense.title), 1 /* TEXT */)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_33, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_34, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.orignalMoney(expense.value)) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(expense.curency), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_35, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.orignalMoney(expense.value2)) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(expense.curency), 1 /* TEXT */)])]);
  }), 128 /* KEYED_FRAGMENT */)), ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.TotalExpenses, total => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
      key: total.id,
      class: "flex flex-row w-full"
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_36, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(total.title), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_37, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_38, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.orignalMoney(total.value)) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(total.curency), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_39, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.orignalMoney(total.value2)) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(total.curency), 1 /* TEXT */)])]);
  }), 128 /* KEYED_FRAGMENT */)), ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.pay, total => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
      key: total.id,
      class: "flex flex-row w-full"
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_40, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(total.title), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_41, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_42, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.orignalMoney(total.value)) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(total.curency), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_43, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.orignalMoney(total.value2)) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(total.curency), 1 /* TEXT */)])]);
  }), 128 /* KEYED_FRAGMENT */)), ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.depreciations, total => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
      key: total.id,
      class: "flex flex-row w-full"
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_44, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(total.title), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_45, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_46, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.orignalMoney(total.value)) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(total.curency), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_47, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.orignalMoney(total.value2)) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(total.curency), 1 /* TEXT */)])]);
  }), 128 /* KEYED_FRAGMENT */))])) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_48, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SkeletonLoader, null, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SkeletonIncomeStateWithoutPeriodSection)]),
    _: 1 /* STABLE */
  })]))])) : !$setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_49, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_EmptyContent, {
    icon: '/icons/no-access-icon-c.svg',
    line1: 'Oups... Vous n\'êtes pas autorisé ',
    line2: 'à afficher ce contenu.',
    action: '',
    height: "100px",
    width: "100px"
  }, null, 8 /* PROPS */, ["icon", "line1", "line2"])])) : $setup.isLoading ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    key: 2,
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SkeletonLoader, null, {
      default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SkeletonIncomeState)]),
      _: 1 /* STABLE */
    })]),

    _: 1 /* STABLE */
  })) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])], 2112 /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsLoans.vue?vue&type=template&id=380441dc&ts=true":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsLoans.vue?vue&type=template&id=380441dc&ts=true ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "workspace-content h-full overflow-x-hidden pb-6 md:pb-0"
};
const _hoisted_2 = {
  class: "w-full h-3/6 lg:h-5/6"
};
const _hoisted_3 = {
  class: "graph-card w-full h-full p-8 bg-white rounded-2xl"
};
const _hoisted_4 = {
  class: "card-header flex justify-between items-center"
};
const _hoisted_5 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "text-lg flex items-center"
}, "Statistiques", -1 /* HOISTED */);
const _hoisted_6 = ["value"];
const _hoisted_7 = {
  key: 0,
  class: "flex flex-row"
};
const _hoisted_8 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/header/rotate-right-icon.svg",
  class: "z-0 mr-4 animate-spin"
}, null, -1 /* HOISTED */);
const _hoisted_9 = {
  key: 0,
  id: "loansChart",
  ref: "loansChart",
  width: "100%",
  height: "100%"
};
const _hoisted_10 = ["src"];
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_WorkspaceHeader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WorkspaceHeader");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_WorkspaceHeader, {
    options: {
      titleBar: true,
      actionsBar: false,
      actions: {
        import: false,
        export: true,
        addItem: false,
        filter: false
      }
    }
  }), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_4, [_hoisted_5, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("select", {
    "onUpdate:modelValue": _cache[0] || (_cache[0] = $event => $setup.loans = $event),
    class: "custom-select text-xl text-right font-light pr-4 active:overflow-none focus:overflow-none"
  }, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.month_options, option => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("option", {
      key: option.key,
      value: option.key
    }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(option.value), 9 /* TEXT, PROPS */, _hoisted_6);
  }), 128 /* KEYED_FRAGMENT */))], 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelSelect, $setup.loans]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)(`${!$setup.useAccess('read', $setup.permissions) ? 'flex justify-center items-center' : ''} card-graph w-full h-5/6 mt-10`)
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isLoadingChart ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_7, [_hoisted_8, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Chargement en cours.. ")])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), $setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("canvas", _hoisted_9, null, 512 /* NEED_PATCH */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "100px"
    }
  }, null, 8 /* PROPS */, _hoisted_10))], 2 /* CLASS */)])])]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsOverview.vue?vue&type=template&id=3b8d07ba&ts=true":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsOverview.vue?vue&type=template&id=3b8d07ba&ts=true ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "h-full pb-5 overflow-x-hidden workspace-content md:pb-0"
};
const _hoisted_2 = {
  class: "relative w-full"
};
const _hoisted_3 = {
  key: 0,
  class: "w-full"
};
const _hoisted_4 = {
  class: "grid w-full gap-6 mt-10 group-cards sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-3 2xl:grid-cols-3"
};
const _hoisted_5 = {
  class: "w-full"
};
const _hoisted_6 = {
  class: "w-full"
};
const _hoisted_7 = {
  class: "w-full"
};
const _hoisted_8 = {
  class: "w-full"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_SkeletonSubOverview = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SkeletonSubOverview");
  const _component_SkeletonLoader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SkeletonLoader");
  const _component_CardBannerDashboardOverview = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("CardBannerDashboardOverview");
  const _component_StatCard3 = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("StatCard3");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isLoading ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_SkeletonLoader, {
      key: 0
    }, {
      default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SkeletonSubOverview, {
        stats: 6
      })]),
      _: 1 /* STABLE */
    })) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), !$setup.isLoading ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_CardBannerDashboardOverview, {
    icon: "settings-banner-picture",
    "line-one": "Rapports",
    "line-two-a": "Dans cette section vous avez une vue d'ensemble des statistiques",
    "line-two-b": "",
    "line-three": "Lancer le tutoriel d'aide ",
    link: "workspace-settings-business-account"
  }), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_4, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <div class=\"w-full\">\n\t\t\t\t\t\t<StatCard1\n\t\t\t\t\t\t\t:lineOne=\"\n\t\t\t\t\t\t\t\t$filters\n\t\t\t\t\t\t\t\t\t.moneyFormat(\n\t\t\t\t\t\t\t\t\toverviewInfos[0].value != undefined\n\t\t\t\t\t\t\t\t\t\t? overviewInfos[0].value\n\t\t\t\t\t\t\t\t\t\t: 0\n\t\t\t\t\t\t\t\t).toString() + overviewInfos[0].curency.toString()\n\t\t\t\t\t\t\t\"\n\t\t\t\t\t\t\tlineTwo=\"Stock Niveau Critique\"\n\t\t\t\t\t\t\tlink=\"workspace-reports-stocks\"\n\t\t\t\t\t\t/>\n\t\t\t\t\t</div> "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_5, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_StatCard3, {
    "line-one": $setup.orignalMoney($setup.overviewInfos[0].value != undefined ? $setup.overviewInfos[0].value : 0).toString(),
    "line-two": $setup.overviewInfos[0].curency,
    "line-three": "Montant",
    "line-four": "Créeance Max",
    link: "workspace-reports-creances",
    access: $setup.useAccess('read', $setup.permissions)
  }, null, 8 /* PROPS */, ["line-one", "line-two", "access"])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_6, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_StatCard3, {
    "line-one": $setup.orignalMoney($setup.overviewInfos[1].value != undefined ? $setup.overviewInfos[1].value : 0).toString(),
    "line-two": $setup.overviewInfos[1].curency,
    "line-three": "Montant",
    "line-four": "Dette Max",
    link: "workspace-reports-debts",
    access: $setup.useAccess('read', $setup.permissions)
  }, null, 8 /* PROPS */, ["line-one", "line-two", "access"])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_7, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_StatCard3, {
    "line-one": ($setup.overviewInfos[2].value != undefined ? $setup.overviewInfos[2].value : 0).toString(),
    "line-three": "Produit le plus vendu",
    "line-four": $setup.overviewInfos[2] != undefined ? $setup.overviewInfos[2]?.curency?.toString() : '',
    link: "workspace-reports-buy-and-selling",
    access: $setup.useAccess('read', $setup.permissions)
  }, null, 8 /* PROPS */, ["line-one", "line-four", "access"])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_8, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_StatCard3, {
    "line-one": $setup.overviewInfos[3].value != undefined ? $setup.overviewInfos[3].value : 0,
    "line-three": "Produit le moins vendu",
    "line-four": $setup.overviewInfos[3] != undefined ? $setup.overviewInfos[3]?.curency?.toString() : '',
    link: "workspace-reports-buy-and-selling",
    access: $setup.useAccess('read', $setup.permissions)
  }, null, 8 /* PROPS */, ["line-one", "line-four", "access"])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" infos non renvoyée par l'API "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <div class=\"w-full\">\n\t\t\t\t\t\t<StatCard1\n\t\t\t\t\t\t\tlineOne=\"5/25\"\n\t\t\t\t\t\t\tlineTwo=\"Opération Achat/Vente\"\n\t\t\t\t\t\t\tlineThree=\"Total de 125/258\"\n\t\t\t\t\t\t\tlink=\"workspace-reports-buy-and-selling\"\n\t\t\t\t\t\t/>\n\t\t\t\t\t</div> ")])])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsStocks.vue?vue&type=template&id=e4f1eac4&ts=true":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsStocks.vue?vue&type=template&id=e4f1eac4&ts=true ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "workspace-content h-full overflow-x-hidden pb-6 md:pb-0"
};
const _hoisted_2 = {
  class: "stats-content w-full h-auto grid gap-6 sm:grid-cols-1 lg:grid-cols-2 2xl:grid-cols-3"
};
const _hoisted_3 = {
  class: "w-full h-72"
};
const _hoisted_4 = {
  class: "graph-card w-full h-full p-8 bg-white rounded-2xl"
};
const _hoisted_5 = {
  class: "card-header flex justify-between items-center"
};
const _hoisted_6 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "text-lg flex items-center"
}, "Categories", -1 /* HOISTED */);
const _hoisted_7 = ["value"];
const _hoisted_8 = {
  key: 0,
  class: "flex flex-row"
};
const _hoisted_9 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/header/rotate-right-icon.svg",
  class: "z-0 mr-4 animate-spin"
}, null, -1 /* HOISTED */);
const _hoisted_10 = {
  key: 0,
  id: "categoriesChart",
  ref: "categoriesChart",
  width: "100%",
  height: "100%"
};
const _hoisted_11 = ["src"];
const _hoisted_12 = {
  class: "w-full h-72"
};
const _hoisted_13 = {
  class: "graph-card w-full h-full p-8 bg-white rounded-2xl"
};
const _hoisted_14 = {
  class: "card-header flex justify-between items-center"
};
const _hoisted_15 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "text-lg flex items-center"
}, "Produits", -1 /* HOISTED */);
const _hoisted_16 = ["value"];
const _hoisted_17 = {
  key: 0,
  class: "flex flex-row"
};
const _hoisted_18 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/header/rotate-right-icon.svg",
  class: "z-0 mr-4 animate-spin"
}, null, -1 /* HOISTED */);
const _hoisted_19 = {
  key: 0,
  id: "productsChart",
  ref: "productsChart",
  width: "100%",
  height: "100%"
};
const _hoisted_20 = ["src"];
const _hoisted_21 = {
  class: "w-full h-72"
};
const _hoisted_22 = {
  class: "graph-card w-full h-full p-8 bg-white rounded-2xl"
};
const _hoisted_23 = {
  class: "card-header flex justify-between items-center"
};
const _hoisted_24 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "text-lg flex items-center"
}, "Services", -1 /* HOISTED */);
const _hoisted_25 = ["value"];
const _hoisted_26 = {
  key: 0,
  class: "flex flex-row"
};
const _hoisted_27 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/header/rotate-right-icon.svg",
  class: "z-0 mr-4 animate-spin"
}, null, -1 /* HOISTED */);
const _hoisted_28 = {
  key: 0,
  id: "servicesChart",
  ref: "servicesChart",
  width: "100%",
  height: "100%"
};
const _hoisted_29 = ["src"];
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_WorkspaceHeader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WorkspaceHeader");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_WorkspaceHeader, {
    options: {
      titleBar: true,
      actionsBar: false,
      actions: {
        import: false,
        export: true,
        addItem: false,
        filter: false
      }
    }
  }), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_4, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_5, [_hoisted_6, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("select", {
    "onUpdate:modelValue": _cache[0] || (_cache[0] = $event => $setup.categories = $event),
    class: "custom-select text-xl text-right font-light pr-4 active:overflow-none focus:overflow-none"
  }, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.month_options, option => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("option", {
      key: option.key,
      value: option.key
    }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(option.value), 9 /* TEXT, PROPS */, _hoisted_7);
  }), 128 /* KEYED_FRAGMENT */))], 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelSelect, $setup.categories]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)(`${!$setup.useAccess('read', $setup.permissions) ? 'flex justify-center items-center' : ''} card-graph w-full h-4/5 mt-5`)
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isLoadingChart ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_8, [_hoisted_9, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Chargement en cours.. ")])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), $setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("canvas", _hoisted_10, null, 512 /* NEED_PATCH */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "100px"
    }
  }, null, 8 /* PROPS */, _hoisted_11))], 2 /* CLASS */)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_12, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_13, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_14, [_hoisted_15, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("select", {
    "onUpdate:modelValue": _cache[1] || (_cache[1] = $event => $setup.products = $event),
    class: "custom-select text-xl text-right font-light pr-4 active:overflow-none focus:overflow-none"
  }, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.month_options, option => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("option", {
      key: option.key,
      value: option.key
    }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(option.value), 9 /* TEXT, PROPS */, _hoisted_16);
  }), 128 /* KEYED_FRAGMENT */))], 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelSelect, $setup.products]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)(`${!$setup.useAccess('read', $setup.permissions) ? 'flex justify-center items-center' : ''} card-graph w-full h-4/5 mt-5`)
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isLoadingChart ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_17, [_hoisted_18, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Chargement en cours.. ")])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), $setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("canvas", _hoisted_19, null, 512 /* NEED_PATCH */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "100px"
    }
  }, null, 8 /* PROPS */, _hoisted_20))], 2 /* CLASS */)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_21, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_22, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_23, [_hoisted_24, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("select", {
    "onUpdate:modelValue": _cache[2] || (_cache[2] = $event => $setup.services = $event),
    class: "custom-select text-xl text-right font-light pr-4 active:overflow-none focus:overflow-none"
  }, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.month_options, option => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("option", {
      key: option.key,
      value: option.key
    }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(option.value), 9 /* TEXT, PROPS */, _hoisted_25);
  }), 128 /* KEYED_FRAGMENT */))], 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelSelect, $setup.services]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)(`${!$setup.useAccess('read', $setup.permissions) ? 'flex justify-center items-center' : ''} card-graph w-full h-4/5 mt-5`)
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isLoadingChart ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_26, [_hoisted_27, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Chargement en cours.. ")])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), $setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("canvas", _hoisted_28, null, 512 /* NEED_PATCH */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "100px"
    }
  }, null, 8 /* PROPS */, _hoisted_29))], 2 /* CLASS */)])])])]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsSubventions.vue?vue&type=template&id=1e2fcbaf&ts=true":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsSubventions.vue?vue&type=template&id=1e2fcbaf&ts=true ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "workspace-content h-full overflow-x-hidden pb-6 md:pb-0"
};
const _hoisted_2 = {
  class: "w-full h-3/6 lg:h-5/6"
};
const _hoisted_3 = {
  class: "graph-card w-full h-full p-8 bg-white rounded-2xl"
};
const _hoisted_4 = {
  class: "card-header flex justify-between items-center"
};
const _hoisted_5 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "text-lg flex items-center"
}, "Statistiques", -1 /* HOISTED */);
const _hoisted_6 = ["value"];
const _hoisted_7 = {
  key: 0,
  class: "flex flex-row"
};
const _hoisted_8 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/header/rotate-right-icon.svg",
  class: "z-0 mr-4 animate-spin"
}, null, -1 /* HOISTED */);
const _hoisted_9 = {
  key: 0,
  id: "subventionsChart",
  ref: "subventionsChart",
  width: "100%",
  height: "100%"
};
const _hoisted_10 = ["src"];
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_WorkspaceHeader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WorkspaceHeader");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_WorkspaceHeader, {
    options: {
      titleBar: true,
      actionsBar: false,
      actions: {
        import: false,
        export: true,
        addItem: false,
        filter: false
      }
    }
  }), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_4, [_hoisted_5, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("select", {
    "onUpdate:modelValue": _cache[0] || (_cache[0] = $event => $setup.subventions = $event),
    class: "custom-select text-xl text-right font-light pr-4 active:overflow-none focus:overflow-none"
  }, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.month_options, option => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("option", {
      key: option.key,
      value: option.key
    }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(option.value), 9 /* TEXT, PROPS */, _hoisted_6);
  }), 128 /* KEYED_FRAGMENT */))], 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelSelect, $setup.subventions]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)(`${!$setup.useAccess('read', $setup.permissions) ? 'flex justify-center items-center' : ''} card-graph w-full h-5/6 mt-10`)
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isLoadingChart ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_7, [_hoisted_8, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Chargement en cours.. ")])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), $setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("canvas", _hoisted_9, null, 512 /* NEED_PATCH */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "100px"
    }
  }, null, 8 /* PROPS */, _hoisted_10))], 2 /* CLASS */)])])]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsThirdParties.vue?vue&type=template&id=1d401344&ts=true":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsThirdParties.vue?vue&type=template&id=1d401344&ts=true ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "workspace-content h-full overflow-x-hidden pb-6 md:pb-0"
};
const _hoisted_2 = {
  class: "stats-content w-full h-auto grid gap-6 sm:grid-cols-1 lg:grid-cols-2 2xl:grid-cols-3"
};
const _hoisted_3 = {
  class: "w-full h-72"
};
const _hoisted_4 = {
  class: "graph-card w-full h-full p-8 bg-white rounded-2xl"
};
const _hoisted_5 = {
  class: "card-header flex justify-between items-center"
};
const _hoisted_6 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "text-lg flex items-center"
}, "Clients", -1 /* HOISTED */);
const _hoisted_7 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("option", {
  value: "1"
}, "Ce mois", -1 /* HOISTED */);
const _hoisted_8 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("option", {
  value: "6"
}, "6 Derniers mois", -1 /* HOISTED */);
const _hoisted_9 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("option", {
  value: "24"
}, "1 an avant", -1 /* HOISTED */);
const _hoisted_10 = [_hoisted_7, _hoisted_8, _hoisted_9];
const _hoisted_11 = {
  key: 0,
  class: "flex flex-row"
};
const _hoisted_12 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/header/rotate-right-icon.svg",
  class: "z-0 mr-4 animate-spin"
}, null, -1 /* HOISTED */);
const _hoisted_13 = {
  key: 0,
  id: "clientsChart",
  ref: "clientsChart",
  width: "100%",
  height: "100%"
};
const _hoisted_14 = ["src"];
const _hoisted_15 = {
  class: "w-full h-72"
};
const _hoisted_16 = {
  class: "graph-card w-full h-full p-8 bg-white rounded-2xl"
};
const _hoisted_17 = {
  class: "card-header flex justify-between items-center"
};
const _hoisted_18 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "text-lg flex items-center"
}, "Fournisseurs", -1 /* HOISTED */);
const _hoisted_19 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("option", {
  value: "2"
}, "Ce mois", -1 /* HOISTED */);
const _hoisted_20 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("option", {
  value: "6"
}, "6 Derniers mois", -1 /* HOISTED */);
const _hoisted_21 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("option", {
  value: "24"
}, "1 an avant", -1 /* HOISTED */);
const _hoisted_22 = [_hoisted_19, _hoisted_20, _hoisted_21];
const _hoisted_23 = {
  key: 0,
  class: "flex flex-row"
};
const _hoisted_24 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/header/rotate-right-icon.svg",
  class: "z-0 mr-4 animate-spin"
}, null, -1 /* HOISTED */);
const _hoisted_25 = {
  key: 0,
  id: "providerChart",
  ref: "providerChart",
  width: "100%",
  height: "100%"
};
const _hoisted_26 = ["src"];
const _hoisted_27 = {
  class: "w-full h-72"
};
const _hoisted_28 = {
  class: "graph-card w-full h-full p-8 bg-white rounded-2xl"
};
const _hoisted_29 = {
  class: "card-header flex justify-between items-center"
};
const _hoisted_30 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "text-lg flex items-center"
}, "Creanciers", -1 /* HOISTED */);
const _hoisted_31 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("option", {
  value: "2"
}, "Ce mois", -1 /* HOISTED */);
const _hoisted_32 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("option", {
  value: "6"
}, "6 Derniers mois", -1 /* HOISTED */);
const _hoisted_33 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("option", {
  value: "24"
}, "1 an avant", -1 /* HOISTED */);
const _hoisted_34 = [_hoisted_31, _hoisted_32, _hoisted_33];
const _hoisted_35 = {
  key: 0,
  class: "flex flex-row"
};
const _hoisted_36 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/header/rotate-right-icon.svg",
  class: "z-0 mr-4 animate-spin"
}, null, -1 /* HOISTED */);
const _hoisted_37 = {
  key: 0,
  id: "creditorsChart",
  ref: "creditorsChart",
  width: "100%",
  height: "100%"
};
const _hoisted_38 = ["src"];
const _hoisted_39 = {
  class: "w-full h-72"
};
const _hoisted_40 = {
  class: "graph-card w-full h-full p-8 bg-white rounded-2xl"
};
const _hoisted_41 = {
  class: "card-header flex justify-between items-center"
};
const _hoisted_42 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "text-lg flex items-center"
}, "Bailleurs de fond", -1 /* HOISTED */);
const _hoisted_43 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("option", {
  value: "2"
}, "Ce mois", -1 /* HOISTED */);
const _hoisted_44 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("option", {
  value: "6"
}, "6 Derniers mois", -1 /* HOISTED */);
const _hoisted_45 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("option", {
  value: "24"
}, "1 an avant", -1 /* HOISTED */);
const _hoisted_46 = [_hoisted_43, _hoisted_44, _hoisted_45];
const _hoisted_47 = {
  key: 0,
  class: "flex flex-row"
};
const _hoisted_48 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/header/rotate-right-icon.svg",
  class: "z-0 mr-4 animate-spin"
}, null, -1 /* HOISTED */);
const _hoisted_49 = {
  key: 0,
  id: "financialBackerChart",
  ref: "financialBackerChart",
  width: "100%",
  height: "100%"
};
const _hoisted_50 = ["src"];
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_WorkspaceHeader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WorkspaceHeader");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_WorkspaceHeader, {
    options: {
      titleBar: true,
      actionsBar: false,
      actions: {
        import: false,
        export: true,
        addItem: false,
        filter: false
      }
    }
  }), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_4, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_5, [_hoisted_6, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("select", {
    "onUpdate:modelValue": _cache[0] || (_cache[0] = $event => $setup.clients = $event),
    class: "custom-select text-xl text-right font-light pr-4 active:overflow-none focus:overflow-none"
  }, [..._hoisted_10], 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelSelect, $setup.clients]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)(`${!$setup.useAccess('read', $setup.permissions) ? 'flex justify-center items-center' : ''} card-graph w-full h-4/5 mt-5`)
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isLoadingChart ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_11, [_hoisted_12, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Chargement en cours.. ")])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), $setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("canvas", _hoisted_13, null, 512 /* NEED_PATCH */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "100px"
    }
  }, null, 8 /* PROPS */, _hoisted_14))], 2 /* CLASS */)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_15, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_16, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_17, [_hoisted_18, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("select", {
    "onUpdate:modelValue": _cache[1] || (_cache[1] = $event => $setup.provider = $event),
    class: "custom-select text-xl text-right font-light pr-4 active:overflow-none focus:overflow-none"
  }, [..._hoisted_22], 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelSelect, $setup.provider]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)(`${!$setup.useAccess('read', $setup.permissions) ? 'flex justify-center items-center' : ''} card-graph w-full h-4/5 mt-5`)
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isLoadingChart ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_23, [_hoisted_24, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Chargement en cours.. ")])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), $setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("canvas", _hoisted_25, null, 512 /* NEED_PATCH */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "100px"
    }
  }, null, 8 /* PROPS */, _hoisted_26))], 2 /* CLASS */)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_27, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_28, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_29, [_hoisted_30, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("select", {
    "onUpdate:modelValue": _cache[2] || (_cache[2] = $event => $setup.creditor = $event),
    class: "custom-select text-xl text-right font-light pr-4 active:overflow-none focus:overflow-none"
  }, [..._hoisted_34], 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelSelect, $setup.creditor]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)(`${!$setup.useAccess('read', $setup.permissions) ? 'flex justify-center items-center' : ''} card-graph w-full h-4/5 mt-5`)
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isLoadingChart ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_35, [_hoisted_36, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Chargement en cours.. ")])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), $setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("canvas", _hoisted_37, null, 512 /* NEED_PATCH */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "100px"
    }
  }, null, 8 /* PROPS */, _hoisted_38))], 2 /* CLASS */)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_39, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_40, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_41, [_hoisted_42, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("select", {
    "onUpdate:modelValue": _cache[3] || (_cache[3] = $event => $setup.financialBacker = $event),
    class: "custom-select text-xl text-right font-light pr-4 active:overflow-none focus:overflow-none"
  }, [..._hoisted_46], 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelSelect, $setup.financialBacker]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)(`${!$setup.useAccess('read', $setup.permissions) ? 'flex justify-center items-center' : ''} card-graph w-full h-4/5 mt-5`)
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isLoadingChart ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_47, [_hoisted_48, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Chargement en cours.. ")])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), $setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("canvas", _hoisted_49, null, 512 /* NEED_PATCH */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "100px"
    }
  }, null, 8 /* PROPS */, _hoisted_50))], 2 /* CLASS */)])])])]);
}

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsArchiving.vue?vue&type=style&index=0&id=449eade4&lang=scss&scoped=true":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsArchiving.vue?vue&type=style&index=0&id=449eade4&lang=scss&scoped=true ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/noSourceMaps.js */ "./node_modules/css-loader/dist/runtime/noSourceMaps.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, "[data-v-449eade4] .p-paginator .p-paginator-current {\n  margin-left: auto;\n}\n[data-v-449eade4] .p-progressbar {\n  height: 0.5rem;\n  background-color: #d8dadc;\n}\n[data-v-449eade4] .p-progressbar .p-progressbar-value {\n  background-color: #607d8b;\n}\n[data-v-449eade4] .p-datepicker {\n  min-width: 25rem;\n}\n[data-v-449eade4] .p-datepicker td {\n  font-weight: 400;\n}\n[data-v-449eade4] .p-datatable.p-datatable-5nkap .p-datatable-header {\n  padding: 1rem;\n  text-align: left;\n  font-size: 1.5rem;\n}\n[data-v-449eade4] .p-datatable.p-datatable-5nkap .p-paginator {\n  padding: 0.3rem;\n}\n[data-v-449eade4] .p-datatable.p-datatable-5nkap .p-datatable-thead > tr > th {\n  text-align: left;\n}\n[data-v-449eade4] .p-datatable.p-datatable-5nkap .p-datatable-tbody > tr > td {\n  cursor: auto;\n}\n[data-v-449eade4] .p-datatable.p-datatable-5nkap .p-dropdown-label:not(.p-placeholder) {\n  text-transform: uppercase;\n}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./src/pages/workspace/reports/reportsAccountingBalance.vue":
/*!******************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsAccountingBalance.vue ***!
  \******************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _reportsAccountingBalance_vue_vue_type_template_id_2e5d8b96_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reportsAccountingBalance.vue?vue&type=template&id=2e5d8b96&ts=true */ "./src/pages/workspace/reports/reportsAccountingBalance.vue?vue&type=template&id=2e5d8b96&ts=true");
/* harmony import */ var _reportsAccountingBalance_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reportsAccountingBalance.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/reports/reportsAccountingBalance.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_reportsAccountingBalance_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_reportsAccountingBalance_vue_vue_type_template_id_2e5d8b96_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/reports/reportsAccountingBalance.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/reports/reportsAccountingJournal.vue":
/*!******************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsAccountingJournal.vue ***!
  \******************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _reportsAccountingJournal_vue_vue_type_template_id_4adc2671_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reportsAccountingJournal.vue?vue&type=template&id=4adc2671&ts=true */ "./src/pages/workspace/reports/reportsAccountingJournal.vue?vue&type=template&id=4adc2671&ts=true");
/* harmony import */ var _reportsAccountingJournal_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reportsAccountingJournal.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/reports/reportsAccountingJournal.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_reportsAccountingJournal_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_reportsAccountingJournal_vue_vue_type_template_id_4adc2671_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/reports/reportsAccountingJournal.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/reports/reportsArchiving.vue":
/*!**********************************************************!*\
  !*** ./src/pages/workspace/reports/reportsArchiving.vue ***!
  \**********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _reportsArchiving_vue_vue_type_template_id_449eade4_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reportsArchiving.vue?vue&type=template&id=449eade4&scoped=true&ts=true */ "./src/pages/workspace/reports/reportsArchiving.vue?vue&type=template&id=449eade4&scoped=true&ts=true");
/* harmony import */ var _reportsArchiving_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reportsArchiving.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/reports/reportsArchiving.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _reportsArchiving_vue_vue_type_style_index_0_id_449eade4_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./reportsArchiving.vue?vue&type=style&index=0&id=449eade4&lang=scss&scoped=true */ "./src/pages/workspace/reports/reportsArchiving.vue?vue&type=style&index=0&id=449eade4&lang=scss&scoped=true");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;


const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_3__["default"])(_reportsArchiving_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_reportsArchiving_vue_vue_type_template_id_449eade4_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__scopeId',"data-v-449eade4"],['__file',"src/pages/workspace/reports/reportsArchiving.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/reports/reportsBankAndFund.vue":
/*!************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsBankAndFund.vue ***!
  \************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _reportsBankAndFund_vue_vue_type_template_id_2f24fd2f_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reportsBankAndFund.vue?vue&type=template&id=2f24fd2f&ts=true */ "./src/pages/workspace/reports/reportsBankAndFund.vue?vue&type=template&id=2f24fd2f&ts=true");
/* harmony import */ var _reportsBankAndFund_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reportsBankAndFund.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/reports/reportsBankAndFund.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_reportsBankAndFund_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_reportsBankAndFund_vue_vue_type_template_id_2f24fd2f_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/reports/reportsBankAndFund.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/reports/reportsBuyAndSelling.vue":
/*!**************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsBuyAndSelling.vue ***!
  \**************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _reportsBuyAndSelling_vue_vue_type_template_id_27ae792e_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reportsBuyAndSelling.vue?vue&type=template&id=27ae792e&ts=true */ "./src/pages/workspace/reports/reportsBuyAndSelling.vue?vue&type=template&id=27ae792e&ts=true");
/* harmony import */ var _reportsBuyAndSelling_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reportsBuyAndSelling.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/reports/reportsBuyAndSelling.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_reportsBuyAndSelling_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_reportsBuyAndSelling_vue_vue_type_template_id_27ae792e_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/reports/reportsBuyAndSelling.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/reports/reportsCreances.vue":
/*!*********************************************************!*\
  !*** ./src/pages/workspace/reports/reportsCreances.vue ***!
  \*********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _reportsCreances_vue_vue_type_template_id_3673f4af_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reportsCreances.vue?vue&type=template&id=3673f4af&ts=true */ "./src/pages/workspace/reports/reportsCreances.vue?vue&type=template&id=3673f4af&ts=true");
/* harmony import */ var _reportsCreances_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reportsCreances.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/reports/reportsCreances.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_reportsCreances_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_reportsCreances_vue_vue_type_template_id_3673f4af_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/reports/reportsCreances.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/reports/reportsDebts.vue":
/*!******************************************************!*\
  !*** ./src/pages/workspace/reports/reportsDebts.vue ***!
  \******************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _reportsDebts_vue_vue_type_template_id_7231f54f_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reportsDebts.vue?vue&type=template&id=7231f54f&ts=true */ "./src/pages/workspace/reports/reportsDebts.vue?vue&type=template&id=7231f54f&ts=true");
/* harmony import */ var _reportsDebts_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reportsDebts.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/reports/reportsDebts.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_reportsDebts_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_reportsDebts_vue_vue_type_template_id_7231f54f_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/reports/reportsDebts.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/reports/reportsExpenses.vue":
/*!*********************************************************!*\
  !*** ./src/pages/workspace/reports/reportsExpenses.vue ***!
  \*********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _reportsExpenses_vue_vue_type_template_id_3c5e8748_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reportsExpenses.vue?vue&type=template&id=3c5e8748&ts=true */ "./src/pages/workspace/reports/reportsExpenses.vue?vue&type=template&id=3c5e8748&ts=true");
/* harmony import */ var _reportsExpenses_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reportsExpenses.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/reports/reportsExpenses.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_reportsExpenses_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_reportsExpenses_vue_vue_type_template_id_3c5e8748_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/reports/reportsExpenses.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/reports/reportsImcomeStatement.vue":
/*!****************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsImcomeStatement.vue ***!
  \****************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _reportsImcomeStatement_vue_vue_type_template_id_e66e5214_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reportsImcomeStatement.vue?vue&type=template&id=e66e5214&ts=true */ "./src/pages/workspace/reports/reportsImcomeStatement.vue?vue&type=template&id=e66e5214&ts=true");
/* harmony import */ var _reportsImcomeStatement_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reportsImcomeStatement.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/reports/reportsImcomeStatement.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_reportsImcomeStatement_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_reportsImcomeStatement_vue_vue_type_template_id_e66e5214_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/reports/reportsImcomeStatement.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/reports/reportsLoans.vue":
/*!******************************************************!*\
  !*** ./src/pages/workspace/reports/reportsLoans.vue ***!
  \******************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _reportsLoans_vue_vue_type_template_id_380441dc_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reportsLoans.vue?vue&type=template&id=380441dc&ts=true */ "./src/pages/workspace/reports/reportsLoans.vue?vue&type=template&id=380441dc&ts=true");
/* harmony import */ var _reportsLoans_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reportsLoans.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/reports/reportsLoans.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_reportsLoans_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_reportsLoans_vue_vue_type_template_id_380441dc_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/reports/reportsLoans.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/reports/reportsOverview.vue":
/*!*********************************************************!*\
  !*** ./src/pages/workspace/reports/reportsOverview.vue ***!
  \*********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _reportsOverview_vue_vue_type_template_id_3b8d07ba_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reportsOverview.vue?vue&type=template&id=3b8d07ba&ts=true */ "./src/pages/workspace/reports/reportsOverview.vue?vue&type=template&id=3b8d07ba&ts=true");
/* harmony import */ var _reportsOverview_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reportsOverview.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/reports/reportsOverview.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_reportsOverview_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_reportsOverview_vue_vue_type_template_id_3b8d07ba_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/reports/reportsOverview.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/reports/reportsStocks.vue":
/*!*******************************************************!*\
  !*** ./src/pages/workspace/reports/reportsStocks.vue ***!
  \*******************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _reportsStocks_vue_vue_type_template_id_e4f1eac4_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reportsStocks.vue?vue&type=template&id=e4f1eac4&ts=true */ "./src/pages/workspace/reports/reportsStocks.vue?vue&type=template&id=e4f1eac4&ts=true");
/* harmony import */ var _reportsStocks_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reportsStocks.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/reports/reportsStocks.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_reportsStocks_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_reportsStocks_vue_vue_type_template_id_e4f1eac4_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/reports/reportsStocks.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/reports/reportsSubventions.vue":
/*!************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsSubventions.vue ***!
  \************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _reportsSubventions_vue_vue_type_template_id_1e2fcbaf_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reportsSubventions.vue?vue&type=template&id=1e2fcbaf&ts=true */ "./src/pages/workspace/reports/reportsSubventions.vue?vue&type=template&id=1e2fcbaf&ts=true");
/* harmony import */ var _reportsSubventions_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reportsSubventions.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/reports/reportsSubventions.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_reportsSubventions_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_reportsSubventions_vue_vue_type_template_id_1e2fcbaf_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/reports/reportsSubventions.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/reports/reportsThirdParties.vue":
/*!*************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsThirdParties.vue ***!
  \*************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _reportsThirdParties_vue_vue_type_template_id_1d401344_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reportsThirdParties.vue?vue&type=template&id=1d401344&ts=true */ "./src/pages/workspace/reports/reportsThirdParties.vue?vue&type=template&id=1d401344&ts=true");
/* harmony import */ var _reportsThirdParties_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reportsThirdParties.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/reports/reportsThirdParties.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_reportsThirdParties_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_reportsThirdParties_vue_vue_type_template_id_1d401344_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/reports/reportsThirdParties.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/reports/reportsAccountingBalance.vue?vue&type=script&setup=true&lang=ts":
/*!*****************************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsAccountingBalance.vue?vue&type=script&setup=true&lang=ts ***!
  \*****************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsAccountingBalance_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsAccountingBalance_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsAccountingBalance.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsAccountingBalance.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/reports/reportsAccountingJournal.vue?vue&type=script&setup=true&lang=ts":
/*!*****************************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsAccountingJournal.vue?vue&type=script&setup=true&lang=ts ***!
  \*****************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsAccountingJournal_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsAccountingJournal_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsAccountingJournal.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsAccountingJournal.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/reports/reportsArchiving.vue?vue&type=script&setup=true&lang=ts":
/*!*********************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsArchiving.vue?vue&type=script&setup=true&lang=ts ***!
  \*********************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsArchiving_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsArchiving_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsArchiving.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsArchiving.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/reports/reportsBankAndFund.vue?vue&type=script&setup=true&lang=ts":
/*!***********************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsBankAndFund.vue?vue&type=script&setup=true&lang=ts ***!
  \***********************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsBankAndFund_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsBankAndFund_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsBankAndFund.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsBankAndFund.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/reports/reportsBuyAndSelling.vue?vue&type=script&setup=true&lang=ts":
/*!*************************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsBuyAndSelling.vue?vue&type=script&setup=true&lang=ts ***!
  \*************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsBuyAndSelling_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsBuyAndSelling_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsBuyAndSelling.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsBuyAndSelling.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/reports/reportsCreances.vue?vue&type=script&setup=true&lang=ts":
/*!********************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsCreances.vue?vue&type=script&setup=true&lang=ts ***!
  \********************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsCreances_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsCreances_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsCreances.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsCreances.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/reports/reportsDebts.vue?vue&type=script&setup=true&lang=ts":
/*!*****************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsDebts.vue?vue&type=script&setup=true&lang=ts ***!
  \*****************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsDebts_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsDebts_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsDebts.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsDebts.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/reports/reportsExpenses.vue?vue&type=script&setup=true&lang=ts":
/*!********************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsExpenses.vue?vue&type=script&setup=true&lang=ts ***!
  \********************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsExpenses_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsExpenses_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsExpenses.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsExpenses.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/reports/reportsImcomeStatement.vue?vue&type=script&setup=true&lang=ts":
/*!***************************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsImcomeStatement.vue?vue&type=script&setup=true&lang=ts ***!
  \***************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsImcomeStatement_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsImcomeStatement_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsImcomeStatement.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsImcomeStatement.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/reports/reportsLoans.vue?vue&type=script&setup=true&lang=ts":
/*!*****************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsLoans.vue?vue&type=script&setup=true&lang=ts ***!
  \*****************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsLoans_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsLoans_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsLoans.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsLoans.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/reports/reportsOverview.vue?vue&type=script&setup=true&lang=ts":
/*!********************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsOverview.vue?vue&type=script&setup=true&lang=ts ***!
  \********************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsOverview_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsOverview_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsOverview.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsOverview.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/reports/reportsStocks.vue?vue&type=script&setup=true&lang=ts":
/*!******************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsStocks.vue?vue&type=script&setup=true&lang=ts ***!
  \******************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsStocks_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsStocks_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsStocks.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsStocks.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/reports/reportsSubventions.vue?vue&type=script&setup=true&lang=ts":
/*!***********************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsSubventions.vue?vue&type=script&setup=true&lang=ts ***!
  \***********************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsSubventions_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsSubventions_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsSubventions.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsSubventions.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/reports/reportsThirdParties.vue?vue&type=script&setup=true&lang=ts":
/*!************************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsThirdParties.vue?vue&type=script&setup=true&lang=ts ***!
  \************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsThirdParties_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsThirdParties_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsThirdParties.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsThirdParties.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/reports/reportsAccountingBalance.vue?vue&type=template&id=2e5d8b96&ts=true":
/*!********************************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsAccountingBalance.vue?vue&type=template&id=2e5d8b96&ts=true ***!
  \********************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsAccountingBalance_vue_vue_type_template_id_2e5d8b96_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsAccountingBalance_vue_vue_type_template_id_2e5d8b96_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsAccountingBalance.vue?vue&type=template&id=2e5d8b96&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsAccountingBalance.vue?vue&type=template&id=2e5d8b96&ts=true");


/***/ }),

/***/ "./src/pages/workspace/reports/reportsAccountingJournal.vue?vue&type=template&id=4adc2671&ts=true":
/*!********************************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsAccountingJournal.vue?vue&type=template&id=4adc2671&ts=true ***!
  \********************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsAccountingJournal_vue_vue_type_template_id_4adc2671_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsAccountingJournal_vue_vue_type_template_id_4adc2671_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsAccountingJournal.vue?vue&type=template&id=4adc2671&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsAccountingJournal.vue?vue&type=template&id=4adc2671&ts=true");


/***/ }),

/***/ "./src/pages/workspace/reports/reportsArchiving.vue?vue&type=template&id=449eade4&scoped=true&ts=true":
/*!************************************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsArchiving.vue?vue&type=template&id=449eade4&scoped=true&ts=true ***!
  \************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsArchiving_vue_vue_type_template_id_449eade4_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsArchiving_vue_vue_type_template_id_449eade4_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsArchiving.vue?vue&type=template&id=449eade4&scoped=true&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsArchiving.vue?vue&type=template&id=449eade4&scoped=true&ts=true");


/***/ }),

/***/ "./src/pages/workspace/reports/reportsBankAndFund.vue?vue&type=template&id=2f24fd2f&ts=true":
/*!**************************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsBankAndFund.vue?vue&type=template&id=2f24fd2f&ts=true ***!
  \**************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsBankAndFund_vue_vue_type_template_id_2f24fd2f_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsBankAndFund_vue_vue_type_template_id_2f24fd2f_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsBankAndFund.vue?vue&type=template&id=2f24fd2f&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsBankAndFund.vue?vue&type=template&id=2f24fd2f&ts=true");


/***/ }),

/***/ "./src/pages/workspace/reports/reportsBuyAndSelling.vue?vue&type=template&id=27ae792e&ts=true":
/*!****************************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsBuyAndSelling.vue?vue&type=template&id=27ae792e&ts=true ***!
  \****************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsBuyAndSelling_vue_vue_type_template_id_27ae792e_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsBuyAndSelling_vue_vue_type_template_id_27ae792e_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsBuyAndSelling.vue?vue&type=template&id=27ae792e&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsBuyAndSelling.vue?vue&type=template&id=27ae792e&ts=true");


/***/ }),

/***/ "./src/pages/workspace/reports/reportsCreances.vue?vue&type=template&id=3673f4af&ts=true":
/*!***********************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsCreances.vue?vue&type=template&id=3673f4af&ts=true ***!
  \***********************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsCreances_vue_vue_type_template_id_3673f4af_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsCreances_vue_vue_type_template_id_3673f4af_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsCreances.vue?vue&type=template&id=3673f4af&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsCreances.vue?vue&type=template&id=3673f4af&ts=true");


/***/ }),

/***/ "./src/pages/workspace/reports/reportsDebts.vue?vue&type=template&id=7231f54f&ts=true":
/*!********************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsDebts.vue?vue&type=template&id=7231f54f&ts=true ***!
  \********************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsDebts_vue_vue_type_template_id_7231f54f_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsDebts_vue_vue_type_template_id_7231f54f_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsDebts.vue?vue&type=template&id=7231f54f&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsDebts.vue?vue&type=template&id=7231f54f&ts=true");


/***/ }),

/***/ "./src/pages/workspace/reports/reportsExpenses.vue?vue&type=template&id=3c5e8748&ts=true":
/*!***********************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsExpenses.vue?vue&type=template&id=3c5e8748&ts=true ***!
  \***********************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsExpenses_vue_vue_type_template_id_3c5e8748_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsExpenses_vue_vue_type_template_id_3c5e8748_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsExpenses.vue?vue&type=template&id=3c5e8748&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsExpenses.vue?vue&type=template&id=3c5e8748&ts=true");


/***/ }),

/***/ "./src/pages/workspace/reports/reportsImcomeStatement.vue?vue&type=template&id=e66e5214&ts=true":
/*!******************************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsImcomeStatement.vue?vue&type=template&id=e66e5214&ts=true ***!
  \******************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsImcomeStatement_vue_vue_type_template_id_e66e5214_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsImcomeStatement_vue_vue_type_template_id_e66e5214_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsImcomeStatement.vue?vue&type=template&id=e66e5214&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsImcomeStatement.vue?vue&type=template&id=e66e5214&ts=true");


/***/ }),

/***/ "./src/pages/workspace/reports/reportsLoans.vue?vue&type=template&id=380441dc&ts=true":
/*!********************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsLoans.vue?vue&type=template&id=380441dc&ts=true ***!
  \********************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsLoans_vue_vue_type_template_id_380441dc_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsLoans_vue_vue_type_template_id_380441dc_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsLoans.vue?vue&type=template&id=380441dc&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsLoans.vue?vue&type=template&id=380441dc&ts=true");


/***/ }),

/***/ "./src/pages/workspace/reports/reportsOverview.vue?vue&type=template&id=3b8d07ba&ts=true":
/*!***********************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsOverview.vue?vue&type=template&id=3b8d07ba&ts=true ***!
  \***********************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsOverview_vue_vue_type_template_id_3b8d07ba_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsOverview_vue_vue_type_template_id_3b8d07ba_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsOverview.vue?vue&type=template&id=3b8d07ba&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsOverview.vue?vue&type=template&id=3b8d07ba&ts=true");


/***/ }),

/***/ "./src/pages/workspace/reports/reportsStocks.vue?vue&type=template&id=e4f1eac4&ts=true":
/*!*********************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsStocks.vue?vue&type=template&id=e4f1eac4&ts=true ***!
  \*********************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsStocks_vue_vue_type_template_id_e4f1eac4_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsStocks_vue_vue_type_template_id_e4f1eac4_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsStocks.vue?vue&type=template&id=e4f1eac4&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsStocks.vue?vue&type=template&id=e4f1eac4&ts=true");


/***/ }),

/***/ "./src/pages/workspace/reports/reportsSubventions.vue?vue&type=template&id=1e2fcbaf&ts=true":
/*!**************************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsSubventions.vue?vue&type=template&id=1e2fcbaf&ts=true ***!
  \**************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsSubventions_vue_vue_type_template_id_1e2fcbaf_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsSubventions_vue_vue_type_template_id_1e2fcbaf_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsSubventions.vue?vue&type=template&id=1e2fcbaf&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsSubventions.vue?vue&type=template&id=1e2fcbaf&ts=true");


/***/ }),

/***/ "./src/pages/workspace/reports/reportsThirdParties.vue?vue&type=template&id=1d401344&ts=true":
/*!***************************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsThirdParties.vue?vue&type=template&id=1d401344&ts=true ***!
  \***************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsThirdParties_vue_vue_type_template_id_1d401344_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsThirdParties_vue_vue_type_template_id_1d401344_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsThirdParties.vue?vue&type=template&id=1d401344&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsThirdParties.vue?vue&type=template&id=1d401344&ts=true");


/***/ }),

/***/ "./src/pages/workspace/reports/reportsArchiving.vue?vue&type=style&index=0&id=449eade4&lang=scss&scoped=true":
/*!*******************************************************************************************************************!*\
  !*** ./src/pages/workspace/reports/reportsArchiving.vue?vue&type=style&index=0&id=449eade4&lang=scss&scoped=true ***!
  \*******************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsArchiving_vue_vue_type_style_index_0_id_449eade4_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!../../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../../node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsArchiving.vue?vue&type=style&index=0&id=449eade4&lang=scss&scoped=true */ "./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsArchiving.vue?vue&type=style&index=0&id=449eade4&lang=scss&scoped=true");
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsArchiving_vue_vue_type_style_index_0_id_449eade4_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsArchiving_vue_vue_type_style_index_0_id_449eade4_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsArchiving_vue_vue_type_style_index_0_id_449eade4_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = function(key) { return _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_reportsArchiving_vue_vue_type_style_index_0_id_449eade4_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__[key]; }.bind(0, __WEBPACK_IMPORT_KEY__)
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);


/***/ }),

/***/ "./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsArchiving.vue?vue&type=style&index=0&id=449eade4&lang=scss&scoped=true":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsArchiving.vue?vue&type=style&index=0&id=449eade4&lang=scss&scoped=true ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(/*! !!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!../../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../../node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./reportsArchiving.vue?vue&type=style&index=0&id=449eade4&lang=scss&scoped=true */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/reports/reportsArchiving.vue?vue&type=style&index=0&id=449eade4&lang=scss&scoped=true");
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(/*! !../../../../node_modules/vue-style-loader/lib/addStylesClient.js */ "./node_modules/vue-style-loader/lib/addStylesClient.js")["default"])
var update = add("22af472f", content, false, {"sourceMap":false,"shadowMode":false});
// Hot Module Replacement
if(false) {}

/***/ })

}]);
//# sourceMappingURL=workspace-reports.js.map