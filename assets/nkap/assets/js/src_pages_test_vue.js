"use strict";
(self["webpackChunk_5nkap"] = self["webpackChunk_5nkap"] || []).push([["src_pages_test_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/test.vue?vue&type=script&setup=true&lang=ts":
/*!**********************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/test.vue?vue&type=script&setup=true&lang=ts ***!
  \**********************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _utils_excel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../utils/excel */ "./src/utils/excel/index.ts");
/* harmony import */ var _utils_fileReader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../utils/fileReader */ "./src/utils/fileReader/index.ts");
/* harmony import */ var jspdf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jspdf */ "./node_modules/jspdf/dist/jspdf.es.min.js");
/* harmony import */ var jspdf_autotable__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jspdf-autotable */ "./node_modules/jspdf-autotable/dist/jspdf.plugin.autotable.js");
/* harmony import */ var jspdf_autotable__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(jspdf_autotable__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var jsprintmanager_JSPrintManager_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! jsprintmanager/JSPrintManager.js */ "./node_modules/jsprintmanager/JSPrintManager.js");
/* harmony import */ var jsprintmanager_JSPrintManager_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(jsprintmanager_JSPrintManager_js__WEBPACK_IMPORTED_MODULE_6__);




// import { isSpeechRecognition, runRecognition } from "./../composable/useAPISpeech"




/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'test',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    jsprintmanager_JSPrintManager_js__WEBPACK_IMPORTED_MODULE_6__.JSPrintManager.auto_reconnect = true;
    jsprintmanager_JSPrintManager_js__WEBPACK_IMPORTED_MODULE_6__.JSPrintManager.start();
    // JSPM.JSPrintManager.WS.onStatusChanged = function () {
    //     if (jspmWSStatus()) {
    //         //get client installed printers
    //         JSPM.JSPrintManager.getPrinters().then( (myPrinters:any) =>{
    //             var options = '';
    //             for (var i = 0; i < myPrinters.length; i++) {
    //                 options += '<option>' + myPrinters[i] + '</option>';
    //             }
    //             $('#installedPrinterName').html(options);
    //         });
    //     }
    // };
    //Check JSPM WebSocket status
    function jspmWSStatus() {
      if (jsprintmanager_JSPrintManager_js__WEBPACK_IMPORTED_MODULE_6__.JSPrintManager.websocket_status == jsprintmanager_JSPrintManager_js__WEBPACK_IMPORTED_MODULE_6__.WSStatus.Open) {
        return true;
      } else if (jsprintmanager_JSPrintManager_js__WEBPACK_IMPORTED_MODULE_6__.JSPrintManager.websocket_status == jsprintmanager_JSPrintManager_js__WEBPACK_IMPORTED_MODULE_6__.WSStatus.Closed) {
        alert("JSPrintManager (JSPM) is not installed or not running! Download JSPM Client App from https://neodynamic.com/downloads/jspm");
        return false;
      } else if (jsprintmanager_JSPrintManager_js__WEBPACK_IMPORTED_MODULE_6__.JSPrintManager.websocket_status == jsprintmanager_JSPrintManager_js__WEBPACK_IMPORTED_MODULE_6__.WSStatus.Blocked) {
        alert("JSPM has blocked this website!");
        return false;
      }
    }
    //Do printing...
    function printt() {
      if (jspmWSStatus()) {
        //Create a ClientPrintJob
        let cpj = new jsprintmanager_JSPrintManager_js__WEBPACK_IMPORTED_MODULE_6__.ClientPrintJob();
        cpj.clientPrinter = new jsprintmanager_JSPrintManager_js__WEBPACK_IMPORTED_MODULE_6__.DefaultPrinter();
        //Set content to print...
        //Create ESP/POS commands for sample label
        let esc = "\x1B"; //ESC byte in hex notation
        let newLine = "\x0A"; //LF byte in hex notation
        let cutPaper = "\x0A\x0A\x0A\x0A\x0A" + "\x1D" + "\x56\x01" + "255";
        let GS = "\x1D";
        let cmds = esc + "@"; //Initializes the printer (ESC @)
        cmds += esc + "!" + "\x38"; //Emphasized + Double-height + Double-width mode selected (ESC ! (8 + 16 + 32)) 56 dec => 38 hex
        cmds += "APPS LERNEN S.A"; //text to print
        cmds += newLine + newLine;
        cmds += esc + "!" + "\x00"; //Character font A selected (ESC ! 0)
        cmds += "COOKIES                   5.00";
        cmds += newLine;
        cmds += "MILK 65 Fl oz             3.78";
        cmds += newLine + newLine;
        cmds += "SUBTOTAL                  8.78";
        cmds += newLine;
        cmds += "TAX 5%                    0.44";
        cmds += newLine;
        cmds += "TOTAL                     9.22";
        cmds += newLine;
        cmds += "CASH TEND                10.00";
        cmds += newLine;
        cmds += "CASH DUE                  0.78";
        cmds += newLine;
        cmds += "AAAAAAAAAAAAAAA...............AAAAAAAAAAAAAAAAA";
        cmds += newLine + newLine;
        cmds += esc + "!" + "\x18"; //Emphasized + Double-height mode selected (ESC ! (16 + 8)) 24 dec => 18 hex
        cmds += "# ITEMS SOLD 2";
        cmds += esc + "!" + "\x00"; //Character font A selected (ESC ! 0)
        cmds += newLine + newLine;
        cmds += "11/03/13  19:53:17";
        cmds += cutPaper;
        cpj.printerCommands = cmds;
        //Send print job to printer!
        cpj.sendToClient();
      }
    }
    const input = {
      title: "Sample",
      thankyouNote: "Welcome...!"
    };
    //applyPlugin(jsPDF)
    function change(evt) {
      (0,_utils_excel__WEBPACK_IMPORTED_MODULE_1__.readExcelFile)(evt.target.files).then(data => {
        // console.log("readFile(evt.target.files): ", data);
      });
    }
    function exported() {
      (0,_utils_excel__WEBPACK_IMPORTED_MODULE_1__.ExportToExcelFile)([{
        champ1: "valeur 1",
        champ2: "valeur 2"
      }, {
        champ1: "valeur 1.1",
        champ2: "valeur 2.2"
      }], "Excel-file-by-zile");
    }
    const dataResume = [["1027", "977", "53.7", "1030.7"]];
    const dataBaseInfo = [["Cash", "Euros", "RC20210827125694"]];
    const data = [[1, "Règle", 7632, 1500, 1500000, "10 %", "15 FCFA"], [2, "PC", 7594, 3000, 1500000, "10 %", "15 FCFA"], [3, "Table", 7555, 200, 1500000, "10 %", "15 FCFA"], [4, "Chaise", 7495, 7000, 1500000, "10 %", "15 FCFA"], [5, "Armoire", 7487, 15000, 1500000, "10 %", "15 FCFA"], [9, "Telephone", 7314, 150000, 1500000, "10 %", "15 FCFA"], [10, "Tablette", 5483, 200000, 1500000, "10 %", "15 FCFA"]];
    function uploadImageToURLData(evt) {
      (0,_utils_fileReader__WEBPACK_IMPORTED_MODULE_2__.getDataURLFromImage)(evt.target.files[0]).then(data => {
        const doc = new jspdf__WEBPACK_IMPORTED_MODULE_3__.jsPDF("p", "cm", "a4");
        setHeader(data, doc, 0);
        // console.log("dataURL BASE: ", data);
        setSubHeaderInfo(doc, "FACTURE", "VENTE DE PRODUITS", "No 20210827-00001", "12 Novembre 2021");
        setClientOrOwnerInfo(doc, {
          name: "BIGBANG LDT",
          phone: "655 00 00 00",
          address: "Yaoundé Carrefour Ceper",
          email: "bigbangltd@gmail.com"
        }, "Owner");
        setClientOrOwnerInfo(doc, {
          name: "BIGBANG LDT",
          phone: "655 00 00 00",
          address: "Yaoundé Carrefour Ceper",
          email: "bigbangltd@gmail.com"
        });
        execAutoTable(doc, [], [], []);
        // Resume Section
        resumeSection("1050 euros", "19.3 euros", doc);
        //Signature
        signature(doc);
        // Copyright
        copyRigth(doc);
        doc.save("table one.pdf");
      });
    }
    /**
     *
     */
    function resumeSection(amountReceipt, amountReimbursed, doc, textAmount1 = "", textAmount2 = "") {
      const text1 = textAmount1 == "" ? "Montant reçu  : " : textAmount1;
      doc.setFontSize(12);
      doc.text(text1, 1.4, doc.lastAutoTable.finalY + 2);
      doc.text(amountReceipt, getXOffset(text1, doc, "left"), doc.lastAutoTable.finalY + 2);
      const test2 = textAmount2 == "" ? "Montant restant : " : textAmount2;
      doc.setFontSize(12);
      doc.text(test2, 1.4, doc.lastAutoTable.finalY + 3);
      doc.text(amountReimbursed, getXOffset(test2, doc, "left"), doc.lastAutoTable.finalY + 3);
    }
    /**
     *
     */
    function signature(doc, text = null) {
      const signature = text ? text : "Bigbang LDT";
      doc.setFontSize(16);
      doc.text(signature, getXOffset(signature, doc, "right") - 2, doc.lastAutoTable.finalY + 4);
    }
    /**
     *
     */
    function copyRigth(doc, text = null) {
      const copyRight = text ? text : "Générée via www.5nkap.net";
      doc.setFontSize(8);
      doc.text(copyRight, 21 - 0.5, doc.lastAutoTable.finalY + 5, {
        angle: 90
      });
    }
    /**
     *
     */
    function setHeader(data, doc, model) {
      switch (model) {
        case 1:
          //
          break;
        default:
          doc.addImage(data, "JPEG", 0, 0, 21, 3, "alias", "MEDIUM", 0);
          break;
      }
    }
    /**
     *
     */
    function setSubHeaderInfo(doc, title, raison, quoteNumber, date) {
      doc.setTextColor(0o0);
      doc.setFontSize(16);
      const text = lodash__WEBPACK_IMPORTED_MODULE_5__.toUpper(title) + " / " + lodash__WEBPACK_IMPORTED_MODULE_5__.toUpper(raison);
      doc.text(text, getXOffset(text, doc, "center"), 5);
      doc.setFontSize(10);
      doc.setTextColor(100);
      const text2 = quoteNumber + " | " + date;
      doc.text(text2, getXOffset(text2, doc, "center"), 5.5, {
        /*align:"left", maxWidth: 5*/
      });
    }
    /**
     *
     */
    function setClientOrOwnerInfo(doc, data, type = "Client") {
      const y = 2;
      const line1 = type != "Owner" ? "Client" : "Marchant";
      const line2 = "Telephone";
      const line3 = "Adresse";
      const line4 = "E-mail";
      doc.setTextColor(100);
      switch (type) {
        case "Owner":
          doc.setFontSize(10);
          doc.text(line1, 1.4, 5 + y /*{align: "left"}*/);
          doc.setFontSize(12);
          doc.text(data.name, 1.4, 5.5 + y /*{align: "left"}*/);
          doc.setFontSize(12);
          doc.text(line2, 1.4, 6.2 + y);
          doc.setFontSize(10);
          doc.text(data.phone, 1.4, 6.7 + y);
          doc.setFontSize(10);
          doc.text(line3, 1.4, 7.4 + y);
          doc.setFontSize(12);
          doc.text(data.address, 1.4, 7.9 + y);
          doc.setFontSize(10);
          doc.text(line4, 1.4, 8.6 + y);
          doc.setFontSize(12);
          doc.text(data.email, 1.4, 9.1 + y);
          break;
        default:
          doc.setFontSize(10);
          doc.text(line1, getXOffset(line1, doc), 5 + y /*{align: "left"}*/);
          doc.setFontSize(12);
          doc.text(data.name, getXOffset(data.name, doc), 5.5 + y /*{align: "left"}*/);
          doc.setFontSize(12);
          doc.text(line2, getXOffset(line2, doc), 6.2 + y);
          doc.setFontSize(10);
          doc.text(data.phone, getXOffset(data.phone, doc), 6.7 + y);
          doc.setFontSize(10);
          doc.text(line3, getXOffset(line3, doc), 7.4 + y);
          doc.setFontSize(12);
          doc.text(data.address, getXOffset(data.address, doc), 7.9 + y);
          doc.setFontSize(10);
          doc.text(line4, getXOffset(line4, doc), 8.6 + y);
          doc.setFontSize(12);
          doc.text(data.email, getXOffset(data.email, doc), 9.1 + y);
          break;
      }
    }
    /**
     *
     */
    function getXOffset(text, doc, position = "") {
      switch (position) {
        case "left":
          return doc.getTextWidth(text) + 1.4 + 0.2;
        // 1.4 -> Margin-left, 0.4 -> Ecart
        case "center":
          return doc.internal.pageSize.width / 2 - doc.getTextWidth(text) / 2;
        default:
          //Right Position
          return doc.internal.pageSize.width - 1.4 - doc.getTextWidth(text);
        // 1.4 -> Margin Right
      }
    }
    /**
     *
     */
    function execAutoTable(doc, baseDataQuote, itemDataQuote, resumeDataQuote) {
      jspdf_autotable__WEBPACK_IMPORTED_MODULE_4___default()(doc, {
        head: headBaseInfoQuote(),
        body: dataBaseInfo,
        startY: 12
        // columnStyles: {
        // 	No: { fillColor: [41, 0o0, 185], textColor: 255, fontStyle: 'bold' },
        // },
        // headStyles: {
        // fillColor: "#fff"
        // }
      });

      jspdf_autotable__WEBPACK_IMPORTED_MODULE_4___default()(doc, {
        head: headItemsQuote(),
        body: data,
        startY: doc.lastAutoTable.finalY + 1
      });
      jspdf_autotable__WEBPACK_IMPORTED_MODULE_4___default()(doc, {
        head: headResume(),
        body: dataResume,
        startY: doc.lastAutoTable.finalY + 1
      });
    }
    /**
     *
     */
    function headResume() {
      return [{
        totalHT: "Total HT",
        totalWithReduce: "Total Reduction Incluse",
        totalTVA: "Total TVA",
        total: "Net à Payer"
      }];
    }
    /**
     *
     */
    function headBaseInfoQuote() {
      return [{
        payment: "Mode de paiement",
        currency: "Dévise",
        ref: "Référence"
      }];
    }
    /**
     *
     */
    function headItemsQuote() {
      return [{
        No: "No",
        label: "Libellé",
        quantity: "Qté",
        priceHT: "PU HT",
        totalPriceHT: "PT HT",
        taxes: "Taxes"
      }];
    }
    const micMic = document.querySelector("#startMic");
    micMic?.addEventListener;
    let fruit = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    const filteredFruits = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(["apple", "orange", "grape"]);
    const fruits = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(["apple", "orange", "grape"]);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onMounted)(() => {
      //
    });
    const __returned__ = {
      jspmWSStatus,
      printt,
      input,
      change,
      exported,
      dataResume,
      dataBaseInfo,
      data,
      uploadImageToURLData,
      resumeSection,
      signature,
      copyRigth,
      setHeader,
      setSubHeaderInfo,
      setClientOrOwnerInfo,
      getXOffset,
      execAutoTable,
      headResume,
      headBaseInfoQuote,
      headItemsQuote,
      micMic,
      get fruit() {
        return fruit;
      },
      set fruit(v) {
        fruit = v;
      },
      filteredFruits,
      fruits,
      get fileExtensions() {
        return _utils_excel__WEBPACK_IMPORTED_MODULE_1__.fileExtensions;
      },
      get fileExtensionsImg() {
        return _utils_fileReader__WEBPACK_IMPORTED_MODULE_2__.fileExtensionsImg;
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/test.vue?vue&type=template&id=2f56c616&ts=true":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/test.vue?vue&type=template&id=2f56c616&ts=true ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "p-24"
};
const _hoisted_2 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", {
  class: "pb-6 text-xl font-normal uppercase"
}, "Test de XLSX JS", -1 /* HOISTED */);
const _hoisted_3 = ["accept"];
const _hoisted_4 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */);
const _hoisted_5 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */);
const _hoisted_6 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  id: "out-table"
}, null, -1 /* HOISTED */);
const _hoisted_7 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", {
  class: "mt-12 mb-6 text-xl font-normal uppercase"
}, "Test de JSPDF", -1 /* HOISTED */);
const _hoisted_8 = ["accept"];
const _hoisted_9 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", {
  class: "mt-12 mb-6 text-xl font-normal uppercase"
}, "Test de JSPDF", -1 /* HOISTED */);
function render(_ctx, _cache, $props, $setup, $data, $options) {
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [_hoisted_2, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    id: "sheetjs-input",
    type: "file",
    multiple: "false",
    accept: $setup.fileExtensions,
    onChange: _cache[0] || (_cache[0] = e => $setup.change(e))
  }, null, 40 /* PROPS, HYDRATE_EVENTS */, _hoisted_3), _hoisted_4, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("button", {
    id: "export-table",
    type: "button",
    stylee: "visibility:hidden",
    onClick: _cache[1] || (_cache[1] = e => $setup.exported())
  }, " Export to XLSX "), _hoisted_5, _hoisted_6]), _hoisted_7, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    type: "file",
    accept: $setup.fileExtensionsImg,
    onChange: _cache[2] || (_cache[2] = e => $setup.uploadImageToURLData(e))
  }, null, 40 /* PROPS, HYDRATE_EVENTS */, _hoisted_8), _hoisted_9, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("button", {
    type: "button",
    onClick: _cache[3] || (_cache[3] = e => $setup.printt())
  }, "Print Now..."), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <p class=\"mt-12 mb-6 text-xl font-normal uppercase\">Test de API Speech Recognition</p>\n\t\t<form action=\"\">\n\t\t\t<button id=\"startMic\" @click.prevent=\"() => runRecognition('start')\">Start</button>\n\n\t\t\t<button id=\"stopMic\" @click.prevent=\"() => runRecognition('stop')\">Stop</button>\n\t\t</form> "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <Calendar v-model=\"value\" style=\"border-radius: 50px\" selectionMode=\"range\" />\n\t\t<AutoComplete\n\t\t\tv-model=\"fruit\"\n\t\t\t:suggestions=\"filteredFruits\"\n\t\t\t@complete=\"search($event)\"\n\t\t\tfield=\"fruit\"\n\t\t>\n\t\t\t<template v-slot:item>\n\t\t\t\t<div>\n\t\t\t\t\t<div>{{ item }}</div>\n\t\t\t\t</div>\n\t\t\t</template>\n\t\t</AutoComplete> ")]);
}

/***/ }),

/***/ "./src/utils/fileReader/index.ts":
/*!***************************************!*\
  !*** ./src/utils/fileReader/index.ts ***!
  \***************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   fileExtensionsImg: function() { return /* binding */ fileExtensionsImg; },
/* harmony export */   getDataURLFromImage: function() { return /* binding */ getDataURLFromImage; }
/* harmony export */ });
const fileExtensionsImg = ["jpeg", "jpg", "png", "svg"].map(function (x) {
  return "." + x;
}).join(",");
function getDataURLFromImage(file) {
  return new Promise((resolve, rejects) => {
    const reader = new FileReader();
    let rawImg;
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      rawImg = reader.result;
      resolve(rawImg);
    };
  });
}


/***/ }),

/***/ "./src/pages/test.vue":
/*!****************************!*\
  !*** ./src/pages/test.vue ***!
  \****************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _test_vue_vue_type_template_id_2f56c616_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./test.vue?vue&type=template&id=2f56c616&ts=true */ "./src/pages/test.vue?vue&type=template&id=2f56c616&ts=true");
/* harmony import */ var _test_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./test.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/test.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_test_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_test_vue_vue_type_template_id_2f56c616_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/test.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/test.vue?vue&type=script&setup=true&lang=ts":
/*!***************************************************************!*\
  !*** ./src/pages/test.vue?vue&type=script&setup=true&lang=ts ***!
  \***************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_test_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_test_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/babel-loader/lib/index.js!../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./test.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/test.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/test.vue?vue&type=template&id=2f56c616&ts=true":
/*!******************************************************************!*\
  !*** ./src/pages/test.vue?vue&type=template&id=2f56c616&ts=true ***!
  \******************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_test_vue_vue_type_template_id_2f56c616_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_test_vue_vue_type_template_id_2f56c616_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/babel-loader/lib/index.js!../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./test.vue?vue&type=template&id=2f56c616&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/test.vue?vue&type=template&id=2f56c616&ts=true");


/***/ })

}]);
//# sourceMappingURL=src_pages_test_vue.js.map