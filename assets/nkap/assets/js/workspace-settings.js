(self["webpackChunk_5nkap"] = self["webpackChunk_5nkap"] || []).push([["workspace-settings"],{

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsBusinessAccount.vue?vue&type=script&setup=true&lang=ts":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsBusinessAccount.vue?vue&type=script&setup=true&lang=ts ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_web_url_search_params_delete_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/web.url-search-params.delete.js */ "./node_modules/core-js/modules/web.url-search-params.delete.js");
/* harmony import */ var core_js_modules_web_url_search_params_delete_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_url_search_params_delete_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_web_url_search_params_has_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/web.url-search-params.has.js */ "./node_modules/core-js/modules/web.url-search-params.has.js");
/* harmony import */ var core_js_modules_web_url_search_params_has_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_url_search_params_has_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_web_url_search_params_size_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/web.url-search-params.size.js */ "./node_modules/core-js/modules/web.url-search-params.size.js");
/* harmony import */ var core_js_modules_web_url_search_params_size_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_url_search_params_size_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/api */ "./src/api/index.ts");
/* harmony import */ var _composable_useNotyf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/composable/useNotyf */ "./src/composable/useNotyf.ts");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _composable_useMediasLoader__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @/composable/useMediasLoader */ "./src/composable/useMediasLoader.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");
/* harmony import */ var _state_services_settingState__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @/state/services/settingState */ "./src/state/services/settingState.ts");














const defaultImage = "/images/avatar-logo/avatar-profile-entreprise.png";
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_3__.defineComponent)({
  __name: 'settingsBusinessAccount',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_11__.u)({
      title: "Paramètres / Comptes business"
    });
    const view = (0,vue__WEBPACK_IMPORTED_MODULE_3__.ref)("default");
    /**
     * Composables
     */
    const notify = (0,_composable_useNotyf__WEBPACK_IMPORTED_MODULE_5__["default"])();
    /**
     * Variables
     */
    const isLoading = (0,vue__WEBPACK_IMPORTED_MODULE_3__.ref)(false);
    const showDescriptionView = (0,vue__WEBPACK_IMPORTED_MODULE_3__.ref)(false);
    const moreInfosArray = (0,vue__WEBPACK_IMPORTED_MODULE_3__.ref)([]);
    const enterpriseName = (0,vue__WEBPACK_IMPORTED_MODULE_3__.ref)({
      editable: false,
      content: "",
      title: "Nom de l'entreprise"
    });
    const phone = (0,vue__WEBPACK_IMPORTED_MODULE_3__.ref)({
      editable: false,
      content: "",
      title: "Numéro de téléphone"
    });
    const pays = (0,vue__WEBPACK_IMPORTED_MODULE_3__.ref)({
      editable: false,
      content: "",
      title: "Pays"
    });
    const registre_commerce = (0,vue__WEBPACK_IMPORTED_MODULE_3__.ref)({
      editable: false,
      content: "",
      title: "Régistre du Commerce"
    });
    const forme_juridique = (0,vue__WEBPACK_IMPORTED_MODULE_3__.ref)({
      editable: false,
      content: "",
      title: "Forme Juridique"
    });
    const ville = (0,vue__WEBPACK_IMPORTED_MODULE_3__.ref)({
      editable: false,
      content: "",
      title: "Ville"
    });
    const code_postal = (0,vue__WEBPACK_IMPORTED_MODULE_3__.ref)({
      editable: false,
      content: "",
      title: "Code Postal"
    });
    const numero_identifiant_unique = (0,vue__WEBPACK_IMPORTED_MODULE_3__.ref)({
      editable: false,
      content: "",
      title: "Matricule Fiscal"
    });
    const profile = (0,vue__WEBPACK_IMPORTED_MODULE_3__.ref)({
      view: "/images/avatar-logo/avatar-profile-entreprise.png",
      title: "Cliquer pour modifer le logo de l'entreprise",
      editable: false,
      content: [],
      response: [],
      show: false
    });
    const bussinessAccountInfos1 = (0,vue__WEBPACK_IMPORTED_MODULE_3__.reactive)([enterpriseName, pays, phone]);
    const currencyObservable = (0,vue__WEBPACK_IMPORTED_MODULE_3__.ref)("");
    const moreInfos = (0,vue__WEBPACK_IMPORTED_MODULE_3__.reactive)({
      footer_print: {
        editable: false,
        loading: false,
        content: "",
        title: "Pied de Facturettes"
      },
      slogan: {
        editable: false,
        loading: false,
        content: "",
        title: "Slogan"
      },
      color_theme: {
        editable: false,
        loading: false,
        content: "",
        title: "Couleur de Thème",
        type: "color"
      },
      debut_exercice: {
        editable: false,
        loading: false,
        content: "",
        title: "Debut d'Exercice",
        type: "date"
      },
      footer_text: {
        editable: false,
        loading: false,
        content: "",
        title: "Texte de Pied de Page"
      },
      header_text: {
        editable: false,
        loading: false,
        content: "",
        title: "Texte d'Entête de page"
      },
      periode: {
        editable: false,
        loading: false,
        content: "",
        title: "Periode",
        type: "number"
      },
      // Files
      signature_file_name: {
        view: "/images/avatar-logo/avatar-profile-entreprise.png",
        title: "Fichier de Signature Principale - 500x500 pixels",
        editable: false,
        content: [],
        response: [],
        show: false
      },
      header_file_name: {
        view: "/images/avatar-logo/avatar-profile-entreprise.png",
        title: "Fichier d'Entête de page (Mode Portrait - 2473x361 pixels)",
        editable: false,
        content: [],
        response: [],
        show: false
      },
      header_file_name_l: {
        view: "/images/avatar-logo/avatar-profile-entreprise.png",
        title: "Fichier d'Entête de page (Mode Paysage - 3508x361 pixels)",
        editable: false,
        content: [],
        response: [],
        show: false
      },
      footer_file_name: {
        view: "/images/avatar-logo/avatar-profile-entreprise.png",
        title: "Fichier de Pied de page (Mode Portrait - 2473x361 pixels)",
        editable: false,
        content: [],
        response: [],
        show: false
      },
      footer_file_name_l: {
        view: "/images/avatar-logo/avatar-profile-entreprise.png",
        title: "Fichier de Pied de page (Mode Paysage - 3508x361 pixels)",
        editable: false,
        content: [],
        response: [],
        show: false
      },
      cache_file_name: {
        view: _state_services_settingState__WEBPACK_IMPORTED_MODULE_10__.cacheFileName.value,
        title: "Fichier du cachet de l'entreprise - 500x500 pixels",
        editable: false,
        content: [],
        response: [],
        show: false
      }
    });
    const bussinessAccountInfos2 = (0,vue__WEBPACK_IMPORTED_MODULE_3__.reactive)([numero_identifiant_unique, code_postal, ville, forme_juridique, registre_commerce]);
    const areInfosModified = (0,vue__WEBPACK_IMPORTED_MODULE_3__.ref)(false);
    (0,vue__WEBPACK_IMPORTED_MODULE_3__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_6__.userCurrentBranch.value.id, newValue => {
      getSettings();
      isLoading.value = true;
      (0,_state_api_userState__WEBPACK_IMPORTED_MODULE_6__.refreshUserInfos)().then(() => {
        if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_9__["default"])("read", permissions_enterprise)) {
          getEnterpriseInfos();
        }
        isLoading.value = false;
        areInfosModified.value = false;
      }).catch(err => {
        notify.error("Erreur de récupération des informations de l'entreprise" + err.data?.response ? " : " + err.data.response.message : 0);
        isLoading.value = false;
      });
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_3__.watch)([bussinessAccountInfos1, bussinessAccountInfos2], ([newvalue]) => {
      if (newvalue) areInfosModified.value = true;
    }, {
      deep: true
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_3__.watch)(() => profile.value.content, async newValue => await fileChanged(profile.value, "logo_file_name", newValue));
    (0,vue__WEBPACK_IMPORTED_MODULE_3__.watch)(() => moreInfos.signature_file_name.content, async newValue => await fileChanged(moreInfos.signature_file_name, "signature_file_name", newValue));
    (0,vue__WEBPACK_IMPORTED_MODULE_3__.watch)(() => moreInfos.header_file_name.content, async newValue => await fileChanged(moreInfos.header_file_name, "header_file_name", newValue));
    (0,vue__WEBPACK_IMPORTED_MODULE_3__.watch)(() => moreInfos.header_file_name_l.content, async newValue => await fileChanged(moreInfos.header_file_name_l, "header_file_name_l", newValue));
    (0,vue__WEBPACK_IMPORTED_MODULE_3__.watch)(() => moreInfos.footer_file_name.content, async newValue => await fileChanged(moreInfos.footer_file_name, "footer_file_name", newValue));
    (0,vue__WEBPACK_IMPORTED_MODULE_3__.watch)(() => moreInfos.footer_file_name_l.content, async newValue => await fileChanged(moreInfos.footer_file_name_l, "footer_file_name_l", newValue));
    (0,vue__WEBPACK_IMPORTED_MODULE_3__.watch)(() => moreInfos.cache_file_name.content, async newValue => await fileChanged(moreInfos.cache_file_name, "cache_file_name", newValue));
    async function infoChanged(index) {
      moreInfos[index].loading = true;
      let data = {};
      data[index] = moreInfos[index].content;
      moreInfos[index].editable = false;
      await (0,_api__WEBPACK_IMPORTED_MODULE_4__["default"])().settings.files.set(data).then(resp => {
        notify.success(`${moreInfos[index].title} de l'entreprise mis à Jour`);
        moreInfos[index].loading = false;
      }).catch(err => {
        console.log(`Not enable to set ${moreInfos[index].title}`);
        notify.error("Erreur de mise à jour");
        profile.value.view = "/images/avatar-logo/avatar-profile-entreprise.png";
        moreInfos[index].loading = false;
      });
    }
    function setSettings(data) {
      enterpriseName.value.content = _state_api_userState__WEBPACK_IMPORTED_MODULE_6__.isUserBranch.value ? data.label : data.nom_entreprise;
      numero_identifiant_unique.value.content = data?.numero_identifiant_unique ?? 'Non disponible';
      code_postal.value.content = data?.code_postal ?? 'Non disponible';
      ville.value.content = data.ville ?? 'Non disponible';
      forme_juridique.value.content = data?.forme_juridique ?? 'Non disponible';
      registre_commerce.value.content = data?.registre_commerce ?? 'Non disponible';
      pays.value.content = data?.pays ?? 'Non disponible';
      phone.value.content = data?.phone ?? 'Non disponible';
    }
    async function getEnterpriseInfos() {
      isLoading.value = true;
      if (_state_api_userState__WEBPACK_IMPORTED_MODULE_6__.isUserBranch.value) {
        await (0,_api__WEBPACK_IMPORTED_MODULE_4__["default"])().branches.get(_state_api_userState__WEBPACK_IMPORTED_MODULE_6__.userCurrentBranch.value.id).then(response => {
          setSettings(response.data);
          isLoading.value = false;
          // refreshUserInfos();
        }).catch(err => {
          notify.error(`Erreur de récupération des données de l'entreprise. ${err.data.response ? err.data.response.message : ""}`);
          isLoading.value = false;
        });
      } else {
        await (0,_api__WEBPACK_IMPORTED_MODULE_4__["default"])().bussiness_account.get(_state_api_userState__WEBPACK_IMPORTED_MODULE_6__.userInfos.value.enterprise ? _state_api_userState__WEBPACK_IMPORTED_MODULE_6__.userInfos.value.enterprise.id : _state_api_userState__WEBPACK_IMPORTED_MODULE_6__.userInfos.value.staff ? _state_api_userState__WEBPACK_IMPORTED_MODULE_6__.userInfos.value.staff.id : _state_api_userState__WEBPACK_IMPORTED_MODULE_6__.userInfos.value.particular.id).then(response => {
          const data = response.data;
          enterpriseName.value.content = data.nom_entreprise;
          numero_identifiant_unique.value.content = data.numero_identifiant_unique;
          code_postal.value.content = data.code_postal;
          ville.value.content = data.ville;
          forme_juridique.value.content = data.forme_juridique;
          registre_commerce.value.content = data.registre_commerce;
          pays.value.content = data.pays;
          phone.value.content = data.phone;
          isLoading.value = false;
          // refreshUserInfos();
        }).catch(err => {
          notify.error(`Erreur de récupération des données de l'entreprise. ${err.data.response ? err.data.response.message : ""}`);
          isLoading.value = false;
        });
      }
    }
    function editEntrepriseName() {
      enterpriseName.value.editable = !enterpriseName.value.editable;
      enterpriseName.value.content = document.getElementById("enterpriseName")?.innerHTML;
      // console.log("enterpriseName.value.content: ", enterpriseName.value.content);
    }

    async function UpdateBussinessAccountInfos() {
      const data = {
        nom_entreprise: enterpriseName.value.content,
        code_postal: Number(code_postal.value.content),
        phone: phone.value.content,
        pays: pays.value.content,
        ville: ville.value.content,
        numero_identifiant_unique: numero_identifiant_unique.value.content,
        registre_commerce: Number(registre_commerce.value.content),
        forme_juridique: forme_juridique.value.content
      };
      isLoading.value = true;
      await (0,_api__WEBPACK_IMPORTED_MODULE_4__["default"])().bussiness_account.put(_state_api_userState__WEBPACK_IMPORTED_MODULE_6__.userInfos.value.enterprise ? _state_api_userState__WEBPACK_IMPORTED_MODULE_6__.userInfos.value.enterprise.id : _state_api_userState__WEBPACK_IMPORTED_MODULE_6__.userInfos.value.staff ? _state_api_userState__WEBPACK_IMPORTED_MODULE_6__.userInfos.value.staff.id : _state_api_userState__WEBPACK_IMPORTED_MODULE_6__.userInfos.value.particular.id, data).then(response => {
        notify.success("Mise à jour réussie");
        if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_9__["default"])("read", permissions_enterprise)) {
          getEnterpriseInfos();
        }
        isLoading.value = false;
        areInfosModified.value = false;
      }).catch(err => {
        isLoading.value = false;
        notify.error(`Erreur de modification des informations! Réessayez plus tard. ${err.data.response ? err.data.response.message : ""}`);
      });
    }
    // watch(moreInfos, async (newValue: any, oldValue: any) => {
    // 	for(let index in newValue){
    // 		console.log("changed", index, " : ", oldValue[index].content)
    // 		if((index != 'signature_file_name') && (index != 'header_file_name') && (index != 'footer_file_name')){
    // 			if(oldValue[index].content != newValue[index].content){
    // 				alert("changed")
    // 				await api().settings.Files.set({'`${index}`': newValue[index].content})
    // 			}
    // 		}
    // 	}
    // })
    async function fileChanged(component, att, newValue) {
      component.editable = false;
      component.view = URL.createObjectURL(newValue[0]);
      if (/image\//.test(newValue[0].type)) {
        await (0,_composable_useMediasLoader__WEBPACK_IMPORTED_MODULE_7__.sendMedias)([newValue[0]], component.response);
        let data = {};
        data[att] = component.response[0].name;
        await (0,_api__WEBPACK_IMPORTED_MODULE_4__["default"])().settings.files.set(data).then(resp => {
          notify.success("Profil mis à jour");
        }).catch(err => {
          console.log("Not enable to set Profile");
          notify.error("Erreur de mise à jour");
          URL.revokeObjectURL(component.view);
          component.view = "/images/avatar-logo/avatar-profile-entreprise.png";
        });
      }
    }
    const getSettings = async () => {
      await (0,_api__WEBPACK_IMPORTED_MODULE_4__["default"])().settings.files.get().then(async resp => {
        moreInfos.footer_print.content = resp.data.data[0]?.footer_print ?? "Non Défini";
        moreInfos.slogan.content = resp.data.data[0]?.slogan ?? "Non Défini";
        moreInfos.color_theme.content = resp.data.data[0]?.color_theme ?? "Non Défini";
        moreInfos.debut_exercice.content = resp.data.data[0]?.debut_exercice ?? "Non Défini";
        moreInfos.footer_text.content = resp.data.data[0]?.footer_text ?? "Non Défini";
        moreInfos.header_text.content = resp.data.data[0]?.header_text ?? "Non Défini";
        moreInfos.periode.content = resp.data.data[0]?.periode ?? "Non Défini";
        currencyObservable.value = resp.data.data[0]?.curency ?? "Non Défini";
        profile.value.view = resp.data.data[0]?.logo_file_name == null ? "/images/avatar-logo/avatar-profile-entreprise.png" : _state_services_settingState__WEBPACK_IMPORTED_MODULE_10__.logoFile.value;
        moreInfos.signature_file_name.view = resp.data.data[0]?.signature_file_name == null ? "/images/avatar-logo/avatar-profile-entreprise.png" : _state_services_settingState__WEBPACK_IMPORTED_MODULE_10__.signatureFileName.value;
        moreInfos.header_file_name.view = resp.data.data[0]?.header_file_name == null ? "/images/avatar-logo/avatar-profile-entreprise.png" : _state_services_settingState__WEBPACK_IMPORTED_MODULE_10__.headerPortrait.value;
        moreInfos.header_file_name_l.view = resp.data.data[0]?.header_file_name_l == null ? "/images/avatar-logo/avatar-profile-entreprise.png" : _state_services_settingState__WEBPACK_IMPORTED_MODULE_10__.headerLandscape.value;
        moreInfos.footer_file_name.view = resp.data.data[0]?.footer_file_name == null ? "/images/avatar-logo/avatar-profile-entreprise.png" : _state_services_settingState__WEBPACK_IMPORTED_MODULE_10__.footerFileNamePortrait.value;
        moreInfos.footer_file_name_l.view = resp.data.data[0]?.footer_file_name_l == null ? "/images/avatar-logo/avatar-profile-entreprise.png" : _state_services_settingState__WEBPACK_IMPORTED_MODULE_10__.footerFileNameLandscape.value;
        moreInfos.cache_file_name.view = resp.data.data[0]?.cache_file_name == null ? "/images/avatar-logo/avatar-profile-entreprise.png" : _state_services_settingState__WEBPACK_IMPORTED_MODULE_10__.cacheFileName.value;
      }).catch(err => {
        console.log("Error when getting images : ", err);
        notify.error(`Erreur de récupération des fichiers. ${err.data ? err.data.response.message : ""}`);
      });
    };
    // User permissions
    const permissions_settings = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_8__["default"])("settings").map(element => {
      return element.action;
    });
    // UserInfos permissions
    // const permissions_userInfos = useFeature("userinfos").map((element: any) => {
    // 	return element.action;
    // });
    //Enterprise permissions
    const permissions_enterprise = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_8__["default"])("enterprises").map(element => {
      return element.action;
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_3__.onMounted)(async () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_9__["default"])("read", permissions_settings)) {
        await getSettings();
      }
      moreInfosArray.value = [moreInfos.signature_file_name, moreInfos.header_file_name, moreInfos.header_file_name_l, moreInfos.footer_file_name, moreInfos.footer_file_name_l, moreInfos.cache_file_name];
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_3__.onBeforeMount)(async () => {
      isLoading.value = true;
      // if (useAccess('read', permissions_userInfos)) {
      (0,_state_api_userState__WEBPACK_IMPORTED_MODULE_6__.refreshUserInfos)().then(() => {
        if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_9__["default"])("read", permissions_enterprise)) {
          getEnterpriseInfos();
        }
        isLoading.value = true;
        areInfosModified.value = false;
      });
      // }
      enterpriseName.value.content = "Non disponible";
      numero_identifiant_unique.value.content = "Non disponible";
      code_postal.value.content = "Non disponible";
      ville.value.content = "Non disponible";
      forme_juridique.value.content = "Non disponible";
      registre_commerce.value.content = "Non disponible";
      pays.value.content = "Non disponible";
      phone.value.content = "Non disponible";
    });
    const __returned__ = {
      view,
      notify,
      isLoading,
      showDescriptionView,
      moreInfosArray,
      defaultImage,
      enterpriseName,
      phone,
      pays,
      registre_commerce,
      forme_juridique,
      ville,
      code_postal,
      numero_identifiant_unique,
      profile,
      bussinessAccountInfos1,
      currencyObservable,
      moreInfos,
      bussinessAccountInfos2,
      areInfosModified,
      infoChanged,
      setSettings,
      getEnterpriseInfos,
      editEntrepriseName,
      UpdateBussinessAccountInfos,
      fileChanged,
      getSettings,
      permissions_settings,
      permissions_enterprise,
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_9__["default"];
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsEmailTemplating.vue?vue&type=script&setup=true&lang=ts":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsEmailTemplating.vue?vue&type=script&setup=true&lang=ts ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");


/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'settingsEmailTemplating',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_1__.u)({
      title: "Devis / Configuration des templates d'email"
    });
    const __returned__ = {};
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsFollowUp.vue?vue&type=script&setup=true&lang=ts":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsFollowUp.vue?vue&type=script&setup=true&lang=ts ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");


/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'settingsFollowUp',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_1__.u)({
      title: "Devis / Configuration des Relances"
    });
    const __returned__ = {};
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsGeneralSettings.vue?vue&type=script&setup=true&lang=ts":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsGeneralSettings.vue?vue&type=script&setup=true&lang=ts ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/api */ "./src/api/index.ts");
/* harmony import */ var _composable_useNotyf__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/composable/useNotyf */ "./src/composable/useNotyf.ts");
/* harmony import */ var _composable_usePrinter__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/composable/usePrinter */ "./src/composable/usePrinter.ts");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");









/**
 * composable
 */
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'settingsGeneralSettings',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_7__.u)({
      title: "Paramètres / Alertes et notifications"
    });
    const notify = (0,_composable_useNotyf__WEBPACK_IMPORTED_MODULE_2__["default"])();
    /**
     * var
     */
    const isAlreadyDefine = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    const isLoading = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    const clientsPrinters = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([]);
    const showInputTag = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    /**
     *data
     */
    const data = (0,vue__WEBPACK_IMPORTED_MODULE_0__.reactive)({
      silencieux: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false),
      dark: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false),
      sonnerie: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false),
      vibreur: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false),
      auto_power_printer: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false),
      widthGlobalpaper_print: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(58),
      printer_device_name: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("")
    });
    const currencyObservable = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const currency = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    /**
     * functions
     */
    const valider = async () => {
      isLoading.value = true;
      const dataToSend = {
        // silencieux: data.silencieux ? 1 : 0,
        // vibreur: data.vibreur ? 1 : 0,
        dark: data.dark ? 1 : 0,
        // sonnerie: data.sonnerie ? 1 : 0,
        auto_power_printer: data.auto_power_printer == true ? 1 : 0,
        widthGlobalpaper_print: data.widthGlobalpaper_print,
        printer_device_name: data.printer_device_name
      };
      await (0,_api__WEBPACK_IMPORTED_MODULE_1__["default"])().settings.account_setting.set(dataToSend).then(response => {
        notify.success("Paramètres Généraux mis à jour.");
        isLoading.value = false;
        isAlreadyDefine.value = false;
        (0,_state_api_userState__WEBPACK_IMPORTED_MODULE_4__.refreshUserInfos)();
      }).catch(err => {
        notify.error("Une erreur est survenu lors de la sauvegarde de vos paramètres: " + err.response.message);
        isLoading.value = false;
        isAlreadyDefine.value = true;
      });
    };
    const updateSettings = async () => {
      if (currency.value != currencyObservable.value) {
        await (0,_api__WEBPACK_IMPORTED_MODULE_1__["default"])().settings.put({
          curency: currency.value
        }).then(response => {
          notify.success("Configuration mise à jour réussie");
          if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_6__["default"])("read", permissions_settings)) {
            getSettings();
          }
          isLoading.value = false;
        }).catch(err => {
          isLoading.value = false;
          notify.error(`Erreur de modification des informations! Réessayez plus tard. ${err.data.response ? err.data.response.message : ""}`);
        });
      }
    };
    const toggle = (value, categorie) => {
      isAlreadyDefine.value = true;
      switch (categorie) {
        case "print_module":
          data.auto_power_printer = value;
          break;
        case "silencieux":
          data.silencieux = value;
          break;
        case "sonnerie":
          data.sonnerie = value;
          break;
        case "dark":
          data.dark = value;
          break;
        case "vibreur":
          data.vibreur = value;
          break;
        default:
          break;
      }
      // }
    };

    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => data.auto_power_printer, async newValue => {
      if (newValue == true) {
        clientsPrinters.value = await (0,_composable_usePrinter__WEBPACK_IMPORTED_MODULE_3__.getClientsPrinter)();
      }
    });
    const getSettings = async () => {
      await (0,_api__WEBPACK_IMPORTED_MODULE_1__["default"])().settings.files.get().then(async resp => {
        if (resp.data.data.length != 0) {
          currencyObservable.value = resp.data.data[0].curency;
          currency.value = resp.data.data[0].curency;
        } else {
          currencyObservable.value = "Non défini";
          currency.value = "Non défini";
        }
      }).catch(err => {
        console.log("Error when getting images");
        notify.error(`Erreur de récupération des données. ${err.data ? err.data.response.message : ""}`);
      });
    };
    // User permissions
    const permissions_settings = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_5__["default"])("settings").map(element => {
      return element.action;
    });
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_5__["default"])("setting_accounts").map(element => {
      return element.action;
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("updateSubView", null);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeView", null);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeFilter", null);
    /**
     * Page lifecycle
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onBeforeMount)(async () => {
      await (0,_api__WEBPACK_IMPORTED_MODULE_1__["default"])().settings.account_setting.get().then(response => {
        if (response.data) {
          data.silencieux = Boolean(response.data.silencieux);
          data.vibreur = Boolean(response.data.vibreur);
          data.sonnerie = Boolean(response.data.sonnerie);
          data.dark = Boolean(response.data.dark);
          data.auto_power_printer = Boolean(response.data.auto_power_printer);
          data.widthGlobalpaper_print = Number(response.data.widthGlobalpaper_print);
          data.printer_device_name = response.data.printer_device_name != null ? response.data.printer_device_name : null;
        }
      }).catch(err => {
        notify.error("Une erreur est survenu lors de la récupération de vos paramètres: " + err.response.message);
      });
      clientsPrinters.value = await (0,_composable_usePrinter__WEBPACK_IMPORTED_MODULE_3__.getClientsPrinter)();
      console.log("clientsPrinters.value: ", clientsPrinters.value);
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onMounted)(async () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_6__["default"])("read", permissions_settings)) {
        await getSettings();
      }
    });
    const __returned__ = {
      notify,
      isAlreadyDefine,
      isLoading,
      clientsPrinters,
      showInputTag,
      data,
      currencyObservable,
      currency,
      valider,
      updateSettings,
      toggle,
      getSettings,
      permissions_settings,
      permissions,
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_6__["default"];
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsHeaderAndFooter.vue?vue&type=script&setup=true&lang=ts":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsHeaderAndFooter.vue?vue&type=script&setup=true&lang=ts ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");


/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'settingsHeaderAndFooter',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_1__.u)({
      title: "Devis et facture / Configurations / Entête et pied de page"
    });
    const __returned__ = {};
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsManaginBranches.vue?vue&type=script&setup=true&lang=ts":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsManaginBranches.vue?vue&type=script&setup=true&lang=ts ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var vue_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vue-router */ "./node_modules/vue-router/dist/vue-router.mjs");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/api */ "./src/api/index.ts");
/* harmony import */ var _composable_useNotyf__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/composable/useNotyf */ "./src/composable/useNotyf.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");








/**
 * Injects
 */
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'settingsManaginBranches',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_5__.u)({
      title: "Paramètres / Gestions des branches"
    });
    const updateAppHeaderAction = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("updateAppHeaderAction");
    /**
     * Composables
     */
    const notify = (0,_composable_useNotyf__WEBPACK_IMPORTED_MODULE_2__["default"])();
    /**
     * Variables
     */
    const router = (0,vue_router__WEBPACK_IMPORTED_MODULE_6__.useRouter)();
    const route = (0,vue_router__WEBPACK_IMPORTED_MODULE_6__.useRoute)();
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("list");
    const isLoading = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    const branches = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([]);
    const selectedBranch = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)({});
    const viewRightContent = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    const windowWidth = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(window.innerWidth);
    /**
     * Functions
     */
    const updateSubView = action => {
      actionType.value = action;
    };
    const getAccessOfView = action => {
      return (0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_4__["default"])("read", permissions) && (actionType.value == "create" && branches.value.length == 0 || branches.value.length > 0);
    };
    async function getAllBranches() {
      isLoading.value = true;
      await (0,_api__WEBPACK_IMPORTED_MODULE_1__["default"])().branches.getAll().then(response => {
        branches.value = response.data.data;
        isLoading.value = false;
      }).catch(err => {
        notify.error("Erreur lors du chargement de la liste des Succursales. Veuillez recharger la page !");
        isLoading.value = false;
      });
    }
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_3__["default"])("branchs").map(element => {
      return element.action;
    });
    /**
     * Providers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissions", permissions);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("updateSubView", updateSubView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("viewRightContent", viewRightContent);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("windowWidth", windowWidth);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("branches", branches);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("isLoading", isLoading);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("selectedBranch", selectedBranch);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("getAllBranches", getAllBranches);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeFilter", null);
    /**
     * page life cycle
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onMounted)(async () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_4__["default"])("read", permissions)) {
        await getAllBranches();
        selectedBranch.value = branches.value[0];
        // branches.value = []
      }
    });

    const __returned__ = {
      updateAppHeaderAction,
      notify,
      router,
      route,
      actionType,
      isLoading,
      branches,
      selectedBranch,
      viewRightContent,
      windowWidth,
      updateSubView,
      getAccessOfView,
      getAllBranches,
      permissions,
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_4__["default"];
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsManagingUsers.vue?vue&type=script&setup=true&lang=ts":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsManagingUsers.vue?vue&type=script&setup=true&lang=ts ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _composable_useExport__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/composable/useExport */ "./src/composable/useExport.ts");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/api */ "./src/api/index.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");
/* harmony import */ var _composable_useNotyf__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @/composable/useNotyf */ "./src/composable/useNotyf.ts");









/**
 * Composables
 */

/**
 * Variable
 */
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'settingsManagingUsers',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_8__.u)({
      title: "Gestions / Utilisateurs"
    });
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("list");
    const activeFilter = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    // Datas
    const roles = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)();
    const isFetching = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    const staffData = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)({
      acces: 1,
      firstname: "",
      lastname: "",
      poste: "",
      phone: "",
      email: "",
      branch: "",
      sex: "",
      file: {},
      roles: []
    });
    const staffs = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([]);
    const notify = (0,_composable_useNotyf__WEBPACK_IMPORTED_MODULE_7__["default"])();
    const imagesList = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([]);
    const selectedStaffId = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)();
    const dateDebut = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const dateFin = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    // Validation Rule
    const validations = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)({
      firstname: {
        value: staffData,
        validator: value => /[ \w]{3,}/.test(value.firstname),
        msg: {
          error: "Nom invalide (minimum 3 carctères."
        }
      },
      roles: {
        value: staffData,
        validator: value => value.roles.length != 0,
        msg: {
          error: "Aucun rôle sélectionné"
        }
      },
      phone: {
        value: staffData,
        validator: value => /^[+]?[1-9]{1,4}[0-9]{9,20}$/.test(value.phone),
        msg: {
          error: "Symbole + avec indicatif pays et numéro de téléphone"
        }
      },
      email: {
        value: staffData,
        validator: value => actionType.value == "create" ? /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]{2,5}$/.test(value.email) : true,
        msg: {
          error: "Adresse e-mail invalide."
        }
      },
      sex: {
        value: staffData,
        validator: value => !lodash__WEBPACK_IMPORTED_MODULE_3__.isEmpty(value.sex),
        msg: {
          error: "Sexe non sélectionné."
        }
      }
    });
    // Loaders
    const isLoading = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_2__.userCurrentBranch.value.id, async newValue => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_6__["default"])("read", permissions)) {
        await getAllRoles();
        await getAllStaffs();
        await profilImageLoader(staffs, imagesList);
      }
    });
    /**
     * Functions
     */
    const updateSubView = action => {
      actionType.value = action;
    };
    const editSelected = data => {
      staffData.value = Object.assign({}, data);
      actionType.value = "update";
    };
    const resetFields = () => {
      staffData.value.firstname = "";
      staffData.value.lastname = "";
      staffData.value.poste = "";
      staffData.value.phone = "";
      staffData.value.email = "";
      staffData.value.branch = "";
      staffData.value.sex = "";
      staffData.value.file = {};
      staffData.value.roles = [];
    };
    async function getAllStaffs(filter) {
      isFetching.value = true;
      await (0,_api__WEBPACK_IMPORTED_MODULE_4__["default"])().staffs.getAll(filter).then(response => {
        staffs.value = response.data.data.filter(staff => staff.user.account != null);
        isFetching.value = false;
      }).catch(err => {
        notify.warning("Erreur lors du chargement des Utilisateurs. Veuillez recharger la page !");
        isFetching.value = false;
      });
    }
    function deleteUser(position, id) {
      isLoading.value = true;
      (0,_api__WEBPACK_IMPORTED_MODULE_4__["default"])().staffs.del(id).then(response => {
        staffs.value.splice(position, staffs.value.length > -1 ? 1 : position - 1);
        notify.success(`L'utilisateur a été supprimé !`);
        isLoading.value = false;
      }).catch(error => {
        isLoading.value = false;
        notify.error(error.data.response.message);
      });
    }
    async function getAllRoles() {
      await (0,_api__WEBPACK_IMPORTED_MODULE_4__["default"])().roles.getAll().then(response => {
        response.data.data.forEach(role => {
          role.value = role.label;
          // delete role.features
          delete role.account;
        });
        roles.value = response.data.data;
      }).catch(err => {
        notify.error(`Erreur lors de la récupération des roles. Veuillez recharger la page`);
        console.log(err.response);
      });
    }
    const profilImageLoader = async (data, galery) => {
      data.value.forEach((item, index) => {
        isLoading.value = true;
        if (item.user?.file != null) {
          const imgName = item.user.file.name;
          (0,_api__WEBPACK_IMPORTED_MODULE_4__["default"])().gadgets.medias.getImage(imgName, {
            responseType: "blob"
          }).then(resp => {
            const reader = new window.FileReader();
            reader.readAsDataURL(resp);
            reader.onload = () => {
              galery.value[index] = reader.result;
            };
            isLoading.value = false;
          }).catch(err => {
            galery.value[index] = "/icons/not-picture-icon.svg";
            isLoading.value = false;
          });
        } else {
          isLoading.value = false;
          galery.value[index] = "/icons/picture-icon.svg";
        }
      });
    };
    const reload = async () => {
      await getAllStaffs();
      await profilImageLoader(staffs, imagesList);
    };
    // User permissions
    let permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_5__["default"])("staffs").map(element => {
      return element.action;
    });
    /**
     * Providers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissions", permissions);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("updateSubView", updateSubView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeFilter", activeFilter);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("isLoading", isLoading);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("staffData", staffData);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("staffs", staffs);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("roles", roles);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("imagesList", imagesList);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("getAllStaffs", getAllStaffs);
    // provide("editSelected", editSelected);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("selectedStaffId", selectedStaffId);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("deleteUser", deleteUser);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("reload", reload);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("validations", validations);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("getAllRoles", getAllRoles);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("resetFields", resetFields);
    /**
     * Page life cyce
     */
    // onBeforeMount(async () => {
    // 	if (useAccess("read", permissions)) {
    // 		await getAllStaffs();
    // 		await profilImageLoader(staffs, imagesList);
    // 	}
    // });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onMounted)(async () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_6__["default"])("read", permissions)) {
        await getAllStaffs();
        await profilImageLoader(staffs, imagesList);
      }
    });
    const __returned__ = {
      actionType,
      activeFilter,
      roles,
      isFetching,
      staffData,
      staffs,
      notify,
      imagesList,
      selectedStaffId,
      dateDebut,
      dateFin,
      validations,
      isLoading,
      updateSubView,
      editSelected,
      resetFields,
      getAllStaffs,
      deleteUser,
      getAllRoles,
      profilImageLoader,
      reload,
      get permissions() {
        return permissions;
      },
      set permissions(v) {
        permissions = v;
      },
      get filename() {
        return _composable_useExport__WEBPACK_IMPORTED_MODULE_1__.filename;
      },
      get isOwnBranch() {
        return _state_api_userState__WEBPACK_IMPORTED_MODULE_2__.isOwnBranch;
      },
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_6__["default"];
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsOverview.vue?vue&type=script&setup=true&lang=ts":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsOverview.vue?vue&type=script&setup=true&lang=ts ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");




/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'settingsOverview',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_2__.u)({
      title: "Paramètres - Vue d'ensemble - 5NKAP"
    });
    /**
     * Injects
     */
    const activePage = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("activePage");
    console.log("activePage: ", activePage);
    const __returned__ = {
      activePage,
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_1__["default"];
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsReset.vue?vue&type=script&setup=true&lang=ts":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsReset.vue?vue&type=script&setup=true&lang=ts ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/api */ "./src/api/index.ts");
/* harmony import */ var _composable_useNotyf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/composable/useNotyf */ "./src/composable/useNotyf.ts");






/**
 * API Calls
 */

/**
 * Composables
 */

/**
 * var
 */
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'settingsReset',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    const activeView = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    const selectedData = (0,vue__WEBPACK_IMPORTED_MODULE_0__.reactive)({
      categories: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false),
      products: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false),
      services: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false),
      sales: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false),
      rebates: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false),
      saleServices: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false),
      purchases: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false),
      thirds: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false),
      expenses: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false),
      loans: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false),
      grants: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false),
      debts: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false),
      creance: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false),
      checkout: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false),
      bank: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false),
      electronic: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false),
      initialisations: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false),
      damages: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false),
      files: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false),
      devis: (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false)
    });
    const numberOfselectedData = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(0);
    const notify = (0,_composable_useNotyf__WEBPACK_IMPORTED_MODULE_5__["default"])();
    const code = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const isLoading = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    /**
     * Function
     */
    const action = async () => {
      const data = {
        verify_code: code.value,
        categories: Number(selectedData.categories),
        produits: Number(selectedData.products),
        services: Number(selectedData.services),
        ristournes: Number(selectedData.rebates),
        vente_marchandises: Number(selectedData.sales),
        vente_services: Number(selectedData.saleServices),
        achats: Number(selectedData.purchases),
        thirds: Number(selectedData.thirds),
        depenses: Number(selectedData.expenses),
        emprunts: Number(selectedData.loans),
        subventions: Number(selectedData.grants),
        initialisations: Number(selectedData.initialisations),
        creance: Number(selectedData.creance),
        files: Number(selectedData.files),
        dette: Number(selectedData.debts),
        caisse: Number(selectedData.checkout),
        banque: Number(selectedData.bank),
        electronique: Number(selectedData.electronic),
        avaries: Number(selectedData.damages),
        devis: Number(selectedData.devis)
      };
      switch (activeView.value) {
        case 2:
          isLoading.value = true;
          await (0,_api__WEBPACK_IMPORTED_MODULE_4__["default"])().resetData.init().then(response => {
            notify.success(response.response.message);
            isLoading.value = false;
            activeView.value = 3;
          }).catch(err => {
            notify.error("Erreur lors de l'envoi du mail de réinitialisation. Veuillez réessayer plus tard !");
            isLoading.value = false;
          });
          break;
        case 3:
          if (code.value.trim().length != 0) {
            isLoading.value = true;
            await (0,_api__WEBPACK_IMPORTED_MODULE_4__["default"])().resetData.reset(data).then(response => {
              notify.success("Données réinitialisées avec succès.");
              (0,_state_api_userState__WEBPACK_IMPORTED_MODULE_1__.refreshUserInfos)();
              isLoading.value = false;
              activeView.value = 1;
            }).catch(err => {
              notify.error("Erreur lors de la réinitialisation de vos données. " + err.data.response.message);
              isLoading.value = false;
            });
          } else {
            notify.warning("Votre code de réinitialisation est incorrecte.");
            isLoading.value = false;
          }
          break;
      }
    };
    const resendCode = async () => {
      isLoading.value = true;
      await (0,_api__WEBPACK_IMPORTED_MODULE_4__["default"])().resetData.init().then(response => {
        notify.success(response.response.message);
        isLoading.value = false;
      }).catch(err => {
        notify.error("Erreur lors de l'envoi du mail de réinitialisation. Veuillez réessayer plus tard.");
        isLoading.value = false;
      });
    };
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(selectedData, (state, newsValue) => {
      numberOfselectedData.value = 0;
      if (newsValue) {
        for (const data in newsValue) {
          if (newsValue[data] === true) {
            numberOfselectedData.value++;
          }
        }
      }
    });
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_6__.u)({
      title: "Paramètres / Sécurité"
    });
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_2__["default"])("resetData").map(element => {
      return element.action;
    });
    const __returned__ = {
      activeView,
      selectedData,
      numberOfselectedData,
      notify,
      code,
      isLoading,
      action,
      resendCode,
      permissions,
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_3__["default"];
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsRolesAndPermissions.vue?vue&type=script&setup=true&lang=ts":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsRolesAndPermissions.vue?vue&type=script&setup=true&lang=ts ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _composable_useExport__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/composable/useExport */ "./src/composable/useExport.ts");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _validator__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/validator */ "./src/validator/index.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/api */ "./src/api/index.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");
/* harmony import */ var _composable_useNotyf__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @/composable/useNotyf */ "./src/composable/useNotyf.ts");










/**
 * Composables
 */

/**
 * Variables
 */
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'settingsRolesAndPermissions',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    const roles = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([]);
    const actionsSelected = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([]);
    const transitionName = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("translate-subview-right");
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("list");
    const activeView = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    const notify = (0,_composable_useNotyf__WEBPACK_IMPORTED_MODULE_8__["default"])();
    const windowWidth = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(window.innerWidth);
    const roleData = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)({
      label: "",
      features: [],
      featuresSelected: {}
    });
    /**
     * loader
     */
    const isFetching = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    const isLoading = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    const selectedRole = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)({});
    const dateDebut = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const dateFin = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const dataFeatures = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)();
    const metaDataFeatures = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)();
    const validations = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)({
      label: {
        value: roleData,
        validator: value => /[ \w]/.test(value.label),
        msg: {
          error: "Minimum 3 caractères"
        }
      },
      features: {
        value: roleData,
        validator: value => !lodash__WEBPACK_IMPORTED_MODULE_4__.isEmpty(value.features),
        msg: {
          error: "Veuillez sélectionner au moins une action"
        }
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_2__.userCurrentBranch.value.id, newValue => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_7__["default"])("read", permissions)) {
        getAllRoles();
      }
    });
    function getAllRoles(filter) {
      isFetching.value = true;
      if (filter != null) {
        (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().roles.getAll(filter).then(response => {
          roles.value = response.data.data;
          isFetching.value = false;
        }).catch(err => {
          isFetching.value = false;
          notify.error(`Erreur lors de la récupération des roles. Veuillez recharger la page`);
          console.log(err.response);
        });
      } else {
        (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().roles.getAll().then(response => {
          roles.value = response.data.data;
          isFetching.value = false;
        }).catch(err => {
          isFetching.value = false;
          notify.error(`Erreur lors de la récupération des roles. Veuillez recharger la page`);
          console.log(err.response);
        });
      }
    }
    const editSelected = data => {
      setData(data.data);
      // selected.value = Object.assign({}, data.data);
      // console.log("selected.value: ", selected.value);
      actionType.value = "update";
    };
    const setData = data => {
      selectedRole.value.id = data.id;
      roleData.value.label = data.label;
      roleData.value.features = data.features;
    };
    const deleteRole = async (index, id) => {
      isLoading.value = false;
      await (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().roles.delete(id).then(response => {
        isLoading.value = false;
        roles.value.splice(index, roles.value.length > -1 ? 1 : index - 1);
        notify.success(`Role supprimé!`);
      }).catch(error => {
        isLoading.value = false;
        notify.error(error.data.response.message);
      });
    };
    const resetFields = () => {
      selectedRole.value.id = null;
      roleData.value.label = "";
      roleData.value.features = [];
    };
    const exportRoles = data => {
      //
    };
    const doAction = async data => {
      if ((0,_validator__WEBPACK_IMPORTED_MODULE_3__.validateAll)(validations.value)) {
        isLoading.value = true;
        switch (actionType.value) {
          case "create":
            (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().roles.create(data).then(response => {
              getAllRoles();
              notify.success(`Rôle ${response.data.label} crée avec succes !`);
              // reload();
              isLoading.value = false;
              actionType.value = "list";
            }).catch(err => {
              notify.error(`Erreur lors de la création du rôle : ${err.data.response.message}`);
              isLoading.value = false;
            });
            break;
          case "update":
            (0,_api__WEBPACK_IMPORTED_MODULE_5__["default"])().roles.update(data, selectedRole.value.id).then(response => {
              getAllRoles();
              notify.success(`Rôle ${response.data.label} mis à jour.`);
              // reload();
              actionType.value = "list";
              isLoading.value = false;
            }).catch(err => {
              notify.error(`Erreur lors de la mise à jour du rôle : ${err.data.response.message}`);
              isLoading.value = false;
            });
            break;
        }
      } else {
        notify.warning("Veuillez remplir tous les champs du formulaire !");
      }
    };
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_6__["default"])("roles").map(element => {
      return element.action;
    });
    /**
     * Provide
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissions", permissions);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("roleData", roleData);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("editSelected", editSelected);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("getAllRoles", getAllRoles);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("deleteRole", deleteRole);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("validations", validations);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("selectedRole", selectedRole);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("actionsSelected", actionsSelected);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("doAction", doAction);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("roles", roles);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("isLoading", isLoading);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("resetFields", resetFields);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("updateSubView", null);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeFilter", null);
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_9__.u)({
      title: "Paramètres / Rôles et Permissions"
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onBeforeMount)(() => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_7__["default"])("read", permissions)) {
        getAllRoles();
        // roles.value = []
      }
    });

    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onMounted)(() => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_7__["default"])("read", permissions)) {
        getAllRoles();
        // roles.value = []
      }
    });

    const __returned__ = {
      roles,
      actionsSelected,
      transitionName,
      actionType,
      activeView,
      notify,
      windowWidth,
      roleData,
      isFetching,
      isLoading,
      selectedRole,
      dateDebut,
      dateFin,
      dataFeatures,
      metaDataFeatures,
      validations,
      getAllRoles,
      editSelected,
      setData,
      deleteRole,
      resetFields,
      exportRoles,
      doAction,
      permissions,
      get filename() {
        return _composable_useExport__WEBPACK_IMPORTED_MODULE_1__.filename;
      },
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_7__["default"];
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsSMSAndEmail.vue?vue&type=script&setup=true&lang=ts":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsSMSAndEmail.vue?vue&type=script&setup=true&lang=ts ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");


/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'settingsSMSAndEmail',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_1__.u)({
      title: "Paramètres / SMS et Emails"
    });
    const __returned__ = {};
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsSecurity.vue?vue&type=script&setup=true&lang=ts":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsSecurity.vue?vue&type=script&setup=true&lang=ts ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");



/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'settingsSecurity',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_1__.u)({
      title: "Paramètres / Sécurité"
    });
    /**
     * data
     */
    const twoFactors = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    /**
     * functions
     */
    const toggle = () => {
      twoFactors.value = !twoFactors.value;
    };
    const __returned__ = {
      twoFactors,
      toggle
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsSubscriptions.vue?vue&type=script&lang=ts&setup=true":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsSubscriptions.vue?vue&type=script&lang=ts&setup=true ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/utils */ "./src/utils/index.ts");
/* harmony import */ var _composable_useFormatMoney__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/composable/useFormatMoney */ "./src/composable/useFormatMoney.ts");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/api */ "./src/api/index.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useSubscription__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/composable/useSubscription */ "./src/composable/useSubscription.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");
/* harmony import */ var _state_modals_subscription__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @/state/modals/subscription */ "./src/state/modals/subscription.ts");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");







/**
 * Composables
 */


/**
 * States
 */


/**
 * Inbjects
 */
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'settingsSubscriptions',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    const platform = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("platform");
    /**
     * Variables
     */
    const enrolsHistorical = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([]);
    const paymentInfoIllustration = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("/img/Payment Information-bro.svg");
    const subscription = (0,_composable_useSubscription__WEBPACK_IMPORTED_MODULE_5__["default"])();
    /**
     * Functions
     */
    async function getEnrols() {
      await (0,_api__WEBPACK_IMPORTED_MODULE_3__["default"])().enrols.getAll().then(response => {
        enrolsHistorical.value = response.data.data;
        console.log("enrolsHistorical.value: ", enrolsHistorical.value);
      });
    }
    function openSubscriptionFullView() {
      _state_modals_subscription__WEBPACK_IMPORTED_MODULE_7__.displaySubscriptionFullView.value = true;
    }
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_4__["default"])("abonnements").map(element => {
      return element.action;
    });
    /**
     * Page lifecyle
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onBeforeMount)(async () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_6__["default"])("read", permissions)) {
        await getEnrols();
      }
    });
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_9__.u)({
      title: "Paramètres / Mes abonnements"
    });
    const __returned__ = {
      platform,
      enrolsHistorical,
      paymentInfoIllustration,
      subscription,
      getEnrols,
      openSubscriptionFullView,
      permissions,
      get getOperatorName() {
        return _utils__WEBPACK_IMPORTED_MODULE_1__.getOperatorName;
      },
      get formatDate() {
        return _utils__WEBPACK_IMPORTED_MODULE_1__.formatDate;
      },
      get formatMoney() {
        return _composable_useFormatMoney__WEBPACK_IMPORTED_MODULE_2__["default"];
      },
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_6__["default"];
      },
      get showSubcriptionButton() {
        return _state_modals_subscription__WEBPACK_IMPORTED_MODULE_7__.showSubcriptionButton;
      },
      get userInfos() {
        return _state_api_userState__WEBPACK_IMPORTED_MODULE_8__.userInfos;
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsBusinessAccount.vue?vue&type=template&id=d47c7508&ts=true":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsBusinessAccount.vue?vue&type=template&id=d47c7508&ts=true ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "h-full pb-5 overflow-y-hidden rounded-lg workspace-content md:pb-0"
};
const _hoisted_2 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row items-center justify-between w-full h-16 workspace-header"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", {
  class: "text-xl text-gray-500"
}, "Paramètres > Comptes Business"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row items-center h-full workspace-actions"
})], -1 /* HOISTED */);
const _hoisted_3 = {
  class: "relative flex flex-col justify-between bg-gray-100 border border-gray-400 rounded-lg border-opacity-40",
  style: {
    "height": "calc(100vh - 13rem)"
  }
};
const _hoisted_4 = {
  key: 0,
  class: "absolute top-0 left-0 flex flex-row items-center justify-center w-full h-full mr-2 space-x-4 overflow-y-hidden"
};
const _hoisted_5 = {
  key: 1,
  class: "w-full h-full overflow-y-hidden"
};
const _hoisted_6 = {
  key: 0,
  class: "flex flex-row w-full p-8 space-y-8"
};
const _hoisted_7 = {
  key: 0,
  class: "flex flex-col w-1/2 pr-5 space-y-6"
};
const _hoisted_8 = {
  class: "flex flex-row w-full space-x-2"
};
const _hoisted_9 = {
  class: "flex flex-col overflow-y-auto",
  style: {
    "height": "calc(100vh - 25rem)"
  }
};
const _hoisted_10 = {
  key: 0
};
const _hoisted_11 = {
  class: "space-y-1 divide-gray-100 divide-y-1 divide-solid"
};
const _hoisted_12 = {
  class: "flex flex-col space-y-1"
};
const _hoisted_13 = {
  class: "font-bold text-gray-600"
};
const _hoisted_14 = {
  key: 1,
  class: "w-full"
};
const _hoisted_15 = {
  flex: "normal",
  usage: "default",
  placeholder: ""
};
const _hoisted_16 = ["onUpdate:modelValue"];
const _hoisted_17 = ["onClick"];
const _hoisted_18 = {
  key: 0,
  src: "/icons/edit-profile.svg",
  class: "flex items-center p-2",
  alt: ""
};
const _hoisted_19 = {
  key: 1
};
const _hoisted_20 = {
  key: 2,
  src: "/icons/success-icon.svg",
  class: "flex items-center w-8 h-8 p-2",
  alt: ""
};
const _hoisted_21 = {
  key: 3,
  class: "text-green-600"
};
const _hoisted_22 = {
  key: 1,
  class: "flex flex-col w-1/2 pl-5 space-y-1 overflow-y-auto",
  style: {
    "height": "calc(100vh - 25rem)"
  }
};
const _hoisted_23 = {
  class: "space-y-1 divide-y-2 divide-gray-100 divide-solid"
};
const _hoisted_24 = {
  class: "flex flex-col space-y-1"
};
const _hoisted_25 = {
  class: "font-bold text-gray-500"
};
const _hoisted_26 = {
  key: 1,
  class: "w-full"
};
const _hoisted_27 = {
  flex: "normal",
  usage: "default",
  placeholder: ""
};
const _hoisted_28 = ["onUpdate:modelValue"];
const _hoisted_29 = ["onClick"];
const _hoisted_30 = {
  key: 0,
  src: "/icons/edit-profile.svg",
  class: "flex items-center p-2",
  alt: ""
};
const _hoisted_31 = {
  key: 1
};
const _hoisted_32 = {
  key: 2,
  src: "/icons/success-icon.svg",
  class: "flex items-center w-8 h-8 p-2",
  alt: ""
};
const _hoisted_33 = {
  key: 3,
  class: "text-green-600"
};
const _hoisted_34 = {
  class: "flex flex-row w-full p-8 space-y-8"
};
const _hoisted_35 = {
  key: 0,
  class: "flex flex-col w-1/2 pr-5 space-y-6"
};
const _hoisted_36 = {
  class: "flex flex-row w-full space-x-2"
};
const _hoisted_37 = {
  class: "space-y-1 overflow-y-auto divide-gray-400 divide-y-1 divide-solid",
  style: {
    "height": "calc(100vh - 25rem)"
  }
};
const _hoisted_38 = {
  class: "flex flex-col space-y-1"
};
const _hoisted_39 = {
  class: "font-bold text-gray-500"
};
const _hoisted_40 = {
  key: 1,
  class: "w-full"
};
const _hoisted_41 = ["usage"];
const _hoisted_42 = ["onUpdate:modelValue", "type", "onChange"];
const _hoisted_43 = ["onClick"];
const _hoisted_44 = {
  key: 0,
  src: "/icons/edit-profile.svg",
  class: "flex items-center p-2",
  alt: ""
};
const _hoisted_45 = {
  key: 1,
  color: "gray",
  class: "flex items-center p-2"
};
const _hoisted_46 = {
  key: 2
};
const _hoisted_47 = {
  key: 3,
  src: "/icons/success-icon.svg",
  class: "flex items-center w-8 h-8 p-2",
  alt: ""
};
const _hoisted_48 = {
  key: 4,
  class: "text-green-600"
};
const _hoisted_49 = {
  key: 1,
  class: "flex flex-col w-1/2 pl-5 space-y-6"
};
const _hoisted_50 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", {
  class: "font-semibold text-gray-600"
}, " NB : Pour un meilleur rendu de vos documents, veuillez respecter les exigeances suivantes : taille en pixels des fichiers, fichiers à fond transparent, résolution à 300ppp. Veuillez confier la tâche à un Inforgraphe. ", -1 /* HOISTED */);
const _hoisted_51 = {
  class: "space-y-3 overflow-y-auto divide-y-2 divide-gray-100 divide-solid",
  style: {
    "height": "calc(100vh - 32rem)"
  }
};
const _hoisted_52 = ["onClick", "onMouseenter", "onMouseleave"];
const _hoisted_53 = {
  key: 0
};
const _hoisted_54 = {
  key: 2,
  class: "flex flex-col items-center justify-center h-full"
};
const _hoisted_55 = {
  class: "flex justify-end w-full px-8 pb-8 mt-8 space-x-2"
};
const _hoisted_56 = {
  key: 0,
  class: ""
};
const _hoisted_57 = ["loading"];
const _hoisted_58 = {
  key: 1,
  class: ""
};
const _hoisted_59 = ["text", "usage", "loading"];
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_Spinner = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("Spinner");
  const _component_SlimTitleBar = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SlimTitleBar");
  const _component_FileUploader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("FileUploader");
  const _component_fileUploader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("fileUploader");
  const _component_EmptyContent = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("EmptyContent");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" eslint-disable vue/no-deprecated-slot-attribute "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Route "), _hoisted_2, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Cadre "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_3, [$setup.isLoading === true && $setup.useAccess('read', $setup.permissions_settings) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_4, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_Spinner, {
    color: "green"
  })])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.useAccess('read', $setup.permissions_settings) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_5, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" infos container "), $setup.view == 'default' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_6, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Section Left "), $setup.isLoading === false ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_7, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Title "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_8, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SlimTitleBar, {
    icon: "navigation-level-icon",
    title: `Informations de l'Entreprises`
  })]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_9, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Set Logo Enterprise "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
      'bg-auto ': $setup.profile.view == $setup.defaultImage,
      'bg-contain ': $setup.profile.view != $setup.defaultImage,
      'bg-center bg-no-repeat bg-contain ': true,
      'opacity-90   flex flex-col': true,
      'items-center justify-center flex-shrink w-full p-4 px-4': true,
      'text-center text-gray-400 bg-gray-50 border-2 border-gray-400': true,
      'border-dashed cursor-pointer hover:border-green-700': true,
      'border-opacity-30 hover:text-green-700 rounded-2xl': true,
      'py-12': !$setup.profile.editable
    }),
    style: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeStyle)(`background-image: url(${$setup.profile.view})`),
    onClick: _cache[1] || (_cache[1] = $event => $setup.profile.editable = !$setup.profile.editable)
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <img :src=\"profile.view\" alt=\"\" class=\"opacity-90\" /> "), $setup.profile.view == $setup.defaultImage ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("p", _hoisted_10, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.profile.title), 1 /* TEXT */)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.profile.editable && $setup.useAccess('add', $setup.permissions_settings) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_FileUploader, {
    key: 1,
    file: $setup.profile.content,
    "onUpdate:file": _cache[0] || (_cache[0] = $event => $setup.profile.content = $event),
    message: "",
    "justify-center": "true"
  }, null, 8 /* PROPS */, ["file"])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)], 6 /* CLASS, STYLE */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_11, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Nom de l'entreprise "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Pays "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Numéro de téléphone "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" unité monétaire "), ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.bussinessAccountInfos1, (info, index) => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
      key: index,
      class: "flex flex-row justify-between py-2 mt-1"
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_12, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", _hoisted_13, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(info.value.title), 1 /* TEXT */), !info.value.editable ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("p", {
      key: 0,
      class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
        'text-gray-500 capitalize': true,
        'border-gray-300 border py-2 px-3 rounded-lg': info.value.editable
      })
    }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(info.value.content), 3 /* TEXT, CLASS */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_14, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-input", _hoisted_15, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
      slot: "native-input",
      "onUpdate:modelValue": $event => info.value.content = $event
    }, null, 8 /* PROPS */, _hoisted_16), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, info.value.content]])])]))]), $setup.useAccess('update', $setup.permissions_enterprise) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
      key: 0,
      class: "flex flex-row items-center text-gray-600 cursor-pointer",
      onClick: $event => info.value.editable = !info.value.editable
    }, [!info.value.editable ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", _hoisted_18)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), !info.value.editable ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_19, "Modifier")) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), info.value.editable ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", _hoisted_20)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), info.value.editable ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_21, "OK")) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)], 8 /* PROPS */, _hoisted_17)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]);
  }), 128 /* KEYED_FRAGMENT */))])])])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Section Right "), $setup.isLoading === false ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_22, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_23, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Matricule au régistre du commerce  "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Forme juridique "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Ville "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Code postal "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Matricule Fiscal "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Numéro de téléphone "), ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.bussinessAccountInfos2, (info, index) => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
      key: index,
      class: "flex flex-row justify-between py-2 mt-1"
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_24, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", _hoisted_25, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(info.value.title), 1 /* TEXT */), !info.value.editable ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("p", {
      key: 0,
      class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
        'text-gray-500 capitalize': true,
        'border-gray-300 border py-2 px-3 rounded-lg': info.value.editable
      })
    }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(info.value.content), 3 /* TEXT, CLASS */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_26, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-input", _hoisted_27, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
      slot: "native-input",
      "onUpdate:modelValue": $event => info.value.content = $event
    }, null, 8 /* PROPS */, _hoisted_28), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, info.value.content]])])]))]), $setup.useAccess('update', $setup.permissions_enterprise) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
      key: 0,
      class: "flex flex-row items-center text-gray-500 cursor-pointer",
      onClick: $event => info.value.editable = !info.value.editable
    }, [!info.value.editable ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", _hoisted_30)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), !info.value.editable ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_31, "Modifier")) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), info.value.editable ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", _hoisted_32)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), info.value.editable ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_33, "OK")) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)], 8 /* PROPS */, _hoisted_29)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]);
  }), 128 /* KEYED_FRAGMENT */))])])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
    key: 1
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" More infos container "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_34, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Section Left LG"), $setup.isLoading === false ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_35, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Title "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_36, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SlimTitleBar, {
    icon: "navigation-level-icon",
    title: "Autres Informations"
  })]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_37, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Couleur de Thème "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Periode "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Debut d'Exercice "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Slogan "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Texte d'Entête de page "), ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)(['footer_print', 'slogan', 'debut_exercice', 'periode', 'color_theme', 'header_text', 'footer_text'], (info, index) => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
      key: index,
      class: "flex flex-row justify-between py-2 mt-1"
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_38, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", _hoisted_39, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.moreInfos[info].title), 1 /* TEXT */), !$setup.moreInfos[info].editable ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("p", {
      key: 0,
      class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
        'text-gray-500 capitalize': true,
        'border-gray-300 border py-3 px-3 rounded-lg': $setup.moreInfos[info].editable
      })
    }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.moreInfos[info].content), 3 /* TEXT, CLASS */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_40, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-input", {
      flex: "normal",
      usage: $setup.moreInfos[info].type ? $setup.moreInfos[info].type : 'default',
      placeholder: ""
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
      slot: "native-input",
      "onUpdate:modelValue": $event => $setup.moreInfos[info].content = $event,
      type: $setup.moreInfos[info].type ? $setup.moreInfos[info].type : 'text',
      onChange: $event => $setup.infoChanged(info)
    }, null, 40 /* PROPS, HYDRATE_EVENTS */, _hoisted_42), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelDynamic, $setup.moreInfos[info].content]])], 8 /* PROPS */, _hoisted_41)]))]), $setup.useAccess('add', $setup.permissions_settings) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
      key: 0,
      class: "flex flex-row items-center text-gray-500 cursor-pointer",
      onClick: $event => $setup.moreInfos[info].editable = !$setup.moreInfos[info].editable
    }, [!$setup.moreInfos[info].editable && !$setup.moreInfos[info].loading ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", _hoisted_44)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.moreInfos[info].loading ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("bb-loading", _hoisted_45)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), !$setup.moreInfos[info].editable ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_46, "Modifier")) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.moreInfos[info].editable ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", _hoisted_47)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.moreInfos[info].editable ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_48, "OK")) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)], 8 /* PROPS */, _hoisted_43)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]);
  }), 64 /* STABLE_FRAGMENT */))])])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Section Right "), $setup.isLoading === false ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_49, [_hoisted_50, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_51, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.moreInfosArray, (file, index) => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
      key: index
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
      class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
        'bg-auto ': file.view == $setup.defaultImage,
        'bg-contain ': file.view != $setup.defaultImage,
        'bg-center bg-no-repeat bg-contain opacity-90': true,
        'flex flex-col items-center justify-center flex-shrink': true,
        'w-full p-4 px-4 text-center text-gray-400 bg-gray-100': true,
        'border-2 border-gray-400 border-dashed cursor-pointer ': true,
        'hover:border-green-700 border-opacity-30 hover:text-green-700 rounded-2xl': true,
        'py-12': !file.editable
      }),
      style: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeStyle)(`background-image: url(${file.view})`),
      onClick: $event => file.editable = !file.editable,
      onMouseenter: $event => $setup.moreInfosArray[index].show = true,
      onMouseleave: $event => $setup.moreInfosArray[index].show = false
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <img :src=\"file.view\" alt=\"\" class=\"opacity-90\" /> "), file.view == $setup.defaultImage || file.show ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("p", _hoisted_53, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(file.title), 1 /* TEXT */)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), file.editable && $setup.useAccess('add', $setup.permissions_settings) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_fileUploader, {
      key: 1,
      file: file.content,
      "onUpdate:file": $event => file.content = $event,
      message: "",
      "justify-center": "true"
    }, null, 8 /* PROPS */, ["file", "onUpdate:file"])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)], 46 /* CLASS, STYLE, PROPS, HYDRATE_EVENTS */, _hoisted_52)]);
  }), 128 /* KEYED_FRAGMENT */))])])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])], 2112 /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */))])) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_54, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_EmptyContent, {
    icon: "/icons/no-access-icon-c.svg",
    line1: 'Oups... Vous n\'êtes pas autorisé ',
    line2: "à afficher ce contenu.",
    action: '',
    callback: () => {},
    height: "100px",
    width: "100px"
  }, null, 8 /* PROPS */, ["line1"])])), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" btn container "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_55, [$setup.areInfosModified && $setup.useAccess('add', $setup.permissions_settings) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_56, [$setup.view == 'default' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("bb-button", {
    key: 0,
    type: "link",
    text: "Valider les modifications",
    "text-color": "white",
    usage: "simple-text",
    url: "javascript:void(0)",
    loading: $setup.isLoading,
    onClick: $setup.UpdateBussinessAccountInfos
  }, null, 8 /* PROPS */, _hoisted_57)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), !$setup.isLoading && $setup.useAccess('read', $setup.permissions_settings) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_58, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-button", {
    type: "link",
    text: `${$setup.view == 'default' ? 'Page Suivante' : 'retourner'}`,
    "text-color": "white",
    "textcolor-hover": "white",
    bgcolor: "gray-600",
    "bgcolor-hover": "gray-700",
    "border-color": "blue-200",
    usage: `${$setup.view == 'default' ? 'next-with-text' : 'back-with-text'}`,
    url: "javascript:void(0)",
    loading: $setup.isLoading,
    onClick: _cache[2] || (_cache[2] = $event => $setup.view = $setup.view == 'default' ? 'moreInfos' : 'default')
  }, null, 8 /* PROPS */, _hoisted_59)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])])])], 2112 /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsEmailTemplating.vue?vue&type=template&id=dd30c0bc&ts=true":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsEmailTemplating.vue?vue&type=template&id=dd30c0bc&ts=true ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "workspace-content h-full overflow-y-auto pb-5 md:pb-0"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row items-center justify-between w-full h-16 workspace-header"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", {
  class: "text-xl text-gray-500"
}, "Devis > Configuration des templates d'email"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row items-center h-full workspace-actions"
})]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Cadre "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row w-full p-8 bg-white border border-gray-400 rounded-md border-opacity-40",
  style: {
    "height": "auto"
  }
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Section Left LG"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-col w-full pr-5 space-y-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Title "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row w-full space-x-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/navigation-level-icon.svg",
  alt: "",
  class: "opacity-60"
}), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h2", {
  class: "text-xl font-medium text-gray-400"
}, "Configuration Modèles E-mail")]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "space-y-4"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" input "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "pl-2 pr-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full mb-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-select", {
  placeholder: "Sélectionner un Template Email",
  usage: "default",
  "options-data": "[\n\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Email Standard\"},\n\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Email de Relance 1\"},\n\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Email de Relance 2\"}\n\t\t\t\t\t\t\t\t\t\t\t\t]"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
  slot: "native-input",
  readonly: ""
})])])]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" input "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "pl-2 pr-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full mb-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-select", {
  flex: "normal",
  placeholder: "Insérer un champ",
  usage: "default",
  "options-data": "[\n\t\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Nom du Client\"},\n\t\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Type de Document\"},\n\t\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Date de Document\"},\n\t\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Reference De Document\"},\n\t\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Nom Utilisateur\"},\n\t\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Nom de Entreprise\"}\n\t\t\t\t\t\t\t\t\t\t\t\t\t]",
  disabledd: "offf",
  statee: "false"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
  slot: "native-input"
})])])]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" input "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "pl-2 pr-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full mb-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-input", {
  flex: "normal",
  placeholder: "Objet du E-mail",
  usage: "default",
  "options-data": "[\n\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"5NKAP | Activation de votre Compte\"}\n\t\t\t\t\t\t\t\t\t\t\t\t]"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
  slot: "native-input"
})])])]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Commentaire "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "px-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full p-4 mb-4 bg-white border border-gray-100 rounded-xl"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "mb-2 text-lg font-normal"
}, "Message"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("textarea", {
  id: "",
  name: "",
  rows: "8",
  placeholder: "",
  class: "w-full pl-2 font-light focus:outline-none"
}, "\n\t\t\t\t\t\t\t\t\tHi Arthur,\n\t\t\t\t\t\t\t\t\tBienvenu dans la communauté 5NKAP!\n\t\t\t\t\t\t\t\t\tCliquez sur  ce Lien  pour activer votre compte\n\n\n\t\t\t\t\t\t\t\t")])]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h2", {
  class: "mt-10 font-bold text-right text-blue-500 cursor-pointer"
}, "Mettre à jour")])])])], -1 /* HOISTED */);
function render(_ctx, _cache, $props, $setup, $data, $options) {
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" eslint-disable vue/no-deprecated-slot-attribute "), _hoisted_1], 2112 /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsFollowUp.vue?vue&type=template&id=1cb5bf8d&ts=true":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsFollowUp.vue?vue&type=template&id=1cb5bf8d&ts=true ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "workspace-content h-full overflow-y-auto pb-5 md:pb-0"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row items-center justify-between w-full h-16 workspace-header"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", {
  class: "text-xl text-gray-500"
}, "Devis > Configuration des Relances"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row items-center h-full workspace-actions"
})]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Cadre "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row w-full p-8 bg-white border border-gray-400 rounded-md border-opacity-40",
  style: {
    "height": "auto"
  }
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Section Left LG"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-col w-full pr-5 space-y-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Title "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row w-full space-x-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/navigation-level-icon.svg",
  alt: "",
  class: "opacity-60"
}), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h2", {
  class: "text-xl font-medium text-gray-400"
}, "Personnalisation des relances")]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", {
  class: "text-lg font-medium text-gray-400"
}, "Gérer les périodes pour lesquelles vous souhaitez envoyer des rappels."), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row mt-5"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-1/2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h2", {
  class: "font-bold"
}, "Relance 1"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full mt-5"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-select", {
  placeholder: "Nombre de jours passés",
  "options-data": "[\n\t\t\t\t\t\t\t\t\t\t\t{\"key\" : \"1\", \"value\" : \"- 8 Jours\"},\n\t\t\t\t\t\t\t\t\t\t\t{\"key\" : \"2\", \"value\" : \"2 Jours\"}\n\t\t\t\t\t\t\t\t\t\t]"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" eslint-disable-next-line  "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
  slot: "native-input",
  readonly: ""
})])])]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-1/2 ml-5"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h2", {
  class: "font-bold"
}, "Relance 2"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full mt-5"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-select", {
  placeholder: "Nombre de jours passés",
  "options-data": "[\n\t\t\t\t\t\t\t\t\t\t\t{\"key\" : \"1\", \"value\" : \"- 8 Jours\"},\n\t\t\t\t\t\t\t\t\t\t\t{\"key\" : \"2\", \"value\" : \"15 Jours\"}\n\t\t\t\t\t\t\t\t\t\t]"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" eslint-disable-next-line  "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
  slot: "native-input",
  readonly: ""
})])]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h2", {
  class: "mt-8 font-bold text-right text-blue-500 cursor-pointer"
}, "Sauvegarder")])])])])], -1 /* HOISTED */);
function render(_ctx, _cache, $props, $setup, $data, $options) {
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" eslint-disable vue/no-deprecated-slot-attribute "), _hoisted_1], 2112 /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsGeneralSettings.vue?vue&type=template&id=a0e4854c&ts=true":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsGeneralSettings.vue?vue&type=template&id=a0e4854c&ts=true ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "h-full pb-5 overflow-y-hidden workspace-content md:pb-0"
};
const _hoisted_2 = {
  class: "flex flex-col justify-between w-full p-8 space-y-4 overflow-y-auto bg-white border border-gray-500 rounded-md border-opacity-40 bg-opacity-40",
  style: {
    "height": "calc(100vh - 13rem)"
  }
};
const _hoisted_3 = {
  key: 0,
  class: "flex flex-col w-full h-auto md:flex-row"
};
const _hoisted_4 = {
  class: "flex flex-col w-full pr-5 mb-12 space-y-4 md:w-1/2 md:mb-0"
};
const _hoisted_5 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row w-full space-x-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/navigation-level-icon.svg",
  alt: "",
  class: "opacity-60"
}), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h2", {
  class: "text-xl font-medium text-gray-600"
}, "Imprimante et papier")], -1 /* HOISTED */);
const _hoisted_6 = {
  class: "w-full"
};
const _hoisted_7 = {
  class: "flex flex-row justify-between py-5"
};
const _hoisted_8 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-col"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", {
  class: "font-medium text-gray-500"
}, "Module d'impression")], -1 /* HOISTED */);
const _hoisted_9 = {
  class: "flex flex-row items-center text-gray-500 cursor-pointer"
};
const _hoisted_10 = {
  key: 0,
  class: "flex flex-col w-full py-2 space-y-2"
};
const _hoisted_11 = {
  class: "flex flex-row items-center space-x-1"
};
const _hoisted_12 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", {
  class: "font-medium text-gray-500"
}, "Format d'impression", -1 /* HOISTED */);
const _hoisted_13 = {
  class: "text-sm font-medium text-gray-400"
};
const _hoisted_14 = {
  class: "flex flex-col w-full text-gray-500 cursor-pointer"
};
const _hoisted_15 = {
  flex: "normal",
  placeholder: "Sélectionner un format",
  usage: "default",
  "options-data": "[\n\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Format 58 mm largeur\",\"key\" : \"58\"},\n\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Format 80 mm largeur\", \"key\" : \"80\"}\n\t\t\t\t\t\t\t\t\t]\n\t\t\t\t\t\t\t\t\t"
};
const _hoisted_16 = {
  key: 1,
  class: "flex flex-col py-5 space-y-2"
};
const _hoisted_17 = {
  class: "flex flex-row items-center space-x-1"
};
const _hoisted_18 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", {
  class: "font-medium text-gray-500"
}, "Péripherique d'impression", -1 /* HOISTED */);
const _hoisted_19 = {
  class: "text-sm font-medium text-gray-400"
};
const _hoisted_20 = {
  key: 0,
  class: "flex flex-col text-gray-500 cursor-pointer"
};
const _hoisted_21 = ["options-data"];
const _hoisted_22 = {
  key: 1,
  class: "flex flex-col text-gray-500"
};
const _hoisted_23 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-col py-2 space-y-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-col space-x-1"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", {
  class: "font-medium text-gray-500"
}, " Télécharger le Pilote du Module D'Impression ")]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-col text-gray-500 cursor-pointer"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-button", {
  flex: "normal",
  type: "link",
  url: "/download/driver/jspm4-4.0.21.1102-win.exe",
  text: "Télécharger (Compatible Windows)",
  usage: "simple-text",
  "margin-text": "0",
  "text-color": "white",
  class: "font-light"
})])], -1 /* HOISTED */);
const _hoisted_24 = {
  class: "flex flex-col w-full space-y-6 overflow-y-hidden md:w-1/2 md:pl-5"
};
const _hoisted_25 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row w-full space-x-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/navigation-level-icon.svg",
  alt: "",
  class: "opacity-60"
}), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h2", {
  class: "text-xl font-medium text-gray-600"
}, "Configurations des Alertes")], -1 /* HOISTED */);
const _hoisted_26 = {
  class: "space-y-2 overflow-y-auto divide-y-2 divide-gray-100 divide-solid",
  style: {
    "height": "calc(100vh - 24.5rem)"
  }
};
const _hoisted_27 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createStaticVNode)("<div class=\"flex flex-row justify-between py-2\"><div class=\"flex flex-col\"><p class=\"font-medium text-gray-500\">Seuil de Stock</p><p class=\"text-sm font-medium text-gray-400\">(Par defaut : 10%)</p></div><div class=\"flex flex-row items-center text-gray-500 cursor-pointer\">10 %</div></div>", 1);
const _hoisted_28 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createStaticVNode)("<div class=\"flex flex-row justify-between py-2\"><div class=\"flex flex-col\"><p class=\"font-medium text-gray-500\">Recevoir les Alertes par</p><p class=\"text-sm font-medium text-gray-400\">(Par defaut : E-mail)</p></div><div class=\"flex flex-row items-center text-gray-500 cursor-pointer\">E-mail</div></div>", 1);
const _hoisted_29 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createStaticVNode)("<div class=\"flex flex-row justify-between py-2\"><div class=\"flex flex-col\"><p class=\"font-medium text-gray-500\">Recevoir les Alertes par</p><p class=\"text-sm font-medium text-gray-400\"> (Par defaut : Centre de notification) </p></div><div class=\"flex flex-row items-center text-gray-500 cursor-pointer\"> Notification </div></div>", 1);
const _hoisted_30 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row w-full py-5 space-x-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/navigation-level-icon.svg",
  alt: "",
  class: "opacity-60"
}), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h2", {
  class: "text-xl font-medium text-gray-600"
}, "Unité Monétaire")], -1 /* HOISTED */);
const _hoisted_31 = {
  class: ""
};
const _hoisted_32 = {
  class: "flex flex-row items-center justify-between"
};
const _hoisted_33 = {
  class: "flex flex-col"
};
const _hoisted_34 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", {
  class: "font-medium text-gray-500"
}, "Valeur actuelle", -1 /* HOISTED */);
const _hoisted_35 = {
  key: 0,
  class: "text-sm font-medium text-gray-400"
};
const _hoisted_36 = {
  key: 1,
  class: "w-full pt-2"
};
const _hoisted_37 = {
  flex: "normal",
  usage: "default",
  placeholder: ""
};
const _hoisted_38 = {
  key: 0,
  src: "/icons/edit-profile.svg",
  class: "flex items-center p-2",
  alt: ""
};
const _hoisted_39 = {
  key: 1
};
const _hoisted_40 = {
  key: 2,
  src: "/icons/success-icon.svg",
  class: "flex items-center w-8 h-8 p-2",
  alt: ""
};
const _hoisted_41 = {
  key: 3,
  class: "text-green-600"
};
const _hoisted_42 = {
  key: 0,
  class: "flex flex-row items-center justify-end w-full py-4"
};
const _hoisted_43 = ["loading"];
const _hoisted_44 = {
  key: 1,
  class: "flex flex-col items-center justify-center h-full"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_WorkspaceHeader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WorkspaceHeader");
  const _component_EmptyContent = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("EmptyContent");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_WorkspaceHeader, {
    options: {
      titleBar: true,
      actionsBar: true,
      fabButtons: true,
      actions: {
        import: false,
        export: false,
        addItem: false,
        filter: false
      },
      fabButtonsActions: {
        addItem: false,
        moreMenu: false
      }
    }
  }), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Cadre "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_2, [!$setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Section Left LG"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_4, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Title "), _hoisted_5, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_6, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Element Module d'impression"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_7, [_hoisted_8, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_9, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
      'flex items-center cursor-pointer justify-end px-2 py-1  rounded-xl h-10': true,
      'bg-gray-200': !$setup.data.auto_power_printer,
      'bg-primary-800': $setup.data.auto_power_printer
    }),
    onClick: _cache[0] || (_cache[0] = $event => $setup.toggle(!$setup.data.auto_power_printer, 'print_module'))
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
      'flex items-center justify-center font-normal w-10 h-6 py-1 rounded-md text-white ': true,
      'bg-gray-500': !$setup.data.auto_power_printer
    })
  }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.data.auto_power_printer ? "" : "Off"), 3 /* TEXT, CLASS */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
      'flex items-center justify-center font-normal w-10 h-6 py-1 rounded-md text-white ': true,
      'bg-green-400 font-medium text-white': $setup.data.auto_power_printer
    })
  }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.data.auto_power_printer ? "On" : ""), 3 /* TEXT, CLASS */)], 2 /* CLASS */)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Element Format d'impression"), $setup.data.auto_power_printer == 1 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_10, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_11, [_hoisted_12, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", _hoisted_13, " (Format actuel : " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.data.widthGlobalpaper_print) + " mm) ", 1 /* TEXT */)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_14, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-select", _hoisted_15, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    slot: "native-input",
    class: "w-full",
    readonly: "",
    onChange: _cache[1] || (_cache[1] = $event => {
      $setup.data.widthGlobalpaper_print = JSON.parse($event.target?.dataset.option).key;
      $setup.isAlreadyDefine = true;
    })
  }, null, 32 /* HYDRATE_EVENTS */)])])])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Element Péripherique d'impression "), $setup.data.auto_power_printer == 1 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_16, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_17, [_hoisted_18, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", _hoisted_19, " (" + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.data.printer_device_name == null ? "Péripherique par defaut" : $setup.data.printer_device_name) + ") ", 1 /* TEXT */)]), $setup.clientsPrinters.length != 0 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_20, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-select", {
    flex: "normal",
    placeholder: "Sélectionner un péripherique",
    usage: "default",
    "options-data": JSON.stringify($setup.clientsPrinters)
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    slot: "native-input",
    class: "w-full",
    readonly: "",
    onChange: _cache[2] || (_cache[2] = $event => {
      $setup.data.printer_device_name = JSON.parse($event.target?.dataset.option).value;
      $setup.isAlreadyDefine = true;
    })
  }, null, 32 /* HYDRATE_EVENTS */)], 8 /* PROPS */, _hoisted_21)])) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_22, " Aucun péripherique d'impression détecté. L'imprimante par défaut de votre systême d'exploitation sera utilisée une fois le pilote de gestion d'imprimante installé et démarré. "))])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Element Pilote du Module d'impression "), _hoisted_23, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Element Mode Sonnerie"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <div class=\"flex flex-row justify-between py-5\">\n\t\t\t\t\t\t\t<div class=\"flex flex-col\">\n\t\t\t\t\t\t\t\t<p class=\"font-medium text-gray-500\">Mode Sonnerie</p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"flex flex-row items-center text-gray-500 cursor-pointer\">\n\t\t\t\t\t\t\t\t<div\n\t\t\t\t\t\t\t\t\t@click=\"toggle(!data.silencieux, 'silencieux')\"\n\t\t\t\t\t\t\t\t\t:class=\"{\n\t\t\t\t\t\t\t\t\t\t'flex items-center cursor-pointer justify-end px-2 py-1  rounded-xl': true,\n\t\t\t\t\t\t\t\t\t\t'bg-gray-200': !data.silencieux,\n\t\t\t\t\t\t\t\t\t\t'bg-primary-800': data.silencieux,\n\t\t\t\t\t\t\t\t\t}\"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t<div\n\t\t\t\t\t\t\t\t\t\t:class=\"{\n\t\t\t\t\t\t\t\t\t\t\t'flex items-center justify-center font-normal w-10 h-6 py-1 rounded-md text-white ': true,\n\t\t\t\t\t\t\t\t\t\t\t'bg-gray-500': !data.silencieux,\n\t\t\t\t\t\t\t\t\t\t}\"\n\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t\t{{ data.silencieux ? \"\" : \"Off\" }}\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div\n\t\t\t\t\t\t\t\t\t\t:class=\"{\n\t\t\t\t\t\t\t\t\t\t\t'flex items-center justify-center font-normal w-10 h-6 py-1 rounded-md text-white ': true,\n\t\t\t\t\t\t\t\t\t\t\t'bg-green-400 font-medium text-white': data.silencieux,\n\t\t\t\t\t\t\t\t\t\t}\"\n\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t\t{{ data.silencieux ? \"On\" : \"\" }}\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div> "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Element Vibrer"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <div class=\"flex flex-row justify-between py-5\">\n\t\t\t\t\t\t\t<div class=\"flex flex-col\">\n\t\t\t\t\t\t\t\t<p class=\"font-medium text-gray-500\">Vibrer</p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div\n\t\t\t\t\t\t\t\t@click=\"toggle(!data.vibreur, 'vibreur')\"\n\t\t\t\t\t\t\t\t:class=\"{\n\t\t\t\t\t\t\t\t\t'flex items-center cursor-pointer justify-end px-2 py-1  rounded-xl': true,\n\t\t\t\t\t\t\t\t\t'bg-gray-200': !data.vibreur,\n\t\t\t\t\t\t\t\t\t'bg-primary-800': data.vibreur,\n\t\t\t\t\t\t\t\t}\"\n\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t<div\n\t\t\t\t\t\t\t\t\t:class=\"{\n\t\t\t\t\t\t\t\t\t\t'flex items-center justify-center font-normal w-10 h-6 py-1 rounded-md text-white ': true,\n\t\t\t\t\t\t\t\t\t\t'bg-gray-500': !data.vibreur,\n\t\t\t\t\t\t\t\t\t}\"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t{{ data.vibreur ? \"\" : \"Off\" }}\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div\n\t\t\t\t\t\t\t\t\t:class=\"{\n\t\t\t\t\t\t\t\t\t\t'flex items-center justify-center font-normal w-10 h-6 py-1 rounded-md text-white ': true,\n\t\t\t\t\t\t\t\t\t\t'bg-green-400 font-medium text-white': data.vibreur,\n\t\t\t\t\t\t\t\t\t}\"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t{{ data.vibreur ? \"On\" : \"\" }}\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div> "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Element Thème Dark"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <div class=\"flex flex-row justify-between py-5\">\n\t\t\t\t\t\t\t<div class=\"flex flex-col\">\n\t\t\t\t\t\t\t\t<p class=\"font-medium text-gray-500\">Thème Dark</p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div\n\t\t\t\t\t\t\t\t@click=\"toggle(!data.dark, 'dark')\"\n\t\t\t\t\t\t\t\t:class=\"{\n\t\t\t\t\t\t\t\t\t'flex items-center cursor-pointer justify-end px-2 py-1  rounded-xl': true,\n\t\t\t\t\t\t\t\t\t'bg-gray-200': !data.dark,\n\t\t\t\t\t\t\t\t\t'bg-primary-800': data.dark,\n\t\t\t\t\t\t\t\t}\"\n\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t<div\n\t\t\t\t\t\t\t\t\t:class=\"{\n\t\t\t\t\t\t\t\t\t\t'flex items-center justify-center font-normal w-10 h-6 py-1 rounded-md text-white ': true,\n\t\t\t\t\t\t\t\t\t\t'bg-gray-500': !data.dark,\n\t\t\t\t\t\t\t\t\t}\"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t{{ data.dark ? \"\" : \"Off\" }}\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div\n\t\t\t\t\t\t\t\t\t:class=\"{\n\t\t\t\t\t\t\t\t\t\t'flex items-center justify-center font-normal w-10 h-6 py-1 rounded-md text-white ': true,\n\t\t\t\t\t\t\t\t\t\t'bg-green-400 font-medium text-white': data.dark,\n\t\t\t\t\t\t\t\t\t}\"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t{{ data.dark ? \"On\" : \"\" }}\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div> "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Element Sonnerie"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <div class=\"flex flex-row justify-between py-5\">\n\t\t\t\t\t\t\t<div class=\"flex flex-col\">\n\t\t\t\t\t\t\t\t<p class=\"font-medium text-gray-500\">Sonnerie</p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div\n\t\t\t\t\t\t\t\t@click=\"toggle(!data.sonnerie, 'sonnerie')\"\n\t\t\t\t\t\t\t\t:class=\"{\n\t\t\t\t\t\t\t\t\t'flex items-center cursor-pointer justify-end px-2 py-1  rounded-xl': true,\n\t\t\t\t\t\t\t\t\t'bg-gray-200': !data.sonnerie,\n\t\t\t\t\t\t\t\t\t'bg-primary-800': data.sonnerie,\n\t\t\t\t\t\t\t\t}\"\n\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t<div\n\t\t\t\t\t\t\t\t\t:class=\"{\n\t\t\t\t\t\t\t\t\t\t'flex items-center justify-center font-normal w-10 h-6 py-1 rounded-md text-white ': true,\n\t\t\t\t\t\t\t\t\t\t'bg-gray-500': !data.sonnerie,\n\t\t\t\t\t\t\t\t\t}\"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t{{ data.sonnerie ? \"\" : \"Off\" }}\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div\n\t\t\t\t\t\t\t\t\t:class=\"{\n\t\t\t\t\t\t\t\t\t\t'flex items-center justify-center font-normal w-10 h-6 py-1 rounded-md text-white ': true,\n\t\t\t\t\t\t\t\t\t\t'bg-green-400 font-medium text-white': data.sonnerie,\n\t\t\t\t\t\t\t\t\t}\"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t{{ data.sonnerie ? \"On\" : \"\" }}\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div> ")])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Section Right LG"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_24, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Title "), _hoisted_25, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_26, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Element Thème Dark"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <div class=\"flex flex-row justify-between pt-5 pb-2\">\n\t\t\t\t\t\t\t<div class=\"flex flex-col\">\n\t\t\t\t\t\t\t\t<p class=\"font-medium text-gray-500\">Thème Dark</p>\n\t\t\t\t\t\t\t\t<p class=\"text-sm font-medium text-gray-400\">(En cours de développement)</p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div\n\t\t\t\t\t\t\t\t@click=\"toggle(!data.dark, 'dark')\"\n\t\t\t\t\t\t\t\t:class=\"{\n\t\t\t\t\t\t\t\t\t'flex items-center cursor-pointer justify-end px-2 py-1  rounded-xl h-10': true,\n\t\t\t\t\t\t\t\t\t'bg-gray-200': !data.dark,\n\t\t\t\t\t\t\t\t\t'bg-primary-800': data.dark,\n\t\t\t\t\t\t\t\t}\"\n\t\t\t\t\t\t\t\tv-if=\"useAccess('add', permissions)\"\n\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t<div\n\t\t\t\t\t\t\t\t\t:class=\"{\n\t\t\t\t\t\t\t\t\t\t'flex items-center justify-center font-normal w-10 h-6 py-1 rounded-md text-white ': true,\n\t\t\t\t\t\t\t\t\t\t'bg-gray-500': !data.dark,\n\t\t\t\t\t\t\t\t\t}\"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t{{ data.dark ? \"\" : \"Off\" }}\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div\n\t\t\t\t\t\t\t\t\t:class=\"{\n\t\t\t\t\t\t\t\t\t\t'flex items-center justify-center font-normal w-10 h-6 py-1 rounded-md text-white ': true,\n\t\t\t\t\t\t\t\t\t\t'bg-green-400 font-medium text-white': data.dark,\n\t\t\t\t\t\t\t\t\t}\"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t{{ data.dark ? \"On\" : \"\" }}\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div> "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Element "), _hoisted_27, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Element "), _hoisted_28, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Element "), _hoisted_29, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Title "), _hoisted_30, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_31, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Element "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_32, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_33, [_hoisted_34, !$setup.showInputTag ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("p", _hoisted_35, " (" + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.currency) + ") ", 1 /* TEXT */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_36, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-input", _hoisted_37, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    slot: "native-input",
    "onUpdate:modelValue": _cache[3] || (_cache[3] = $event => $setup.currency = $event)
  }, null, 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, $setup.currency]])])]))]), $setup.useAccess('update', $setup.permissions_settings) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
    key: 0,
    class: "flex flex-row items-center text-gray-600 cursor-pointer",
    onClick: _cache[4] || (_cache[4] = () => {
      $setup.showInputTag = !$setup.showInputTag;
      $setup.updateSettings();
    })
  }, [!$setup.showInputTag ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", _hoisted_38)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), !$setup.showInputTag ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_39, "Modifier")) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.showInputTag ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", _hoisted_40)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.showInputTag ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_41, "valider")) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <div class=\"flex flex-row items-center text-gray-500 cursor-pointer\">Modifier</div> ")])])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Button "), $setup.isAlreadyDefine ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_42, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: "w-auto",
    onClick: $setup.valider
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-button", {
    flex: "normal",
    type: "link",
    url: "javascript:void(0)",
    text: "Valider les modifications",
    loading: $setup.isLoading,
    usage: "simple-text",
    "text-color": "white"
  }, null, 8 /* PROPS */, _hoisted_43)])])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])])) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_44, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_EmptyContent, {
    icon: "/icons/no-access-icon-c.svg",
    line1: 'Oups... Vous n\'êtes pas autorisé ',
    line2: "à afficher ce contenu.",
    action: '',
    callback: () => {},
    height: "100px",
    width: "100px"
  }, null, 8 /* PROPS */, ["line1"])]))])]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsHeaderAndFooter.vue?vue&type=template&id=07c46d18&ts=true":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsHeaderAndFooter.vue?vue&type=template&id=07c46d18&ts=true ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "workspace-content h-full overflow-y-auto pb-5 md:pb-0"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row items-center justify-between w-full h-16 workspace-header"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", {
  class: "text-xl text-gray-500"
}, "Devis et factures > Configurations > Entête et pied de page"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row items-center h-full workspace-actions"
})]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Cadre "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-col w-full p-8 bg-white border border-gray-400 rounded-md border-opacity-40",
  style: {
    "height": "auto"
  }
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Banner Set Header file "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-col w-full pr-5 mb-10 space-y-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Title "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row w-full space-x-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/navigation-level-icon.svg",
  alt: "",
  class: "opacity-60"
}), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h2", {
  class: "text-xl font-medium text-gray-400"
}, "Informations d'entêtes")]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "space-y-4"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Set Logo Enterprise "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-col items-center justify-center flex-shrink w-full p-4 px-4 py-12 text-center text-gray-400 bg-gray-100 border-2 border-gray-400 border-dashed cursor-pointer hover:border-green-700 border-opacity-30 hover:text-green-700 rounded-2xl"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/images/avatar-logo/avatar-profile-entreprise.svg",
  alt: "",
  class: "opacity-90"
}), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", null, "2480px - 362px Ratio 1/7"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", null, "Cliquez ici pour configurer votre entête de papier")])])]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Section Left"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-col w-1/3 pr-5 space-y-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Title "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row w-full space-x-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/navigation-level-icon.svg",
  alt: "",
  class: "opacity-60"
}), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h2", {
  class: "text-xl font-medium text-gray-400"
}, "Informations de pieds de page")]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "space-y-4"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Select Component 1"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full mb-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-select", {
  placeholder: "Status Juridique",
  usage: "default",
  "options-data": "[\n\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Sarl \"},\n\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"S.A\"},\n\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Etablissement\"}\n\t\t\t\t\t\t\t\t\t\t\t]",
  disabledd: "offf",
  statee: "false"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
  slot: "native-input",
  readonly: ""
})])]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Input Component 1"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full mb-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-input", {
  flex: "normal",
  placeholder: "Capital Fiancier",
  usage: "default"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
  slot: "native-input"
})])]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Input Component 2"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full mb-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-input", {
  flex: "normal",
  placeholder: "Siege Social",
  usage: "default"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
  slot: "native-input"
})])])])]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Section Middle "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-col w-1/3 pl-5 space-y-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Title "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row w-full space-x-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/navigation-level-icon.svg",
  alt: "",
  class: "opacity-60"
}), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h2", {
  class: "text-xl font-medium text-gray-400"
}, "Informations Sociale")]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "space-y-4"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Input Component 1"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full mb-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-input", {
  flex: "normal",
  placeholder: "Pseudo Facebook",
  usage: "default"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
  slot: "native-input"
})])]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Input Component 2"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full mb-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-input", {
  flex: "normal",
  placeholder: "Pseudo Twitter",
  usage: "default"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
  slot: "native-input"
})])]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Input Component 3"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full mb-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-input", {
  flex: "normal",
  placeholder: "Pseudo LinkedIn",
  usage: "default"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
  slot: "native-input"
})])])])]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Section Right "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-col w-1/3 pl-5 space-y-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Title "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row w-full space-x-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/navigation-level-icon.svg",
  alt: "",
  class: "opacity-60"
}), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h2", {
  class: "text-xl font-medium text-gray-400"
}, "Contact")]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "space-y-4"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Input Component 1"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full mb-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-input", {
  flex: "normal",
  placeholder: "Numéro de Téléphone",
  usage: "tel"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
  slot: "native-input"
})])]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Input Component 2"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full mb-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-input", {
  flex: "normal",
  placeholder: "Adresse E-mail",
  usage: "email"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
  slot: "native-input"
})])])])])]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Footer Set Theme "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-col w-1/3 pr-5 mt-8 mb-10 space-y-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Title "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row w-full space-x-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/navigation-level-icon.svg",
  alt: "",
  class: "opacity-60"
}), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h2", {
  class: "text-xl font-medium text-gray-400"
}, "Thème de Couleur")]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row items-center content-center space-x-4"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", null, " • Couleur par défaut "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "block w-10 h-10 bg-green-600 rounded-full"
})]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Set the new Color "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Input Component 1"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full mb-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-select", {
  placeholder: "Nouvelle couleur",
  usage: "default",
  "options-data": "[\n\t\t\t\t\t\t\t\t{\"value\" : \"Vert \"},\n\t\t\t\t\t\t\t\t{\"value\" : \"Bleu\"},\n\t\t\t\t\t\t\t\t{\"value\" : \"Noir\"},\n\t\t\t\t\t\t\t\t{\"value\" : \"Marron\"},\n\t\t\t\t\t\t\t\t{\"value\" : \"Rouge\"}\n\t\t\t\t\t\t\t\t]"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
  slot: "native-input",
  readonly: ""
})])])]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h2", {
  class: "mt-10 font-bold text-right text-blue-500 cursor-pointer"
}, "Mettre à jour les changements")])], -1 /* HOISTED */);
function render(_ctx, _cache, $props, $setup, $data, $options) {
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" eslint-disable vue/no-deprecated-slot-attribute "), _hoisted_1], 2112 /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsManaginBranches.vue?vue&type=template&id=8b9e6f78&ts=true":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsManaginBranches.vue?vue&type=template&id=8b9e6f78&ts=true ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "workspace-content h-full overflow-hidden md:overflow-y-auto pb-5 md:pb-0"
};
const _hoisted_2 = {
  key: 0,
  class: "w-full md:w-3/6 flex flex-col space-y-8"
};
const _hoisted_3 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full flex flex-row space-x-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/navigation-level-icon.svg",
  alt: "",
  class: "opacity-60"
}), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h2", {
  class: "font-medium text-xl text-gray-600"
}, "Liste des Succursales")], -1 /* HOISTED */);
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_WorkspaceHeader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WorkspaceHeader");
  const _component_BranchesList = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("BranchesList");
  const _component_BranchDetails = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("BranchDetails");
  const _component_EmptyContent = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("EmptyContent");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_WorkspaceHeader, {
    if: "!viewRightContent",
    "active-type-view": $setup.actionType,
    "items-list": $setup.branches,
    "new-item": () => {
      $setup.actionType = 'create';
    },
    options: {
      titleBar: true,
      actionsBar: true,
      fabButtons: true,
      actions: {
        import: false,
        export: false,
        addItem: $setup.useAccess('add', $setup.permissions),
        filter: false
      },
      fabButtonsActions: {
        addItem: $setup.useAccess('add', $setup.permissions),
        moreMenu: true
      }
    }
  }, null, 8 /* PROPS */, ["active-type-view", "items-list", "new-item", "options"]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)(["w-full relative overflow-hidden md:overflow-auto flex border rounded-2xl border-gray-400 border-opacity-40 bg-gray-50 p-8 space-x-0 md:space-x-8", {
      'flex-col justify-center items-center h-full': !$setup.useAccess('read', $setup.permissions) || $setup.branches.length == 0 && $setup.actionType != 'create',
      'flex-row ': $setup.useAccess('read', $setup.permissions) && $setup.branches.length > 0 || $setup.useAccess('read', $setup.permissions) && $setup.branches.length == 0 && $setup.actionType == 'create'
    }]),
    style: {
      "height": "calc(100vh - 13rem)"
    }
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" BRANCHS list "), $setup.useAccess('read', $setup.permissions) && ($setup.branches.length > 0 || $setup.useAccess('read', $setup.permissions) && $setup.branches.length == 0 && $setup.actionType == 'create') ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_2, [_hoisted_3, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_BranchesList)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.useAccess('read', $setup.permissions) && ($setup.branches.length > 0 || $setup.useAccess('read', $setup.permissions) && $setup.branches.length == 0 && $setup.actionType == 'create') ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_BranchDetails, {
    key: 1
  })) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), (!$setup.useAccess('read', $setup.permissions) || $setup.branches.length == 0) && $setup.actionType != 'create' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
    key: 2,
    class: "flex flex-col justify-center items-center h-full",
    onClick: _cache[0] || (_cache[0] = $event => $setup.useAccess('add', $setup.permissions) && $setup.branches.length == 0 ? $setup.actionType = 'create' : null)
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_EmptyContent, {
    icon: $setup.useAccess('read', $setup.permissions) && $setup.branches.length == 0 ? '/img/branches-img.svg' : '/icons/no-access-icon-c.svg',
    line1: $setup.useAccess('read', $setup.permissions) && $setup.branches.length == 0 ? 'Oups ! Vous n\'avez aucune Succursale.' : 'Oups... Vous n\'êtes pas autorisé ',
    line2: $setup.useAccess('read', $setup.permissions) && $setup.branches.length == 0 ? '' : 'à afficher ce contenu.',
    action: $setup.useAccess('read', $setup.permissions) && $setup.branches.length == 0 ? 'Créez-en une ?' : '',
    callback: () => {},
    height: $setup.useAccess('read', $setup.permissions) && $setup.branches.length == 0 ? '' : '100px',
    width: $setup.useAccess('read', $setup.permissions) && $setup.branches.length == 0 ? '' : '100px'
  }, null, 8 /* PROPS */, ["icon", "line1", "line2", "action", "height", "width"])])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)], 2 /* CLASS */)]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsManagingUsers.vue?vue&type=template&id=ed1f9296&scoped=true&ts=true":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsManagingUsers.vue?vue&type=template&id=ed1f9296&scoped=true&ts=true ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _withScopeId = n => ((0,vue__WEBPACK_IMPORTED_MODULE_0__.pushScopeId)("data-v-ed1f9296"), n = n(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.popScopeId)(), n);
const _hoisted_1 = {
  class: "relative h-full pb-5 overflow-y-auto workspace-content md:pb-0"
};
const _hoisted_2 = {
  class: "flex w-full py-5"
};
const _hoisted_3 = {
  key: 0
};
const _hoisted_4 = {
  key: 0,
  class: "flex flex-col w-full h-full pr-2 md:hidden item-list"
};
const _hoisted_5 = {
  class: "flex flex-col left-infos"
};
const _hoisted_6 = {
  class: "text-xl"
};
const _hoisted_7 = {
  class: "text-sm text-gray-400"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_SkeletonDataTable = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SkeletonDataTable");
  const _component_SkeletonLoader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SkeletonLoader");
  const _component_WorkspaceHeader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WorkspaceHeader");
  const _component_UsersDatatable = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("UsersDatatable");
  const _component_UsersDetails = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("UsersDetails");
  const _component_ItemCard = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("ItemCard");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isFetching ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_SkeletonLoader, {
      key: 0
    }, {
      default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SkeletonDataTable)]),
      _: 1 /* STABLE */
    })) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), !$setup.isFetching ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_WorkspaceHeader, {
    key: 0,
    "active-type-view": $setup.actionType,
    "items-list": $setup.staffs,
    "file-name": $setup.filename('Personnal'),
    "new-item": () => {
      if ($setup.roles?.length == 0) {
        $setup.notify.warning('Veuillez créer des roles avant de créer un utilisateur pour cette succursale');
      } else {
        $setup.updateSubView('create');
      }
    },
    "data-to-p-d-f": {
      data: $setup.staffs,
      which: 'Staff'
    },
    "get-filtered": $setup.getAllStaffs,
    "date-debut": $setup.dateDebut,
    "date-fin": $setup.dateFin,
    options: {
      titleBar: true,
      actionsBar: true,
      fabButtons: true,
      actions: {
        import: false,
        export: $setup.useAccess('read', $setup.permissions),
        addItem: $setup.useAccess('add', $setup.permissions) && $setup.isOwnBranch,
        filter: $setup.useAccess('read', $setup.permissions)
      },
      fabButtonsActions: {
        addItem: $setup.useAccess('add', $setup.permissions) && $setup.isOwnBranch,
        moreMenu: true
      }
    }
  }, null, 8 /* PROPS */, ["active-type-view", "items-list", "file-name", "new-item", "data-to-p-d-f", "date-debut", "date-fin", "options"])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
      'hidden md:flex transition-all duration-500 ease-in-out': true,
      'w-full': $setup.actionType == 'list',
      'w-3/5': $setup.actionType != 'list'
    })
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_UsersDatatable)], 2 /* CLASS */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
      'transition-all duration-500 ease-in-out bg-gray-100 ': true,
      'w-0': $setup.actionType == 'list',
      'w-full md:w-2/5 px-0 md:px-4': $setup.actionType != 'list'
    })
  }, [$setup.actionType != 'list' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_UsersDetails, {
    key: 0
  })) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)], 2 /* CLASS */), !$setup.isFetching ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("  Products list For Mobile "), $setup.actionType == 'list' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_4, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.staffs, (item, index) => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_ItemCard, {
      key: index,
      label: `${item?.user.firstname} ${item?.user.lastname}`,
      "avatar-image": $setup.imagesList[index],
      onClick: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withModifiers)($event => $setup.useAccess('update', $setup.permissions) ? $setup.editSelected({
        index,
        data: item
      }) : null, ["prevent"])
    }, {
      "card-content": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_5, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_6, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(`${item?.user.firstname} ${item?.user.lastname}`), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_7, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item?.user.email), 1 /* TEXT */)])]),

      _: 2 /* DYNAMIC */
    }, 1032 /* PROPS, DYNAMIC_SLOTS */, ["label", "avatar-image", "onClick"]);
  }), 128 /* KEYED_FRAGMENT */))])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsOverview.vue?vue&type=template&id=4e0f205a&ts=true":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsOverview.vue?vue&type=template&id=4e0f205a&ts=true ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "workspace-content h-full overflow-x-hidden pb-5 md:pb-0 relative"
};
const _hoisted_2 = {
  class: "settings-page-links w-full grid gap-6 mt-10 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-3 2xl:grid-cols-4"
};
const _hoisted_3 = {
  key: 0,
  class: "w-full"
};
const _hoisted_4 = {
  key: 1,
  class: "w-full"
};
const _hoisted_5 = {
  key: 2,
  class: "w-full"
};
const _hoisted_6 = {
  key: 3,
  class: "w-full"
};
const _hoisted_7 = {
  key: 4,
  class: "w-full"
};
const _hoisted_8 = {
  key: 5,
  class: "w-full"
};
const _hoisted_9 = {
  key: 6,
  class: "w-full"
};
const _hoisted_10 = {
  key: 7,
  class: "w-full"
};
const _hoisted_11 = {
  key: 8,
  class: "absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 mt-12 z-100"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_CardBannerSettings = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("CardBannerSettings");
  const _component_SettingPageCard = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SettingPageCard");
  const _component_EmptyContent = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("EmptyContent");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_CardBannerSettings, {
    icon: "settings-banner-picture",
    "line-one": "Paramètres",
    "line-two-a": "Dans cette section vous avez la possibilité ",
    "line-two-b": "de configurer l’application selon vos préférences.",
    "line-three": "Lancer le tutoriel d'aide ",
    link: "workspace-settings-business-account"
  }), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_2, [$setup.useAccess('visibility', ['enterprise', 'staff', 'particular']) == true ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SettingPageCard, {
    icon: "profile-icon-line",
    "line-one": "Mon compte",
    "line-two": "business",
    link: "workspace-settings-business-account"
  })])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.useAccess('visibility', ['particular', 'enterprise', 'staff']) == true ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_4, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SettingPageCard, {
    icon: "wallet-tick-icon",
    "line-one": "Mon historique",
    "line-two": "d'abonements",
    link: "workspace-settings-subscriptions"
  })])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.useAccess('visibility', ['particular', 'enterprise', 'staff']) == true ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_5, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SettingPageCard, {
    icon: "notification-bing-icon",
    "line-one": "Alertes",
    "line-two": "& notifications",
    link: "workspace-settings-general-settings"
  })])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.useAccess('visibility', ['particular', 'enterprise', 'staff']) == true ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_6, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SettingPageCard, {
    icon: "sms-icon",
    "line-one": "SMS",
    "line-two": "& Emails",
    link: "workspace-settings-sms-and-emails"
  })])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.useAccess('visibility', ['enterprise', 'staff']) == true ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_7, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SettingPageCard, {
    icon: "shield-tick-icon-line",
    "line-one": "Roles",
    "line-two": "& permissions",
    link: "workspace-settings-roles-and-permissions"
  })])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.useAccess('visibility', ['enterprise', 'staff']) == true ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_8, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SettingPageCard, {
    icon: "profile-2user-icon-line",
    "line-one": "Gestion",
    "line-two": "des utilisateurs",
    link: "workspace-settings-managing-users"
  })])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.useAccess('visibility', ['enterprise', 'staff']) == true ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_9, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SettingPageCard, {
    icon: "buildings-2-icon",
    "line-one": "Gestion",
    "line-two": "des branches",
    link: "workspace-settings-managing-branches"
  })])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <div\n\t\t\t\tv-if=\"useAccess('visibility', ['particular', 'enterprise']) == true\"\n\t\t\t\tclass=\"w-full\"\n\t\t\t>\n\t\t\t\t<SettingPageCard\n\t\t\t\t\ticon=\"key-icon\"\n\t\t\t\t\tlineOne=\"Sécurité\"\n\t\t\t\t\tlineTwo=\"& surveillances\"\n\t\t\t\t\tlink=\"workspace-settings-security\"\n\t\t\t\t/>\n\t\t\t</div> "), $setup.useAccess('visibility', ['particular', 'enterprise', 'staff']) == true ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_10, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SettingPageCard, {
    icon: "reset-icon",
    "line-one": "Rénitialisation",
    "line-two": "Effacer les données",
    link: "workspace-settings-reset"
  })])) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_11, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_EmptyContent, {
    icon: '/icons/no-access-icon-c.svg',
    line1: 'Oups... Contenu inaccessible ',
    line2: '',
    action: '',
    height: "100px",
    width: "100px"
  }, null, 8 /* PROPS */, ["icon", "line1"])]))])]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsReset.vue?vue&type=template&id=1e15ef84&ts=true":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsReset.vue?vue&type=template&id=1e15ef84&ts=true ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "h-full pb-5 overflow-y-auto workspace-content md:pb-0"
};
const _hoisted_2 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row items-center justify-between w-full h-16 workspace-header"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", {
  class: "text-xl text-gray-500"
}, "Paramètres > Réinitialisation des données"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row items-center h-full workspace-actions"
})], -1 /* HOISTED */);
const _hoisted_3 = {
  key: 0,
  class: "flex flex-col w-full px-8 pt-8 space-y-4 bg-white bg-opacity-40 border border-gray-400 border-opacity-40 rounded-lg",
  style: {
    "height": "calc(100% - 5rem)"
  }
};
const _hoisted_4 = {
  class: "w-full mb-4"
};
const _hoisted_5 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full"
}, " Vous êtes sur le point d'effectuer une réinitialisation des données de votre compte. Cela signifie que vous perdrez certaines informations de votre compte. Après avoir sélectionné les données à réinitialiser et validé, nous vous enverrons un code de confirmation par email. ", -1 /* HOISTED */);
const _hoisted_6 = {
  key: 1,
  class: "w-full flex flex-col overflow-y-auto",
  style: {
    "height": "calc(100% - 12rem)"
  }
};
const _hoisted_7 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h2", {
  class: "font-medium text-gray-600 mb-2"
}, " Veuillez sélectionner les données à réinitialiser ", -1 /* HOISTED */);
const _hoisted_8 = {
  class: "w-full flex flex-row space-x-2 items-center p-4 border-t border-opacity-50 border-gray-400"
};
const _hoisted_9 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "h-full w-1 rounded-xl bg-green-600"
}, null, -1 /* HOISTED */);
const _hoisted_10 = ["checked"];
const _hoisted_11 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
  for: "categories",
  class: "text-xl"
}, "Toutes les Catégories", -1 /* HOISTED */);
const _hoisted_12 = {
  class: "w-full flex flex-row space-x-2 items-center p-4 border-t border-opacity-50 border-gray-400"
};
const _hoisted_13 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "h-full w-1 rounded-xl bg-green-600"
}, null, -1 /* HOISTED */);
const _hoisted_14 = ["checked"];
const _hoisted_15 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
  for: "products",
  class: "text-xl"
}, "Vos Produits", -1 /* HOISTED */);
const _hoisted_16 = {
  class: "w-full flex flex-row space-x-2 items-center p-4 border-t border-opacity-50 border-gray-400"
};
const _hoisted_17 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "h-full w-1 rounded-xl bg-green-600"
}, null, -1 /* HOISTED */);
const _hoisted_18 = ["checked"];
const _hoisted_19 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
  for: "services",
  class: "text-xl"
}, "Vos Services", -1 /* HOISTED */);
const _hoisted_20 = {
  class: "w-full flex flex-row space-x-2 items-center p-4 border-t border-opacity-50 border-gray-400"
};
const _hoisted_21 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "h-full w-1 rounded-xl bg-green-600"
}, null, -1 /* HOISTED */);
const _hoisted_22 = ["checked"];
const _hoisted_23 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
  for: "rebates",
  class: "text-xl"
}, "Enregistrements de Ristournes", -1 /* HOISTED */);
const _hoisted_24 = {
  class: "w-full flex flex-row space-x-2 items-center p-4 border-t border-opacity-50 border-gray-400"
};
const _hoisted_25 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "h-full w-1 rounded-xl bg-green-600"
}, null, -1 /* HOISTED */);
const _hoisted_26 = ["checked"];
const _hoisted_27 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
  for: "damages",
  class: "text-xl"
}, "Enregistrements d'avaries", -1 /* HOISTED */);
const _hoisted_28 = {
  class: "w-full flex flex-row space-x-2 items-center p-4 border-t border-opacity-50 border-gray-400"
};
const _hoisted_29 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "h-full w-1 rounded-xl bg-green-600"
}, null, -1 /* HOISTED */);
const _hoisted_30 = ["checked"];
const _hoisted_31 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
  for: "sales",
  class: "text-xl"
}, "Opérations de vente de marchandises", -1 /* HOISTED */);
const _hoisted_32 = {
  class: "w-full flex flex-row space-x-2 items-center p-4 border-t border-opacity-50 border-gray-400"
};
const _hoisted_33 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "h-full w-1 rounded-xl bg-green-600"
}, null, -1 /* HOISTED */);
const _hoisted_34 = ["checked"];
const _hoisted_35 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
  for: "saleServices",
  class: "text-xl"
}, "Opérations de vente de services", -1 /* HOISTED */);
const _hoisted_36 = {
  class: "w-full flex flex-row space-x-2 items-center p-4 border-t border-opacity-50 border-gray-400"
};
const _hoisted_37 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "h-full w-1 rounded-xl bg-green-600"
}, null, -1 /* HOISTED */);
const _hoisted_38 = ["checked"];
const _hoisted_39 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
  for: "purchases",
  class: "text-xl"
}, "Opérations d'achats", -1 /* HOISTED */);
const _hoisted_40 = {
  class: "w-full flex flex-row space-x-2 items-center p-4 border-t border-opacity-50 border-gray-400"
};
const _hoisted_41 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "h-full w-1 rounded-xl bg-green-600"
}, null, -1 /* HOISTED */);
const _hoisted_42 = ["checked"];
const _hoisted_43 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
  for: "thirds",
  class: "text-xl"
}, "Tiers (Clients, fournisseurs, Bailleurs de fonds, Créancier, etc", -1 /* HOISTED */);
const _hoisted_44 = {
  class: "w-full flex flex-row space-x-2 items-center p-4 border-t border-opacity-50 border-gray-400"
};
const _hoisted_45 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "h-full w-1 rounded-xl bg-green-600"
}, null, -1 /* HOISTED */);
const _hoisted_46 = ["checked"];
const _hoisted_47 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
  for: "expenses",
  class: "text-xl"
}, "Opérations de Dépenses", -1 /* HOISTED */);
const _hoisted_48 = {
  class: "w-full flex flex-row space-x-2 items-center p-4 border-t border-opacity-50 border-gray-400"
};
const _hoisted_49 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "h-full w-1 rounded-xl bg-green-600"
}, null, -1 /* HOISTED */);
const _hoisted_50 = ["checked"];
const _hoisted_51 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
  for: "loans",
  class: "text-xl"
}, "Opérations d'Emprunts", -1 /* HOISTED */);
const _hoisted_52 = {
  class: "w-full flex flex-row space-x-2 items-center p-4 border-t border-opacity-50 border-gray-400"
};
const _hoisted_53 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "h-full w-1 rounded-xl bg-green-600"
}, null, -1 /* HOISTED */);
const _hoisted_54 = ["checked"];
const _hoisted_55 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
  for: "grants",
  class: "text-xl"
}, "Opérations de Subventions d'exploitation", -1 /* HOISTED */);
const _hoisted_56 = {
  class: "w-full flex flex-row space-x-2 items-center p-4 border-t border-opacity-50 border-gray-400"
};
const _hoisted_57 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "h-full w-1 rounded-xl bg-green-600"
}, null, -1 /* HOISTED */);
const _hoisted_58 = ["checked"];
const _hoisted_59 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
  for: "initialisations",
  class: "text-xl"
}, "Initialisations des Comptes (banque, caisse, electronique)", -1 /* HOISTED */);
const _hoisted_60 = {
  class: "w-full flex flex-row space-x-2 items-center p-4 border-t border-opacity-50 border-gray-400"
};
const _hoisted_61 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "h-full w-1 rounded-xl bg-green-600"
}, null, -1 /* HOISTED */);
const _hoisted_62 = ["checked"];
const _hoisted_63 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
  for: "creance",
  class: "text-xl"
}, "Créances Clients", -1 /* HOISTED */);
const _hoisted_64 = {
  class: "w-full flex flex-row space-x-2 items-center p-4 border-t border-opacity-50 border-gray-400"
};
const _hoisted_65 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "h-full w-1 rounded-xl bg-green-600"
}, null, -1 /* HOISTED */);
const _hoisted_66 = ["checked"];
const _hoisted_67 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
  for: "debts",
  class: "text-xl"
}, "Enregistrements de Dettes", -1 /* HOISTED */);
const _hoisted_68 = {
  class: "w-full flex flex-row space-x-2 items-center p-4 border-t border-opacity-50 border-gray-400"
};
const _hoisted_69 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "h-full w-1 rounded-xl bg-green-600"
}, null, -1 /* HOISTED */);
const _hoisted_70 = ["checked"];
const _hoisted_71 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
  for: "checkout",
  class: "text-xl"
}, "Fonds de Compte Caisse (hormis les initialisations de compte)", -1 /* HOISTED */);
const _hoisted_72 = {
  class: "w-full flex flex-row space-x-2 items-center p-4 border-t border-opacity-50 border-gray-400"
};
const _hoisted_73 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "h-full w-1 rounded-xl bg-green-600"
}, null, -1 /* HOISTED */);
const _hoisted_74 = ["checked"];
const _hoisted_75 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
  for: "bank",
  class: "text-xl"
}, "Fonds de Compte Banque (hormis les initialisations de compte)", -1 /* HOISTED */);
const _hoisted_76 = {
  class: "w-full flex flex-row space-x-2 items-center p-4 border-t border-opacity-50 border-gray-400"
};
const _hoisted_77 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "h-full w-1 rounded-xl bg-green-600"
}, null, -1 /* HOISTED */);
const _hoisted_78 = ["checked"];
const _hoisted_79 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
  for: "electronic",
  class: "text-xl"
}, "Fonds de Comptes Mobile Money (hormis les initialisations de compte)", -1 /* HOISTED */);
const _hoisted_80 = {
  class: "w-full flex flex-row space-x-2 items-center p-4 border-t border-opacity-50 border-gray-400"
};
const _hoisted_81 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "h-full w-1 rounded-xl bg-green-600"
}, null, -1 /* HOISTED */);
const _hoisted_82 = ["checked"];
const _hoisted_83 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
  for: "files",
  class: "text-xl"
}, "Fichiers de pièces jointes", -1 /* HOISTED */);
const _hoisted_84 = {
  class: "w-full flex flex-row space-x-2 items-center p-4 border-t border-opacity-50 border-gray-400"
};
const _hoisted_85 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "h-full w-1 rounded-xl bg-green-600"
}, null, -1 /* HOISTED */);
const _hoisted_86 = ["checked"];
const _hoisted_87 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("label", {
  for: "devis",
  class: "text-xl"
}, "Opérations de devis", -1 /* HOISTED */);
const _hoisted_88 = {
  key: 2,
  class: "w-full flex flex-row space-x-2 items-center justify-end"
};
const _hoisted_89 = {
  key: 0
};
const _hoisted_90 = {
  key: 1
};
const _hoisted_91 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", null, " Entrez le code de confirmation reçu par mail. ", -1 /* HOISTED */);
const _hoisted_92 = {
  key: 2,
  class: "w-auto"
};
const _hoisted_93 = {
  flex: "normal",
  placeholder: "Code de confirmation"
};
const _hoisted_94 = ["text", "loading"];
const _hoisted_95 = {
  key: 3,
  class: "w-full flex flex-row space-x-2 items-center justify-end"
};
const _hoisted_96 = {
  key: 1,
  class: "flex flex-col justify-center items-center h-full"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_SlimTitleBar = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SlimTitleBar");
  const _component_EmptyContent = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("EmptyContent");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" eslint-disable vue/no-deprecated-slot-attribute "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Route "), _hoisted_2, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Cadre "), $setup.useAccess('add', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Title "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_4, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SlimTitleBar, {
    title: "Formulaire de réinitialisation",
    icon: "navigation-level-icon"
  })]), _hoisted_5, $setup.activeView === 1 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
    key: 0,
    class: "font-medium text-green-500 hover:text-green-400 cursor-pointer",
    onClick: _cache[0] || (_cache[0] = $event => $setup.activeView = 2)
  }, " Êtes-vous sûr de vouloir continuer ? ")) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeView != 1 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_6, [_hoisted_7, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_8, [_hoisted_9, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    id: "categories",
    "onUpdate:modelValue": _cache[1] || (_cache[1] = $event => $setup.selectedData.categories = $event),
    type: "checkbox",
    name: "",
    class: "border w-4 border-gray-400 h-full",
    checked: $setup.selectedData.categories
  }, null, 8 /* PROPS */, _hoisted_10), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelCheckbox, $setup.selectedData.categories]]), _hoisted_11]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_12, [_hoisted_13, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    id: "products",
    "onUpdate:modelValue": _cache[2] || (_cache[2] = $event => $setup.selectedData.products = $event),
    type: "checkbox",
    name: "",
    class: "border w-4 border-gray-400 h-full",
    checked: $setup.selectedData.products
  }, null, 8 /* PROPS */, _hoisted_14), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelCheckbox, $setup.selectedData.products]]), _hoisted_15]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_16, [_hoisted_17, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    id: "services",
    "onUpdate:modelValue": _cache[3] || (_cache[3] = $event => $setup.selectedData.services = $event),
    type: "checkbox",
    name: "",
    class: "border w-4 border-gray-400 h-full",
    checked: $setup.selectedData.services
  }, null, 8 /* PROPS */, _hoisted_18), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelCheckbox, $setup.selectedData.services]]), _hoisted_19]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_20, [_hoisted_21, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    id: "rebates",
    "onUpdate:modelValue": _cache[4] || (_cache[4] = $event => $setup.selectedData.rebates = $event),
    type: "checkbox",
    name: "",
    class: "border w-4 border-gray-400 h-full",
    checked: $setup.selectedData.rebates
  }, null, 8 /* PROPS */, _hoisted_22), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelCheckbox, $setup.selectedData.rebates]]), _hoisted_23]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_24, [_hoisted_25, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    id: "damages",
    "onUpdate:modelValue": _cache[5] || (_cache[5] = $event => $setup.selectedData.damages = $event),
    type: "checkbox",
    name: "",
    class: "border w-4 border-gray-400 h-full",
    checked: $setup.selectedData.damages
  }, null, 8 /* PROPS */, _hoisted_26), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelCheckbox, $setup.selectedData.damages]]), _hoisted_27]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_28, [_hoisted_29, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    id: "sales",
    "onUpdate:modelValue": _cache[6] || (_cache[6] = $event => $setup.selectedData.sales = $event),
    type: "checkbox",
    name: "",
    class: "border w-4 border-gray-400 h-full",
    checked: $setup.selectedData.sales
  }, null, 8 /* PROPS */, _hoisted_30), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelCheckbox, $setup.selectedData.sales]]), _hoisted_31]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_32, [_hoisted_33, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    id: "saleServices",
    "onUpdate:modelValue": _cache[7] || (_cache[7] = $event => $setup.selectedData.saleServices = $event),
    type: "checkbox",
    name: "",
    class: "border w-4 border-gray-400 h-full",
    checked: $setup.selectedData.saleServices
  }, null, 8 /* PROPS */, _hoisted_34), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelCheckbox, $setup.selectedData.saleServices]]), _hoisted_35]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_36, [_hoisted_37, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    id: "purchases",
    "onUpdate:modelValue": _cache[8] || (_cache[8] = $event => $setup.selectedData.purchases = $event),
    type: "checkbox",
    name: "",
    class: "border w-4 border-gray-400 h-full",
    checked: $setup.selectedData.purchases
  }, null, 8 /* PROPS */, _hoisted_38), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelCheckbox, $setup.selectedData.purchases]]), _hoisted_39]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_40, [_hoisted_41, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    id: "thirds",
    "onUpdate:modelValue": _cache[9] || (_cache[9] = $event => $setup.selectedData.thirds = $event),
    type: "checkbox",
    name: "",
    class: "border w-4 border-gray-400 h-full",
    checked: $setup.selectedData.thirds
  }, null, 8 /* PROPS */, _hoisted_42), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelCheckbox, $setup.selectedData.thirds]]), _hoisted_43]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_44, [_hoisted_45, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    id: "expenses",
    "onUpdate:modelValue": _cache[10] || (_cache[10] = $event => $setup.selectedData.expenses = $event),
    type: "checkbox",
    name: "",
    class: "border w-4 border-gray-400 h-full",
    checked: $setup.selectedData.expenses
  }, null, 8 /* PROPS */, _hoisted_46), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelCheckbox, $setup.selectedData.expenses]]), _hoisted_47]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_48, [_hoisted_49, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    id: "loans",
    "onUpdate:modelValue": _cache[11] || (_cache[11] = $event => $setup.selectedData.loans = $event),
    type: "checkbox",
    name: "",
    class: "border w-4 border-gray-400 h-full",
    checked: $setup.selectedData.loans
  }, null, 8 /* PROPS */, _hoisted_50), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelCheckbox, $setup.selectedData.loans]]), _hoisted_51]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_52, [_hoisted_53, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    id: "grants",
    "onUpdate:modelValue": _cache[12] || (_cache[12] = $event => $setup.selectedData.grants = $event),
    type: "checkbox",
    name: "",
    class: "border w-4 border-gray-400 h-full",
    checked: $setup.selectedData.grants
  }, null, 8 /* PROPS */, _hoisted_54), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelCheckbox, $setup.selectedData.grants]]), _hoisted_55]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_56, [_hoisted_57, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    id: "initialisations",
    "onUpdate:modelValue": _cache[13] || (_cache[13] = $event => $setup.selectedData.initialisations = $event),
    type: "checkbox",
    name: "",
    class: "border w-4 border-gray-400 h-full",
    checked: $setup.selectedData.initialisations
  }, null, 8 /* PROPS */, _hoisted_58), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelCheckbox, $setup.selectedData.initialisations]]), _hoisted_59]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_60, [_hoisted_61, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    id: "creance",
    "onUpdate:modelValue": _cache[14] || (_cache[14] = $event => $setup.selectedData.creance = $event),
    type: "checkbox",
    name: "",
    class: "border w-4 border-gray-400 h-full",
    checked: $setup.selectedData.creance
  }, null, 8 /* PROPS */, _hoisted_62), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelCheckbox, $setup.selectedData.creance]]), _hoisted_63]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_64, [_hoisted_65, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    id: "debts",
    "onUpdate:modelValue": _cache[15] || (_cache[15] = $event => $setup.selectedData.debts = $event),
    type: "checkbox",
    name: "",
    class: "border w-4 border-gray-400 h-full",
    checked: $setup.selectedData.debts
  }, null, 8 /* PROPS */, _hoisted_66), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelCheckbox, $setup.selectedData.debts]]), _hoisted_67]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_68, [_hoisted_69, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    id: "checkout",
    "onUpdate:modelValue": _cache[16] || (_cache[16] = $event => $setup.selectedData.checkout = $event),
    type: "checkbox",
    name: "",
    class: "border w-4 border-gray-400 h-full",
    checked: $setup.selectedData.checkout
  }, null, 8 /* PROPS */, _hoisted_70), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelCheckbox, $setup.selectedData.checkout]]), _hoisted_71]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_72, [_hoisted_73, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    id: "bank",
    "onUpdate:modelValue": _cache[17] || (_cache[17] = $event => $setup.selectedData.bank = $event),
    type: "checkbox",
    name: "",
    class: "border w-4 border-gray-400 h-full",
    checked: $setup.selectedData.bank
  }, null, 8 /* PROPS */, _hoisted_74), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelCheckbox, $setup.selectedData.bank]]), _hoisted_75]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_76, [_hoisted_77, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    id: "electronic",
    "onUpdate:modelValue": _cache[18] || (_cache[18] = $event => $setup.selectedData.electronic = $event),
    type: "checkbox",
    name: "",
    class: "border w-4 border-gray-400 h-full",
    checked: $setup.selectedData.electronic
  }, null, 8 /* PROPS */, _hoisted_78), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelCheckbox, $setup.selectedData.electronic]]), _hoisted_79]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_80, [_hoisted_81, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    id: "files",
    "onUpdate:modelValue": _cache[19] || (_cache[19] = $event => $setup.selectedData.files = $event),
    type: "checkbox",
    name: "",
    class: "border w-4 border-gray-400 h-full",
    checked: $setup.selectedData.files
  }, null, 8 /* PROPS */, _hoisted_82), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelCheckbox, $setup.selectedData.files]]), _hoisted_83]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_84, [_hoisted_85, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    id: "devis",
    "onUpdate:modelValue": _cache[20] || (_cache[20] = $event => $setup.selectedData.devis = $event),
    type: "checkbox",
    name: "",
    class: "border w-4 border-gray-400 h-full",
    checked: $setup.selectedData.devis
  }, null, 8 /* PROPS */, _hoisted_86), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelCheckbox, $setup.selectedData.devis]]), _hoisted_87])])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeView != 1 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_88, [$setup.activeView === 2 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("p", _hoisted_89, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.numberOfselectedData) + " élément" + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.numberOfselectedData > 1 ? "s" : "") + " sélectonné" + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.numberOfselectedData > 1 ? "s" : ""), 1 /* TEXT */)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeView === 3 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_90, [_hoisted_91, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
    class: "font-medium text-green-500 hover:text-green-400 cursor-pointer",
    onClick: $setup.resendCode
  }, " (Renvoyer le code) ")])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeView === 3 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_92, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-input", _hoisted_93, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
    slot: "native-input",
    "onUpdate:modelValue": _cache[21] || (_cache[21] = $event => $setup.code = $event)
  }, null, 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelText, $setup.code]])])])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: "w-auto cursor-pointer",
    onClick: $setup.action
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-button", {
    flex: "normal",
    type: "link",
    url: "javascript:void(0)",
    text: $setup.activeView == 2 ? $setup.isLoading ? 'Validation en cours ...' : 'Valider' : $setup.isLoading ? 'Réinitialisation en cours ...' : 'Réinitialiser',
    usage: "simple-text",
    loading: $setup.isLoading,
    "text-color": "white",
    class: "font-light"
  }, null, 8 /* PROPS */, _hoisted_94)])])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeView != 1 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_95)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_96, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_EmptyContent, {
    icon: "/icons/no-access-icon-c.svg",
    line1: 'Oups... Vous n\'êtes pas autorisé ',
    line2: "à afficher ce contenu.",
    action: '',
    callback: () => {},
    height: "100px",
    width: "100px"
  }, null, 8 /* PROPS */, ["line1"])]))])], 2112 /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsRolesAndPermissions.vue?vue&type=template&id=6235a379&scoped=true&ts=true":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsRolesAndPermissions.vue?vue&type=template&id=6235a379&scoped=true&ts=true ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _withScopeId = n => ((0,vue__WEBPACK_IMPORTED_MODULE_0__.pushScopeId)("data-v-6235a379"), n = n(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.popScopeId)(), n);
const _hoisted_1 = {
  class: "relative w-full h-full pb-5 overflow-x-hidden overflow-y-auto workspace-content md:pb-0"
};
const _hoisted_2 = {
  key: 1,
  class: "hidden w-full transition-all duration-500 ease-in-out bg-gray-100 md:flex"
};
const _hoisted_3 = {
  class: "flex pt-56 md:hidden"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_SkeletonDataTable = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SkeletonDataTable");
  const _component_SkeletonLoader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SkeletonLoader");
  const _component_WorkspaceHeader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WorkspaceHeader");
  const _component_RolesDatatable = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("RolesDatatable");
  const _component_RolesDetails = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("RolesDetails");
  const _component_EmptyContent = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("EmptyContent");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isFetching ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_SkeletonLoader, {
      key: 0,
      class: "hidden md:block"
    }, {
      default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SkeletonDataTable)]),
      _: 1 /* STABLE */
    })) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), !$setup.isFetching && $setup.activeView == 1 && $setup.windowWidth > 425 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_WorkspaceHeader, {
    key: 0,
    "active-view": $setup.activeView,
    "items-list": $setup.roles,
    "file-name": $setup.filename('roles'),
    "data-to-export": $setup.exportRoles(''),
    "data-to-p-d-f": {
      data: $setup.roles,
      which: 'Roles'
    },
    "new-item": () => {
      $setup.actionType = $setup.actionType == 'list' ? 'create' : $setup.actionType;
      $setup.resetFields();
    },
    "get-filtered": $setup.getAllRoles,
    "date-debut": $setup.dateDebut,
    "date-fin": $setup.dateFin,
    options: {
      titleBar: true,
      actionsBar: true,
      fabButtons: true,
      actions: {
        import: false,
        export: $setup.useAccess('read', $setup.permissions),
        addItem: $setup.useAccess('add', $setup.permissions),
        filter: $setup.useAccess('read', $setup.permissions)
      },
      fabButtonsActions: {
        addItem: $setup.useAccess('add', $setup.permissions),
        moreMenu: true
      }
    }
  }, null, 8 /* PROPS */, ["active-view", "items-list", "file-name", "data-to-export", "data-to-p-d-f", "new-item", "date-debut", "date-fin", "options"])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), !$setup.isLoading ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
      'transition-all duration-500 ease-in-out': true,
      'w-full hidden md:flex': $setup.actionType == 'list',
      'w-3/5 hidden md:flex': $setup.actionType != 'list'
    })
  }, [$setup.activeView == 1 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_RolesDatatable, {
    key: 0
  })) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)], 2 /* CLASS */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
      'transition-all duration-500 ease-in-out bg-gray-100': true,
      'w-0': $setup.actionType == 'list',
      'w-full md:w-2/5 px-0 md:px-4': $setup.actionType != 'list'
    })
  }, [$setup.actionType != 'list' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_RolesDetails, {
    key: 0
  })) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)], 2 /* CLASS */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_EmptyContent, {
    "height-parent": "50vh -25rem",
    icon: $setup.useAccess('read', $setup.permissions) ? '/img/Box-empty-img.svg' : '/img/Box-empty-img-no-access.svg',
    line1: $setup.useAccess('read', $setup.permissions) ? 'Non disponible sur mobile' : 'Non disponible sur mobile',
    line2: "",
    action: $setup.useAccess('read', $setup.permissions) ? 'Retour à la page précédente' : 'Retour à la page précédente'
  }, null, 8 /* PROPS */, ["icon", "line1", "action"])])]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsSMSAndEmail.vue?vue&type=template&id=3e2162ad&ts=true":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsSMSAndEmail.vue?vue&type=template&id=3e2162ad&ts=true ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "workspace-content h-full overflow-y-auto pb-5 md:pb-0"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Route "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row items-center justify-between w-full h-16 workspace-header"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", {
  class: "text-xl text-gray-500"
}, "Paramètres > SMS et Emails"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row items-center h-full workspace-actions"
})]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Cadre "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row w-full p-8 bg-white border border-gray-400 border-opacity-40",
  style: {
    "height": "auto"
  }
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Section Left LG"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-col w-1/2 pr-5 space-y-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Title "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row w-full space-x-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/navigation-level-icon.svg",
  alt: "",
  class: "opacity-60"
}), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h2", {
  class: "text-xl font-medium text-gray-400"
}, "Configuration Modèles E-mail")]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "space-y-4"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" input "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "pl-2 pr-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full mb-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-select", {
  placeholder: "Sélectionner un Template Email",
  usage: "default",
  "options-data": "[\n\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Réinitialisation de mot de passe\"},\n\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"E-mail Acceuil\"}\n\t\t\t\t\t\t\t\t\t\t\t\t]",
  disabledd: "offf",
  statee: "false"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
  slot: "native-input",
  readonly: ""
})])])]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" input "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "pl-2 pr-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full mb-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-select", {
  flex: "normal",
  placeholder: "Insérer un champ",
  usage: "default",
  "options-data": "[\n\t\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Nom du Client\"},\n\t\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Type de Document\"},\n\t\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Date de Document\"},\n\t\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Reference De Document\"},\n\t\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Nom Utilisateur\"},\n\t\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Nom de Entreprise\"}\n\t\t\t\t\t\t\t\t\t\t\t\t\t]",
  disabledd: "offf",
  statee: "false"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
  slot: "native-input",
  readonly: ""
})])])]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" input "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "pl-2 pr-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full mb-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-input", {
  flex: "normal",
  placeholder: "Objet du E-mail",
  usage: "default",
  "options-data": "[\n\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"5NKAP | Activation de votre Compte\"}\n\t\t\t\t\t\t\t\t\t\t\t\t]"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
  slot: "native-input"
})])])]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Commentaire "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "px-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full p-4 mb-4 bg-white border border-gray-100 rounded-xl"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "mb-2 text-lg font-normal"
}, "Message"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("textarea", {
  id: "",
  name: "",
  rows: "8",
  placeholder: "",
  class: "w-full pl-2 font-light focus:outline-none"
}, "\n\tHi Arthur,\n\tBienvenu dans la communauté 5NKAP!\n\tCliquez sur  ce Lien  pour activer votre compte\n\n\n\t\t\t\t\t\t\t\t")])])])]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Section Right LG"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-col w-1/2 pl-5 space-y-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Title "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row w-full space-x-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/navigation-level-icon.svg",
  alt: "",
  class: "opacity-60"
}), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h2", {
  class: "text-xl font-medium text-gray-400"
}, "Configuration Modèles SMS")]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "space-y-4"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" input "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "pl-2 pr-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full mb-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-select", {
  placeholder: "Sélectionner un Template SMS",
  usage: "default",
  "options-data": "[\n\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Réinitialisation de mot de passe\"},\n\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"E-mail Acceuil\"}\n\t\t\t\t\t\t\t\t\t\t\t\t]",
  disabledd: "offf",
  statee: "false"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
  slot: "native-input",
  readonly: ""
})])])]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" input "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "pl-2 pr-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full mb-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-select", {
  placeholder: "Insérer un champ",
  usage: "default",
  "options-data": "[\n\t\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Nom du Client\"},\n\t\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Type de Document\"},\n\t\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Date de Document\"},\n\t\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Reference De Document\"},\n\t\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Nom Utilisateur\"},\n\t\t\t\t\t\t\t\t\t\t\t\t\t{\"value\" : \"Nom de Entreprise\"}\n\t\t\t\t\t\t\t\t\t\t\t\t\t]",
  disabledd: "offf",
  statee: "false"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("input", {
  slot: "native-input",
  readonly: ""
})])])]), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Commentaire "), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "px-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-full p-4 mb-4 bg-white border border-gray-100 rounded-xl"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "mb-2 text-lg font-normal"
}, "Message"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("textarea", {
  id: "",
  name: "",
  rows: "8",
  placeholder: "",
  class: "w-full pl-2 font-light focus:outline-none"
}, "\n\tHi Arthur,\n\tBienvenu dans la communauté 5NKAP!\n\tCliquez sur  ce Lien  pour activer votre compte\n\n\n\t\t\t\t\t\t\t\t")])])])])])], -1 /* HOISTED */);
function render(_ctx, _cache, $props, $setup, $data, $options) {
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" eslint-disable vue/no-deprecated-slot-attribute "), _hoisted_1], 2112 /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsSecurity.vue?vue&type=template&id=4fdf22be&ts=true":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsSecurity.vue?vue&type=template&id=4fdf22be&ts=true ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "workspace-content h-full overflow-y-auto pb-5 md:pb-0"
};
const _hoisted_2 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row items-center justify-between w-full h-16 workspace-header"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", {
  class: "text-xl text-gray-500"
}, "Paramètres > Sécurité"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row items-center h-full workspace-actions"
})], -1 /* HOISTED */);
const _hoisted_3 = {
  class: "flex flex-row w-full p-8 space-y-8 bg-white border border-gray-400 border-opacity-40",
  style: {
    "height": "auto"
  }
};
const _hoisted_4 = {
  class: "flex flex-col w-1/2 pr-5 space-y-6"
};
const _hoisted_5 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row w-full space-x-2"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/navigation-level-icon.svg",
  alt: "",
  class: "opacity-60"
}), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h2", {
  class: "text-xl font-medium text-gray-400"
}, "Notifications")], -1 /* HOISTED */);
const _hoisted_6 = {
  class: "space-y-4 divide-y-2 divide-gray-100 divide-solid"
};
const _hoisted_7 = {
  class: "flex flex-row justify-between py-5"
};
const _hoisted_8 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-col"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", {
  class: "font-bold text-gray-500"
}, "Authentification à Double Facteur 2FA")], -1 /* HOISTED */);
const _hoisted_9 = {
  class: "flex flex-row items-center text-gray-400 cursor-pointer"
};
const _hoisted_10 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-col w-1/2 pl-5 space-y-6"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "space-y-4 divide-y-2 divide-gray-100 divide-solid"
}, " .. ")], -1 /* HOISTED */);
function render(_ctx, _cache, $props, $setup, $data, $options) {
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Route "), _hoisted_2, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Cadre "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Section Left LG"), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_4, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Title "), _hoisted_5, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_6, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Element "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_7, [_hoisted_8, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_9, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
      'flex items-center cursor-pointer justify-end px-2 py-1  rounded-xl': true,
      'bg-gray-200': !$setup.twoFactors,
      'bg-green-300': $setup.twoFactors
    }),
    onClick: $setup.toggle
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
      'flex items-center justify-center font-normal w-10 h-6 py-1 rounded-md text-white ': true,
      'bg-gray-400': !$setup.twoFactors
    })
  }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.twoFactors ? "" : "Off"), 3 /* TEXT, CLASS */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
      'flex items-center justify-center font-normal w-10 h-6 py-1 rounded-md text-white ': true,
      'bg-green-400 font-medium text-white': $setup.twoFactors
    })
  }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.twoFactors ? "On" : ""), 3 /* TEXT, CLASS */)], 2 /* CLASS */)])])])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Section Right LG"), _hoisted_10])]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsSubscriptions.vue?vue&type=template&id=3dce7936&ts=true":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsSubscriptions.vue?vue&type=template&id=3dce7936&ts=true ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "h-full pb-5 overflow-y-auto workspace-content md:pb-0"
};
const _hoisted_2 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row items-center justify-between w-full h-16 workspace-header"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", {
  class: "text-xl text-gray-500"
}, "Paramètres > Mes abonnements"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row items-center h-full workspace-actions"
})], -1 /* HOISTED */);
const _hoisted_3 = {
  key: 0,
  class: "flex flex-col md:flex-row w-full md:space-x-8",
  style: {
    "height": "calc(100% - 5rem)"
  }
};
const _hoisted_4 = {
  class: "flex flex-col w-full md:w-5/12 lg:w-4/12 space-y-5 mb-8 md:mb-0"
};
const _hoisted_5 = {
  class: ""
};
const _hoisted_6 = {
  class: "p-4 bg-white border border-gray-300 rounded-xl border-opacity-40"
};
const _hoisted_7 = {
  key: 0,
  class: "w-full"
};
const _hoisted_8 = {
  key: 0,
  class: "flex flex-row items-start justify-between w-full"
};
const _hoisted_9 = {
  class: "flex flex-col items-start"
};
const _hoisted_10 = {
  class: "text-2xl font-medium text-green-600"
};
const _hoisted_11 = {
  key: 0,
  class: "mt-2 text-gray-400 font-lg"
};
const _hoisted_12 = {
  key: 1,
  class: "mt-2 text-gray-400 font-lg"
};
const _hoisted_13 = {
  key: 2,
  class: "mt-2 text-gray-400 font-lg"
};
const _hoisted_14 = {
  class: "flex flex-col items-end justify-between h-auto mt-2"
};
const _hoisted_15 = {
  class: "font-medium text-black"
};
const _hoisted_16 = {
  class: "text-sm font-normal text-gray-400"
};
const _hoisted_17 = {
  key: 1,
  class: "flex flex-row items-start justify-between w-full"
};
const _hoisted_18 = {
  class: "flex flex-col"
};
const _hoisted_19 = {
  class: "text-4xl font-medium text-green-600"
};
const _hoisted_20 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", {
  class: "mt-2 text-gray-600 font-medium"
}, "En attente de paiement...", -1 /* HOISTED */);
const _hoisted_21 = {
  class: "flex flex-row space-x-2"
};
const _hoisted_22 = {
  class: "text-base opacity-60"
};
const _hoisted_23 = {
  class: "flex flex-col justify-between h-auto items-center"
};
const _hoisted_24 = {
  class: "mt-2 text-gray-800 font-semibold opacity-95"
};
const _hoisted_25 = {
  key: 1,
  class: "flex flex-row items-start justify-between w-full"
};
const _hoisted_26 = {
  class: "flex flex-col items-start"
};
const _hoisted_27 = {
  class: "text-4xl font-medium text-green-600"
};
const _hoisted_28 = {
  class: "mt-2 text-gray-400 font-xl"
};
const _hoisted_29 = ["text"];
const _hoisted_30 = {
  class: "flex flex-col w-full md:w-7/12 lg:w-8/12 space-y-5"
};
const _hoisted_31 = {
  class: ""
};
const _hoisted_32 = {
  class: "w-full p-4 space-y-6 overflow-y-auto bg-white border border-gray-300 bg-opacity-40 rounded-xl border-opacity-40",
  style: {
    "height": "calc(100vh - 13rem)"
  }
};
const _hoisted_33 = {
  class: "flex flex-col w-full space-y-4"
};
const _hoisted_34 = {
  class: "flex flex-col items-start"
};
const _hoisted_35 = {
  class: "text-2xl font-medium"
};
const _hoisted_36 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", {
  class: "text-sm font-normal text-gray-400"
}, "Expiré", -1 /* HOISTED */);
const _hoisted_37 = {
  class: "flex flex-col items-end"
};
const _hoisted_38 = {
  class: "font-medium text-black"
};
const _hoisted_39 = {
  class: "text-sm font-normal text-gray-400"
};
const _hoisted_40 = {
  key: 0,
  class: "flex flex-col justify-center items-center",
  style: {
    "height": "calc(100vh - 21rem)"
  }
};
const _hoisted_41 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", null, "Pas de paiements expirés pour le moment", -1 /* HOISTED */);
const _hoisted_42 = {
  key: 1,
  class: "absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 mt-12"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_SlimTitleBar = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SlimTitleBar");
  const _component_Icon = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("Icon");
  const _component_EmptyContent = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("EmptyContent");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [_hoisted_2, $setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" left "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_4, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_5, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SlimTitleBar, {
    title: "Abonnement en cours"
  })]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_6, [$setup.enrolsHistorical.length > 0 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_7, [$setup.useAccess('payment', []) == true ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_8, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_9, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h2", _hoisted_10, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.enrolsHistorical[0].label), 1 /* TEXT */), $setup.useAccess('payment', []) == true && $setup.enrolsHistorical[0].state == 'encour' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("h3", _hoisted_11, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.subscription.count) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.subscription.count >= 2 ? "Jours" : "Jour") + " restants ", 1 /* TEXT */)) : $setup.enrolsHistorical[0].state == 'initier' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("h3", _hoisted_12, " Encours de validation ")) : $setup.enrolsHistorical[0].state == 'expirer' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("h3", _hoisted_13, " Abonnement expiré ")) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_14, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", _hoisted_15, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.formatMoney($setup.enrolsHistorical[0].cost)) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.enrolsHistorical[0].currency), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", _hoisted_16, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.formatDate($setup.enrolsHistorical[0].date_fin)), 1 /* TEXT */)])])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.useAccess('payment', []) == false ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_17, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_18, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h2", _hoisted_19, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.enrolsHistorical[0].label), 1 /* TEXT */), _hoisted_20, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_21, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_22, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.getOperatorName($setup.userInfos?.paymentOperations[0].operator_id)), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_Icon, {
    name: "arrow-up-down-line",
    class: "opacity-40"
  })])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_23, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", _hoisted_24, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.enrolsHistorical[0].cost) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.enrolsHistorical[0].currency), 1 /* TEXT */)])])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.enrolsHistorical.length == 0 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_25, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_26, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h2", _hoisted_27, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.subscription.title), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", _hoisted_28, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.subscription.count) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.subscription.count >= 2 ? "Jours" : "Jour") + " restants ", 1 /* TEXT */)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <div class=\"flex flex-col items-end justify-between h-auto\">\n\t\t\t\t\t\t\t<h3 class=\"font-medium text-black\">Gratuit</h3>\n\t\t\t\t\t\t\t<h3 class=\"text-sm font-normal text-gray-400\">\n\t\t\t\t\t\t\t\t{{ formatDate(userInfos?.abonnement?.date_fin) }}\n\t\t\t\t\t\t\t</h3>\n\t\t\t\t\t\t</div> ")])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]), ($setup.subscription.title == 'Essai' || $setup.subscription.title == 'Expiré') && $setup.useAccess('visibility', ['particular', 'enterprise']) == true && $setup.useAccess('payment', []) == true && $setup.showSubcriptionButton && $setup.platform.runtime != 'reactnative' && $setup.useAccess('add', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
    key: 0,
    class: "w-full",
    onClick: $setup.openSubscriptionFullView
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("bb-button", {
    flex: "normal",
    type: "link",
    url: "javascript:void(0)",
    text: $setup.enrolsHistorical[0] && $setup.enrolsHistorical[0].state == 'initier' && $setup.useAccess('payment', []) == true ? 'Reéssayez le paiement ?' : 'Renouveler maintenant',
    usage: "simple-text",
    "text-color": "white",
    class: "font-light"
  }, null, 8 /* PROPS */, _hoisted_29)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("Rigth "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_30, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_31, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SlimTitleBar, {
    title: "Historique des paiements"
  })]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_32, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_33, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" bar "), ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.enrolsHistorical.filter(item => item.state == 'expirer'), (item, index) => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
      key: index,
      class: "flex flex-row items-center justify-between w-full p-4 bg-white rounded-xl"
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_34, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h2", _hoisted_35, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.label), 1 /* TEXT */), _hoisted_36]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_37, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", _hoisted_38, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.cost) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.currency), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", _hoisted_39, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.formatDate(item.date_fin)), 1 /* TEXT */)])]);
  }), 128 /* KEYED_FRAGMENT */))]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Illustration "), $setup.enrolsHistorical.filter(item => item.state == 'expirer').length == 0 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_40, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: "bg-cover bg-no-repeat h-72 w-72 flex bg-center",
    style: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeStyle)(`background-image: url('${$setup.paymentInfoIllustration}');`)
  }, null, 4 /* STYLE */), _hoisted_41])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])])])) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_42, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_EmptyContent, {
    "height-parent": "50vh -25rem",
    icon: '/icons/no-access-icon-c.svg',
    line1: 'Oups... Vous n\'êtes pas autorisé ',
    line2: 'à afficher ce contenu.',
    action: '',
    height: "100px",
    width: "100px"
  }, null, 8 /* PROPS */, ["icon", "line1", "line2"])]))]);
}

/***/ }),

/***/ "./src/composable/useFeature.ts":
/*!**************************************!*\
  !*** ./src/composable/useFeature.ts ***!
  \**************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");

/* harmony default export */ __webpack_exports__["default"] = (feature_route_name => {
  if (feature_route_name == "" || feature_route_name == undefined || _state_api_userState__WEBPACK_IMPORTED_MODULE_0__.userInfos.value?.AllFeatures?.length == 0) return [];
  try {
    return _state_api_userState__WEBPACK_IMPORTED_MODULE_0__.userInfos.value?.AllFeatures.filter(feature => {
      return feature.route == feature_route_name;
    });
  } catch (err) {
    return [];
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsBusinessAccount.vue?vue&type=style&index=0&id=d47c7508&lang=scss":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsBusinessAccount.vue?vue&type=style&index=0&id=d47c7508&lang=scss ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/noSourceMaps.js */ "./node_modules/css-loader/dist/runtime/noSourceMaps.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, "[contenteditable]:focus {\n  outline: 0px solid transparent;\n}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsManagingUsers.vue?vue&type=style&index=0&id=ed1f9296&lang=scss&scoped=true":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsManagingUsers.vue?vue&type=style&index=0&id=ed1f9296&lang=scss&scoped=true ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/noSourceMaps.js */ "./node_modules/css-loader/dist/runtime/noSourceMaps.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, "[data-v-ed1f9296] .p-paginator .p-paginator-current {\n  margin-left: auto;\n}\n[data-v-ed1f9296] .p-progressbar {\n  height: 0.5rem;\n  background-color: #d8dadc;\n}\n[data-v-ed1f9296] .p-progressbar .p-progressbar-value {\n  background-color: #607d8b;\n}\n[data-v-ed1f9296] .p-datepicker {\n  min-width: 25rem;\n}\n[data-v-ed1f9296] .p-datepicker td {\n  font-weight: 400;\n}\n[data-v-ed1f9296] .p-datatable.p-datatable-5nkap .p-datatable-header {\n  padding: 1rem;\n  text-align: left;\n  font-size: 1.5rem;\n}\n[data-v-ed1f9296] .p-datatable.p-datatable-5nkap .p-paginator {\n  padding: 0.3rem;\n}\n[data-v-ed1f9296] .p-datatable.p-datatable-5nkap .p-datatable-thead > tr > th {\n  text-align: left;\n}\n[data-v-ed1f9296] .p-datatable.p-datatable-5nkap .p-datatable-tbody > tr > td {\n  cursor: auto;\n}\n[data-v-ed1f9296] .p-datatable.p-datatable-5nkap .p-dropdown-label:not(.p-placeholder) {\n  text-transform: uppercase;\n}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsRolesAndPermissions.vue?vue&type=style&index=0&id=6235a379&lang=scss&scoped=true":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsRolesAndPermissions.vue?vue&type=style&index=0&id=6235a379&lang=scss&scoped=true ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/noSourceMaps.js */ "./node_modules/css-loader/dist/runtime/noSourceMaps.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, "[data-v-6235a379] .p-paginator .p-paginator-current {\n  margin-left: auto;\n}\n[data-v-6235a379] .p-progressbar {\n  height: 0.5rem;\n  background-color: #d8dadc;\n}\n[data-v-6235a379] .p-progressbar .p-progressbar-value {\n  background-color: #607d8b;\n}\n[data-v-6235a379] .p-datepicker {\n  min-width: 25rem;\n}\n[data-v-6235a379] .p-datepicker td {\n  font-weight: 400;\n}\n[data-v-6235a379] .p-datatable.p-datatable-5nkap .p-datatable-header {\n  padding: 1rem;\n  text-align: left;\n  font-size: 1.5rem;\n}\n[data-v-6235a379] .p-datatable.p-datatable-5nkap .p-paginator {\n  padding: 0.3rem;\n}\n[data-v-6235a379] .p-datatable.p-datatable-5nkap .p-datatable-thead > tr > th {\n  text-align: left;\n}\n[data-v-6235a379] .p-datatable.p-datatable-5nkap .p-datatable-tbody > tr > td {\n  cursor: auto;\n}\n[data-v-6235a379] .p-datatable.p-datatable-5nkap .p-dropdown-label:not(.p-placeholder) {\n  text-transform: uppercase;\n}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./src/pages/workspace/settings/settingsBusinessAccount.vue":
/*!******************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsBusinessAccount.vue ***!
  \******************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _settingsBusinessAccount_vue_vue_type_template_id_d47c7508_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./settingsBusinessAccount.vue?vue&type=template&id=d47c7508&ts=true */ "./src/pages/workspace/settings/settingsBusinessAccount.vue?vue&type=template&id=d47c7508&ts=true");
/* harmony import */ var _settingsBusinessAccount_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./settingsBusinessAccount.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/settings/settingsBusinessAccount.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _settingsBusinessAccount_vue_vue_type_style_index_0_id_d47c7508_lang_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./settingsBusinessAccount.vue?vue&type=style&index=0&id=d47c7508&lang=scss */ "./src/pages/workspace/settings/settingsBusinessAccount.vue?vue&type=style&index=0&id=d47c7508&lang=scss");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;


const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_3__["default"])(_settingsBusinessAccount_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_settingsBusinessAccount_vue_vue_type_template_id_d47c7508_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/settings/settingsBusinessAccount.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/settings/settingsEmailTemplating.vue":
/*!******************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsEmailTemplating.vue ***!
  \******************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _settingsEmailTemplating_vue_vue_type_template_id_dd30c0bc_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./settingsEmailTemplating.vue?vue&type=template&id=dd30c0bc&ts=true */ "./src/pages/workspace/settings/settingsEmailTemplating.vue?vue&type=template&id=dd30c0bc&ts=true");
/* harmony import */ var _settingsEmailTemplating_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./settingsEmailTemplating.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/settings/settingsEmailTemplating.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_settingsEmailTemplating_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_settingsEmailTemplating_vue_vue_type_template_id_dd30c0bc_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/settings/settingsEmailTemplating.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/settings/settingsFollowUp.vue":
/*!***********************************************************!*\
  !*** ./src/pages/workspace/settings/settingsFollowUp.vue ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _settingsFollowUp_vue_vue_type_template_id_1cb5bf8d_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./settingsFollowUp.vue?vue&type=template&id=1cb5bf8d&ts=true */ "./src/pages/workspace/settings/settingsFollowUp.vue?vue&type=template&id=1cb5bf8d&ts=true");
/* harmony import */ var _settingsFollowUp_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./settingsFollowUp.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/settings/settingsFollowUp.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_settingsFollowUp_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_settingsFollowUp_vue_vue_type_template_id_1cb5bf8d_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/settings/settingsFollowUp.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/settings/settingsGeneralSettings.vue":
/*!******************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsGeneralSettings.vue ***!
  \******************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _settingsGeneralSettings_vue_vue_type_template_id_a0e4854c_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./settingsGeneralSettings.vue?vue&type=template&id=a0e4854c&ts=true */ "./src/pages/workspace/settings/settingsGeneralSettings.vue?vue&type=template&id=a0e4854c&ts=true");
/* harmony import */ var _settingsGeneralSettings_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./settingsGeneralSettings.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/settings/settingsGeneralSettings.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_settingsGeneralSettings_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_settingsGeneralSettings_vue_vue_type_template_id_a0e4854c_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/settings/settingsGeneralSettings.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/settings/settingsHeaderAndFooter.vue":
/*!******************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsHeaderAndFooter.vue ***!
  \******************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _settingsHeaderAndFooter_vue_vue_type_template_id_07c46d18_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./settingsHeaderAndFooter.vue?vue&type=template&id=07c46d18&ts=true */ "./src/pages/workspace/settings/settingsHeaderAndFooter.vue?vue&type=template&id=07c46d18&ts=true");
/* harmony import */ var _settingsHeaderAndFooter_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./settingsHeaderAndFooter.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/settings/settingsHeaderAndFooter.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_settingsHeaderAndFooter_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_settingsHeaderAndFooter_vue_vue_type_template_id_07c46d18_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/settings/settingsHeaderAndFooter.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/settings/settingsManaginBranches.vue":
/*!******************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsManaginBranches.vue ***!
  \******************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _settingsManaginBranches_vue_vue_type_template_id_8b9e6f78_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./settingsManaginBranches.vue?vue&type=template&id=8b9e6f78&ts=true */ "./src/pages/workspace/settings/settingsManaginBranches.vue?vue&type=template&id=8b9e6f78&ts=true");
/* harmony import */ var _settingsManaginBranches_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./settingsManaginBranches.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/settings/settingsManaginBranches.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_settingsManaginBranches_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_settingsManaginBranches_vue_vue_type_template_id_8b9e6f78_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/settings/settingsManaginBranches.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/settings/settingsManagingUsers.vue":
/*!****************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsManagingUsers.vue ***!
  \****************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _settingsManagingUsers_vue_vue_type_template_id_ed1f9296_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./settingsManagingUsers.vue?vue&type=template&id=ed1f9296&scoped=true&ts=true */ "./src/pages/workspace/settings/settingsManagingUsers.vue?vue&type=template&id=ed1f9296&scoped=true&ts=true");
/* harmony import */ var _settingsManagingUsers_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./settingsManagingUsers.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/settings/settingsManagingUsers.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _settingsManagingUsers_vue_vue_type_style_index_0_id_ed1f9296_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./settingsManagingUsers.vue?vue&type=style&index=0&id=ed1f9296&lang=scss&scoped=true */ "./src/pages/workspace/settings/settingsManagingUsers.vue?vue&type=style&index=0&id=ed1f9296&lang=scss&scoped=true");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;


const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_3__["default"])(_settingsManagingUsers_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_settingsManagingUsers_vue_vue_type_template_id_ed1f9296_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__scopeId',"data-v-ed1f9296"],['__file',"src/pages/workspace/settings/settingsManagingUsers.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/settings/settingsOverview.vue":
/*!***********************************************************!*\
  !*** ./src/pages/workspace/settings/settingsOverview.vue ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _settingsOverview_vue_vue_type_template_id_4e0f205a_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./settingsOverview.vue?vue&type=template&id=4e0f205a&ts=true */ "./src/pages/workspace/settings/settingsOverview.vue?vue&type=template&id=4e0f205a&ts=true");
/* harmony import */ var _settingsOverview_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./settingsOverview.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/settings/settingsOverview.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_settingsOverview_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_settingsOverview_vue_vue_type_template_id_4e0f205a_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/settings/settingsOverview.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/settings/settingsReset.vue":
/*!********************************************************!*\
  !*** ./src/pages/workspace/settings/settingsReset.vue ***!
  \********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _settingsReset_vue_vue_type_template_id_1e15ef84_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./settingsReset.vue?vue&type=template&id=1e15ef84&ts=true */ "./src/pages/workspace/settings/settingsReset.vue?vue&type=template&id=1e15ef84&ts=true");
/* harmony import */ var _settingsReset_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./settingsReset.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/settings/settingsReset.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_settingsReset_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_settingsReset_vue_vue_type_template_id_1e15ef84_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/settings/settingsReset.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/settings/settingsRolesAndPermissions.vue":
/*!**********************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsRolesAndPermissions.vue ***!
  \**********************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _settingsRolesAndPermissions_vue_vue_type_template_id_6235a379_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./settingsRolesAndPermissions.vue?vue&type=template&id=6235a379&scoped=true&ts=true */ "./src/pages/workspace/settings/settingsRolesAndPermissions.vue?vue&type=template&id=6235a379&scoped=true&ts=true");
/* harmony import */ var _settingsRolesAndPermissions_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./settingsRolesAndPermissions.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/settings/settingsRolesAndPermissions.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _settingsRolesAndPermissions_vue_vue_type_style_index_0_id_6235a379_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./settingsRolesAndPermissions.vue?vue&type=style&index=0&id=6235a379&lang=scss&scoped=true */ "./src/pages/workspace/settings/settingsRolesAndPermissions.vue?vue&type=style&index=0&id=6235a379&lang=scss&scoped=true");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;


const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_3__["default"])(_settingsRolesAndPermissions_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_settingsRolesAndPermissions_vue_vue_type_template_id_6235a379_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__scopeId',"data-v-6235a379"],['__file',"src/pages/workspace/settings/settingsRolesAndPermissions.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/settings/settingsSMSAndEmail.vue":
/*!**************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsSMSAndEmail.vue ***!
  \**************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _settingsSMSAndEmail_vue_vue_type_template_id_3e2162ad_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./settingsSMSAndEmail.vue?vue&type=template&id=3e2162ad&ts=true */ "./src/pages/workspace/settings/settingsSMSAndEmail.vue?vue&type=template&id=3e2162ad&ts=true");
/* harmony import */ var _settingsSMSAndEmail_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./settingsSMSAndEmail.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/settings/settingsSMSAndEmail.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_settingsSMSAndEmail_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_settingsSMSAndEmail_vue_vue_type_template_id_3e2162ad_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/settings/settingsSMSAndEmail.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/settings/settingsSecurity.vue":
/*!***********************************************************!*\
  !*** ./src/pages/workspace/settings/settingsSecurity.vue ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _settingsSecurity_vue_vue_type_template_id_4fdf22be_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./settingsSecurity.vue?vue&type=template&id=4fdf22be&ts=true */ "./src/pages/workspace/settings/settingsSecurity.vue?vue&type=template&id=4fdf22be&ts=true");
/* harmony import */ var _settingsSecurity_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./settingsSecurity.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/settings/settingsSecurity.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_settingsSecurity_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_settingsSecurity_vue_vue_type_template_id_4fdf22be_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/settings/settingsSecurity.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/settings/settingsSubscriptions.vue":
/*!****************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsSubscriptions.vue ***!
  \****************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _settingsSubscriptions_vue_vue_type_template_id_3dce7936_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./settingsSubscriptions.vue?vue&type=template&id=3dce7936&ts=true */ "./src/pages/workspace/settings/settingsSubscriptions.vue?vue&type=template&id=3dce7936&ts=true");
/* harmony import */ var _settingsSubscriptions_vue_vue_type_script_lang_ts_setup_true__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./settingsSubscriptions.vue?vue&type=script&lang=ts&setup=true */ "./src/pages/workspace/settings/settingsSubscriptions.vue?vue&type=script&lang=ts&setup=true");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_settingsSubscriptions_vue_vue_type_script_lang_ts_setup_true__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_settingsSubscriptions_vue_vue_type_template_id_3dce7936_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/settings/settingsSubscriptions.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/settings/settingsBusinessAccount.vue?vue&type=script&setup=true&lang=ts":
/*!*****************************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsBusinessAccount.vue?vue&type=script&setup=true&lang=ts ***!
  \*****************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsBusinessAccount_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsBusinessAccount_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsBusinessAccount.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsBusinessAccount.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/settings/settingsEmailTemplating.vue?vue&type=script&setup=true&lang=ts":
/*!*****************************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsEmailTemplating.vue?vue&type=script&setup=true&lang=ts ***!
  \*****************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsEmailTemplating_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsEmailTemplating_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsEmailTemplating.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsEmailTemplating.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/settings/settingsFollowUp.vue?vue&type=script&setup=true&lang=ts":
/*!**********************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsFollowUp.vue?vue&type=script&setup=true&lang=ts ***!
  \**********************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsFollowUp_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsFollowUp_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsFollowUp.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsFollowUp.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/settings/settingsGeneralSettings.vue?vue&type=script&setup=true&lang=ts":
/*!*****************************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsGeneralSettings.vue?vue&type=script&setup=true&lang=ts ***!
  \*****************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsGeneralSettings_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsGeneralSettings_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsGeneralSettings.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsGeneralSettings.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/settings/settingsHeaderAndFooter.vue?vue&type=script&setup=true&lang=ts":
/*!*****************************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsHeaderAndFooter.vue?vue&type=script&setup=true&lang=ts ***!
  \*****************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsHeaderAndFooter_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsHeaderAndFooter_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsHeaderAndFooter.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsHeaderAndFooter.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/settings/settingsManaginBranches.vue?vue&type=script&setup=true&lang=ts":
/*!*****************************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsManaginBranches.vue?vue&type=script&setup=true&lang=ts ***!
  \*****************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsManaginBranches_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsManaginBranches_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsManaginBranches.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsManaginBranches.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/settings/settingsManagingUsers.vue?vue&type=script&setup=true&lang=ts":
/*!***************************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsManagingUsers.vue?vue&type=script&setup=true&lang=ts ***!
  \***************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsManagingUsers_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsManagingUsers_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsManagingUsers.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsManagingUsers.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/settings/settingsOverview.vue?vue&type=script&setup=true&lang=ts":
/*!**********************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsOverview.vue?vue&type=script&setup=true&lang=ts ***!
  \**********************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsOverview_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsOverview_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsOverview.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsOverview.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/settings/settingsReset.vue?vue&type=script&setup=true&lang=ts":
/*!*******************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsReset.vue?vue&type=script&setup=true&lang=ts ***!
  \*******************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsReset_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsReset_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsReset.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsReset.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/settings/settingsRolesAndPermissions.vue?vue&type=script&setup=true&lang=ts":
/*!*********************************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsRolesAndPermissions.vue?vue&type=script&setup=true&lang=ts ***!
  \*********************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsRolesAndPermissions_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsRolesAndPermissions_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsRolesAndPermissions.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsRolesAndPermissions.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/settings/settingsSMSAndEmail.vue?vue&type=script&setup=true&lang=ts":
/*!*************************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsSMSAndEmail.vue?vue&type=script&setup=true&lang=ts ***!
  \*************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsSMSAndEmail_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsSMSAndEmail_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsSMSAndEmail.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsSMSAndEmail.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/settings/settingsSecurity.vue?vue&type=script&setup=true&lang=ts":
/*!**********************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsSecurity.vue?vue&type=script&setup=true&lang=ts ***!
  \**********************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsSecurity_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsSecurity_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsSecurity.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsSecurity.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/settings/settingsSubscriptions.vue?vue&type=script&lang=ts&setup=true":
/*!***************************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsSubscriptions.vue?vue&type=script&lang=ts&setup=true ***!
  \***************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsSubscriptions_vue_vue_type_script_lang_ts_setup_true__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsSubscriptions_vue_vue_type_script_lang_ts_setup_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsSubscriptions.vue?vue&type=script&lang=ts&setup=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsSubscriptions.vue?vue&type=script&lang=ts&setup=true");
 

/***/ }),

/***/ "./src/pages/workspace/settings/settingsBusinessAccount.vue?vue&type=template&id=d47c7508&ts=true":
/*!********************************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsBusinessAccount.vue?vue&type=template&id=d47c7508&ts=true ***!
  \********************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsBusinessAccount_vue_vue_type_template_id_d47c7508_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsBusinessAccount_vue_vue_type_template_id_d47c7508_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsBusinessAccount.vue?vue&type=template&id=d47c7508&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsBusinessAccount.vue?vue&type=template&id=d47c7508&ts=true");


/***/ }),

/***/ "./src/pages/workspace/settings/settingsEmailTemplating.vue?vue&type=template&id=dd30c0bc&ts=true":
/*!********************************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsEmailTemplating.vue?vue&type=template&id=dd30c0bc&ts=true ***!
  \********************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsEmailTemplating_vue_vue_type_template_id_dd30c0bc_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsEmailTemplating_vue_vue_type_template_id_dd30c0bc_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsEmailTemplating.vue?vue&type=template&id=dd30c0bc&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsEmailTemplating.vue?vue&type=template&id=dd30c0bc&ts=true");


/***/ }),

/***/ "./src/pages/workspace/settings/settingsFollowUp.vue?vue&type=template&id=1cb5bf8d&ts=true":
/*!*************************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsFollowUp.vue?vue&type=template&id=1cb5bf8d&ts=true ***!
  \*************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsFollowUp_vue_vue_type_template_id_1cb5bf8d_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsFollowUp_vue_vue_type_template_id_1cb5bf8d_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsFollowUp.vue?vue&type=template&id=1cb5bf8d&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsFollowUp.vue?vue&type=template&id=1cb5bf8d&ts=true");


/***/ }),

/***/ "./src/pages/workspace/settings/settingsGeneralSettings.vue?vue&type=template&id=a0e4854c&ts=true":
/*!********************************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsGeneralSettings.vue?vue&type=template&id=a0e4854c&ts=true ***!
  \********************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsGeneralSettings_vue_vue_type_template_id_a0e4854c_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsGeneralSettings_vue_vue_type_template_id_a0e4854c_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsGeneralSettings.vue?vue&type=template&id=a0e4854c&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsGeneralSettings.vue?vue&type=template&id=a0e4854c&ts=true");


/***/ }),

/***/ "./src/pages/workspace/settings/settingsHeaderAndFooter.vue?vue&type=template&id=07c46d18&ts=true":
/*!********************************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsHeaderAndFooter.vue?vue&type=template&id=07c46d18&ts=true ***!
  \********************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsHeaderAndFooter_vue_vue_type_template_id_07c46d18_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsHeaderAndFooter_vue_vue_type_template_id_07c46d18_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsHeaderAndFooter.vue?vue&type=template&id=07c46d18&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsHeaderAndFooter.vue?vue&type=template&id=07c46d18&ts=true");


/***/ }),

/***/ "./src/pages/workspace/settings/settingsManaginBranches.vue?vue&type=template&id=8b9e6f78&ts=true":
/*!********************************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsManaginBranches.vue?vue&type=template&id=8b9e6f78&ts=true ***!
  \********************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsManaginBranches_vue_vue_type_template_id_8b9e6f78_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsManaginBranches_vue_vue_type_template_id_8b9e6f78_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsManaginBranches.vue?vue&type=template&id=8b9e6f78&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsManaginBranches.vue?vue&type=template&id=8b9e6f78&ts=true");


/***/ }),

/***/ "./src/pages/workspace/settings/settingsManagingUsers.vue?vue&type=template&id=ed1f9296&scoped=true&ts=true":
/*!******************************************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsManagingUsers.vue?vue&type=template&id=ed1f9296&scoped=true&ts=true ***!
  \******************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsManagingUsers_vue_vue_type_template_id_ed1f9296_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsManagingUsers_vue_vue_type_template_id_ed1f9296_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsManagingUsers.vue?vue&type=template&id=ed1f9296&scoped=true&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsManagingUsers.vue?vue&type=template&id=ed1f9296&scoped=true&ts=true");


/***/ }),

/***/ "./src/pages/workspace/settings/settingsOverview.vue?vue&type=template&id=4e0f205a&ts=true":
/*!*************************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsOverview.vue?vue&type=template&id=4e0f205a&ts=true ***!
  \*************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsOverview_vue_vue_type_template_id_4e0f205a_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsOverview_vue_vue_type_template_id_4e0f205a_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsOverview.vue?vue&type=template&id=4e0f205a&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsOverview.vue?vue&type=template&id=4e0f205a&ts=true");


/***/ }),

/***/ "./src/pages/workspace/settings/settingsReset.vue?vue&type=template&id=1e15ef84&ts=true":
/*!**********************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsReset.vue?vue&type=template&id=1e15ef84&ts=true ***!
  \**********************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsReset_vue_vue_type_template_id_1e15ef84_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsReset_vue_vue_type_template_id_1e15ef84_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsReset.vue?vue&type=template&id=1e15ef84&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsReset.vue?vue&type=template&id=1e15ef84&ts=true");


/***/ }),

/***/ "./src/pages/workspace/settings/settingsRolesAndPermissions.vue?vue&type=template&id=6235a379&scoped=true&ts=true":
/*!************************************************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsRolesAndPermissions.vue?vue&type=template&id=6235a379&scoped=true&ts=true ***!
  \************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsRolesAndPermissions_vue_vue_type_template_id_6235a379_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsRolesAndPermissions_vue_vue_type_template_id_6235a379_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsRolesAndPermissions.vue?vue&type=template&id=6235a379&scoped=true&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsRolesAndPermissions.vue?vue&type=template&id=6235a379&scoped=true&ts=true");


/***/ }),

/***/ "./src/pages/workspace/settings/settingsSMSAndEmail.vue?vue&type=template&id=3e2162ad&ts=true":
/*!****************************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsSMSAndEmail.vue?vue&type=template&id=3e2162ad&ts=true ***!
  \****************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsSMSAndEmail_vue_vue_type_template_id_3e2162ad_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsSMSAndEmail_vue_vue_type_template_id_3e2162ad_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsSMSAndEmail.vue?vue&type=template&id=3e2162ad&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsSMSAndEmail.vue?vue&type=template&id=3e2162ad&ts=true");


/***/ }),

/***/ "./src/pages/workspace/settings/settingsSecurity.vue?vue&type=template&id=4fdf22be&ts=true":
/*!*************************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsSecurity.vue?vue&type=template&id=4fdf22be&ts=true ***!
  \*************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsSecurity_vue_vue_type_template_id_4fdf22be_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsSecurity_vue_vue_type_template_id_4fdf22be_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsSecurity.vue?vue&type=template&id=4fdf22be&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsSecurity.vue?vue&type=template&id=4fdf22be&ts=true");


/***/ }),

/***/ "./src/pages/workspace/settings/settingsSubscriptions.vue?vue&type=template&id=3dce7936&ts=true":
/*!******************************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsSubscriptions.vue?vue&type=template&id=3dce7936&ts=true ***!
  \******************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsSubscriptions_vue_vue_type_template_id_3dce7936_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsSubscriptions_vue_vue_type_template_id_3dce7936_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsSubscriptions.vue?vue&type=template&id=3dce7936&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsSubscriptions.vue?vue&type=template&id=3dce7936&ts=true");


/***/ }),

/***/ "./src/pages/workspace/settings/settingsBusinessAccount.vue?vue&type=style&index=0&id=d47c7508&lang=scss":
/*!***************************************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsBusinessAccount.vue?vue&type=style&index=0&id=d47c7508&lang=scss ***!
  \***************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsBusinessAccount_vue_vue_type_style_index_0_id_d47c7508_lang_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!../../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../../node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsBusinessAccount.vue?vue&type=style&index=0&id=d47c7508&lang=scss */ "./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsBusinessAccount.vue?vue&type=style&index=0&id=d47c7508&lang=scss");
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsBusinessAccount_vue_vue_type_style_index_0_id_d47c7508_lang_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsBusinessAccount_vue_vue_type_style_index_0_id_d47c7508_lang_scss__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsBusinessAccount_vue_vue_type_style_index_0_id_d47c7508_lang_scss__WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = function(key) { return _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsBusinessAccount_vue_vue_type_style_index_0_id_d47c7508_lang_scss__WEBPACK_IMPORTED_MODULE_0__[key]; }.bind(0, __WEBPACK_IMPORT_KEY__)
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);


/***/ }),

/***/ "./src/pages/workspace/settings/settingsManagingUsers.vue?vue&type=style&index=0&id=ed1f9296&lang=scss&scoped=true":
/*!*************************************************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsManagingUsers.vue?vue&type=style&index=0&id=ed1f9296&lang=scss&scoped=true ***!
  \*************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsManagingUsers_vue_vue_type_style_index_0_id_ed1f9296_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!../../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../../node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsManagingUsers.vue?vue&type=style&index=0&id=ed1f9296&lang=scss&scoped=true */ "./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsManagingUsers.vue?vue&type=style&index=0&id=ed1f9296&lang=scss&scoped=true");
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsManagingUsers_vue_vue_type_style_index_0_id_ed1f9296_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsManagingUsers_vue_vue_type_style_index_0_id_ed1f9296_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsManagingUsers_vue_vue_type_style_index_0_id_ed1f9296_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = function(key) { return _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsManagingUsers_vue_vue_type_style_index_0_id_ed1f9296_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__[key]; }.bind(0, __WEBPACK_IMPORT_KEY__)
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);


/***/ }),

/***/ "./src/pages/workspace/settings/settingsRolesAndPermissions.vue?vue&type=style&index=0&id=6235a379&lang=scss&scoped=true":
/*!*******************************************************************************************************************************!*\
  !*** ./src/pages/workspace/settings/settingsRolesAndPermissions.vue?vue&type=style&index=0&id=6235a379&lang=scss&scoped=true ***!
  \*******************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsRolesAndPermissions_vue_vue_type_style_index_0_id_6235a379_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!../../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../../node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsRolesAndPermissions.vue?vue&type=style&index=0&id=6235a379&lang=scss&scoped=true */ "./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsRolesAndPermissions.vue?vue&type=style&index=0&id=6235a379&lang=scss&scoped=true");
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsRolesAndPermissions_vue_vue_type_style_index_0_id_6235a379_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsRolesAndPermissions_vue_vue_type_style_index_0_id_6235a379_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsRolesAndPermissions_vue_vue_type_style_index_0_id_6235a379_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = function(key) { return _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_settingsRolesAndPermissions_vue_vue_type_style_index_0_id_6235a379_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__[key]; }.bind(0, __WEBPACK_IMPORT_KEY__)
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);


/***/ }),

/***/ "./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsBusinessAccount.vue?vue&type=style&index=0&id=d47c7508&lang=scss":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsBusinessAccount.vue?vue&type=style&index=0&id=d47c7508&lang=scss ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(/*! !!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!../../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../../node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsBusinessAccount.vue?vue&type=style&index=0&id=d47c7508&lang=scss */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsBusinessAccount.vue?vue&type=style&index=0&id=d47c7508&lang=scss");
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(/*! !../../../../node_modules/vue-style-loader/lib/addStylesClient.js */ "./node_modules/vue-style-loader/lib/addStylesClient.js")["default"])
var update = add("b19ad954", content, false, {"sourceMap":false,"shadowMode":false});
// Hot Module Replacement
if(false) {}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsManagingUsers.vue?vue&type=style&index=0&id=ed1f9296&lang=scss&scoped=true":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsManagingUsers.vue?vue&type=style&index=0&id=ed1f9296&lang=scss&scoped=true ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(/*! !!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!../../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../../node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsManagingUsers.vue?vue&type=style&index=0&id=ed1f9296&lang=scss&scoped=true */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsManagingUsers.vue?vue&type=style&index=0&id=ed1f9296&lang=scss&scoped=true");
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(/*! !../../../../node_modules/vue-style-loader/lib/addStylesClient.js */ "./node_modules/vue-style-loader/lib/addStylesClient.js")["default"])
var update = add("161ef0aa", content, false, {"sourceMap":false,"shadowMode":false});
// Hot Module Replacement
if(false) {}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsRolesAndPermissions.vue?vue&type=style&index=0&id=6235a379&lang=scss&scoped=true":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsRolesAndPermissions.vue?vue&type=style&index=0&id=6235a379&lang=scss&scoped=true ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(/*! !!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!../../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../../node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./settingsRolesAndPermissions.vue?vue&type=style&index=0&id=6235a379&lang=scss&scoped=true */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/settings/settingsRolesAndPermissions.vue?vue&type=style&index=0&id=6235a379&lang=scss&scoped=true");
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(/*! !../../../../node_modules/vue-style-loader/lib/addStylesClient.js */ "./node_modules/vue-style-loader/lib/addStylesClient.js")["default"])
var update = add("2c1588aa", content, false, {"sourceMap":false,"shadowMode":false});
// Hot Module Replacement
if(false) {}

/***/ })

}]);
//# sourceMappingURL=workspace-settings.js.map