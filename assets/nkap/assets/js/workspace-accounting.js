(self["webpackChunk_5nkap"] = self["webpackChunk_5nkap"] || []).push([["workspace-accounting"],{

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingDisbursement.vue?vue&type=script&setup=true&lang=ts":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingDisbursement.vue?vue&type=script&setup=true&lang=ts ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var vue_router__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! vue-router */ "./node_modules/vue-router/dist/vue-router.mjs");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _composable_useExport__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/composable/useExport */ "./src/composable/useExport.ts");
/* harmony import */ var dayjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! dayjs */ "./node_modules/dayjs/dayjs.min.js");
/* harmony import */ var dayjs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(dayjs__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var dayjs_plugin_advancedFormat__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! dayjs/plugin/advancedFormat */ "./node_modules/dayjs/plugin/advancedFormat.js");
/* harmony import */ var dayjs_plugin_advancedFormat__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(dayjs_plugin_advancedFormat__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _validator__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/validator */ "./src/validator/index.ts");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/utils */ "./src/utils/index.ts");
/* harmony import */ var numeral__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! numeral */ "./node_modules/numeral/numeral.js");
/* harmony import */ var numeral__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(numeral__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _composable_useNotyf__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @/composable/useNotyf */ "./src/composable/useNotyf.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @/api */ "./src/api/index.ts");
/* harmony import */ var _constants_operations__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @/constants/operations */ "./src/constants/operations/index.ts");












/**
 * Composables
 */



/**
 * API Calls
 */


/**
 * Injects
 */
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'accountingDisbursement',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    dayjs__WEBPACK_IMPORTED_MODULE_3___default().extend((dayjs_plugin_advancedFormat__WEBPACK_IMPORTED_MODULE_4___default()));
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_13__.u)({
      title: "Comptabilité / Décaissement"
    });
    const updateAppHeaderAction = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("updateAppHeaderAction");
    const getPaymentMethods = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("getPaymentMethods");
    const platform = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("platform");
    /**
     * Variables
     */
    const router = (0,vue_router__WEBPACK_IMPORTED_MODULE_14__.useRouter)();
    const route = (0,vue_router__WEBPACK_IMPORTED_MODULE_14__.useRoute)();
    const notify = (0,_composable_useNotyf__WEBPACK_IMPORTED_MODULE_8__["default"])();
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("list");
    const totalAmmount = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)();
    const currency = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)();
    // Pagination
    const metaDataAllDisbursements = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)();
    // Datas
    const activeFilter = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(_constants_operations__WEBPACK_IMPORTED_MODULE_12__.REMBOURSEMENT_EMPRUNTS);
    const isNewClient = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    const paymentMethods = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([]);
    const allDisbursements = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([]);
    const allLoans = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([]);
    const loansList = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([]);
    const dateDebut = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const dateFin = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const selectedLoan = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    const selectedDebt = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    const refundData = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)({
      emprunt: {
        id: null
      },
      label: "",
      cash_pay: null,
      currency: "XAF",
      paymentmethod: {},
      medias: null,
      start: null
    });
    const debtData = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)({
      cash_pay: null,
      label: "",
      paymentmethod: {},
      transaction: {
        id: null
      },
      start: null
    });
    const otherRefundData = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)({
      cost: null,
      label: "",
      paymentmethod: {},
      start: null
      // media : null
    });
    /**
     * Validation Object
     */
    const validations = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)();
    // Loaders
    const isFetching = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    const isLoading = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    const isLoadingForm = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    const permissions = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)();
    // Watchers
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => activeFilter.value, newValue => {
      doGet();
      // Use Permissions
      permissions.value = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_9__["default"])(activeFilter.value == _constants_operations__WEBPACK_IMPORTED_MODULE_12__.REMBOURSEMENT_EMPRUNTS ? "remboursement" : activeFilter.value == _constants_operations__WEBPACK_IMPORTED_MODULE_12__.DETTES ? "reglement_dette" : "autre_decaissement").map(element => {
        return element.action;
      });
    });
    /**
     * Watchers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => allDisbursements.value, newValue => {
      console.log("allDisbursements newValue: ", newValue);
      calculTotalAmount(newValue);
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_1__.userCurrentBranch.value.id, async newValue => {
      // Load datas
      // Load datas
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_10__["default"])("read", permissions.value)) {
        await doGet();
        calculTotalAmount(allDisbursements.value);
      }
      getPaymentMethods(paymentMethods, "credit");
    });
    /**
     * Functions
     */
    const updateSubView = action => {
      actionType.value = action;
    };
    const openRefundDebt = selected => {
      actionType.value = "create";
      switch (activeFilter.value) {
        case _constants_operations__WEBPACK_IMPORTED_MODULE_12__.DETTES:
          debtData.value.cash_pay = selected.data.dette;
          debtData.value.transaction = {
            id: selected.data.id
          };
          selectedDebt.value = selected.data;
          break;
        case _constants_operations__WEBPACK_IMPORTED_MODULE_12__.REMBOURSEMENT_EMPRUNTS:
          refundData.value.emprunt.id = selected.data.id;
      }
    };
    const updateActiveFilter = action => {
      activeFilter.value = action;
    };
    async function getRefunds(filter, page, enableLoader = true) {
      if (enableLoader) isFetching.value = true;
      if (filter != null) {
        await (0,_api__WEBPACK_IMPORTED_MODULE_11__["default"])().comptability.cashCollections.getLoans(filter).then(response => {
          isFetching.value = false;
          metaDataAllDisbursements.value = response.data;
          allDisbursements.value = allLoans.value = response.data.data;
          // console.log("GET Emprunts", allLoans.value);
          loansList.value = allLoans.value.map(item => {
            return {
              key: item.id,
              value: item.label,
              id: item.id
            };
          });
        }).catch(error => {
          isFetching.value = false;
        });
      } else {
        await (0,_api__WEBPACK_IMPORTED_MODULE_11__["default"])().comptability.cashCollections.getLoans(page).then(response => {
          isFetching.value = false;
          metaDataAllDisbursements.value = response.data;
          allDisbursements.value = allLoans.value = response.data.data;
          // console.log("GET Emprunts", allLoans.value);
          loansList.value = allLoans.value.map(item => {
            return {
              key: item.id,
              value: item.label,
              id: item.id
            };
          });
        }).catch(error => {
          isFetching.value = false;
          notify.error("Erreur lors de la récupération des données. " + error.data?.response ? error.data.response.message : 0);
        });
      }
    }
    async function getOtherDisbursement(filter, page, enableLoader = true) {
      if (enableLoader) isFetching.value = true;
      if (filter != null) {
        await (0,_api__WEBPACK_IMPORTED_MODULE_11__["default"])().comptability.othersDisbursements.get(filter).then(response => {
          isFetching.value = false;
          metaDataAllDisbursements.value = response.data;
          allDisbursements.value = allLoans.value = response.data.data;
          // console.log("GET Emprunts", allLoans.value);
          // loansList.value = allLoans.value.map((item: any) => {
          // 	return {
          // 		key: item.id,
          // 		value: item.label,
          // 		id: item.id,
          // 	};
          // });
        }).catch(error => {
          isFetching.value = false;
        });
      } else {
        await (0,_api__WEBPACK_IMPORTED_MODULE_11__["default"])().comptability.othersDisbursements.get(page).then(response => {
          isFetching.value = false;
          metaDataAllDisbursements.value = response.data;
          allDisbursements.value = allLoans.value = response.data.data;
          // console.log("GET Emprunts", allLoans.value);
          // loansList.value = allLoans.value.map((item: any) => {
          // 	return {
          // 		key: item.id,
          // 		value: item.label,
          // 		id: item.id,
          // 	};
          // });
        }).catch(error => {
          isFetching.value = false;
          notify.error("Erreur lors de la récupération des données. " + error.data?.response ? error.data.response.message : 0);
        });
      }
    }
    function getDept(filter, page, enableLoader = true) {
      if (enableLoader) isFetching.value = true;
      if (filter != null) {
        (0,_api__WEBPACK_IMPORTED_MODULE_11__["default"])().comptability.cashCollections.getDebts(filter).then(response => {
          metaDataAllDisbursements.value = response.data;
          allDisbursements.value = response.data.data;
          isFetching.value = false;
        }).catch(error => {
          isFetching.value = false;
          notify.error("Erreur lors de la récupération des données. " + error.data?.response ? error.data.response.message : 0);
        });
      } else {
        if (page != null) {
          (0,_api__WEBPACK_IMPORTED_MODULE_11__["default"])().comptability.cashCollections.getDebts(null, page).then(response => {
            metaDataAllDisbursements.value = response.data;
            allDisbursements.value = response.data.data;
            isFetching.value = false;
          }).catch(error => {
            isFetching.value = false;
            notify.error("Erreur lors de la récupération des données. " + error.data?.response ? error.data.response.message : 0);
          });
        } else {
          (0,_api__WEBPACK_IMPORTED_MODULE_11__["default"])().comptability.cashCollections.getDebts().then(response => {
            metaDataAllDisbursements.value = response.data;
            allDisbursements.value = response.data.data;
            isFetching.value = false;
          }).catch(error => {
            isFetching.value = false;
          });
        }
      }
    }
    function createRefund() {
      isLoadingForm.value = true;
      const dataToSend = Object.assign({}, refundData.value);
      dataToSend.emprunt = {
        id: dataToSend.emprunt.id
      };
      // console.log("createRefund dataToSend: ", dataToSend);
      (0,_api__WEBPACK_IMPORTED_MODULE_11__["default"])().comptability.cashCollections.createRefund(dataToSend).then(response => {
        isLoadingForm.value = false;
        notify.success(`Remboursement réglé au montant de ${response.data.cost}`);
        actionType.value = "list";
        doGet();
        // Reset values
        refundData.value = {
          emprunt: {
            id: null
          },
          label: "",
          cash_pay: null,
          currency: "XAF",
          paymentmethod: {},
          medias: null
        };
        (0,_state_api_userState__WEBPACK_IMPORTED_MODULE_1__.refreshUserInfos)().then(() => {
          notify.success("Comptes mis à jour");
        }).catch(error => {
          notify.error("Echec ! Veuillez Réessayer");
        });
      }).catch(error => {
        isLoadingForm.value = false;
        notify.error("Erreur lors du remboursement de l'emprunts: " + error.data.response ? error.data?.response.message : 0);
      });
    }
    function createOtherRefund() {
      isLoadingForm.value = true;
      // const dataToSend = Object.assign({}, otherRefundData.value);
      // console.log("createRefund dataToSend: ", dataToSend);
      (0,_api__WEBPACK_IMPORTED_MODULE_11__["default"])().comptability.othersDisbursements.create(otherRefundData.value).then(response => {
        isLoadingForm.value = false;
        notify.success(`Opération Enregistrée avec succès`);
        actionType.value = "list";
        doGet();
        // Reset values
        otherRefundData.value = {
          label: "",
          cost: null,
          paymentmethod: {},
          start: null
          // medias: null,
        };

        (0,_state_api_userState__WEBPACK_IMPORTED_MODULE_1__.refreshUserInfos)().then(() => {
          notify.success("Comptes mis à jour");
        }).catch(error => {
          notify.error("Echec de mise à jour des données de comptes! Recharger la page.");
        });
      }).catch(error => {
        isLoadingForm.value = false;
        notify.error("Erreur lors de l'enregistrement de l'opération: " + error.data.response ? error.data?.response.message : 0);
      });
    }
    function refundDebt() {
      isLoadingForm.value = true;
      if ((0,_validator__WEBPACK_IMPORTED_MODULE_5__.validateAll)(validations.value.debtData)) {
        const dataToSend = Object.assign({}, debtData.value);
        // console.log("refundDebt dataToSend: ", dataToSend);
        (0,_api__WEBPACK_IMPORTED_MODULE_11__["default"])().comptability.cashCollections.refundDept(dataToSend).then(response => {
          isLoadingForm.value = false;
          notify.success(`Dette réglée au montant de ${response.data.cost}`);
          doGet();
          actionType.value = "list";
          // Reset values
          debtData.value = {
            cash_pay: null,
            label: "",
            paymentmethod: {},
            transaction: {
              id: null
            },
            start: null
          };
          (0,_state_api_userState__WEBPACK_IMPORTED_MODULE_1__.refreshUserInfos)().then(() => {
            notify.success("Comptes mis à jour.");
          }).catch(error => {
            notify.error("Echec de la mise à jour du compte. Actualiser la page.");
          });
        }).catch(error => {
          isLoadingForm.value = false;
          notify.error("Erreur lors du paiement de la dette. " + error.data?.response ? error.data.response.message : 0);
        });
      } else {
        notify.error("Veuillez remplir tous les champs.");
      }
    }
    function getSelected(id, type) {
      let selected = [];
      switch (type) {
        case "loan":
          selected = allLoans.value.filter(item => item.id == id);
          break;
        default:
          break;
      }
      return selected[0];
    }
    async function doGet(filter, page, enableLoader = true) {
      switch (activeFilter.value) {
        case _constants_operations__WEBPACK_IMPORTED_MODULE_12__.REMBOURSEMENT_EMPRUNTS:
          await getRefunds(filter, page, enableLoader);
          break;
        case _constants_operations__WEBPACK_IMPORTED_MODULE_12__.DETTES:
          getDept(filter, page, enableLoader);
          break;
        case _constants_operations__WEBPACK_IMPORTED_MODULE_12__.AUTRES_DECAISSEMENTS:
          await getOtherDisbursement(filter, page, enableLoader);
          break;
        default:
          break;
      }
    }
    function doCreate() {
      switch (activeFilter.value) {
        case _constants_operations__WEBPACK_IMPORTED_MODULE_12__.REMBOURSEMENT_EMPRUNTS:
          createRefund();
          break;
        case _constants_operations__WEBPACK_IMPORTED_MODULE_12__.DETTES:
          refundDebt();
          break;
        case _constants_operations__WEBPACK_IMPORTED_MODULE_12__.AUTRES_DECAISSEMENTS:
          createOtherRefund();
          break;
        default:
          break;
      }
    }
    function getExportedData() {
      let data;
      switch (activeFilter.value) {
        case _constants_operations__WEBPACK_IMPORTED_MODULE_12__.REMBOURSEMENT_EMPRUNTS:
          // console.log("getExportedData refunds.value: ", allDisbursements.value);
          data = (0,_composable_useExport__WEBPACK_IMPORTED_MODULE_2__.exportRefundDataOps)(allDisbursements.value);
          break;
        case _constants_operations__WEBPACK_IMPORTED_MODULE_12__.DETTES:
          // console.log("getExportedData debts.value: ", allDisbursements.value);
          data = (0,_composable_useExport__WEBPACK_IMPORTED_MODULE_2__.exportDebtsDataOps)(allDisbursements.value);
          break;
        case _constants_operations__WEBPACK_IMPORTED_MODULE_12__.AUTRES_DECAISSEMENTS:
          // console.log("getExportedData debts.value: ", allDisbursements.value);
          data = (0,_composable_useExport__WEBPACK_IMPORTED_MODULE_2__.exportOtherDisbursementDataOps)(allDisbursements.value);
          break;
        default:
          break;
      }
      return data;
    }
    const calculTotalAmount = data => {
      totalAmmount.value = 0;
      currency.value = data[0]?.currency;
      switch (activeFilter.value) {
        case _constants_operations__WEBPACK_IMPORTED_MODULE_12__.REMBOURSEMENT_EMPRUNTS:
          for (const item of data) {
            if (item.reste != item.total_price) {
              totalAmmount.value += item.value + item.reste;
            }
          }
          break;
        case _constants_operations__WEBPACK_IMPORTED_MODULE_12__.DETTES:
          for (const item of data) {
            totalAmmount.value += item.dette;
          }
          break;
        case _constants_operations__WEBPACK_IMPORTED_MODULE_12__.AUTRES_DECAISSEMENTS:
          for (const item of data) {
            totalAmmount.value += item.cost;
          }
          break;
      }
    };
    // User permissions
    const permissions_reading = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_9__["default"])(activeFilter.value == _constants_operations__WEBPACK_IMPORTED_MODULE_12__.REMBOURSEMENT_EMPRUNTS ? "remboursement" : activeFilter.value == _constants_operations__WEBPACK_IMPORTED_MODULE_12__.DETTES ? "reglement_dette" : "autre_decaissement").map(element => {
      return element.action;
    });
    /**
     * Providers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissions", permissions);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissions_reading", permissions_reading);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("updateSubView", updateSubView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeFilter", activeFilter);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("isLoadingForm", isLoadingForm);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("updateActiveFilter", updateActiveFilter);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("paymentMethods", paymentMethods);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("allDisbursements", allDisbursements);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("allLoans", allLoans);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("loansList", loansList);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("refundData", refundData);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("otherRefundData", otherRefundData);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("debtData", debtData);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("selectedDebt", selectedDebt);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("doCreate", doCreate);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("getSelected", getSelected);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("openRefundDebt", openRefundDebt);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("validations", validations);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("totalAmmount", totalAmmount);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("currency", currency);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("calculTotalAmount", calculTotalAmount);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("metaDataAllDisbursements", metaDataAllDisbursements);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("getDept", getDept);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("getRefunds", getRefunds);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("doGet", doGet);
    /**
     * Page Lifecycle
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onBeforeMount)(async () => {
      // Use Permissions
      permissions.value = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_9__["default"])(activeFilter.value == _constants_operations__WEBPACK_IMPORTED_MODULE_12__.REMBOURSEMENT_EMPRUNTS ? "remboursement" : "reglement_dette").map(element => {
        return element.action;
      });
      // Load datas
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_10__["default"])("read", permissions_reading)) {
        await doGet();
      }
      calculTotalAmount(allDisbursements.value);
      getPaymentMethods(paymentMethods, "credit");
    });
    const __returned__ = {
      updateAppHeaderAction,
      getPaymentMethods,
      platform,
      router,
      route,
      notify,
      actionType,
      totalAmmount,
      currency,
      metaDataAllDisbursements,
      activeFilter,
      isNewClient,
      paymentMethods,
      allDisbursements,
      allLoans,
      loansList,
      dateDebut,
      dateFin,
      selectedLoan,
      selectedDebt,
      refundData,
      debtData,
      otherRefundData,
      validations,
      isFetching,
      isLoading,
      isLoadingForm,
      permissions,
      updateSubView,
      openRefundDebt,
      updateActiveFilter,
      getRefunds,
      getOtherDisbursement,
      getDept,
      createRefund,
      createOtherRefund,
      refundDebt,
      getSelected,
      doGet,
      doCreate,
      getExportedData,
      calculTotalAmount,
      permissions_reading,
      get filename() {
        return _composable_useExport__WEBPACK_IMPORTED_MODULE_2__.filename;
      },
      get formatDate() {
        return _utils__WEBPACK_IMPORTED_MODULE_6__.formatDate;
      },
      get formatRefSystem() {
        return _utils__WEBPACK_IMPORTED_MODULE_6__.formatRefSystem;
      },
      get numeral() {
        return (numeral__WEBPACK_IMPORTED_MODULE_7___default());
      },
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_10__["default"];
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingGenerateDocument.vue?vue&type=script&setup=true&lang=ts":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingGenerateDocument.vue?vue&type=script&setup=true&lang=ts ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_push_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.push.js */ "./node_modules/core-js/modules/es.array.push.js");
/* harmony import */ var core_js_modules_es_array_push_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_push_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var vue_router__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! vue-router */ "./node_modules/vue-router/dist/vue-router.mjs");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var dayjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! dayjs */ "./node_modules/dayjs/dayjs.min.js");
/* harmony import */ var dayjs__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(dayjs__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _composable_useSendFile__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/composable/useSendFile */ "./src/composable/useSendFile.ts");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _state_services_settingState__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/state/services/settingState */ "./src/state/services/settingState.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _composable_useDocuments__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @/composable/useDocuments */ "./src/composable/useDocuments.ts");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @/api */ "./src/api/index.ts");
/* harmony import */ var _composable_useNotyf__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @/composable/useNotyf */ "./src/composable/useNotyf.ts");
/* harmony import */ var _composable_useEmail__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @/composable/useEmail */ "./src/composable/useEmail.ts");
/* harmony import */ var _composable_useValidator__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @/composable/useValidator */ "./src/composable/useValidator.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");











/**
 * API Calls
 */

/**
 * Composables
 */

/**
 * Composables
 */





/**
 * Variables
 */
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_1__.defineComponent)({
  __name: 'accountingGenerateDocument',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)();
    // Pagination
    const metaDataListDocuments = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)();
    const router = (0,vue_router__WEBPACK_IMPORTED_MODULE_14__.useRouter)();
    const listDocuments = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([]);
    const content = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)({});
    const dateCreation = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)({});
    const receiver = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)({
      to: "",
      grade: "",
      companyName: ""
    });
    const object = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)("");
    const emailReceiver = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)("");
    const notify = (0,_composable_useNotyf__WEBPACK_IMPORTED_MODULE_9__["default"])();
    const documentType = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([]);
    const cachet = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(true);
    const statusDocumentCreated = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)();
    const isLoading = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(false);
    const documentTypeSelected = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)({});
    const selectedDocument = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)({});
    const error = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)({});
    const lastSelectedIndex = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(0);
    let activeButton = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(true);
    const viewRightContent = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(false);
    const windowWidth = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(window.innerWidth);
    const sendingEmail = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(false);
    let errorReceiverFormat = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(false);
    let errorSignatureFormat = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(false);
    content.value = {
      greeting: "Formule de politesse",
      body: "Corps du document",
      conclusion: "Conclusion",
      signatureName: "Signature",
      signatureMore: "Autre contenu"
    };
    documentType.value = [{
      value: "Lettre administrative",
      id: 1
    }
    // { value: "Communiqué", id: 2 },
    // { value: "Lettre de préavis de remboursement", id: 3 },
    // { value: "Lettre de recouvrement", id: 4 },
    // { value: "Demande de contact et proposition", id: 5 },
    // { value: "Demande de remboursement pour marchandise non livrée", id: 6 },
    // { value: "Demande de réparation ou de remplacement de produit", id: 7 },
    // { value: "Demande de prix de marchandises", id: 8 },
    ];

    (0,vue__WEBPACK_IMPORTED_MODULE_1__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userCurrentBranch.value.id, newValue => {
      actionType.value = listDocuments.value.length == 0 ? "empty" : "list";
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_13__["default"])("read", permissions)) {
        getAllDocuments();
      }
    });
    /**
     * Functions
     */
    const updateSubView = action => {
      // actionType.value = action;
    };
    /**
     * id: 1, object: "Demande de Sponsoring", type:"Lettre Administrative",
     * from: "b.ziletankeu@gmail.com",
     * to: "Monsieur le CEO de APPS LERNEN", selected: false, content: ""
     */
    const deleteDocument = async document => {
      await (0,_api__WEBPACK_IMPORTED_MODULE_8__["default"])().accounting.deleteDocument(document.id).then(response => {
        //réinitialisation/remise à zero
        listDocuments.value = [];
        getAllDocuments();
        actionType.value = listDocuments.value.length > 0 ? "list" : "empty";
        notify.success(`${document.object} supprimé.`);
      }).catch(err => {
        console.log("err: ", err);
        actionType.value = listDocuments.value.length > 0 ? "list" : "empty";
        notify.error(`Erreur lors de la Suppression du document : ${document.object}. Veuillez recharger la page`);
      });
    };
    const APIDocuments = async page => {
      await (0,_api__WEBPACK_IMPORTED_MODULE_8__["default"])().accounting.getAllDocuments(page).then(response => {
        metaDataListDocuments.value = response.data;
        let docs = response.data.data;
        //réinitialisation/remise à zero
        listDocuments.value = [];
        // selectedDocument.value = {};
        if (docs.length > 0) {
          docs.forEach(element => {
            let fullDocument;
            fullDocument = extractContentDocument(element.type, element.content, element.receiver);
            // console.log(
            // 	"fullDocument: ",
            // 	fullDocument,
            // 	getTypeDocument({ key: "id", data: doc.type })
            // );
            if (fullDocument) {
              fullDocument.id = element.id;
              fullDocument.email = element.email == "" ? "" : element.email;
              fullDocument.type = getTypeDocument({
                key: "name",
                data: element.type
              });
              fullDocument.typeId = getTypeDocument({
                key: "id",
                data: element.type
              });
              fullDocument.created_at = element.created_at;
              listDocuments.value.push(fullDocument);
            }
            // console.log("fullDocument: ", fullDocument);
          });
        }

        actionType.value = listDocuments.value.length > 0 ? "list" : "empty";
      }).catch(err => {
        console.log("err: ", err);
        actionType.value = listDocuments.value.length > 0 ? "list" : "empty";
        notify.error(`Erreur lors de la collecte des documents. Veuillez recharger la page`);
      });
    };
    const getAllDocuments = async (page, enableLoader = true) => {
      actionType.value = "loading";
      // selectedDocument.value = {}
      APIDocuments(page);
    };
    const saveDocument = async (sendEmail = false, update = false) => {
      activeButton.value = false;
      isLoading.value = true;
      const filename = object.value.replaceAll(" ", "-") + ".pdf"; // name of file
      if (sendEmail) {
        update ? await updateDoc() : await newDoc();
        const file = [];
        file.push(await (0,_composable_useSendFile__WEBPACK_IMPORTED_MODULE_3__.uploadPDFwithBLOB)(generatePDFtoBLOB(), filename));
        await (0,_composable_useEmail__WEBPACK_IMPORTED_MODULE_10__.sendMail)(file,
        // Fichier joint
        _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.email,
        // adresse expediteur
        emailReceiver.value,
        // adresse recepteur
        object.value,
        // Objet du mail
        "Ci joint : - " + object.value,
        // contenu du mail
        {
          successMsg: "Mail envoyé avec succès",
          errorMsg: "Une erreur c'est produite lors de l'envoi du mail"
        });
        if ((await _composable_useEmail__WEBPACK_IMPORTED_MODULE_10__.responseEmailSended) && statusDocumentCreated.value) {
          actionType.value = "list";
          getAllDocuments();
          // console.log("actionType.value: ", actionType.value);
        } else {
          activeButton.value = true;
        }
        isLoading.value = false;
      } else {
        update ? await updateDoc() : await newDoc();
        if (statusDocumentCreated.value) {
          actionType.value = "list";
          getAllDocuments();
        } else {
          activeButton.value = true;
        }
        isLoading.value = false;
      }
    };
    async function updateDoc() {
      // console.log("selectedDocument.value.id: ", selectedDocument.value.id);
      await (0,_api__WEBPACK_IMPORTED_MODULE_8__["default"])().accounting.updateDocument({
        content: contractContentDocument(),
        receiver: Object.values(receiver.value).join("##"),
        email: emailReceiver.value,
        type: [documentTypeSelected.value.value, documentTypeSelected.value.id].join("##")
      }, selectedDocument.value.id).then(response => {
        let result = response.data;
        isLoading.value = false;
        statusDocumentCreated.value = true;
        notify.success("Succès. Le document " + result.type + " a été mis à Jour");
      }).catch(err => {
        isLoading.value = false;
        statusDocumentCreated.value = false;
        notify.error(`Erreur lors de la mise à jour de votre document. Veuillez reéssayer `);
      });
    }
    async function newDoc() {
      const validate = _composable_useValidator__WEBPACK_IMPORTED_MODULE_11__["default"].adminLetter(object.value, sendingEmail.value, emailReceiver.value, errorReceiverFormat.value, errorSignatureFormat.value, content.value, dateCreation.value);
      if (validate.error) {
        let msg = "";
        validate.content.forEach(errorMsg => {
          msg = msg + "<li>" + errorMsg + "</li>";
        });
        notify.error(`Veuillez remplir tous les champs.`); //`Rapport des erreurs : <br/> <ul>`+msg+`</ul>`
        error.value = validate.errorObject;
      } else {
        await (0,_api__WEBPACK_IMPORTED_MODULE_8__["default"])().accounting.newDocument({
          content: contractContentDocument(),
          receiver: Object.values(receiver.value).join("##"),
          email: emailReceiver.value,
          type: [documentTypeSelected.value.value, documentTypeSelected.value.id].join("##")
        }).then(response => {
          let result = response.data;
          isLoading.value = false;
          statusDocumentCreated.value = true;
          notify.success("Succès. Le document " + result.type + " a été sauvegardé");
        }).catch(err => {
          isLoading.value = false;
          statusDocumentCreated.value = false;
          notify.error(`Erreur lors de la sauvegarde de votre document. Veuillez reéssayer `);
        });
      }
    }
    function contractContentDocument() {
      let result = "";
      switch (documentTypeSelected.value.id) {
        case 1:
          result = [dateCreation.value.date, object.value, content.value.greeting, content.value.body, content.value.conclusion, cachet.value == true ? "true" : "false", content.value.signatureName, content.value.signatureMore].join("##");
          break;
        case 2:
          break;
        default:
          break;
      }
      return result;
    }
    function extractContentDocument(type, content, receiver) {
      const contents = content.split("##");
      const idType = getTypeDocument({
        key: "id",
        data: type
      });
      let result;
      const receivers = receiver.split("##");
      switch (idType) {
        case "1":
          result = {
            dateCreation: {
              date: contents[0]
            },
            object: contents[1],
            selected: false,
            receiver: {
              to: receivers[0],
              grade: receivers[1],
              companyName: receivers[2] ? receivers[2] : ""
            },
            content: {
              greeting: contents[2],
              body: contents[3],
              conclusion: contents[4],
              signatureName: contents[6],
              signatureMore: contents[7] ? contents[7] : ""
            },
            cachet: contents[5] == "true" ? true : false
          };
          break;
        case "2":
          activeButton.value = false;
          break;
        case "3":
          break;
        case "4":
          break;
        default:
          break;
      }
      return result;
    }
    function getTypeDocument(type) {
      if (type.key == "id") {
        return type.data.split("##").pop();
      } else {
        return type.data.split("##").reverse().pop();
      }
    }
    function generatePDFtoBLOB() {
      const data = {
        organisationName: !lodash__WEBPACK_IMPORTED_MODULE_6__.isEmpty(_state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.enterprise) ? _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.enterprise.nom_entreprise : _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.particular,
        email: _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.email,
        phone: !lodash__WEBPACK_IMPORTED_MODULE_6__.isEmpty(_state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.enterprise) ? _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.enterprise.phone : _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.particular.phone,
        address: !lodash__WEBPACK_IMPORTED_MODULE_6__.isEmpty(_state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.enterprise) ? _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.enterprise.ville + _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.enterprise.pays : _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.particular.ville + _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.particular.pays,
        date: dayjs__WEBPACK_IMPORTED_MODULE_2___default()(dateCreation.value.date).format("dddd").toString() + ", " + dayjs__WEBPACK_IMPORTED_MODULE_2___default()(dateCreation.value.date).format("DD MMMM YYYY").toString(),
        receiver: {
          to: receiver.value.to,
          grade: receiver.value.grade,
          companyName: receiver.value.companyName
        },
        signature: {
          signatureName: content.value.signatureName,
          signatureMore: content.value.signatureMore
        },
        object: object.value,
        cachet: {
          status: cachet.value == true ? "true" : "false",
          data: _state_services_settingState__WEBPACK_IMPORTED_MODULE_5__.cacheFileName.value
        },
        header: "",
        content: {
          greeting: content.value.greeting,
          body: content.value.body,
          conclusion: content.value.conclusion
        }
      };
      const blob = _composable_useDocuments__WEBPACK_IMPORTED_MODULE_7__.adminLetter.generateBlob(_state_services_settingState__WEBPACK_IMPORTED_MODULE_5__.headerPortrait.value, data.object.replaceAll(" ", "-") + "-" + data.organisationName.replaceAll(" ", ".") + "-",
      // name of file
      data, {
        style: "bold",
        name: "helvetica"
      });
      return blob;
    }
    function printDocToPDF(type = "pdf") {
      const data = {
        organisationName: !lodash__WEBPACK_IMPORTED_MODULE_6__.isEmpty(_state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.enterprise) ? _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.enterprise.nom_entreprise : _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.staff ? _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.staff.label : _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.particular.nom,
        email: _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.email,
        phone: !lodash__WEBPACK_IMPORTED_MODULE_6__.isEmpty(_state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.enterprise) ? _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.enterprise.phone : _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.staff ? _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.staff.phone : _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.particular.phone,
        address: !lodash__WEBPACK_IMPORTED_MODULE_6__.isEmpty(_state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.enterprise) ? _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.enterprise.ville + _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.enterprise.pays : _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.staff ? _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.staff.ville + _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.staff.pays : _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.particular.ville + _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userInfos.value.particular.pays,
        date: dayjs__WEBPACK_IMPORTED_MODULE_2___default()(selectedDocument.value.dateCreation.date).format("dddd").toString() + ", " + dayjs__WEBPACK_IMPORTED_MODULE_2___default()(selectedDocument.value.dateCreation.date).format("DD MMMM YYYY").toString(),
        receiver: {
          to: selectedDocument.value.receiver.to,
          grade: selectedDocument.value.receiver.grade,
          companyName: selectedDocument.value.receiver.companyName
        },
        signature: {
          signatureName: selectedDocument.value.content.signatureName,
          signatureMore: selectedDocument.value.content.signatureMore
        },
        object: selectedDocument.value.object,
        cachet: {
          status: selectedDocument.value.cachet,
          data: _state_services_settingState__WEBPACK_IMPORTED_MODULE_5__.cacheFileName.value
        },
        header: "",
        content: {
          greeting: selectedDocument.value.content.greeting,
          body: selectedDocument.value.content.body,
          conclusion: selectedDocument.value.content.conclusion
        }
      };
      if (type == "pdf") {
        _composable_useDocuments__WEBPACK_IMPORTED_MODULE_7__.adminLetter.printToPDF(_state_services_settingState__WEBPACK_IMPORTED_MODULE_5__.headerPortrait.value,
        // header
        data.object.replaceAll(" ", "-") + "-" + data.organisationName.replaceAll(" ", ".") + "-",
        // name of file
        data,
        // data
        {
          style: "bold",
          name: "helvetica"
        } // style of font
        );
      } else if (type == "blob") {
        const blob = _composable_useDocuments__WEBPACK_IMPORTED_MODULE_7__.adminLetter.generateBlob(_state_services_settingState__WEBPACK_IMPORTED_MODULE_5__.headerPortrait.value, data.object.replaceAll(" ", "-") + "-" + data.organisationName.replaceAll(" ", ".") + "-",
        // name of file
        data, {
          style: "bold",
          name: "helvetica"
        });
      }
    }
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_12__["default"])("depense_achats").map(element => {
      return element.action;
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("permissions", permissions);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("content", content);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("updateSubView", updateSubView);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("activeView", null);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("activeFilter", null);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("viewRightContent", viewRightContent);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("dateCreation", dateCreation);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("object", object);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("receiver", receiver);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("cachet", cachet);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("documentType", documentType);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("emailReceiver", emailReceiver);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("saveDocument", saveDocument);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("documentTypeSelected", documentTypeSelected);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("statusDocumentCreated", statusDocumentCreated);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("activeButton", activeButton);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("isLoading", isLoading);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("listDocuments", listDocuments);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("selectedDocument", selectedDocument);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("deleteDocument", deleteDocument);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("lastSelectedIndex", lastSelectedIndex);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("errorSignatureFormat", errorSignatureFormat);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("errorReceiverFormat", errorReceiverFormat);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("sendingEmail", sendingEmail);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("error", error);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("metaDataListDocuments", metaDataListDocuments);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("getAllDocuments", getAllDocuments);
    /**
     * Page Lifecycle
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.onBeforeMount)(() => {
      actionType.value = listDocuments.value.length == 0 ? "empty" : "list";
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.onMounted)(() => {
      (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_15__.u)({
        title: "Comptabilité / Générer un Document"
      });
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_13__["default"])("read", permissions)) {
        getAllDocuments();
      }
    });
    const __returned__ = {
      actionType,
      metaDataListDocuments,
      router,
      listDocuments,
      content,
      dateCreation,
      receiver,
      object,
      emailReceiver,
      notify,
      documentType,
      cachet,
      statusDocumentCreated,
      isLoading,
      documentTypeSelected,
      selectedDocument,
      error,
      lastSelectedIndex,
      get activeButton() {
        return activeButton;
      },
      set activeButton(v) {
        activeButton = v;
      },
      viewRightContent,
      windowWidth,
      sendingEmail,
      get errorReceiverFormat() {
        return errorReceiverFormat;
      },
      set errorReceiverFormat(v) {
        errorReceiverFormat = v;
      },
      get errorSignatureFormat() {
        return errorSignatureFormat;
      },
      set errorSignatureFormat(v) {
        errorSignatureFormat = v;
      },
      updateSubView,
      deleteDocument,
      APIDocuments,
      getAllDocuments,
      saveDocument,
      updateDoc,
      newDoc,
      contractContentDocument,
      extractContentDocument,
      getTypeDocument,
      generatePDFtoBLOB,
      printDocToPDF,
      permissions,
      get isOwnBranch() {
        return _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.isOwnBranch;
      },
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_13__["default"];
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingOverview.vue?vue&type=script&setup=true&lang=ts":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingOverview.vue?vue&type=script&setup=true&lang=ts ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/api */ "./src/api/index.ts");
/* harmony import */ var _composable_useNotyf__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/composable/useNotyf */ "./src/composable/useNotyf.ts");
/* harmony import */ var _composable_useFormatMoney__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/composable/useFormatMoney */ "./src/composable/useFormatMoney.ts");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");









/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'accountingOverview',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_7__.u)({
      title: "Comptabilité / Vue d'ensemble"
    });
    /**
     * Composables
     */
    const notify = (0,_composable_useNotyf__WEBPACK_IMPORTED_MODULE_2__["default"])();
    /**
     * Variables
     */
    const isLoading = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    const infosOverview = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([]);
    const sales = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)({});
    const purchases = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)({});
    const spents = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)({});
    const amountReceive = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)({});
    const bankAmount = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)({});
    const mobileAmount = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)({});
    const debt = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)({});
    const creance = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)({});
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_4__.userCurrentBranch.value.id, async newValue => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_6__["default"])('read', permissions)) {
        await getOverviewInfo();
      }
    });
    /**
     * functions
     */
    async function getOverviewInfo() {
      isLoading.value = true;
      await (0,_api__WEBPACK_IMPORTED_MODULE_1__["default"])().overview.accounting().then(resp => {
        console.log("response overview accounting:", resp.data);
        [sales.value, purchases.value, spents.value, amountReceive.value, bankAmount.value, mobileAmount.value, debt.value, creance.value] = resp.data;
        infosOverview.value = resp.data;
        isLoading.value = false;
      }).catch(err => {
        isLoading.value = false;
      });
      // await api()
      // 	.comptability.WalletState()
      // 	.then((response: any) => {
      // 		console.log("response walet infos :", response.data);
      // 	});
    }
    // function formatInfos(info: any){
    // 	$filters.moneyFormat(
    // 		info != undefined
    // 			? info.value
    // 			: 0
    // 	)
    // }
    /**
     * Injects
     */
    const goBack = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("goBack");
    const goNext = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("goNext");
    const reloadPage = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("reloadPage");
    /**
     * page life cycle hook
     */
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_5__["default"])("etatComptas").map(element => {
      return element.action;
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onBeforeMount)(async () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_6__["default"])('read', permissions)) {
        await getOverviewInfo();
      }
      // console.log("sales :", sales.value);
    });

    const __returned__ = {
      notify,
      isLoading,
      infosOverview,
      sales,
      purchases,
      spents,
      amountReceive,
      bankAmount,
      mobileAmount,
      debt,
      creance,
      getOverviewInfo,
      goBack,
      goNext,
      reloadPage,
      permissions,
      get orignalMoney() {
        return _composable_useFormatMoney__WEBPACK_IMPORTED_MODULE_3__["default"];
      },
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_6__["default"];
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingPurchases.vue?vue&type=script&setup=true&lang=ts":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingPurchases.vue?vue&type=script&setup=true&lang=ts ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var vue_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! vue-router */ "./node_modules/vue-router/dist/vue-router.mjs");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/api */ "./src/api/index.ts");
/* harmony import */ var _composable_useExport__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/composable/useExport */ "./src/composable/useExport.ts");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/utils */ "./src/utils/index.ts");
/* harmony import */ var numeral__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! numeral */ "./node_modules/numeral/numeral.js");
/* harmony import */ var numeral__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(numeral__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");
/* harmony import */ var _constants_operations__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @/constants/operations */ "./src/constants/operations/index.ts");
/* harmony import */ var _composable_useNotyf__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @/composable/useNotyf */ "./src/composable/useNotyf.ts");













/**
 * Composables
 */

/**
 * Injects
 */
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'accountingPurchases',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_10__.u)({
      title: "Comptabilité / Achats"
    });
    const platform = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("platform");
    const updateAppHeaderAction = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("updateAppHeaderAction");
    /**
     * Variables
     */
    const router = (0,vue_router__WEBPACK_IMPORTED_MODULE_11__.useRouter)();
    const route = (0,vue_router__WEBPACK_IMPORTED_MODULE_11__.useRoute)();
    const transitionName = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("translate-subview-right");
    const activeView = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("list");
    const activeFilter = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const openTab = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    const purchases = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([]);
    const notify = (0,_composable_useNotyf__WEBPACK_IMPORTED_MODULE_9__["default"])();
    // Pagination
    const metaDataPurchases = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)();
    const dateDebut = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const dateFin = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    // Datas
    const name = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const phone = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const email = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const type = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(route.params.page);
    const id = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const legalForm = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const fiscalNumber = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const commercialRegiste = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const selected = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)({});
    const imageProfile = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([]);
    // Loaders
    const isFetching = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    const isLoading = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    // Watchers
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => activeView.value, (newValue, oldValue) => {
      // Manage app header back button
      if (newValue > 1) {
        const callback = () => {
          if (activeView.value > 1) {
            activeView.value = activeView.value - 1;
          } else {
            router.go(-1);
          }
        };
        updateAppHeaderAction("back", callback);
      }
      if (newValue > oldValue) {
        transitionName.value = "translate-subview-right";
      } else {
        transitionName.value = "translate-subview-left";
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_3__.userCurrentBranch.value.id, newValue => {
      // Getting all Sellers
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_7__["default"])("read", permissions)) {
        getAllPurchases();
      }
    });
    /**
     * Functions
     */
    const updateSubView = (view, action) => {
      activeView.value = view;
      actionType.value = action;
    };
    function getAllPurchases(filter, page, enableLoader = true) {
      if (enableLoader) isFetching.value = true;
      if (filter != null) {
        (0,_api__WEBPACK_IMPORTED_MODULE_1__["default"])().comptability.purchases.getAll(filter).then(response => {
          metaDataPurchases.value = response.data;
          purchases.value = response.data.data;
          isFetching.value = false;
        }).catch(err => {
          isFetching.value = false;
          notify.error(`Erreur lors du chargement des oprérations d'achats. Veuillez recharger la page`);
          console.log(err.response);
        });
      } else {
        if (page != null) {
          (0,_api__WEBPACK_IMPORTED_MODULE_1__["default"])().comptability.purchases.getAll(null, page).then(response => {
            metaDataPurchases.value = response.data;
            purchases.value = response.data.data;
            isFetching.value = false;
          }).catch(err => {
            isFetching.value = false;
            notify.error(`Erreur lors du chargement des opérations d'achats. Veuillez recharger la page`);
            console.log(err.response);
          });
        } else {
          (0,_api__WEBPACK_IMPORTED_MODULE_1__["default"])().comptability.purchases.getAll().then(response => {
            metaDataPurchases.value = response.data;
            purchases.value = response.data.data;
            isFetching.value = false;
          }).catch(err => {
            isFetching.value = false;
            notify.error(`Erreur lors du chargement des opérations d'achats. Veuillez recharger la page`);
            console.log(err.response);
          });
        }
      }
    }
    const deletePurchase = async (position, id) => {
      isLoading.value = true;
      await (0,_api__WEBPACK_IMPORTED_MODULE_1__["default"])().comptability.purchases.delete(id).then(response => {
        purchases.value.splice(position, purchases.value.length > -1 ? 1 : position - 1);
        notify.success("Opération supprimée avec succès");
        (0,_state_api_userState__WEBPACK_IMPORTED_MODULE_3__.refreshUserInfos)().then(() => {
          isLoading.value = false;
          notify.success("Vos comptes ont été mis à jour.");
          getAllPurchases();
        }).catch(error => {
          console.log("error: ", error);
          isLoading.value = false;
          notify.error("Nous n'avons pas pu rafraichir les données de vos comptes. Rechargez la page SVP");
        });
      }).catch(error => {
        isLoading.value = false;
        notify.error("Une erreur est survenue lors de la suppression de l'opération <br/>" + error.data.response.message);
      });
    };
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_6__["default"])("depense_achats").map(element => {
      return element.action;
    });
    /**
     * Providers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissions", permissions);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("updateSubView", updateSubView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeView", activeView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeFilter", activeFilter);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("isFetching", isFetching);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("isLoading", isLoading);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("purchases", purchases);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("name", name);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("phone", phone);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("email", email);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("type", type);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("imageProfile", imageProfile);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("legalForm", legalForm);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("fiscalNumber", fiscalNumber);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("commercialRegiste", commercialRegiste);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("getAllPurchases", getAllPurchases);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("metaDataPurchases", metaDataPurchases);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("deletePurchase", deletePurchase);
    /**
     * Page Lifecycle
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onBeforeMount)(() => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_7__["default"])("read", permissions)) {
        getAllPurchases();
      }
    });
    const __returned__ = {
      platform,
      updateAppHeaderAction,
      router,
      route,
      transitionName,
      activeView,
      actionType,
      activeFilter,
      openTab,
      purchases,
      notify,
      metaDataPurchases,
      dateDebut,
      dateFin,
      name,
      phone,
      email,
      type,
      id,
      legalForm,
      fiscalNumber,
      commercialRegiste,
      selected,
      imageProfile,
      isFetching,
      isLoading,
      updateSubView,
      getAllPurchases,
      deletePurchase,
      permissions,
      get exportProductPurchases() {
        return _composable_useExport__WEBPACK_IMPORTED_MODULE_2__.exportProductPurchases;
      },
      get filename() {
        return _composable_useExport__WEBPACK_IMPORTED_MODULE_2__.filename;
      },
      get isOwnBranch() {
        return _state_api_userState__WEBPACK_IMPORTED_MODULE_3__.isOwnBranch;
      },
      get formatDate() {
        return _utils__WEBPACK_IMPORTED_MODULE_4__.formatDate;
      },
      get formatRefSystem() {
        return _utils__WEBPACK_IMPORTED_MODULE_4__.formatRefSystem;
      },
      get numeral() {
        return (numeral__WEBPACK_IMPORTED_MODULE_5___default());
      },
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_7__["default"];
      },
      get PRODUCT_PURCHASES() {
        return _constants_operations__WEBPACK_IMPORTED_MODULE_8__.PRODUCT_PURCHASES;
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingReceipt.vue?vue&type=script&setup=true&lang=ts":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingReceipt.vue?vue&type=script&setup=true&lang=ts ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_push_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.push.js */ "./node_modules/core-js/modules/es.array.push.js");
/* harmony import */ var core_js_modules_es_array_push_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_push_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var vue_router__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! vue-router */ "./node_modules/vue-router/dist/vue-router.mjs");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var dayjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! dayjs */ "./node_modules/dayjs/dayjs.min.js");
/* harmony import */ var dayjs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(dayjs__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var dayjs_plugin_advancedFormat__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! dayjs/plugin/advancedFormat */ "./node_modules/dayjs/plugin/advancedFormat.js");
/* harmony import */ var dayjs_plugin_advancedFormat__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(dayjs_plugin_advancedFormat__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _composable_useSendFile__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/composable/useSendFile */ "./src/composable/useSendFile.ts");
/* harmony import */ var _composable_useRetrievingAll__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/composable/useRetrievingAll */ "./src/composable/useRetrievingAll.ts");
/* harmony import */ var _composable_useFacturation__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @/composable/useFacturation */ "./src/composable/useFacturation.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _composable_useExport__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @/composable/useExport */ "./src/composable/useExport.ts");
/* harmony import */ var _validator__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @/validator */ "./src/validator/index.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @/utils */ "./src/utils/index.ts");
/* harmony import */ var numeral__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! numeral */ "./node_modules/numeral/numeral.js");
/* harmony import */ var numeral__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(numeral__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _constants_operations__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @/constants/operations */ "./src/constants/operations/index.ts");
/* harmony import */ var _composable_useNotyf__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @/composable/useNotyf */ "./src/composable/useNotyf.ts");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @/api */ "./src/api/index.ts");




















/**
 * Composables
 */

/**
 * API Calls
 */

/**
 * Injects
 */
const pattern = "#";
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_1__.defineComponent)({
  __name: 'accountingReceipt',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    dayjs__WEBPACK_IMPORTED_MODULE_3___default().extend((dayjs_plugin_advancedFormat__WEBPACK_IMPORTED_MODULE_4___default()));
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_18__.u)({
      title: "Comptabilité / Encaissement"
    });
    const updateAppHeaderAction = (0,vue__WEBPACK_IMPORTED_MODULE_1__.inject)("updateAppHeaderAction");
    const getPaymentMethods = (0,vue__WEBPACK_IMPORTED_MODULE_1__.inject)("getPaymentMethods");
    const platform = (0,vue__WEBPACK_IMPORTED_MODULE_1__.inject)("platform");
    /**
     * Variables
     */
    const router = (0,vue_router__WEBPACK_IMPORTED_MODULE_19__.useRouter)();
    const route = (0,vue_router__WEBPACK_IMPORTED_MODULE_19__.useRoute)();
    const notify = (0,_composable_useNotyf__WEBPACK_IMPORTED_MODULE_16__["default"])();
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)("list");
    const activeFilter = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(_constants_operations__WEBPACK_IMPORTED_MODULE_15__.RISTOURNES);
    const dateDebut = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)("");
    const dateFin = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)("");
    // Pagination
    const metaDataAllReceipts = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)();
    const metaDataAllFunders = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)();
    const metaDataAllCreditors = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)();
    const metaDataAllProducts = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)();
    const metaDataAllProviders = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)();
    // Datas
    const isNewClient = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(false);
    const isRetrievedData = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(false);
    const paymentMethods = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([]);
    const allReceipts = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([]);
    const allProducts = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([]);
    const productsList = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([]);
    const allFunders = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([]);
    const fundersList = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([]);
    const allProviders = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([]);
    const providersList = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([]);
    const allClients = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([]);
    const clientsList = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([]);
    const allCreditors = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([]);
    const creditorsList = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([]);
    const claimsList = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([]);
    const selectedEcheance = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)();
    const selectedProduct = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)();
    const selectedClaim = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)({});
    const validations = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)();
    const newClientValidations = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)();
    const thirdValidation = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)();
    const discountData = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)({
      label: "",
      cost: null,
      currency: "XAF",
      third: {
        id: null
      },
      products: Array(),
      medias: Array(),
      description: "",
      start: new Date("yyyy-MM-dd")
    });
    const loanData = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)({
      name: "",
      email: "",
      contact: null,
      postal_code: "",
      label: "",
      cost: "",
      interet: "",
      total_price: "",
      suffix: "%",
      echeances: Array(),
      paymentmethod: {},
      third: {
        id: null
      },
      currency: "XAF",
      medias: Array(),
      description: "",
      start: new Date("yyyy-MM-dd")
    });
    const subventionData = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)({
      name: "",
      email: "",
      contact: null,
      postal_code: "",
      third: {
        id: null
      },
      label: "",
      cost: null,
      paymentmethod: {},
      medias: Array(),
      currency: "XAF",
      description: "",
      start: new Date("yyyy-MM-dd")
    });
    const ownFundData = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)({
      name: "",
      email: "",
      contact: null,
      label: "",
      cost: null,
      paymentmethod: {},
      // medias: Array<any>(),
      currency: "XAF",
      start: ""
    });
    const claimData = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)({
      cash_pay: null,
      label: "",
      paymentmethod: {},
      transcation: {},
      medias: Array(),
      start: new Date("yyyy-MM-dd")
    });
    // Loaders
    const isFetching = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(false);
    const isLoading = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(false);
    const isLoadingForm = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(false);
    const permissions = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)();
    // Watchers
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.watch)(() => activeFilter.value, newValue => {
      // Use Permissions
      permissions.value = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_11__["default"])(activeFilter.value == _constants_operations__WEBPACK_IMPORTED_MODULE_15__.FONDS_PROPRES || activeFilter.value == _constants_operations__WEBPACK_IMPORTED_MODULE_15__.SUBVENTIONS_EXPLOITATION ? activeFilter.value == _constants_operations__WEBPACK_IMPORTED_MODULE_15__.FONDS_PROPRES ? "fond_propre" : "subventions" : activeFilter.value).map(element => {
        return element.action;
      });
      console.log('permissions.value: ', activeFilter.value == _constants_operations__WEBPACK_IMPORTED_MODULE_15__.FONDS_PROPRES || activeFilter.value == _constants_operations__WEBPACK_IMPORTED_MODULE_15__.SUBVENTIONS_EXPLOITATION ? activeFilter.value == _constants_operations__WEBPACK_IMPORTED_MODULE_15__.FONDS_PROPRES ? "fond_propre" : "subventions" : activeFilter.value);
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_12__["default"])("read", permissions.value)) {
        doGet();
        getAllProductsAndProviders();
      }
    });
    // Watch for convert and add date to echeance
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.watch)(() => selectedEcheance.value, newValue => {
      // Function to save date
      const addDate = () => {
        loanData.value.echeances.push({
          date: newValue
        });
      };
      if (newValue != "" && newValue != null) {
        // converting date to timestamp
        const selectedDateConverted = dayjs__WEBPACK_IMPORTED_MODULE_3___default()(newValue.toString()).format("X");
        const today = dayjs__WEBPACK_IMPORTED_MODULE_3___default()().format("X");
        // Check if date inferior to today
        if (selectedDateConverted < today) {
          notify.warning("Vous devez choisir une date supérieure à celle d'aujourd'hui");
        } else {
          addDate();
        }
      }
    });
    // Watch for initialize produc quantity
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.watch)(() => selectedProduct.value, newValue => {
      const isExist = discountData.value.products.filter(item => item.id == newValue.id);
      if (isExist.length == 0) {
        const product = Object.assign({}, getSelected(newValue.id, "product"));
        product.quantity = 1;
        discountData.value.products.push(product);
      } else {
        notify.warning("Ce produit est déjà dans la liste");
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_2__.userCurrentBranch.value.id, async newValue => {
      // Load datas
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_12__["default"])("read", permissions.value)) {
        doGet();
        getAllProductsAndProviders();
      }
      // Load payment methods
      getPaymentMethods(paymentMethods, "credit");
    });
    /**
     * Functions
     */
    const updateSubView = action => {
      actionType.value = action;
    };
    const openRefundClaim = selected => {
      selectedClaim.value = selected.data;
      // console.log('selectedClaim.value' , isClaimAboutQuote(selectedClaim.value.real_reference) );
      actionType.value = "create";
      claimData.value.cash_pay = selected.data.creance;
      claimData.value.transcation = {
        id: selected.data.id
      };
    };
    const updateActiveFilter = action => {
      activeFilter.value = action;
    };
    const deleteEcheance = position => {
      loanData.value.echeances.splice(position, loanData.value.echeances.length > -1 ? 1 : position - 1);
    };
    const deleteProducts = position => {
      discountData.value.products.splice(position, discountData.value.products.length > -1 ? 1 : position - 1);
    };
    const getAllProductsAndProviders = () => {
      // Get all products
      (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().stocks.product.getAll().then(async response => {
        allProducts.value = response.data.data;
        const retrievedData = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)(response.data.data);
        if (response.data.last_page != 1) await (0,_composable_useRetrievingAll__WEBPACK_IMPORTED_MODULE_6__["default"])({
          which: "Products-for-discount",
          totalPages: response.data.last_page
        }, retrievedData, isRetrievedData);
        productsList.value = retrievedData.value.map(item => {
          return {
            key: item.id,
            id: item.id,
            value: item.label
          };
        });
        // Get all providers
        (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().thirds.specificList("fournisseur").then(response => {
          allProviders.value = response.data.data;
          providersList.value = allProviders.value.map(item => {
            return {
              key: item.id,
              id: item.id,
              value: item.name
            };
          });
        });
      });
    };
    const getDiscounts = (filter, page, enableLoader = true) => {
      if (enableLoader) isFetching.value = true;
      if (filter != null) {
        (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().comptability.cashCollections.getDiscounts(filter).then(response => {
          metaDataAllReceipts.value = response.data;
          allReceipts.value = response.data.data;
          // Stop loading
          isFetching.value = false;
        }).catch(error => {
          isFetching.value = false;
        });
      } else {
        (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().comptability.cashCollections.getDiscounts(null, page).then(response => {
          metaDataAllReceipts.value = response.data;
          allReceipts.value = response.data.data;
          // Stop loading
          isFetching.value = false;
        }).catch(error => {
          isFetching.value = false;
        });
      }
    };
    const getLoans = (filter, page, enableLoader = true) => {
      if (enableLoader) isFetching.value = true;
      if (filter != null) {
        (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().comptability.cashCollections.getLoans(filter).then(response => {
          metaDataAllReceipts.value = response.data;
          allReceipts.value = response.data.data;
          // Get all clients
          (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().thirds.specificList("creancier").then(response => {
            allCreditors.value = response.data.data;
            creditorsList.value = allCreditors.value.map(item => {
              return {
                key: item.id,
                id: item.id,
                value: item.name
              };
            });
            // Stop loading
            isFetching.value = false;
          });
        }).catch(error => {
          isFetching.value = false;
        });
      } else {
        (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().comptability.cashCollections.getLoans(null, page).then(response => {
          metaDataAllReceipts.value = response.data;
          allReceipts.value = response.data.data;
          // Get all clients
          (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().thirds.specificList("creancier").then(response => {
            allCreditors.value = response.data.data;
            creditorsList.value = allCreditors.value.map(item => {
              return {
                key: item.id,
                id: item.id,
                value: item.name
              };
            });
            // Stop loading
            isFetching.value = false;
          });
        }).catch(error => {
          isFetching.value = false;
        });
      }
    };
    function getSubventions(filter, page, enableLoader = true) {
      if (enableLoader) isFetching.value = true;
      if (filter != null) {
        (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().comptability.cashCollections.getSubventions(filter).then(response => {
          metaDataAllReceipts.value = response.data;
          allReceipts.value = response.data.data;
          // Get all funders
          (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().thirds.specificList("subventionneur").then(response => {
            allFunders.value = response.data.data;
            fundersList.value = allFunders.value.map(item => ({
              id: item.id,
              key: item.id,
              value: item.name
            }));
            // Stop loading
            isFetching.value = false;
          });
        }).catch(error => {
          isFetching.value = false;
        });
      } else {
        (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().comptability.cashCollections.getSubventions(null, page).then(response => {
          metaDataAllReceipts.value = response.data;
          allReceipts.value = response.data.data;
          // Get all funders
          (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().thirds.specificList("subventionneur").then(response => {
            allFunders.value = response.data.data;
            fundersList.value = allFunders.value.map(item => ({
              id: item.id,
              key: item.id,
              value: item.name
            }));
            // Stop loading
            isFetching.value = false;
          });
        }).catch(error => {
          isFetching.value = false;
        });
      }
    }
    function getClaims(filter, page, enableLoader = true) {
      if (enableLoader) isFetching.value = true;
      if (filter != null) {
        (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().comptability.cashCollections.getClaims(filter).then(response => {
          metaDataAllReceipts.value = response.data;
          allReceipts.value = response.data.data;
          claimsList.value = allReceipts.value.map(item => {
            return {
              key: item.id,
              id: item.id,
              value: item.label
            };
          });
          // Stop loading
          isFetching.value = false;
        }).catch(error => {
          isFetching.value = false;
        });
      } else {
        (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().comptability.cashCollections.getClaims(null, page).then(response => {
          metaDataAllReceipts.value = response.data;
          allReceipts.value = response.data.data;
          claimsList.value = allReceipts.value.map(item => {
            return {
              key: item.id,
              id: item.id,
              value: item.label
            };
          });
          // Stop loading
          isFetching.value = false;
        }).catch(error => {
          isFetching.value = false;
        });
      }
    }
    function getOwnFund(filter, page, enableLoader = true) {
      if (enableLoader) isFetching.value = true;
      if (filter != null) {
        (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().comptability.ownFunds.get(filter).then(response => {
          metaDataAllReceipts.value = response.data;
          allReceipts.value = response.data.data;
          // claimsList.value = allReceipts.value.map((item: any) => {
          // 	return {
          // 		key: item.id,
          // 		id: item.id,
          // 		value: item.label,
          // 	};
          // });
          // Stop loading
          isFetching.value = false;
        }).catch(error => {
          isFetching.value = false;
        });
      } else {
        (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().comptability.ownFunds.get(null, page).then(response => {
          metaDataAllReceipts.value = response.data;
          allReceipts.value = response.data.data;
          // claimsList.value = allReceipts.value.map((item: any) => {
          // 	return {
          // 		key: item.id,
          // 		id: item.id,
          // 		value: item.label,
          // 	};
          // });
          // Stop loading
          isFetching.value = false;
        }).catch(error => {
          isFetching.value = false;
        });
      }
    }
    function getSelected(id, type) {
      let selected = [];
      switch (type) {
        case "provider":
          selected = allProviders.value.filter(item => item.id == id);
          break;
        case "client":
          selected = allClients.value.filter(item => item.id == id);
          break;
        case "funder":
          selected = allFunders.value.filter(item => item.id == id);
          break;
        case "creditor":
          selected = allCreditors.value.filter(item => item.id == id);
          break;
        case "product":
          selected = allProducts.value.filter(item => item.id == id);
          break;
        default:
          break;
      }
      return selected[0];
    }
    function createDiscount() {
      isLoadingForm.value = true;
      if ((0,_validator__WEBPACK_IMPORTED_MODULE_10__.validateAll)(validations.value.discountData)) {
        let dataToSend = {};
        Object.assign(dataToSend, discountData.value);
        dataToSend.third = getSelected(dataToSend.third.id, "provider");
        console.log("medias : ", dataToSend.medias);
        console.log("dataToSend: ", dataToSend);
        (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().comptability.cashCollections.createDiscount(dataToSend).then(response => {
          doGet();
          isLoadingForm.value = false;
          actionType.value = "list";
          // Reset values
          discountData.value = {
            label: "",
            cost: null,
            currency: "XAF",
            third: {
              id: null
            },
            products: Array(),
            medias: Array(),
            description: ""
          };
          (0,_state_api_userState__WEBPACK_IMPORTED_MODULE_2__.refreshUserInfos)().then(() => {
            notify.success("Compte mis à jour");
          }).catch(error => {
            notify.error("Echec ! Veuillez Réessayer");
          });
          notify.success(`Ristourne ajouté à la liste`);
        }).catch(error => {
          isLoadingForm.value = false;
          notify.error(error.data.response.message);
        });
      } else {
        isLoadingForm.value = false;
        notify.error("Veuiller remplir tous les champs obligatoires");
      }
    }
    const deleteDiscount = (position, id) => {
      isLoading.value = true;
      (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().comptability.cashCollections.deleteDiscount(id).then(response => {
        notify.success(`Ristourne supprimée`);
        (0,_state_api_userState__WEBPACK_IMPORTED_MODULE_2__.refreshUserInfos)().then(() => {
          notify.success("Stock mis à jour");
        }).catch(error => {
          notify.error("Echec d'actulisation du Stock. Veuillez recharger la page SVP");
        });
        allReceipts.value.splice(position, allReceipts.value.length > -1 ? 1 : position - 1);
        isLoading.value = false;
      }).catch(error => {
        isLoading.value = false;
      });
    };
    function loanOperation() {
      let dataToSend = {};
      Object.assign(dataToSend, loanData.value);
      // dataToSend.medias = medias.value
      dataToSend.third = getSelected(dataToSend.third.id, "creditor");
      const differenceOfPrices = parseInt(dataToSend.total_price) - parseInt(dataToSend.cost);
      const interet = differenceOfPrices / parseInt(dataToSend.total_price) * 100;
      dataToSend.interet = interet.toFixed(2).toString();
      console.log("dataToSend: ", dataToSend);
      (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().comptability.cashCollections.createLoan(dataToSend).then(response => {
        doGet();
        actionType.value = "list";
        loanData.value = {
          name: "",
          email: "",
          contact: null,
          postal_code: "",
          label: "",
          cost: "",
          interet: "",
          total_price: "",
          suffix: "",
          echeances: Array(),
          paymentmethod: {},
          third: {
            id: null
          },
          currency: "XAF",
          medias: Array(),
          description: ""
        };
        (0,_state_api_userState__WEBPACK_IMPORTED_MODULE_2__.refreshUserInfos)().then(() => {
          isLoadingForm.value = false;
          notify.success("Compte mis à jour");
        }).catch(error => {
          notify.error("Echec ! Veuillez Réessayer");
        });
        notify.success(`Emprunt ajouté à la liste`);
      }).catch(error => {
        notify.error("Erreur lors de l'enregistrement de l'enregistrement ! " + error.data.response.message);
        isLoadingForm.value = false;
      });
    }
    function createLoan() {
      isLoadingForm.value = true;
      if (!isNewClient.value && (0,_validator__WEBPACK_IMPORTED_MODULE_10__.validateAll)(validations.value.loanData)) {
        loanOperation();
      } else if (isNewClient.value && (0,_validator__WEBPACK_IMPORTED_MODULE_10__.validateAll)(validations.value.loanNewThird)) {
        loanOperation();
      } else {
        isLoadingForm.value = false;
        notify.error("Veuiller remplir tous les champs obligatoires");
      }
    }
    const deleteLoan = (position, id) => {
      isLoading.value = true;
      (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().comptability.cashCollections.deleteLoan(id).then(response => {
        notify.success(`Emprunt supprimé`);
        (0,_state_api_userState__WEBPACK_IMPORTED_MODULE_2__.refreshUserInfos)().then(() => {
          notify.success("Comptes mis à jour");
        }).catch(error => {
          notify.error("Echec d'actulisation des comptes. Veuillez recharger la page SVP");
        });
        allReceipts.value.splice(position, allReceipts.value.length > -1 ? 1 : position - 1);
        isLoading.value = false;
      }).catch(error => {
        isLoading.value = false;
      });
    };
    const deleteOwnFund = (position, id) => {
      isLoading.value = true;
      (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().comptability.ownFunds.delete(id).then(response => {
        notify.success(`Opération supprimée`);
        (0,_state_api_userState__WEBPACK_IMPORTED_MODULE_2__.refreshUserInfos)().then(() => {
          notify.success("Comptes mis à jour");
        }).catch(error => {
          notify.error("Echec d'actulisation des comptes. Veuillez recharger la page SVP");
        });
        allReceipts.value.splice(position, allReceipts.value.length > -1 ? 1 : position - 1);
        isLoading.value = false;
      }).catch(error => {
        isLoading.value = false;
      });
    };
    function subventionOperation() {
      let dataToSend = {};
      Object.assign(dataToSend, subventionData.value);
      // dataToSend.medias = medias.value;
      dataToSend.third = getSelected(dataToSend.third.id, "funder");
      console.log("dataToSend: ", dataToSend);
      (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().comptability.cashCollections.createSubvention(dataToSend).then(response => {
        doGet();
        actionType.value = "list";
        // Reset values
        subventionData.value = {
          name: "",
          email: "",
          contact: null,
          postal_code: "",
          third: {
            id: null
          },
          label: "",
          cost: null,
          paymentmethod: {},
          medias: Array(),
          currency: "XAF",
          description: ""
        };
        (0,_state_api_userState__WEBPACK_IMPORTED_MODULE_2__.refreshUserInfos)().then(() => {
          isLoadingForm.value = false;
          notify.success("Compte mis à jour");
        }).catch(error => {
          notify.error("Echec ! Veuillez Réessayer");
        });
        notify.success(`Suvention ajouté à la liste`);
      }).catch(error => {
        notify.error("Erreur lors de l'enregistrement de l'enregistrement ! " + error.data.response.message);
        isLoadingForm.value = false;
      });
    }
    function createSubvention() {
      isLoadingForm.value = true;
      if (!isNewClient.value && (0,_validator__WEBPACK_IMPORTED_MODULE_10__.validateAll)(validations.value.subventionData)) {
        subventionOperation();
      } else if (isNewClient.value && (0,_validator__WEBPACK_IMPORTED_MODULE_10__.validateAll)(validations.value.subventionNewThird) && (0,_validator__WEBPACK_IMPORTED_MODULE_10__.validateAll)(validations.value.subventionData)) {
        subventionOperation();
      } else {
        isLoadingForm.value = false;
        notify.error("Veuiller remplir tous les champs obligatoires");
      }
    }
    const deleteSubvention = (position, id) => {
      isLoading.value = true;
      (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().comptability.cashCollections.deleteSubvention(id).then(response => {
        notify.success(`Subvention supprimée avec succès.`);
        (0,_state_api_userState__WEBPACK_IMPORTED_MODULE_2__.refreshUserInfos)().then(() => {
          notify.success("Comptes mis à jour");
        }).catch(error => {
          notify.error("Echec d'actulisation des comptes. Veuillez recharger la page SVP");
        });
        allReceipts.value.splice(position, allReceipts.value.length > -1 ? 1 : position - 1);
        isLoading.value = false;
      }).catch(error => {
        isLoading.value = false;
      });
    };
    async function sendBillQuote(data) {
      console.log("data: ", data);
      const galeries = (0,vue__WEBPACK_IMPORTED_MODULE_1__.ref)([]);
      const result = selectedClaim.value.real_reference.split(pattern);
      const dataQuote = new Map();
      dataQuote.set("reference_system", result[0] ? result[0] : "DV_unknow");
      dataQuote.set("start", result[1] ? result[1] : "Non Défini");
      dataQuote.set("end", result[2] ? result[2] : "Non Défini");
      // Generate de binary of Quote
      const blob = await (0,_composable_useFacturation__WEBPACK_IMPORTED_MODULE_7__.outputBlobPDF)(data, "Facture-Devis-" + dataQuote.get("reference_system") + "-", lodash__WEBPACK_IMPORTED_MODULE_8__.isEmpty(data.items) ? "Services" : "Produits");
      console.log("blob: ", blob);
      try {
        // console.log('blob.filename: ', blob.filename);
        const result = await (0,_composable_useSendFile__WEBPACK_IMPORTED_MODULE_5__.uploadPDFwithBLOB)(blob.data, blob.filename);
        if (lodash__WEBPACK_IMPORTED_MODULE_8__.isEmpty(result)) {
          throw "Une erreur c'est produite lors de la sauvegarde de la pièce jointe.";
        } else {
          galeries.value.push(result);
        }
      } catch (error) {
        console.log("Une erreur c'est produite: ", error);
        notify.error("Une erreur c'est produite lors de la sauvegarde de la pièce jointe.");
      }
      const dataToSend_ = (0,_composable_useFacturation__WEBPACK_IMPORTED_MODULE_7__.getTemplateMail)(dataQuote.get("reference_system"), selectedClaim.value.third.email, galeries.value);
      await (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().gadgets.mail.send(dataToSend_).then(resp => {
        isLoading.value = false;
        notify.success("Votre client a été notifié via un courrier.");
      }).catch(err => {
        console.log("Une erreur c'est produite lors de l'envoi du courrier: ", err);
        isLoading.value = false;
        notify.success("Une erreur c'est produite lors de l'envoi du courrier.");
      });
    }
    function isClaimAboutQuote(realRef) {
      const regex = new RegExp(pattern);
      return regex.test(realRef);
    }
    async function refundClaim(data) {
      // console.log('SelectedClaim: ', data);
      isLoadingForm.value = true;
      if ((0,_validator__WEBPACK_IMPORTED_MODULE_10__.validateAll)(validations.value.claimData)) {
        let dataToSend = {};
        Object.assign(dataToSend, claimData.value);
        // dataToSend.medias = medias.value;
        // console.log("dataToSend: ", dataToSend);
        await (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().comptability.cashCollections.refundClaims(dataToSend).then(response => {
          notify.success(`Créance réglée au montant de ${response.data.cost}`);
          data.cash_pay += parseFloat(response.data.cost);
          data.creance -= parseFloat(response.data.cost);
          console.log("data: ", data);
          // Reset values
          claimData.value = {
            cash_pay: null,
            label: "",
            paymentmethod: {},
            transcation: {},
            medias: Array()
          };
          // Send Bill if the claim is instance of QUOTE OPERATION
          if (isClaimAboutQuote(selectedClaim.value.real_reference)) sendBillQuote(data);
          (0,_state_api_userState__WEBPACK_IMPORTED_MODULE_2__.refreshUserInfos)().then(() => {
            doGet();
            notify.success("Comptes mis à jour");
          }).catch(error => {
            notify.error("Echec ! Veuillez Réessayer");
          });
          isLoadingForm.value = false;
          actionType.value = "list";
        }).catch(error => {
          isLoadingForm.value = false;
          notify.error("Erreur lors de la validation: " + error.data.response.message);
          isLoadingForm.value = false;
        });
      } else {
        isLoadingForm.value = false;
        notify.error("Veuiller remplir tous les champs obligatoires");
      }
    }
    async function createOwnFund() {
      // console.log('SelectedClaim: ', data);
      isLoadingForm.value = true;
      if ((0,_validator__WEBPACK_IMPORTED_MODULE_10__.validateAll)(validations.value.ownFundData)) {
        let dataToSend = {};
        Object.assign(dataToSend, ownFundData.value);
        // dataToSend.medias = medias.value;
        // console.log("dataToSend: ", dataToSend);
        await (0,_api__WEBPACK_IMPORTED_MODULE_17__["default"])().comptability.ownFunds.create(dataToSend).then(response => {
          notify.success(`Fonds Propres encaissés au montant de ${response.data.cost}`);
          // Reset values
          ownFundData.value = {
            cost: null,
            email: "",
            contact: null,
            name: "",
            label: "",
            paymentmethod: {},
            start: ""
          };
          (0,_state_api_userState__WEBPACK_IMPORTED_MODULE_2__.refreshUserInfos)().then(() => {
            doGet();
            notify.success("Comptes mis à jour");
          }).catch(error => {
            notify.error("Echec de mise à des données de Comptes ! Veuillez recharger la page SVP.");
          });
          isLoadingForm.value = false;
          actionType.value = "list";
        }).catch(error => {
          isLoadingForm.value = false;
          notify.error("Erreur lors de l'enregistrement de l'opération: " + error.data.response.message);
          isLoadingForm.value = false;
        });
      } else {
        isLoadingForm.value = false;
        notify.error("Veuiller remplir tous les champs obligatoires");
      }
    }
    function doGet(filter, page, enableLoader = true) {
      switch (activeFilter.value) {
        case _constants_operations__WEBPACK_IMPORTED_MODULE_15__.RISTOURNES:
          getDiscounts(filter, page, enableLoader);
          break;
        case _constants_operations__WEBPACK_IMPORTED_MODULE_15__.SUBVENTIONS_EXPLOITATION:
          getSubventions(filter, page, enableLoader);
          break;
        case _constants_operations__WEBPACK_IMPORTED_MODULE_15__.EMPRUNTS:
          getLoans(filter, page, enableLoader);
          break;
        case _constants_operations__WEBPACK_IMPORTED_MODULE_15__.CREANCES:
          getClaims(filter, page, enableLoader);
          break;
        case _constants_operations__WEBPACK_IMPORTED_MODULE_15__.FONDS_PROPRES:
          getOwnFund(filter, page, enableLoader);
          break;
        default:
          break;
      }
    }
    function doDelete(position, id) {
      switch (activeFilter.value) {
        case _constants_operations__WEBPACK_IMPORTED_MODULE_15__.RISTOURNES:
          deleteDiscount(position, id);
          break;
        case _constants_operations__WEBPACK_IMPORTED_MODULE_15__.SUBVENTIONS_EXPLOITATION:
          deleteSubvention(position, id);
          break;
        case _constants_operations__WEBPACK_IMPORTED_MODULE_15__.EMPRUNTS:
          deleteLoan(position, id);
          break;
        case _constants_operations__WEBPACK_IMPORTED_MODULE_15__.FONDS_PROPRES:
          deleteOwnFund(position, id);
          break;
        default:
          break;
      }
    }
    function doCreate() {
      switch (activeFilter.value) {
        case _constants_operations__WEBPACK_IMPORTED_MODULE_15__.RISTOURNES:
          createDiscount();
          break;
        case _constants_operations__WEBPACK_IMPORTED_MODULE_15__.SUBVENTIONS_EXPLOITATION:
          createSubvention();
          break;
        case _constants_operations__WEBPACK_IMPORTED_MODULE_15__.EMPRUNTS:
          createLoan();
          break;
        case _constants_operations__WEBPACK_IMPORTED_MODULE_15__.CREANCES:
          refundClaim(selectedClaim.value);
          break;
        case _constants_operations__WEBPACK_IMPORTED_MODULE_15__.FONDS_PROPRES:
          createOwnFund();
          break;
        default:
          break;
      }
    }
    function getExportedData() {
      let data;
      switch (activeFilter.value) {
        case _constants_operations__WEBPACK_IMPORTED_MODULE_15__.RISTOURNES:
          // console.log("discountData.value: ", allReceipts.value);
          data = (0,_composable_useExport__WEBPACK_IMPORTED_MODULE_9__.exportDiscountsDataOps)(allReceipts.value);
          break;
        case _constants_operations__WEBPACK_IMPORTED_MODULE_15__.EMPRUNTS:
          // console.log("loans.value: ", allReceipts.value);
          data = (0,_composable_useExport__WEBPACK_IMPORTED_MODULE_9__.exportLoansDataOps)(allReceipts.value);
          break;
        case _constants_operations__WEBPACK_IMPORTED_MODULE_15__.SUBVENTIONS_EXPLOITATION:
          // console.log("subventions.value: ", allReceipts.value);
          data = (0,_composable_useExport__WEBPACK_IMPORTED_MODULE_9__.exportSubventionsDataOps)(allReceipts.value);
          break;
        case _constants_operations__WEBPACK_IMPORTED_MODULE_15__.CREANCES:
          // console.log("claims.value: ", allReceipts.value);
          data = (0,_composable_useExport__WEBPACK_IMPORTED_MODULE_9__.exportClaimsDataOps)(allReceipts.value);
          break;
        case _constants_operations__WEBPACK_IMPORTED_MODULE_15__.FONDS_PROPRES:
          // console.log("claims.value: ", allReceipts.value);
          data = (0,_composable_useExport__WEBPACK_IMPORTED_MODULE_9__.exportOwnFundDataOps)(allReceipts.value);
          break;
        default:
          break;
      }
      return data;
    }
    // User permissions
    const permissionsRefundCreance = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_11__["default"])("reglement_creance").map(element => {
      return element.action;
    });
    /**
     * Providers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("permissions", permissions);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("permissionsRefundCreance", permissionsRefundCreance);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("updateSubView", updateSubView);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("activeFilter", activeFilter);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("isLoading", isLoading);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("isRetrievedData", isRetrievedData);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("isLoadingForm", isLoadingForm);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("updateActiveFilter", updateActiveFilter);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("paymentMethods", paymentMethods);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("allReceipts", allReceipts);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("productsList", productsList);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("fundersList", fundersList);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("providersList", providersList);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("clientsList", clientsList);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("creditorsList", creditorsList);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("claimsList", claimsList);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("isNewClient", isNewClient);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("selectedEcheance", selectedEcheance);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("selectedProduct", selectedProduct);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("selectedClaim", selectedClaim);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("deleteEcheance", deleteEcheance);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("deleteProducts", deleteProducts);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("discountData", discountData);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("loanData", loanData);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("ownFundData", ownFundData);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("subventionData", subventionData);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("claimData", claimData);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("doCreate", doCreate);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("getSelected", getSelected);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("openRefundClaim", openRefundClaim);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("validations", validations);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("newClientValidations", newClientValidations);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("thirdValidation", thirdValidation);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("metaDataAllReceipts", metaDataAllReceipts);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("metaDataAllFunders", metaDataAllFunders);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("metaDataAllCreditors", metaDataAllCreditors);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("isClaimAboutQuote", isClaimAboutQuote);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("selectedClaim", selectedClaim);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("pattern", pattern);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("doGet", doGet);
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.provide)("doDelete", doDelete);
    /**
     * Page Lifecycle
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_1__.onBeforeMount)(() => {
      // Use Permissions
      permissions.value = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_11__["default"])(activeFilter.value == _constants_operations__WEBPACK_IMPORTED_MODULE_15__.FONDS_PROPRES || activeFilter.value == _constants_operations__WEBPACK_IMPORTED_MODULE_15__.SUBVENTIONS_EXPLOITATION ? activeFilter.value == _constants_operations__WEBPACK_IMPORTED_MODULE_15__.FONDS_PROPRES ? "fond_propre" : "subventions" : activeFilter.value).map(element => {
        return element.action;
      });
      // console.log("permissions.value: ", permissions.value);
      // Load datas
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_12__["default"])("read", permissions.value)) {
        doGet();
        getAllProductsAndProviders();
      }
      // Load payment methods
      getPaymentMethods(paymentMethods, "credit");
      discountData.value.start = new Date("yyyy-MM-dd");
      ownFundData.value.start = new Date("yyyy-MM-dd").toString();
    });
    const __returned__ = {
      updateAppHeaderAction,
      getPaymentMethods,
      platform,
      pattern,
      router,
      route,
      notify,
      actionType,
      activeFilter,
      dateDebut,
      dateFin,
      metaDataAllReceipts,
      metaDataAllFunders,
      metaDataAllCreditors,
      metaDataAllProducts,
      metaDataAllProviders,
      isNewClient,
      isRetrievedData,
      paymentMethods,
      allReceipts,
      allProducts,
      productsList,
      allFunders,
      fundersList,
      allProviders,
      providersList,
      allClients,
      clientsList,
      allCreditors,
      creditorsList,
      claimsList,
      selectedEcheance,
      selectedProduct,
      selectedClaim,
      validations,
      newClientValidations,
      thirdValidation,
      discountData,
      loanData,
      subventionData,
      ownFundData,
      claimData,
      isFetching,
      isLoading,
      isLoadingForm,
      permissions,
      updateSubView,
      openRefundClaim,
      updateActiveFilter,
      deleteEcheance,
      deleteProducts,
      getAllProductsAndProviders,
      getDiscounts,
      getLoans,
      getSubventions,
      getClaims,
      getOwnFund,
      getSelected,
      createDiscount,
      deleteDiscount,
      loanOperation,
      createLoan,
      deleteLoan,
      deleteOwnFund,
      subventionOperation,
      createSubvention,
      deleteSubvention,
      sendBillQuote,
      isClaimAboutQuote,
      refundClaim,
      createOwnFund,
      doGet,
      doDelete,
      doCreate,
      getExportedData,
      permissionsRefundCreance,
      get isOwnBranch() {
        return _state_api_userState__WEBPACK_IMPORTED_MODULE_2__.isOwnBranch;
      },
      get filename() {
        return _composable_useExport__WEBPACK_IMPORTED_MODULE_9__.filename;
      },
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_12__["default"];
      },
      get formatDate() {
        return _utils__WEBPACK_IMPORTED_MODULE_13__.formatDate;
      },
      get formatRefSystem() {
        return _utils__WEBPACK_IMPORTED_MODULE_13__.formatRefSystem;
      },
      get numeral() {
        return (numeral__WEBPACK_IMPORTED_MODULE_14___default());
      },
      get SUBVENTIONS_EXPLOITATION() {
        return _constants_operations__WEBPACK_IMPORTED_MODULE_15__.SUBVENTIONS_EXPLOITATION;
      },
      get RISTOURNES() {
        return _constants_operations__WEBPACK_IMPORTED_MODULE_15__.RISTOURNES;
      },
      get FONDS_PROPRES() {
        return _constants_operations__WEBPACK_IMPORTED_MODULE_15__.FONDS_PROPRES;
      },
      get EMPRUNTS() {
        return _constants_operations__WEBPACK_IMPORTED_MODULE_15__.EMPRUNTS;
      },
      get CREANCES() {
        return _constants_operations__WEBPACK_IMPORTED_MODULE_15__.CREANCES;
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingRecordingExpense.vue?vue&type=script&setup=true&lang=ts":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingRecordingExpense.vue?vue&type=script&setup=true&lang=ts ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var vue_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! vue-router */ "./node_modules/vue-router/dist/vue-router.mjs");
/* harmony import */ var _composable_useExport__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/composable/useExport */ "./src/composable/useExport.ts");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/api */ "./src/api/index.ts");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/utils */ "./src/utils/index.ts");
/* harmony import */ var numeral__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! numeral */ "./node_modules/numeral/numeral.js");
/* harmony import */ var numeral__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(numeral__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");
/* harmony import */ var _composable_useMediasLoader__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @/composable/useMediasLoader */ "./src/composable/useMediasLoader.ts");
/* harmony import */ var _composable_useNotyf__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @/composable/useNotyf */ "./src/composable/useNotyf.ts");













/**
 * Composables
 */

/**
 * Injects
 */
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'accountingRecordingExpense',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_10__.u)({
      title: "Comptabilité / Dépenses"
    });
    const platform = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("platform");
    const updateAppHeaderAction = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("updateAppHeaderAction");
    const newSpent = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)({
      label: "",
      start: "",
      description: null,
      currency: "XAF",
      cost: null,
      cash_pay: null,
      paymentmethod: {
        id: null
      },
      typesdepense: {
        id: null
      },
      sousdepense: {
        id: null
      },
      medias: [],
      third: {
        id: 0
      }
    });
    // Validation
    const validations = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)();
    const notify = (0,_composable_useNotyf__WEBPACK_IMPORTED_MODULE_9__["default"])();
    // Pagination
    const metaDataSpents = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)();
    /**
     * Variables
     */
    const router = (0,vue_router__WEBPACK_IMPORTED_MODULE_11__.useRouter)();
    const route = (0,vue_router__WEBPACK_IMPORTED_MODULE_11__.useRoute)();
    const activeView = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("list");
    const activeFilter = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const spents = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)();
    const isFetching = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    const isLoading = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    const dateDebut = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const dateFin = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    /**
     * Watchers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_1__.userCurrentBranch.value.id, async newValue => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_7__["default"])("read", permissions)) {
        await getAllRecordingExpenses();
      }
    });
    /**
     * Functions
     */
    const updateSubView = action => {
      actionType.value = action;
    };
    /**
     * functions
     */
    const getAllRecordingExpenses = async (filter, page, enableLoader = true) => {
      if (enableLoader) isFetching.value = true;
      if (filter != null) {
        await (0,_api__WEBPACK_IMPORTED_MODULE_3__["default"])().comptability.spents.getAll(filter).then(response => {
          metaDataSpents.value = response.data;
          spents.value = response.data.data;
          isFetching.value = false;
        }).catch();
      } else {
        if (page != null) {
          await (0,_api__WEBPACK_IMPORTED_MODULE_3__["default"])().comptability.spents.getAll(null, page).then(response => {
            metaDataSpents.value = response.data;
            spents.value = response.data.data;
            isFetching.value = false;
          }).catch();
        } else {
          await (0,_api__WEBPACK_IMPORTED_MODULE_3__["default"])().comptability.spents.getAll().then(response => {
            metaDataSpents.value = response.data;
            spents.value = response.data.data;
            isFetching.value = false;
          }).catch();
        }
      }
    };
    function deleteRexp(position, id) {
      isLoading.value = true;
      (0,_api__WEBPACK_IMPORTED_MODULE_3__["default"])().comptability.spents.delete(id).then(response => {
        spents.value.splice(position, spents.value.length > -1 ? 1 : position - 1);
        notify.success("Opération supprimée avec succès.");
        (0,_state_api_userState__WEBPACK_IMPORTED_MODULE_1__.refreshUserInfos)().then(() => {
          notify.success("Comptes mis à jour");
        }).catch(error => {
          notify.error("Nous n'avons pas pu rafraichir les données de vos comptes. Rechargez la page SVP !");
        });
        isLoading.value = false;
      }).catch(err => {
        notify.error("Une erreur c'est produite lors de la suppression de l'opération.");
        console.log(err);
        isLoading.value = false;
      });
    }
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_6__["default"])("depenses").map(element => {
      return element.action;
    });
    /**
     * Providers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissions", permissions);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("updateSubView", updateSubView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeView", activeView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeFilter", activeFilter);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("isLoading", isLoading);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("newSpent", newSpent);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("spents", spents);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("metaDataSpents", metaDataSpents);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("getAllRecordingExpenses", getAllRecordingExpenses);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("deleteRexp", deleteRexp);
    // Providing Validations
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("validations", validations);
    /**
     * Page Lifecycle
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onBeforeMount)(async () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_7__["default"])("read", permissions)) {
        await getAllRecordingExpenses();
      }
    });
    const __returned__ = {
      platform,
      updateAppHeaderAction,
      newSpent,
      validations,
      notify,
      metaDataSpents,
      router,
      route,
      activeView,
      actionType,
      activeFilter,
      spents,
      isFetching,
      isLoading,
      dateDebut,
      dateFin,
      updateSubView,
      getAllRecordingExpenses,
      deleteRexp,
      permissions,
      get isOwnBranch() {
        return _state_api_userState__WEBPACK_IMPORTED_MODULE_1__.isOwnBranch;
      },
      get exportRecordingExpense() {
        return _composable_useExport__WEBPACK_IMPORTED_MODULE_2__.exportRecordingExpense;
      },
      get filename() {
        return _composable_useExport__WEBPACK_IMPORTED_MODULE_2__.filename;
      },
      get formatDate() {
        return _utils__WEBPACK_IMPORTED_MODULE_4__.formatDate;
      },
      get formatRefSystem() {
        return _utils__WEBPACK_IMPORTED_MODULE_4__.formatRefSystem;
      },
      get numeral() {
        return (numeral__WEBPACK_IMPORTED_MODULE_5___default());
      },
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_7__["default"];
      },
      get nameReferImage() {
        return _composable_useMediasLoader__WEBPACK_IMPORTED_MODULE_8__.nameReferImage;
      },
      get nameReferFile() {
        return _composable_useMediasLoader__WEBPACK_IMPORTED_MODULE_8__.nameReferFile;
      },
      get setURL() {
        return _composable_useMediasLoader__WEBPACK_IMPORTED_MODULE_8__.setURL;
      },
      get previewMetaData() {
        return _composable_useMediasLoader__WEBPACK_IMPORTED_MODULE_8__.previewMetaData;
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingSendEmail.vue?vue&type=script&setup=true&lang=ts":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingSendEmail.vue?vue&type=script&setup=true&lang=ts ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");




// User permissions
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'accountingSendEmail',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_1__["default"])("send_email").map(element => {
      return element.action;
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissions", permissions);
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_2__.u)({
      title: "Comptabilité / Envoyer un E-mail"
    });
    const __returned__ = {
      permissions
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingWallets.vue?vue&type=script&setup=true&lang=ts":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingWallets.vue?vue&type=script&setup=true&lang=ts ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var vue_router__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! vue-router */ "./node_modules/vue-router/dist/vue-router.mjs");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _validator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/validator */ "./src/validator/index.ts");
/* harmony import */ var _composable_useExport__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/composable/useExport */ "./src/composable/useExport.ts");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/utils */ "./src/utils/index.ts");
/* harmony import */ var numeral__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! numeral */ "./node_modules/numeral/numeral.js");
/* harmony import */ var numeral__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(numeral__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _constants_operations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/constants/operations */ "./src/constants/operations/index.ts");
/* harmony import */ var _constants_pages_accountings_wallets__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @/constants/pages/accountings/wallets */ "./src/constants/pages/accountings/wallets/index.ts");
/* harmony import */ var _composable_useNotyf__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @/composable/useNotyf */ "./src/composable/useNotyf.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");
/* harmony import */ var _composable_useMediasLoader__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @/composable/useMediasLoader */ "./src/composable/useMediasLoader.ts");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @/api */ "./src/api/index.ts");












/**
 * Composables
 */




/**
 * API Calls
 */


/**
 * Injects
 */
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'accountingWallets',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_13__.u)({
      title: "Comptabilité / Porte-feuille"
    });
    const platform = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("platform");
    const updateAppHeaderAction = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("updateAppHeaderAction");
    const activeWorkspace = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("activeWorkspace");
    const getPaymentMethods = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("getPaymentMethods");
    const getSelectedPaymentMethod = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("getSelectedPaymentMethod");
    const activePage = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("activePage");
    const userStats = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("userStats");
    const validations = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)();
    /**
     * Variables
     */
    const router = (0,vue_router__WEBPACK_IMPORTED_MODULE_14__.useRouter)();
    const route = (0,vue_router__WEBPACK_IMPORTED_MODULE_14__.useRoute)();
    const notify = (0,_composable_useNotyf__WEBPACK_IMPORTED_MODULE_8__["default"])();
    const activeView = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(1);
    const actionType = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("list");
    const activeFilter = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(route.params.page);
    const viewAccountDetails = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    const allData = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([]);
    const metaData = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)({});
    const isFetching = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    const dateDebut = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const dateFin = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    // Datas
    let parsedPayments = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([]);
    const cost = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const selectedPaymentMethod = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const description = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const reference = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    const attachment = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([]);
    const paymentMethods = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("[]");
    const displayPayments = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(true);
    const date = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("");
    let medias = [];
    const breadcrumbs = (0,vue__WEBPACK_IMPORTED_MODULE_0__.computed)(() => {
      return route.params.page != "chifre_affaire" && route.params.page != "dette" && route.params.page != "creance" ? [route.params.page, _constants_pages_accountings_wallets__WEBPACK_IMPORTED_MODULE_7__.HISTORIQUES, _constants_pages_accountings_wallets__WEBPACK_IMPORTED_MODULE_7__.OPERATIONS /* OPERATIONS_IN*/] : [];
    });
    // Loaders
    const isLoading = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    const isLoadingAttachment = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(false);
    /**
     * Watchers
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_1__.userCurrentBranch.value.id, newValue => {
      // Load payment methods
      getPaymentMethods(paymentMethods).then(() => {
        parsedPayments.value = JSON.parse(paymentMethods.value).filter(item => item.value != "credit");
        getPaymentMethodsByParams();
      });
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => activeFilter.value, async () => {
      await doGet();
    });
    // Watchers
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(attachment, async (newValue, oldValue) => {
      if (newValue) {
        (0,_composable_useMediasLoader__WEBPACK_IMPORTED_MODULE_11__.sendMedias)(newValue, medias, isLoadingAttachment);
      } else {
        console.log("Error when retriving Files");
        notify.error("Erreur lors du traitement de la pièce jointe. Veuillez réessayer.");
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(isLoadingAttachment, () => {
      isLoading.value = isLoadingAttachment.value;
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => route.path, newValue => {
      displayPayments.value = false;
      setTimeout(() => {
        getPaymentMethodsByParams();
        displayPayments.value = true;
      }, 200);
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => route.params.page, () => {
      activeFilter.value = route.params.page;
    });
    /**
     * Functions
     */
    const updateSubView = action => {
      actionType.value = action;
    };
    const resetFields = () => {
      cost.value = "";
      description.value = "";
      reference.value = "";
      attachment.value = [];
      date.value = "";
      medias = [];
    };
    const resetMetaData = () => {
      metaData.value = {};
      allData.value = [];
    };
    function initializeOthers() {
      isLoading.value = true;
      if (cost.value != "" && description.value != "" && reference.value != "" && date.value != "") {
        const dataToSend = {
          cost: cost.value,
          currency: "XAF",
          reference: reference.value,
          date: date.value,
          medias: medias,
          paymentmethod: {
            id: userStats.value[route.params.page.toString()].id
          }
          /* medias: [
              {
                  "file_id": 1,
                  "label": "FACTURE00012",
                  "file": "facture.png",
                  "description": " "
              }
          ] */
        };
        // console.log("dataToSend: ", dataToSend);
        // console.log("userStats: ", userStats.value);
        // console.log("userStats param: ", userStats.value[route.params.page.toString()]);
        switch (activePage.value?.params.page) {
          case "creance":
            (0,_api__WEBPACK_IMPORTED_MODULE_12__["default"])().accounting.initCreances(dataToSend).then(resp => {
              (0,_state_api_userState__WEBPACK_IMPORTED_MODULE_1__.refreshUserInfos)().then(() => {
                isLoading.value = false;
                notify.success("Compte initialisé avec succès");
                // Reset values
                cost.value = "";
                selectedPaymentMethod.value = "";
                description.value = "";
                reference.value = "";
                date.value = "";
              }).catch(error => {
                isLoading.value = false;
                notify.error("Nous n'avons pas pu rafraichir les infos. Rechargez la page ou contactez nous si le problème persiste");
              });
            }).catch(error => {
              isLoading.value = false;
              notify.error(error.data.response.message);
            });
            break;
          case "dette":
            (0,_api__WEBPACK_IMPORTED_MODULE_12__["default"])().accounting.initDette(dataToSend).then(resp => {
              (0,_state_api_userState__WEBPACK_IMPORTED_MODULE_1__.refreshUserInfos)().then(() => {
                isLoading.value = false;
                notify.success("Compte initialisé avec succès");
                // Reset values
                cost.value = "";
                selectedPaymentMethod.value = "";
                description.value = "";
                date.value = "";
                reference.value = "";
              }).catch(error => {
                isLoading.value = false;
                notify.error("Nous n'avons pas pu rafraichir les infos. Rechargez la page ou contactez nous si le problème persiste");
              });
            }).catch(error => {
              isLoading.value = false;
              notify.error(error.data.response.message);
            });
            break;
          case "chifre_affaire":
            (0,_api__WEBPACK_IMPORTED_MODULE_12__["default"])().accounting.initChiffreAffaire(dataToSend).then(resp => {
              (0,_state_api_userState__WEBPACK_IMPORTED_MODULE_1__.refreshUserInfos)().then(() => {
                isLoading.value = false;
                notify.success("Compte initialisé avec succès");
                // Reset values
                cost.value = "";
                selectedPaymentMethod.value = "";
                description.value = "";
                date.value = "";
                reference.value = "";
              }).catch(error => {
                isLoading.value = false;
                notify.error("Nous n'avons pas pu rafraichir les infos. Rechargez la page ou contactez nous si le problème persiste");
              });
            }).catch(error => {
              isLoading.value = false;
              notify.error(error.data.response.message);
            });
            break;
          default:
            break;
        }
        updateSubView("list");
      } else {
        isLoading.value = false;
        notify.warning("Remplissez le formulaire");
      }
    }
    function initializeAccount() {
      isLoading.value = true;
      if (cost.value != "" && description.value != "" && reference.value != "" && date.value != "") {
        const dataToSend = {
          cost: cost.value,
          currency: "XAF",
          reference: reference.value,
          medias: medias,
          date: date.value,
          paymentmethod: {
            id: userStats.value[route.params.page.toString()].id
          }
        };
        // console.log("dataToSend: ", dataToSend);
        // console.log("userStats: ", userStats.value);
        // console.log("userStats param: ", userStats.value[route.params.page.toString()]);
        (0,_api__WEBPACK_IMPORTED_MODULE_12__["default"])().accounting.initAccount(dataToSend).then(data => {
          (0,_state_api_userState__WEBPACK_IMPORTED_MODULE_1__.refreshUserInfos)().then(() => {
            isLoading.value = false;
            notify.success("Compte initialisé avec succès");
            // Reset values
            resetFields();
            updateSubView("list");
          }).catch(error => {
            isLoading.value = false;
            notify.error("Nous n'avons pas pu rafraichir les infos. Rechargez la page ou contactez nous si le problème persiste");
          });
        }).catch(error => {
          isLoading.value = false;
          notify.error(error.data.response.message);
        });
      } else {
        isLoading.value = false;
        notify.warning("Veuillez remplir tous les champs");
      }
    }
    function fundingAccount() {
      isLoading.value = true;
      const paymentMethodDetails = getSelectedPaymentMethod(paymentMethods, selectedPaymentMethod.value);
      if ((0,_validator__WEBPACK_IMPORTED_MODULE_2__.validateAll)(validations.value)) {
        const dataToSend = {
          cost: cost.value,
          ref: reference.value,
          medias: medias,
          label: description.value,
          currency: "XAF",
          source: {
            id: selectedPaymentMethod.value.id
          },
          destination: {
            id: userStats.value[route.params.page.toString()].id
          },
          start: date.value
        };
        (0,_api__WEBPACK_IMPORTED_MODULE_12__["default"])().accounting.fundingAccount(dataToSend).then(data => {
          (0,_state_api_userState__WEBPACK_IMPORTED_MODULE_1__.refreshUserInfos)().then(() => {
            isLoading.value = false;
            notify.success("Votre compte a été approvionné avec succès");
            // Reset values
            resetFields();
            console.log("reset 11");
          }).catch(error => {
            isLoading.value = false;
            notify.error("Nous n'avons pas pu rafraichir les infos. Rechargez la page ou contactez nous si le problème persiste");
          });
        }).catch(error => {
          isLoading.value = false;
          notify.error(error.data.response.message);
        });
      } else {
        isLoading.value = false;
        notify.warning("Remplissez le formulaire");
      }
    }
    function getPaymentMethodsByParams() {
      const excludeList = parsedPayments.value.filter(item => item.ref != route.params.page);
      paymentMethods.value = JSON.stringify(excludeList);
    }
    const doGet = async (filter, page, enableLoader = true) => {
      if (enableLoader) isFetching.value = true;
      resetMetaData();
      switch (activeFilter.value) {
        case _constants_pages_accountings_wallets__WEBPACK_IMPORTED_MODULE_7__.HISTORIQUES:
          if (filter != null) {
            await (0,_api__WEBPACK_IMPORTED_MODULE_12__["default"])().comptability.collection.history(route.params.page, filter).then(response => {
              metaData.value = response.data;
              allData.value = response.data.data;
            }).catch(err => {
              notify.error(`Erreur lors du chargement des données. Veuillez recharger la page` + err.data ? err.data.response.message : 0);
              console.log(err.response);
            });
          } else {
            if (page != null) {
              await (0,_api__WEBPACK_IMPORTED_MODULE_12__["default"])().comptability.collection.history(route.params.page, null, page).then(response => {
                metaData.value = response.data;
                allData.value = response.data.data;
              }).catch(err => {
                notify.error(`Erreur lors du chargement des données. Veuillez recharger la page` + err.data ? err.data.response.message : 0);
                console.log(err.response);
              });
            } else {
              await (0,_api__WEBPACK_IMPORTED_MODULE_12__["default"])().comptability.collection.history(route.params.page).then(response => {
                metaData.value = response.data;
                allData.value = response.data.data;
              }).catch(err => {
                notify.error(`Erreur lors du chargement des données. Veuillez recharger la page` + err.data ? err.data.response.message : 0);
                console.log(err.response);
              });
            }
          }
          break;
        case _constants_pages_accountings_wallets__WEBPACK_IMPORTED_MODULE_7__.OPERATIONS:
          if (filter != null) {
            await (0,_api__WEBPACK_IMPORTED_MODULE_12__["default"])().comptability.collection.wallet(route.params.page, filter).then(response => {
              metaData.value = response.data;
              allData.value = response.data.data;
            }).catch(err => {
              notify.error(`Erreur lors du chargement des données. Veuillez recharger la page` + err.data ? err.data.response.message : 0);
              console.log(err.response);
            });
          } else {
            if (page != null) {
              await (0,_api__WEBPACK_IMPORTED_MODULE_12__["default"])().comptability.collection.wallet(route.params.page, null, page).then(response => {
                metaData.value = response.data;
                allData.value = response.data.data;
              }).catch(err => {
                notify.error(`Erreur lors du chargement des données. Veuillez recharger la page` + err.data ? err.data.response.message : 0);
                console.log(err.response);
              });
            } else {
              await (0,_api__WEBPACK_IMPORTED_MODULE_12__["default"])().comptability.collection.wallet(route.params.page).then(response => {
                metaData.value = response.data;
                allData.value = response.data.data;
              }).catch(err => {
                notify.error(`Erreur lors du chargement des données. Veuillez recharger la page` + (err.data ? err.data.response.message : ""));
                console.log(err.response);
              });
            }
          }
          break;
        case _constants_pages_accountings_wallets__WEBPACK_IMPORTED_MODULE_7__.OPERATIONS_IN:
          if (filter != null) {
            await (0,_api__WEBPACK_IMPORTED_MODULE_12__["default"])().comptability.collection.wallet(route.params.page, filter, null, true, "in").then(response => {
              metaData.value = response.data;
              allData.value = response.data.data;
            }).catch(err => {
              notify.error(`Erreur lors du chargement des données. Veuillez recharger la page` + err.data ? err.data.response.message : 0);
              console.log(err.response);
            });
          } else {
            if (page != null) {
              await (0,_api__WEBPACK_IMPORTED_MODULE_12__["default"])().comptability.collection.wallet(route.params.page, null, page, true, "in").then(response => {
                metaData.value = response.data;
                allData.value = response.data.data;
              }).catch(err => {
                notify.error(`Erreur lors du chargement des données. Veuillez recharger la page` + err.data ? err.data.response.message : 0);
                console.log(err.response);
              });
            } else {
              await (0,_api__WEBPACK_IMPORTED_MODULE_12__["default"])().comptability.collection.wallet(route.params.page, null, null, true, "in").then(response => {
                metaData.value = response.data;
                allData.value = response.data.data;
              }).catch(err => {
                notify.error(`Erreur lors du chargement des données. Veuillez recharger la page` + (err.data ? err.data.response.message : ""));
                console.log(err.response);
              });
            }
          }
          break;
      }
      if (enableLoader) isFetching.value = false;
    };
    function dynamicTitle() {
      switch (activeFilter.value) {
        case _constants_pages_accountings_wallets__WEBPACK_IMPORTED_MODULE_7__.HISTORIQUES:
          if (route.params.page == "caisse") {
            return "Historiques des approvisionnements de la caisse";
          } else if (route.params.page == "banque") {
            return "Historiques des approvisionnements de la banque";
          } else if (route.params.page == "electronique") {
            return "Historiques des approvisionnements du portefeuille électronique";
          }
          break;
        case _constants_pages_accountings_wallets__WEBPACK_IMPORTED_MODULE_7__.OPERATIONS:
          if (route.params.page == "caisse") {
            return "Toutes les opérations d'entrées/sorties sur la caisse";
          } else if (route.params.page == "banque") {
            return "Toutes les opérations d'entrées/sorties sur la banque";
          } else if (route.params.page == "electronique") {
            return "Toutes les opérations d'entrées/sorties sur le portefeuille électronique";
          }
          break;
        case _constants_pages_accountings_wallets__WEBPACK_IMPORTED_MODULE_7__.OPERATIONS_IN:
          if (route.params.page == "caisse") {
            return "Toutes les opérations d'entrées/sorties sur la caisse";
          } else if (route.params.page == "banque") {
            return "Toutes les opérations d'entrées/sorties sur la banque";
          } else if (route.params.page == "electronique") {
            return "Toutes les opérations d'entrées/sorties sur le portefeuille électronique";
          }
          break;
      }
    }
    // User permissions
    const permissions = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_9__["default"])("approvisionnement").map(element => {
      return element.action;
    });
    // User permissions
    const permissionsInitAccount = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_9__["default"])("initaccounts").map(element => {
      return element.action;
    });
    const permissionsInitCreances = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_9__["default"])("initcreances").map(element => {
      return element.action;
    });
    const permissionsInitChiffreAffaires = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_9__["default"])("initchiffreAffaires").map(element => {
      return element.action;
    });
    const permissionsInitDettes = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_9__["default"])("initdettes").map(element => {
      return element.action;
    });
    const permissionsCash = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_9__["default"])("caisses").map(element => {
      return element.action;
    });
    const permissionsBank = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_9__["default"])("banques").map(element => {
      return element.action;
    });
    const permissionsElectronic = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_9__["default"])("electroniques").map(element => {
      return element.action;
    });
    /**
     * Providers
     */
    // provide("permissionsElectronic", permissionsElectronic);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissionsCash", permissionsCash);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissionsBank", permissionsBank);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissionsElectronic", permissionsElectronic);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissionsInitDettes", permissionsInitDettes);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissionsInitChiffreAffaires", permissionsInitChiffreAffaires);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissionsInitCreances", permissionsInitCreances);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissionsInitAccount", permissionsInitAccount);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("permissions", permissions);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("updateSubView", updateSubView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeView", activeView);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("actionType", actionType);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("activeFilter", activeFilter);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("viewAccountDetails", viewAccountDetails);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("isLoading", isLoading);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("isFetching", isFetching);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("isLoadingAttachment", isLoadingAttachment);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("doGet", doGet);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("cost", cost);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("selectedPaymentMethod", selectedPaymentMethod);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("description", description);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("reference", reference);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("date", date);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("paymentMethods", paymentMethods);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("displayPayments", displayPayments);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("initializeAccount", initializeAccount);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("fundingAccount", fundingAccount);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("initializeOthers", initializeOthers);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("attachment", attachment);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("validations", validations);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("allData", allData);
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.provide)("metaData", metaData);
    /**
     * Page Lifecycle
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onMounted)(async () => {
      // Load payment methods
      getPaymentMethods(paymentMethods).then(() => {
        parsedPayments.value = JSON.parse(paymentMethods.value).filter(item => item.value != "credit");
        getPaymentMethodsByParams();
      });
    });
    const __returned__ = {
      platform,
      updateAppHeaderAction,
      activeWorkspace,
      getPaymentMethods,
      getSelectedPaymentMethod,
      activePage,
      userStats,
      validations,
      router,
      route,
      notify,
      activeView,
      actionType,
      activeFilter,
      viewAccountDetails,
      allData,
      metaData,
      isFetching,
      dateDebut,
      dateFin,
      get parsedPayments() {
        return parsedPayments;
      },
      set parsedPayments(v) {
        parsedPayments = v;
      },
      cost,
      selectedPaymentMethod,
      description,
      reference,
      attachment,
      paymentMethods,
      displayPayments,
      date,
      get medias() {
        return medias;
      },
      set medias(v) {
        medias = v;
      },
      breadcrumbs,
      isLoading,
      isLoadingAttachment,
      updateSubView,
      resetFields,
      resetMetaData,
      initializeOthers,
      initializeAccount,
      fundingAccount,
      getPaymentMethodsByParams,
      doGet,
      dynamicTitle,
      permissions,
      permissionsInitAccount,
      permissionsInitCreances,
      permissionsInitChiffreAffaires,
      permissionsInitDettes,
      permissionsCash,
      permissionsBank,
      permissionsElectronic,
      get exportWalletsData() {
        return _composable_useExport__WEBPACK_IMPORTED_MODULE_3__.exportWalletsData;
      },
      get isOwnBranch() {
        return _state_api_userState__WEBPACK_IMPORTED_MODULE_1__.isOwnBranch;
      },
      get formatDate() {
        return _utils__WEBPACK_IMPORTED_MODULE_4__.formatDate;
      },
      get formatRefSystem() {
        return _utils__WEBPACK_IMPORTED_MODULE_4__.formatRefSystem;
      },
      get numeral() {
        return (numeral__WEBPACK_IMPORTED_MODULE_5___default());
      },
      get HISTORY_WALLET() {
        return _constants_operations__WEBPACK_IMPORTED_MODULE_6__.HISTORY_WALLET;
      },
      get OPERATIONS() {
        return _constants_pages_accountings_wallets__WEBPACK_IMPORTED_MODULE_7__.OPERATIONS;
      },
      get HISTORIQUES() {
        return _constants_pages_accountings_wallets__WEBPACK_IMPORTED_MODULE_7__.HISTORIQUES;
      },
      get OPERATIONS_IN() {
        return _constants_pages_accountings_wallets__WEBPACK_IMPORTED_MODULE_7__.OPERATIONS_IN;
      },
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_10__["default"];
      },
      get nameReferImage() {
        return _composable_useMediasLoader__WEBPACK_IMPORTED_MODULE_11__.nameReferImage;
      },
      get nameReferFile() {
        return _composable_useMediasLoader__WEBPACK_IMPORTED_MODULE_11__.nameReferFile;
      },
      get setURL() {
        return _composable_useMediasLoader__WEBPACK_IMPORTED_MODULE_11__.setURL;
      },
      get previewMetaData() {
        return _composable_useMediasLoader__WEBPACK_IMPORTED_MODULE_11__.previewMetaData;
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingDisbursement.vue?vue&type=template&id=7140f842&scoped=true&ts=true":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingDisbursement.vue?vue&type=template&id=7140f842&scoped=true&ts=true ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _withScopeId = n => ((0,vue__WEBPACK_IMPORTED_MODULE_0__.pushScopeId)("data-v-7140f842"), n = n(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.popScopeId)(), n);
const _hoisted_1 = {
  class: "relative h-full pb-5 overflow-x-hidden overflow-y-auto workspace-content md:pb-0"
};
const _hoisted_2 = {
  key: 1,
  class: "flex w-full mt-4 transition-all duration-500 ease-in-out"
};
const _hoisted_3 = {
  key: 1,
  class: "w-full"
};
const _hoisted_4 = {
  class: "flex flex-col w-full h-full px-2 md:hidden item-list"
};
const _hoisted_5 = {
  class: "flex flex-col left-infos"
};
const _hoisted_6 = {
  class: "text-xl"
};
const _hoisted_7 = ["onClick"];
const _hoisted_8 = {
  key: 1,
  class: "text-lg font-medium cursor-pointer text-green"
};
const _hoisted_9 = {
  key: 0,
  class: "flex flex-col justify-end text-right right-infos"
};
const _hoisted_10 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm text-gray-400"
}, "Total", -1 /* HOISTED */));
const _hoisted_11 = {
  key: 0,
  class: "text-xl"
};
const _hoisted_12 = {
  key: 1,
  class: "flex flex-col justify-end text-right right-infos"
};
const _hoisted_13 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm text-gray-400"
}, "Total", -1 /* HOISTED */));
const _hoisted_14 = {
  key: 0,
  class: "text-xl"
};
const _hoisted_15 = {
  class: "flex flex-col space-y-3"
};
const _hoisted_16 = {
  class: "flex flex-row items-center justify-between"
};
const _hoisted_17 = {
  class: "text-black"
};
const _hoisted_18 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Date", -1 /* HOISTED */));
const _hoisted_19 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_20 = {
  class: "text-xl"
};
const _hoisted_21 = {
  key: 0,
  class: "pt-2 text-right text-black border-t border-gray-600 border-opacity-20"
};
const _hoisted_22 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Total", -1 /* HOISTED */));
const _hoisted_23 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_24 = {
  class: "text-xl"
};
const _hoisted_25 = {
  key: 1,
  class: "pt-2 text-right text-black border-t border-gray-600 border-opacity-20"
};
const _hoisted_26 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Total", -1 /* HOISTED */));
const _hoisted_27 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_28 = {
  class: "text-xl"
};
const _hoisted_29 = {
  class: "flex flex-col space-y-3"
};
const _hoisted_30 = {
  key: 0,
  class: "flex flex-col items-start mb-1"
};
const _hoisted_31 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, " Référence Systême ", -1 /* HOISTED */));
const _hoisted_32 = {
  class: "text-xl"
};
const _hoisted_33 = {
  key: 1,
  class: "flex flex-col items-start mb-1"
};
const _hoisted_34 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, " Référence Systême ", -1 /* HOISTED */));
const _hoisted_35 = {
  class: "text-xl"
};
const _hoisted_36 = {
  key: 2,
  class: "flex flex-col items-start mb-1"
};
const _hoisted_37 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, " Dette sur ", -1 /* HOISTED */));
const _hoisted_38 = {
  class: "text-xl"
};
const _hoisted_39 = {
  key: 3,
  class: "flex flex-col items-start mb-1"
};
const _hoisted_40 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, " Date limite de règlement ", -1 /* HOISTED */));
const _hoisted_41 = {
  class: "text-xl"
};
const _hoisted_42 = {
  key: 4,
  class: "flex flex-col items-start mb-1"
};
const _hoisted_43 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, " Date de règlement ", -1 /* HOISTED */));
const _hoisted_44 = {
  class: "text-xl"
};
const _hoisted_45 = {
  key: 5,
  class: "flex flex-col items-start mb-1"
};
const _hoisted_46 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, " Réference ", -1 /* HOISTED */));
const _hoisted_47 = {
  class: "text-xl"
};
const _hoisted_48 = {
  key: 6,
  class: "flex flex-col items-start mb-1"
};
const _hoisted_49 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, " Succurssale/Siège ", -1 /* HOISTED */));
const _hoisted_50 = {
  class: "text-xl"
};
const _hoisted_51 = {
  key: 7,
  class: "flex flex-col items-start mb-1"
};
const _hoisted_52 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, " Enregistré par ", -1 /* HOISTED */));
const _hoisted_53 = {
  class: "text-xl"
};
const _hoisted_54 = {
  key: 8,
  class: "flex flex-col items-start mb-1"
};
const _hoisted_55 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, " Enregistré par ", -1 /* HOISTED */));
const _hoisted_56 = {
  class: "text-xl"
};
const _hoisted_57 = {
  key: 9,
  class: "flex flex-col items-start mb-1"
};
const _hoisted_58 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, " Description ", -1 /* HOISTED */));
const _hoisted_59 = {
  class: "text-xl"
};
const _hoisted_60 = {
  key: 10,
  class: "flex flex-col items-start mb-1"
};
const _hoisted_61 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, " Créancié ", -1 /* HOISTED */));
const _hoisted_62 = {
  class: "text-xl"
};
const _hoisted_63 = {
  key: 11,
  class: "flex flex-col items-start mb-1"
};
const _hoisted_64 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, " Créancié ", -1 /* HOISTED */));
const _hoisted_65 = {
  class: "text-xl"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_SkeletonDataTable2 = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SkeletonDataTable2");
  const _component_SkeletonLoader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SkeletonLoader");
  const _component_WorkspaceHeader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WorkspaceHeader");
  const _component_DisbursementDatatable = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("DisbursementDatatable");
  const _component_ItemCard = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("ItemCard");
  const _component_DisbursementDetails = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("DisbursementDetails");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isFetching ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_SkeletonLoader, {
      key: 0
    }, {
      default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SkeletonDataTable2)]),
      _: 1 /* STABLE */
    })) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), !$setup.isFetching ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_WorkspaceHeader, {
    key: 0,
    "active-type-view": $setup.actionType,
    "items-list": $setup.allDisbursements,
    "file-name": $setup.filename($setup.activeFilter),
    "data-to-export": () => $setup.getExportedData(),
    "data-to-p-d-f": {
      data: $setup.allDisbursements,
      which: $setup.activeFilter
    },
    "export-all-data": {
      pdf: true,
      excel: true,
      filter: $setup.activeFilter,
      target: null
    },
    "new-item": () => {
      $setup.actionType = $setup.actionType == 'list' ? 'create' : $setup.actionType;
    },
    "breadcums-nav-list": ['remboursements emprunts', 'dettes', 'Autres décaissements'],
    "get-filtered": $setup.doGet,
    "date-debut": $setup.dateDebut,
    "date-fin": $setup.dateFin,
    options: {
      titleBar: true,
      actionsBar: true,
      fabButtons: true,
      actions: {
        import: false,
        export: $setup.useAccess('read', $setup.permissions_reading),
        addItem: $setup.useAccess('add', $setup.permissions) && $setup.activeFilter == 'Autres décaissements',
        filter: true
      },
      fabButtonsActions: {
        addItem: $setup.useAccess('add', $setup.permissions) && $setup.activeFilter == 'Autres décaissements',
        moreMenu: true
      }
    }
  }, null, 8 /* PROPS */, ["active-type-view", "items-list", "file-name", "data-to-export", "data-to-p-d-f", "export-all-data", "new-item", "date-debut", "date-fin", "options"])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), !$setup.isFetching ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_2, [$setup.platform.runtime != 'reactnative' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
    key: 0,
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
      'transition-all duration-500 ease-in-out': true,
      'w-full flex flex-col': $setup.actionType == 'list',
      'w-0 flex flex-col': $setup.actionType != 'list'
    })
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_DisbursementDatatable)], 2 /* CLASS */)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("  Disbursement Datatable For Mobile"), !$setup.isLoading && $setup.actionType == 'list' && $setup.platform.runtime == 'reactnative' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_4, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.allDisbursements, (item, index) => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_ItemCard, {
      key: index,
      label: item.label != '' ? item.label : '',
      "modal-title": `#${item.id} ${item?.label}`,
      "modal-title-color": "text-black",
      "modal-header-background": "bg-white",
      "modal-height": "h-auto"
    }, {
      "card-content": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_5, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_6, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item?.label), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", null, [(item.dette > 0 && $setup.actionType == 'list' || item.reste > 0 && $setup.actionType == 'list') && $setup.useAccess('add', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("a", {
        key: 0,
        class: "text-lg font-medium text-green-800 cursor-pointer",
        onClick: $event => $setup.openRefundDebt({
          data: item
        })
      }, "Décaisser", 8 /* PROPS */, _hoisted_7)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), item.dette == 0 || item.reste == 0 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_8, "Réglé")) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])]), $setup.activeFilter == 'remboursements emprunts' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_9, [_hoisted_10, $setup.activeFilter ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_11, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.transaction?.cost) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.transaction?.currency), 1 /* TEXT */)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter == 'dettes' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_12, [_hoisted_13, $setup.activeFilter ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_14, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.dette) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.currency), 1 /* TEXT */)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
      "modal-header-content": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_15, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_16, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <span class=\"text-black\">\n\t\t\t\t\t\t\t\t\t\t<span class=\"text-sm opacity-50\">Reference système</span>\n\t\t\t\t\t\t\t\t\t\t<br />\n\t\t\t\t\t\t\t\t\t\t<span class=\"text-xl\">{{ formatRefSystem(item?.system_reference) }}</span>\n\t\t\t\t\t\t\t\t\t</span> "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_17, [_hoisted_18, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(), _hoisted_19, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_20, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.formatDate(item.created_at)), 1 /* TEXT */)])]), $setup.activeFilter == 'remboursements emprunts' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_21, [_hoisted_22, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(), _hoisted_23, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_24, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.numeral(item.transaction?.cost).format("0,0.00")) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.currency), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter == 'dettes' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_25, [_hoisted_26, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(), _hoisted_27, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_28, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.numeral(item.dette).format("0,0.00")) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.currency), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])]),
      "modal-content": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_29, [$setup.activeFilter == 'dettes' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_30, [_hoisted_31, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_32, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.system_reference == "" && item.system_reference == null ? "Non défini" : $setup.formatRefSystem(item.system_reference)), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter == 'remboursements emprunts' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_33, [_hoisted_34, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_35, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.transaction == "" && item?.transaction == null ? "Non défini" : $setup.formatRefSystem(item.transaction?.system_reference)), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter == 'dettes' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_36, [_hoisted_37, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_38, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.type == "achat" ? "Achat" : item.type), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter == 'dettes' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_39, [_hoisted_40, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_41, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.end != "" && item.end != null ? item.end : "Non défini"), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter == 'emprunts' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_42, [_hoisted_43, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_44, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.date_reglement != null ? item.date_reglement : "Non défini"), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter == 'dettes' /*|| activeFilter == 'ristournes' || activeFilter == 'creances'*/ ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_45, [_hoisted_46, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_47, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.real_reference != "" && item.real_reference != null ? item.real_reference : "Non défini"), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter == 'dettes' || $setup.activeFilter == 'remboursements emprunts' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_48, [_hoisted_49, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_50, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.branch == null ? "Siège" : item.branch), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter == 'dettes' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_51, [_hoisted_52, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_53, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.payments[0]?.account?.user?.firstname != "" && item.payments[0]?.account?.user?.firstname != null ? item.payments[0]?.account?.user?.firstname : "Non défini"), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter == 'remboursements emprunts' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_54, [_hoisted_55, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_56, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.account?.user?.firstname != "" && item.account?.user?.firstname != null ? item.account?.user?.firstname : "Non défini"), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter != 'creances' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_57, [_hoisted_58, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_59, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.description != "" && item.description != null ? item.description : "Non défini"), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter != 'creances' && $setup.activeFilter != 'dettes' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_60, [_hoisted_61, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_62, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.third != null ? item.third.name : "Non défini"), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter == 'dettes' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_63, [_hoisted_64, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_65, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.third != null ? item.third.name : item.payments[0]?.account?.user?.firstname ?? "Non défini"), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])]),
      _: 2 /* DYNAMIC */
    }, 1032 /* PROPS, DYNAMIC_SLOTS */, ["label", "modal-title"]);
  }), 128 /* KEYED_FRAGMENT */))])])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
      'transition-all duration-500 ease-in-out bg-gray-100 z-105': true,
      'w-0': $setup.actionType == 'list',
      'w-full md:w-2/5 px-0 md:px-4': $setup.actionType != 'list'
    })
  }, [$setup.actionType != 'list' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_DisbursementDetails, {
    key: 0
  })) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)], 2 /* CLASS */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingGenerateDocument.vue?vue&type=template&id=094da8d1&ts=true":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingGenerateDocument.vue?vue&type=template&id=094da8d1&ts=true ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "h-full pb-5 overflow-hidden workspace-content md:pb-0"
};
const _hoisted_2 = {
  class: "relative flex flex-row overflow-hidden bg-opacity-40"
};
const _hoisted_3 = {
  class: "w-full py-8 pr-4 overflow-hidden border border-gray-400 rounded-md md:w-3/6 lg:w-2/5 md:mr-4 pl-7 border-opacity-40 bg-gray-50"
};
const _hoisted_4 = {
  key: 0
};
const _hoisted_5 = {
  key: 1
};
const _hoisted_6 = {
  class: "flex items-center justify-center h-full justify-items-center"
};
const _hoisted_7 = {
  class: "flex flex-row items-end justify-end w-full space-x-1 font-medium"
};
const _hoisted_8 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: ""
}, "Supprimer", -1 /* HOISTED */);
const _hoisted_9 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", {
  class: "px-2"
}, "|", -1 /* HOISTED */);
const _hoisted_10 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: ""
}, "modifier", -1 /* HOISTED */);
const _hoisted_11 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", {
  class: "px-2"
}, "|", -1 /* HOISTED */);
const _hoisted_12 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: ""
}, "Imprimer", -1 /* HOISTED */);
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_WorkspaceHeader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WorkspaceHeader");
  const _component_DocumentsList = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("DocumentsList");
  const _component_CreateDocument = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("CreateDocument");
  const _component_Spinner = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("Spinner");
  const _component_PreviousDocument = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("PreviousDocument");
  const _component_iconButton = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("iconButton");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_WorkspaceHeader, {
    "active-type-view": $setup.actionType,
    "items-list": $setup.listDocuments,
    "new-item": () => {
      $setup.actionType = 'create';
    },
    options: {
      titleBar: true,
      actionsBar: true,
      fabButtons: true,
      actions: {
        import: false,
        export: false,
        addItem: $setup.useAccess('add', $setup.permissions) && $setup.isOwnBranch,
        filter: false
      },
      fabButtonsActions: {
        addItem: $setup.useAccess('add', $setup.permissions) && $setup.isOwnBranch,
        moreMenu: true
      }
    }
  }, null, 8 /* PROPS */, ["active-type-view", "items-list", "new-item", "options"]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Section Left "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_3, [$setup.actionType == 'list' || $setup.actionType == 'empty' || $setup.actionType == 'document-selected' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_4, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: " translate-page-x-custom-right ",
    mode: "out-in"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_DocumentsList)]),
    _: 1 /* STABLE */
  })])) : $setup.actionType == 'create' || $setup.actionType == 'edit' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_5, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: " translate-page-x-custom-right ",
    mode: "out-in"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_CreateDocument)]),
    _: 1 /* STABLE */
  })])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_6, [$setup.actionType == 'loading' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_Spinner, {
    key: 0
  })) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Section Right "), ($setup.windowWidth > 425 ? true : $setup.viewRightContent) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
    key: 0,
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
      'flex flex-col w-full md:w-3/6 lg:w-3/5 absolute md:relative lg:border-none border-gray-300 bg-gray-50 z-20 transform transition-all lg:translate-y-0 duration-300 ease-linear': true,
      'opacity-100 translate-y-0': $setup.viewRightContent,
      'opacity-0 lg:opacity-100 translate-y-56 mt-96 md:mt-0': !$setup.viewRightContent
    })
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
      'py-8 pl-8 pr-4 overflow-hidden border': true,
      'border-gray-400 rounded-md w-full': true,
      'border-opacity-40 bg-gray-50': true
    }),
    style: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeStyle)({
      height: $setup.actionType == 'list' ? 'calc( 100vh - 15rem )' : $setup.actionType == 'document-selected' ? 'calc( 100vh - 18rem )' : $setup.actionType == 'create' || $setup.actionType == 'edit' ? 'calc( 100vh - 12.5rem )' : ''
    })
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_PreviousDocument)], 4 /* STYLE */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Button action "), $setup.actionType == 'document-selected' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
    key: 0,
    class: "flex items-center w-full pl-8 pr-4 mt-1 overflow-hidden border border-gray-400 rounded-md border-opacity-40 bg-gray-50",
    style: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeStyle)({
      height: 'calc( 4.5rem )'
    })
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_7, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <div\n\t\t\t\t\t\t\tclass=\"flex flex-row items-center space-x-1 text-red-500 cursor-pointer hover:text-red-600\"\n\t\t\t\t\t\t\t@click=\"\n\t\t\t\t\t\t\t() => {\n\t\t\t\t\t\t\t\tdeleteDocument(selectedDocument)\n\t\t\t\t\t\t\t}\n\t\t\t\t\t\t\t\">\n\t\t\t\t\t\t\t<iconButton icon=\"/icons/Cancel-icon.svg\" width=\"1rem\" height=\"1rem\" v-if=\"!isDeleting\"/>\n\t\t\t\t\t\t\t<Spinner v-else/>\n\t\t\t\t\t\t\t<span class=\"\">Supprimer</span>\n\t\t\t\t\t\t\t<p class=\"px-2\">|</p>\n\t\t\t\t\t\t</div> "), $setup.useAccess('delete', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
    key: 0,
    class: "flex flex-row items-center space-x-1 text-red-400 cursor-pointer hover:text-gray-600",
    onClick: _cache[0] || (_cache[0] = () => {
      $setup.deleteDocument($setup.selectedDocument);
    })
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_iconButton, {
    icon: "/icons/remove-icon.svg",
    width: "1rem",
    height: "1rem",
    class: "opacity-60"
  }), _hoisted_8])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), _hoisted_9, $setup.useAccess('update', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
    key: 1,
    class: "flex flex-row items-center space-x-1 text-blue-500 cursor-pointer hover:text-blue-700",
    onClick: _cache[1] || (_cache[1] = () => {
      // SI nous sommes en mode visualistion de document(document-selected) passage en état d'edition de document
      $setup.actionType = $setup.actionType == 'document-selected' ? 'edit' : 'list';
    })
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_iconButton, {
    icon: "/icons/edit-quote-icon.svg",
    width: "1rem",
    height: "1rem",
    class: "opacity-60"
  }), _hoisted_10])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), _hoisted_11, $setup.useAccess('read', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
    key: 2,
    class: "flex flex-row items-center space-x-1 text-gray-500 cursor-pointer hover:text-green-600",
    onClick: _cache[2] || (_cache[2] = () => {
      // Impression du document
      // notify.blue('Cette fonctionnalité est en chantier pour le moment !')
      $setup.printDocToPDF();
    })
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_iconButton, {
    icon: "/icons/printer-icon.svg",
    width: "1rem",
    height: "1rem",
    class: "opacity-60"
  }), _hoisted_12])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])], 4 /* STYLE */)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)], 2 /* CLASS */)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingOverview.vue?vue&type=template&id=5efc1d1a&ts=true":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingOverview.vue?vue&type=template&id=5efc1d1a&ts=true ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "h-full pb-5 overflow-x-hidden workspace-content md:pb-0 scroll-area"
};
const _hoisted_2 = {
  class: "relative w-full"
};
const _hoisted_3 = {
  key: 0,
  class: "w-full"
};
const _hoisted_4 = {
  class: "grid w-full gap-6 mt-10 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-3 2xl:grid-cols-3 group-cards"
};
const _hoisted_5 = {
  class: "w-full"
};
const _hoisted_6 = {
  class: "w-full"
};
const _hoisted_7 = {
  class: "w-full"
};
const _hoisted_8 = {
  class: "w-full"
};
const _hoisted_9 = {
  class: "w-full"
};
const _hoisted_10 = {
  class: "w-full"
};
const _hoisted_11 = {
  class: "w-full"
};
const _hoisted_12 = {
  class: "w-full"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_SkeletonSubOverview = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SkeletonSubOverview");
  const _component_SkeletonLoader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SkeletonLoader");
  const _component_CardBannerDashboardOverview = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("CardBannerDashboardOverview");
  const _component_StatCard3 = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("StatCard3");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isLoading ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_SkeletonLoader, {
      key: 0
    }, {
      default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SkeletonSubOverview, {
        stats: 8
      })]),
      _: 1 /* STABLE */
    })) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), !$setup.isLoading ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_CardBannerDashboardOverview, {
    icon: "settings-banner-picture",
    "line-one": "Comptabilité",
    "line-two-a": "Dans cette section vous avez vue d'ensemble des statistiques",
    "line-two-b": "",
    "line-three": "Lancer le tutoriel d'aide ",
    link: "workspace-settings-business-account"
  }), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_4, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_5, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_StatCard3, {
    "line-one": ($setup.sales.value != undefined ? $setup.sales.value : 0).toString(),
    "line-three": "Opérations de Vente",
    "line-two": "",
    link: "workspace-accounting-sales",
    access: $setup.useAccess('read', $setup.permissions)
  }, null, 8 /* PROPS */, ["line-one", "access"])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_6, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_StatCard3, {
    "line-one": ($setup.purchases.value != undefined ? $setup.purchases.value : 0).toString(),
    "line-three": "Opérations d'Achat",
    "line-two": "",
    link: "workspace-accounting-purchases",
    access: $setup.useAccess('read', $setup.permissions)
  }, null, 8 /* PROPS */, ["line-one", "access"])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_7, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_StatCard3, {
    "line-one": ($setup.spents.value != undefined ? $setup.spents.value : 0).toString(),
    "line-three": "Opération Dépenses",
    "line-two": "",
    link: "workspace-accounting-recording-expense",
    access: $setup.useAccess('read', $setup.permissions)
  }, null, 8 /* PROPS */, ["line-one", "access"])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_8, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_StatCard3, {
    "line-one": _ctx.$filters.moneyFormat($setup.amountReceive.value != undefined ? $setup.amountReceive.value : 0).toString(),
    "line-two": $setup.amountReceive?.curency?.toString(),
    "line-three": "Montant Caisse",
    "line-four": $setup.orignalMoney($setup.amountReceive.value != undefined ? $setup.amountReceive.value : 0).toString() + ' ' + $setup.amountReceive?.curency?.toString(),
    link: "workspace-accounting-wallets",
    "route-params": {
      page: 'caisse'
    },
    access: $setup.useAccess('read', $setup.permissions)
  }, null, 8 /* PROPS */, ["line-one", "line-two", "line-four", "access"])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_9, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_StatCard3, {
    "line-one": _ctx.$filters.moneyFormat($setup.bankAmount.value != undefined ? $setup.bankAmount.value : 0).toString(),
    "line-two": $setup.bankAmount?.curency?.toString(),
    "line-three": "Montant Banque",
    "line-four": $setup.orignalMoney($setup.bankAmount.value != undefined ? $setup.bankAmount.value : 0).toString() + ' ' + $setup.bankAmount?.curency?.toString(),
    link: "workspace-accounting-wallets",
    "route-params": {
      page: 'banque'
    },
    access: $setup.useAccess('read', $setup.permissions)
  }, null, 8 /* PROPS */, ["line-one", "line-two", "line-four", "access"])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_10, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_StatCard3, {
    "line-one": _ctx.$filters.moneyFormat($setup.mobileAmount.value != undefined ? $setup.mobileAmount.value : 0).toString(),
    "line-two": $setup.mobileAmount?.curency?.toString(),
    "line-three": "Montant Electronique",
    "line-four": $setup.orignalMoney($setup.mobileAmount.value != undefined ? $setup.mobileAmount.value : 0).toString() + ' ' + $setup.mobileAmount?.curency?.toString(),
    link: "workspace-accounting-wallets",
    "route-params": {
      page: 'electronique'
    },
    access: $setup.useAccess('read', $setup.permissions)
  }, null, 8 /* PROPS */, ["line-one", "line-two", "line-four", "access"])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <div class=\"w-full\">\n\t\t\t\t<StatCard3\n\t\t\t\t\tlineOne=\"205k\"\n\t\t\t\t\t:lineTwo=\"overviewInfos[2].curency.toString()\"\n\t\t\t\t\tlineThree=\"Encaissement\"\n\t\t\t\t\tlink=\"workspace-accounting-receipt\"\n\t\t\t\t/>\n\t\t\t</div>\n\t\t\t<div class=\"w-full\">\n\t\t\t\t<StatCard3\n\t\t\t\t\tlineOne=\"905k\"\n\t\t\t\t\t:lineTwo=\"overviewInfos[2].curency.toString()\"\n\t\t\t\t\tlineThree=\"Décaissement\"\n\t\t\t\t\tlink=\"workspace-accounting-disbursement\"\n\t\t\t\t/>\n\t\t\t</div> "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_11, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_StatCard3, {
    "line-one": _ctx.$filters.moneyFormat($setup.debt.value != undefined ? $setup.debt.value : 0).toString(),
    "line-two": $setup.debt?.curency?.toString(),
    "line-three": "Montant Dettes",
    "line-four": $setup.orignalMoney($setup.debt.value != undefined ? $setup.debt.value : 0).toString() + ' ' + $setup.debt?.curency?.toString(),
    link: "workspace-accounting-wallets",
    "route-params": {
      page: 'dette'
    },
    access: $setup.useAccess('read', $setup.permissions)
  }, null, 8 /* PROPS */, ["line-one", "line-two", "line-four", "access"])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_12, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_StatCard3, {
    "line-one": _ctx.$filters.moneyFormat($setup.creance.value != undefined ? $setup.creance.value : 0).toString(),
    "line-two": $setup.creance?.curency?.toString(),
    "line-three": "Montant Créances",
    "line-four": $setup.orignalMoney($setup.creance.value != undefined ? $setup.creance.value : 0).toString() + ' ' + $setup.creance?.curency?.toString(),
    link: "workspace-accounting-wallets",
    "route-params": {
      page: 'creance'
    },
    access: $setup.useAccess('read', $setup.permissions)
  }, null, 8 /* PROPS */, ["line-one", "line-two", "line-four", "access"])])])])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingPurchases.vue?vue&type=template&id=156c84c1&scoped=true&ts=true":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingPurchases.vue?vue&type=template&id=156c84c1&scoped=true&ts=true ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _withScopeId = n => ((0,vue__WEBPACK_IMPORTED_MODULE_0__.pushScopeId)("data-v-156c84c1"), n = n(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.popScopeId)(), n);
const _hoisted_1 = {
  key: 1
};
const _hoisted_2 = {
  key: 0,
  class: "hidden w-full h-full md:flex"
};
const _hoisted_3 = {
  key: 2
};
const _hoisted_4 = {
  key: 0,
  class: "flex flex-col w-full h-full pr-2 md:hidden item-list"
};
const _hoisted_5 = {
  class: "flex flex-col left-infos"
};
const _hoisted_6 = {
  class: "text-xl"
};
const _hoisted_7 = {
  class: "text-sm text-gray-400"
};
const _hoisted_8 = {
  class: "flex flex-col justify-end text-right right-infos"
};
const _hoisted_9 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm text-gray-400"
}, "Total", -1 /* HOISTED */));
const _hoisted_10 = {
  class: "text-xl"
};
const _hoisted_11 = {
  class: "flex flex-col space-y-3"
};
const _hoisted_12 = {
  class: "flex flex-row items-center justify-between"
};
const _hoisted_13 = {
  class: "text-white"
};
const _hoisted_14 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Reference système", -1 /* HOISTED */));
const _hoisted_15 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_16 = {
  class: "text-xl"
};
const _hoisted_17 = {
  class: "text-white"
};
const _hoisted_18 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Date", -1 /* HOISTED */));
const _hoisted_19 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_20 = {
  class: "text-xl"
};
const _hoisted_21 = {
  class: "text-white"
};
const _hoisted_22 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Montant payé", -1 /* HOISTED */));
const _hoisted_23 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_24 = {
  class: "text-xl"
};
const _hoisted_25 = {
  class: "pt-2 text-right text-white border-t border-gray-100 border-opacity-20"
};
const _hoisted_26 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Total", -1 /* HOISTED */));
const _hoisted_27 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_28 = {
  class: "text-xl"
};
const _hoisted_29 = {
  class: "item-tab-content"
};
const _hoisted_30 = {
  class: "flex flex-row items-center justify-between py-4 tab-links"
};
const _hoisted_31 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", null, "Listes des produits", -1 /* HOISTED */));
const _hoisted_32 = [_hoisted_31];
const _hoisted_33 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "w-0.5 h-4 bg-gray-300"
}, null, -1 /* HOISTED */));
const _hoisted_34 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", null, "Paiements effectués", -1 /* HOISTED */));
const _hoisted_35 = [_hoisted_34];
const _hoisted_36 = {
  class: "tab-content"
};
const _hoisted_37 = {
  key: 0,
  class: "flex flex-col space-y-2 sub-item-list"
};
const _hoisted_38 = {
  class: "flex flex-col left-infos"
};
const _hoisted_39 = {
  class: "text-xl"
};
const _hoisted_40 = {
  class: "text-sm text-gray-400"
};
const _hoisted_41 = {
  class: "flex flex-col justify-end text-right right-infos"
};
const _hoisted_42 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm text-gray-400"
}, "Total", -1 /* HOISTED */));
const _hoisted_43 = {
  class: "text-xl"
};
const _hoisted_44 = {
  key: 1,
  class: "flex flex-col mb-4 space-y-4 sub-item-list"
};
const _hoisted_45 = {
  class: "flex flex-row items-center space-x-3 left-infos"
};
const _hoisted_46 = {
  key: 0,
  src: "/icons/bulk-received-icon-2c.svg",
  alt: ""
};
const _hoisted_47 = {
  key: 1,
  src: "/icons/bulk-send-icon-2c.svg",
  alt: ""
};
const _hoisted_48 = {
  class: "flex flex-col"
};
const _hoisted_49 = {
  class: "text-lg"
};
const _hoisted_50 = {
  class: "text-sm text-gray-700"
};
const _hoisted_51 = {
  class: "flex flex-col justify-end text-right right-infos"
};
const _hoisted_52 = {
  class: "text-sm text-gray-400"
};
const _hoisted_53 = {
  key: 0,
  class: "flex w-full h-full"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_SkeletonDataTable = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SkeletonDataTable");
  const _component_SkeletonLoader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SkeletonLoader");
  const _component_WorkspaceHeader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WorkspaceHeader");
  const _component_PurchasesDatatable = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("PurchasesDatatable");
  const _component_ItemThird = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("ItemThird");
  const _component_ItemCard = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("ItemCard");
  const _component_PurchasesDetails = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("PurchasesDetails");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
      'workspace-content relative w-full h-full pb-5 md:pb-0': true,
      'overflow-y-auto lg:overflow-y-hidden': $setup.activeView == 1,
      'overflow-y-auto': $setup.activeView == 2
    })
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isFetching ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_SkeletonLoader, {
      key: 0
    }, {
      default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SkeletonDataTable)]),
      _: 1 /* STABLE */
    })) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), !$setup.isFetching && $setup.activeView == 1 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_WorkspaceHeader, {
    key: 0,
    "active-view": $setup.activeView,
    "items-list": $setup.purchases,
    "file-name": $setup.filename('Purchases'),
    "data-to-export": () => $setup.exportProductPurchases($setup.purchases),
    "data-to-p-d-f": {
      data: $setup.purchases,
      which: $setup.PRODUCT_PURCHASES
    },
    "export-all-data": {
      pdf: true,
      excel: true,
      filter: null
    },
    "new-item": () => {
      if ($setup.useAccess('add', $setup.permissions)) {
        $setup.updateSubView(2, 'create');
      } else {
        $setup.notify.info('Vous ne pouvez pas effectuer cette action');
      }
    },
    "get-filtered": $setup.getAllPurchases,
    "date-debut": $setup.dateDebut,
    "date-fin": $setup.dateFin,
    options: {
      titleBar: true,
      actionsBar: true,
      fabButtons: true,
      actions: {
        import: false,
        export: $setup.useAccess('read', $setup.permissions),
        addItem: $setup.useAccess('add', $setup.permissions) && $setup.isOwnBranch,
        filter: $setup.useAccess('read', $setup.permissions)
      },
      fabButtonsActions: {
        addItem: $setup.useAccess('add', $setup.permissions) && $setup.isOwnBranch,
        moreMenu: true
      }
    }
  }, null, 8 /* PROPS */, ["active-view", "items-list", "file-name", "data-to-export", "data-to-p-d-f", "new-item", "date-debut", "date-fin", "options"])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("  Purchases Datatable For Web/Desktop "), !$setup.isFetching ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: $setup.transitionName
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.activeView == 1 && $setup.platform.runtime != 'reactnative' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_PurchasesDatatable)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }, 8 /* PROPS */, ["name"])])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("  Purchases List For Mobile "), !$setup.isFetching ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: $setup.transitionName
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.activeView == 1 && $setup.platform.runtime == 'reactnative' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_4, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.purchases, (item, index) => {
      return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_ItemCard, {
        key: index,
        label: item.label,
        "modal-icon": "receipt-item-2c-white.svg",
        "modal-title": `#${item.id} ${item.label}`
      }, {
        "card-content": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_5, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_6, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.label), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_7, "À " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.third?.name), 1 /* TEXT */)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_8, [_hoisted_9, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_10, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.cash_pay) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.currency), 1 /* TEXT */)])]),

        "modal-header-content": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_11, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_12, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_13, [_hoisted_14, _hoisted_15, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_16, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.formatRefSystem(item.system_reference)), 1 /* TEXT */)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_17, [_hoisted_18, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(), _hoisted_19, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_20, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.formatDate(item.created_at)), 1 /* TEXT */)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_21, [_hoisted_22, _hoisted_23, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_24, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.numeral(item.cash_pay).format("0,0.00")) + " XAF", 1 /* TEXT */)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_25, [_hoisted_26, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(), _hoisted_27, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_28, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.numeral(item.cost).format("0,0.00")) + " XAF", 1 /* TEXT */)])])]),

        "modal-content": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_ItemThird, {
          third: item.third
        }, null, 8 /* PROPS */, ["third"]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_29, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_30, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
          class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
            'w-auto': true,
            'font-medium text-green-800': $setup.openTab == 1
          }),
          onClick: _cache[0] || (_cache[0] = $event => $setup.openTab = 1)
        }, [..._hoisted_32], 2 /* CLASS */), _hoisted_33, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
          class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
            'w-auto': true,
            'font-medium text-green-800': $setup.openTab == 2
          }),
          onClick: _cache[1] || (_cache[1] = $event => $setup.openTab = 2)
        }, [..._hoisted_35], 2 /* CLASS */)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_36, [$setup.openTab == 1 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_37, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)(item.products, (purchasedProduct, index) => {
          return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
            key: index,
            class: "flex flex-row items-center justify-between w-full item-infos"
          }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_38, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_39, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(purchasedProduct.label), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_40, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(purchasedProduct.quantity) + " x " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(purchasedProduct.product?.buying_price), 1 /* TEXT */)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_41, [_hoisted_42, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_43, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(purchasedProduct.total) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(purchasedProduct.currency), 1 /* TEXT */)])]);
        }), 128 /* KEYED_FRAGMENT */))])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.openTab == 2 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_44, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)(item.payments, (productPayment, index) => {
          return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
            key: index,
            class: "flex flex-row items-start justify-between w-full item-infos"
          }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_45, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", null, [productPayment.type == 'entree' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", _hoisted_46)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), productPayment.type == 'sortie' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", _hoisted_47)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_48, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_49, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(productPayment.paymentmethod?.label), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_50, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(productPayment.cost) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(productPayment.currency), 1 /* TEXT */)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_51, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_52, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.formatDate(productPayment.created_at)), 1 /* TEXT */)])]);
        }), 128 /* KEYED_FRAGMENT */))])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])])]),
        _: 2 /* DYNAMIC */
      }, 1032 /* PROPS, DYNAMIC_SLOTS */, ["label", "modal-title"]);
    }), 128 /* KEYED_FRAGMENT */))])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }, 8 /* PROPS */, ["name"])])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" New Purchase Operation "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: $setup.transitionName
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.activeView == 2 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_53, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_PurchasesDetails)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }, 8 /* PROPS */, ["name"])], 2 /* CLASS */);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingReceipt.vue?vue&type=template&id=7da6dbb2&scoped=true&ts=true":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingReceipt.vue?vue&type=template&id=7da6dbb2&scoped=true&ts=true ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _withScopeId = n => ((0,vue__WEBPACK_IMPORTED_MODULE_0__.pushScopeId)("data-v-7da6dbb2"), n = n(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.popScopeId)(), n);
const _hoisted_1 = {
  class: "relative h-full pb-5 overflow-x-hidden overflow-y-auto workspace-content md:pb-0"
};
const _hoisted_2 = {
  key: 1,
  class: "flex w-full mt-4 transition-all duration-500 ease-in-out"
};
const _hoisted_3 = {
  key: 1,
  class: "w-full"
};
const _hoisted_4 = {
  class: "flex flex-col w-full h-full px-2 md:hidden item-list"
};
const _hoisted_5 = {
  class: "flex flex-col left-infos"
};
const _hoisted_6 = {
  class: "text-xl"
};
const _hoisted_7 = {
  key: 0,
  class: "text-sm text-gray-400"
};
const _hoisted_8 = {
  key: 1
};
const _hoisted_9 = ["onClick"];
const _hoisted_10 = {
  key: 1,
  class: "text-lg font-medium cursor-pointer text-green"
};
const _hoisted_11 = {
  key: 0,
  class: "flex flex-col justify-end text-right right-infos"
};
const _hoisted_12 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm text-gray-400"
}, "Total", -1 /* HOISTED */));
const _hoisted_13 = {
  class: "text-xl"
};
const _hoisted_14 = {
  key: 1,
  class: "flex flex-col justify-end text-right right-infos"
};
const _hoisted_15 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm text-gray-400"
}, "Total", -1 /* HOISTED */));
const _hoisted_16 = {
  key: 0,
  class: "text-xl"
};
const _hoisted_17 = {
  key: 2,
  class: "flex flex-col justify-end text-right right-infos"
};
const _hoisted_18 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm text-gray-400"
}, "Total", -1 /* HOISTED */));
const _hoisted_19 = {
  key: 0,
  class: "text-xl"
};
const _hoisted_20 = {
  class: "flex flex-col space-y-3"
};
const _hoisted_21 = {
  class: "flex flex-row items-center justify-between"
};
const _hoisted_22 = {
  class: "text-black"
};
const _hoisted_23 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Date", -1 /* HOISTED */));
const _hoisted_24 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_25 = {
  class: "text-xl"
};
const _hoisted_26 = {
  key: 0,
  class: "pt-2 text-right text-black border-t border-gray-600 border-opacity-20"
};
const _hoisted_27 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Total", -1 /* HOISTED */));
const _hoisted_28 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_29 = {
  class: "text-xl"
};
const _hoisted_30 = {
  key: 1,
  class: "pt-2 text-right text-black border-t border-gray-600 border-opacity-20"
};
const _hoisted_31 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Total", -1 /* HOISTED */));
const _hoisted_32 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_33 = {
  class: "text-xl"
};
const _hoisted_34 = {
  key: 2,
  class: "pt-2 text-right text-black border-t border-gray-600 border-opacity-20"
};
const _hoisted_35 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Total", -1 /* HOISTED */));
const _hoisted_36 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_37 = {
  class: "text-xl"
};
const _hoisted_38 = {
  class: "flex flex-col space-y-3"
};
const _hoisted_39 = {
  key: 0
};
const _hoisted_40 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Créance sur", -1 /* HOISTED */));
const _hoisted_41 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_42 = {
  class: "text-xl"
};
const _hoisted_43 = {
  key: 1,
  class: "pt-2 border-t border-gray-600 border-opacity-20"
};
const _hoisted_44 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Date limite de règlement", -1 /* HOISTED */));
const _hoisted_45 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_46 = {
  class: "text-xl"
};
const _hoisted_47 = {
  key: 2,
  class: "pt-2 border-t border-gray-600 border-opacity-20"
};
const _hoisted_48 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Créancié", -1 /* HOISTED */));
const _hoisted_49 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_50 = {
  class: "text-xl"
};
const _hoisted_51 = {
  key: 3,
  class: "pt-2 border-t border-gray-600 border-opacity-20"
};
const _hoisted_52 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Date de règlement ", -1 /* HOISTED */));
const _hoisted_53 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_54 = {
  class: "text-xl"
};
const _hoisted_55 = {
  key: 4,
  class: "pt-2 border-t border-gray-600 border-opacity-20"
};
const _hoisted_56 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Méthode Payement", -1 /* HOISTED */));
const _hoisted_57 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_58 = {
  class: "text-xl"
};
const _hoisted_59 = {
  key: 5,
  class: "pt-2 border-t border-gray-600 border-opacity-20"
};
const _hoisted_60 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Enregistré par", -1 /* HOISTED */));
const _hoisted_61 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_62 = {
  class: "text-xl"
};
const _hoisted_63 = {
  key: 6,
  class: "pt-2 border-t border-gray-600 border-opacity-20"
};
const _hoisted_64 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Enregistré par", -1 /* HOISTED */));
const _hoisted_65 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_66 = {
  class: "text-xl"
};
const _hoisted_67 = {
  key: 7,
  class: "pt-2 border-t border-gray-600 border-opacity-20"
};
const _hoisted_68 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Référence Systême", -1 /* HOISTED */));
const _hoisted_69 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_70 = {
  class: "text-xl"
};
const _hoisted_71 = {
  key: 8,
  class: "pt-2 border-t border-gray-600 border-opacity-20"
};
const _hoisted_72 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Succurssale/Siège", -1 /* HOISTED */));
const _hoisted_73 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_74 = {
  class: "text-xl"
};
const _hoisted_75 = {
  key: 9,
  class: "pt-2 border-t border-gray-600 border-opacity-20"
};
const _hoisted_76 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Description", -1 /* HOISTED */));
const _hoisted_77 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_78 = {
  class: "text-xl"
};
const _hoisted_79 = {
  key: 2,
  class: /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
    'w-full md:w-2/5 px-0 md:px-4 transition-all duration-500 ease-in-out': true
  })
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_SkeletonDataTable2 = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SkeletonDataTable2");
  const _component_SkeletonLoader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SkeletonLoader");
  const _component_WorkspaceHeader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WorkspaceHeader");
  const _component_ReceiptDatatable = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("ReceiptDatatable");
  const _component_ItemCard = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("ItemCard");
  const _component_ReceiptDetails = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("ReceiptDetails");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isFetching ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_SkeletonLoader, {
      key: 0
    }, {
      default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SkeletonDataTable2)]),
      _: 1 /* STABLE */
    })) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), !$setup.isFetching ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_WorkspaceHeader, {
    key: 0,
    "active-type-view": $setup.actionType,
    "items-list": $setup.allReceipts,
    "file-name": $setup.filename($setup.activeFilter),
    "data-to-export": () => $setup.getExportedData(),
    "data-to-p-d-f": {
      data: $setup.allReceipts,
      which: $setup.activeFilter
    },
    "export-all-data": {
      pdf: true,
      excel: true,
      filter: $setup.activeFilter,
      target: null
    },
    "get-filtered": $setup.doGet,
    "date-debut": $setup.dateDebut,
    "date-fin": $setup.dateFin,
    "new-item": () => {
      $setup.updateSubView('create');
    },
    "breadcums-nav-list": [$setup.RISTOURNES, $setup.SUBVENTIONS_EXPLOITATION, $setup.EMPRUNTS, $setup.CREANCES, $setup.FONDS_PROPRES],
    options: {
      titleBar: true,
      actionsBar: true,
      fabButtons: true,
      actions: {
        import: false,
        export: $setup.useAccess('read', $setup.permissions),
        addItem: $setup.isOwnBranch && $setup.useAccess('add', $setup.permissions) ? $setup.activeFilter == 'creances' ? false : true : false,
        filter: true
      },
      fabButtonsActions: {
        addItem: $setup.isOwnBranch && $setup.useAccess('add', $setup.permissions) ? $setup.activeFilter == 'creances' ? false : true : false,
        moreMenu: true
      }
    }
  }, null, 8 /* PROPS */, ["active-type-view", "items-list", "file-name", "data-to-export", "data-to-p-d-f", "export-all-data", "date-debut", "date-fin", "new-item", "breadcums-nav-list", "options"])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), !$setup.isFetching ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_2, [$setup.platform.runtime != 'reactnative' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
    key: 0,
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
      'transition-all duration-500 ease-in-out': true,
      'w-full flex flex-col': $setup.actionType == 'list',
      'w-3/5 flex flex-col': $setup.actionType != 'list'
    })
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_ReceiptDatatable)], 2 /* CLASS */)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("  Receipt Datatable For Mobile"), !$setup.isLoading && $setup.actionType == 'list' && $setup.platform.runtime == 'reactnative' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_4, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.allReceipts, (item, index) => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_ItemCard, {
      key: index,
      label: item.label != '' ? item.label : '',
      "modal-title": `#${item.id} ${item.label}`,
      "modal-title-color": "text-black",
      "modal-header-background": "bg-white",
      "modal-height": "h-auto"
    }, {
      "card-content": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_5, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_6, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item?.label), 1 /* TEXT */), $setup.activeFilter == 'subventions d\'exploitation' || $setup.activeFilter == 'ristournes' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_7, " de " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.third?.name), 1 /* TEXT */)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter == 'creances' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_8, [item.creance > 0 && $setup.actionType == 'list' && $setup.useAccess('add', $setup.permissions) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("a", {
        key: 0,
        class: "text-lg font-medium text-green-800 cursor-pointer",
        onClick: $event => $setup.openRefundClaim({
          data: item
        })
      }, "Encaisser", 8 /* PROPS */, _hoisted_9)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), item.dette == 0 || item.reste == 0 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_10, "Réglé")) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]), $setup.activeFilter == 'ristournes' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_11, [_hoisted_12, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_13, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.items[0]?.total) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.items[0]?.currency), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter == 'subventions d\'exploitation' || $setup.activeFilter == 'emprunts' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_14, [_hoisted_15, $setup.activeFilter ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_16, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.transaction?.cost) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.transaction?.currency), 1 /* TEXT */)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter == 'creances' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_17, [_hoisted_18, $setup.activeFilter ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_19, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.creance) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.currency), 1 /* TEXT */)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
      "modal-header-content": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_20, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_21, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" span class=\"text-black\">\n\t\t\t\t\t\t\t\t\t\t<span class=\"text-sm opacity-50\">Reference système</span>\n\t\t\t\t\t\t\t\t\t\t<br />\n\t\t\t\t\t\t\t\t\t\t<span class=\"text-xl\">{{ formatRefSystem(item.transaction) }}</span>\n\t\t\t\t\t\t\t\t\t</span> "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_22, [_hoisted_23, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(), _hoisted_24, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_25, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.formatDate(item.created_at)), 1 /* TEXT */)])]), $setup.activeFilter == 'ristournes' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_26, [_hoisted_27, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(), _hoisted_28, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_29, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.numeral(item.items[0]?.total).format("0,0.00")) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.items[0]?.currency), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter == 'subventions d\'exploitation' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_30, [_hoisted_31, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(), _hoisted_32, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_33, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.numeral(item.transaction?.cost).format("0,0.00")) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.transaction?.currency), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter == 'creances' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_34, [_hoisted_35, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(), _hoisted_36, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_37, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.numeral(item.creance).format("0,0.00")) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.currency), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])]),
      "modal-content": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_38, [$setup.activeFilter == 'creances' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_39, [_hoisted_40, _hoisted_41, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_42, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.type == "vente" || item.type != null ? item.type : "Non défini"), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter == 'creances' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_43, [_hoisted_44, _hoisted_45, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_46, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.end != "" && item.end != null ? item.end : "Non défini"), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter == 'creances' || $setup.activeFilter == 'emprunts' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_47, [_hoisted_48, _hoisted_49, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_50, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.third?.name != "" && item.third?.name != null ? item.third?.name : "Non défini"), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter == 'emprunts' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_51, [_hoisted_52, _hoisted_53, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_54, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.date_reglement != null ? item.date_reglement : "Non défini"), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter != 'creances' && $setup.activeFilter != 'ristournes' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_55, [_hoisted_56, _hoisted_57, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_58, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.paymentmethod.label), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter == 'emprunts' || $setup.activeFilter == 'ristournes' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_59, [_hoisted_60, _hoisted_61, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_62, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.account?.user?.firstname != "" && item.account?.user?.firstname != null ? item.account?.user?.firstname : "Non défini"), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter == 'subventions d\'exploitation' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_63, [_hoisted_64, _hoisted_65, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_66, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.transaction?.payments[0]?.account?.user?.firstname != "" && item.transaction?.payments[0]?.account?.user?.firstname != null ? item.transaction?.payments[0]?.account?.user?.firstname : "Non défini"), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter == 'emprunts' || $setup.activeFilter == 'ristournes' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_67, [_hoisted_68, _hoisted_69, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_70, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.transaction == "" && item?.transaction == null ? "Non défini" : $setup.formatRefSystem(item.transaction?.system_reference)), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter == 'emprunts' || $setup.activeFilter == 'creances' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_71, [_hoisted_72, _hoisted_73, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_74, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.branch == null ? "Siège" : item.branch), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter != 'creances' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("span", _hoisted_75, [_hoisted_76, _hoisted_77, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_78, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.description != "" && item.description != null ? item.description : "Non défini"), 1 /* TEXT */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])]),
      _: 2 /* DYNAMIC */
    }, 1032 /* PROPS, DYNAMIC_SLOTS */, ["label", "modal-title"]);
  }), 128 /* KEYED_FRAGMENT */))])])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.actionType != 'list' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_79, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_ReceiptDetails)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingRecordingExpense.vue?vue&type=template&id=5304de70&scoped=true&ts=true":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingRecordingExpense.vue?vue&type=template&id=5304de70&scoped=true&ts=true ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _withScopeId = n => ((0,vue__WEBPACK_IMPORTED_MODULE_0__.pushScopeId)("data-v-5304de70"), n = n(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.popScopeId)(), n);
const _hoisted_1 = {
  class: "relative w-full h-full pb-5 overflow-x-hidden overflow-y-auto workspace-content md:pb-0"
};
const _hoisted_2 = {
  key: 1,
  class: "flex w-full transition-all duration-500 ease-in-out bg-gray-100"
};
const _hoisted_3 = {
  class: "flex flex-col left-infos"
};
const _hoisted_4 = {
  class: "text-xl"
};
const _hoisted_5 = {
  class: "text-sm text-gray-400"
};
const _hoisted_6 = {
  class: "flex flex-col justify-end text-right right-infos"
};
const _hoisted_7 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm text-gray-400"
}, "Total", -1 /* HOISTED */));
const _hoisted_8 = {
  class: "text-xl"
};
const _hoisted_9 = {
  class: "flex flex-col space-y-3"
};
const _hoisted_10 = {
  class: "flex flex-row items-center justify-between mb-2"
};
const _hoisted_11 = {
  class: "text-black"
};
const _hoisted_12 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Reference système", -1 /* HOISTED */));
const _hoisted_13 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_14 = {
  class: "text-xl"
};
const _hoisted_15 = {
  class: "text-black"
};
const _hoisted_16 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Date", -1 /* HOISTED */));
const _hoisted_17 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_18 = {
  class: "text-xl"
};
const _hoisted_19 = {
  class: "pt-2 text-right text-black border-t border-gray-600 border-opacity-20"
};
const _hoisted_20 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Total", -1 /* HOISTED */));
const _hoisted_21 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_22 = {
  class: "text-xl"
};
const _hoisted_23 = {
  class: "flex flex-col space-y-3"
};
const _hoisted_24 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Moyen de paiement", -1 /* HOISTED */));
const _hoisted_25 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_26 = {
  class: "text-xl"
};
const _hoisted_27 = {
  class: "pt-2 border-t border-gray-600 border-opacity-20"
};
const _hoisted_28 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Initié par", -1 /* HOISTED */));
const _hoisted_29 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_30 = {
  class: "text-xl"
};
const _hoisted_31 = {
  class: "pt-2 border-t border-gray-600 border-opacity-20"
};
const _hoisted_32 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Initié par", -1 /* HOISTED */));
const _hoisted_33 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_34 = {
  class: "text-xl"
};
const _hoisted_35 = {
  class: "flex flex-col pt-2 border-t border-gray-600 border-opacity-20"
};
const _hoisted_36 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "mb-2 text-sm opacity-50"
}, "Pièces jointes", -1 /* HOISTED */));
const _hoisted_37 = {
  key: 0,
  class: "w-full p-2 bg-gray-300 rounded-xl"
};
const _hoisted_38 = ["src", "alt", "title", "onClick"];
const _hoisted_39 = ["href", "title"];
const _hoisted_40 = {
  key: 1,
  class: "w-full p-2 italic bg-gray-300 color-gray-500 rounded-xl"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_SkeletonDataTable = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SkeletonDataTable");
  const _component_SkeletonLoader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SkeletonLoader");
  const _component_WorkspaceHeader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WorkspaceHeader");
  const _component_RexpDatatable = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("RexpDatatable");
  const _component_iconButton = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("iconButton");
  const _component_ItemCard = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("ItemCard");
  const _component_RexpDetails = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("RexpDetails");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "fade-slow"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.isFetching ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_SkeletonLoader, {
      key: 0
    }, {
      default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SkeletonDataTable)]),
      _: 1 /* STABLE */
    })) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), !$setup.isFetching ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_WorkspaceHeader, {
    key: 0,
    "items-list": $setup.spents,
    "file-name": $setup.filename('Expenses'),
    "data-to-export": async () => $setup.exportRecordingExpense($setup.spents),
    "data-to-p-d-f": {
      data: $setup.spents,
      which: 'RecordingExpenses',
      withMetaData: $setup.metaDataSpents
    },
    "export-all-data": {
      pdf: true,
      excel: true,
      filter: null
    },
    "new-item": () => {
      $setup.actionType = $setup.actionType == 'list' ? 'create' : $setup.actionType;
    },
    "get-filtered": $setup.getAllRecordingExpenses,
    "date-debut": $setup.dateDebut,
    "date-fin": $setup.dateFin,
    "active-type-view": $setup.actionType,
    options: {
      titleBar: true,
      actionsBar: true,
      fabButtons: true,
      actions: {
        import: false,
        export: $setup.useAccess('read', $setup.permissions),
        addItem: $setup.useAccess('add', $setup.permissions) && $setup.isOwnBranch,
        filter: true
      },
      fabButtonsActions: {
        addItem: $setup.useAccess('add', $setup.permissions) && $setup.isOwnBranch,
        moreMenu: true
      }
    }
  }, null, 8 /* PROPS */, ["items-list", "file-name", "data-to-export", "data-to-p-d-f", "new-item", "date-debut", "date-fin", "active-type-view", "options"])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), !$setup.isFetching ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_2, [$setup.activeView == 1 && $setup.platform.runtime != 'reactnative' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
    key: 0,
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
      'transition-all duration-500 ease-in-out': true,
      'w-full flex': $setup.actionType == 'list',
      'w-3/5 flex': $setup.actionType != 'list'
    })
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_RexpDatatable)], 2 /* CLASS */)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("  Recording expense Datatable For Mobile"), !$setup.isLoading && !$setup.isFetching && $setup.activeView == 1 && $setup.platform.runtime == 'reactnative' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
    key: 1,
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
      'flex flex-col h-full md:hidden item-list': true,
      'w-full pr-2': $setup.actionType == 'list',
      'w-0': $setup.actionType != 'list'
    })
  }, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.spents, (item, index) => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_ItemCard, {
      key: index,
      label: item.label,
      "modal-icon": "receipt-item-2c-white.svg",
      "modal-title": `#${item.id} ${item.label}`,
      "modal-title-color": "text-black",
      "modal-header-background": "bg-white",
      "modal-height": "h-auto"
    }, {
      "card-content": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_4, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.label), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_5, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.typesdepense?.libele), 1 /* TEXT */)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_6, [_hoisted_7, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_8, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.cost), 1 /* TEXT */)])]),

      "modal-header-content": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_9, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_10, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_11, [_hoisted_12, _hoisted_13, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_14, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.transaction?.system_reference != "" && item.transaction?.system_reference != null ? $setup.formatRefSystem(item.transaction.system_reference) : "Non défini"), 1 /* TEXT */)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_15, [_hoisted_16, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(), _hoisted_17, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_18, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.formatDate(item.created_at)), 1 /* TEXT */)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_19, [_hoisted_20, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(), _hoisted_21, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_22, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.numeral(item.cost).format("0,0.00")) + " XAF", 1 /* TEXT */)])])]),

      "modal-content": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_23, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", null, [_hoisted_24, _hoisted_25, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_26, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.paymentmethod?.label ?? "Non défini"), 1 /* TEXT */)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_27, [_hoisted_28, _hoisted_29, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_30, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.account?.user?.firstname ?? "Non défini"), 1 /* TEXT */)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_31, [_hoisted_32, _hoisted_33, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_34, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.transaction?.third?.name ?? "Non défini"), 1 /* TEXT */)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Medias "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_35, [_hoisted_36, item.medias.length ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("p", _hoisted_37, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)(item.medias, media => {
        return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
          key: media.id
        }, [$setup.nameReferImage(media.name) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
          key: 0,
          src: $setup.setURL(media),
          alt: media.name,
          title: media.name,
          class: "w-10 h-10 rounded-lg cursor-pointer",
          onClick: $event => $setup.previewMetaData(media, item.medias)
        }, null, 8 /* PROPS */, _hoisted_38)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.nameReferFile(media.name) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("a", {
          key: 1,
          href: $setup.setURL(media),
          title: media.name,
          target: "_blank",
          class: "w-10 h-10 rounded-md",
          onclick: "if(this.href.charAt(this.href.length -1) == '#') return false;",
          rel: "noopener noreferrer"
        }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_iconButton, {
          icon: "/icons/documents/pdf-icon.svg",
          width: "18px",
          height: "18px"
        })], 8 /* PROPS */, _hoisted_39)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)], 64 /* STABLE_FRAGMENT */);
      }), 128 /* KEYED_FRAGMENT */))])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), !item.medias.length ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("p", _hoisted_40, " Aucune Pièce Jointe ")) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])])]),
      _: 2 /* DYNAMIC */
    }, 1032 /* PROPS, DYNAMIC_SLOTS */, ["label", "modal-title"]);
  }), 128 /* KEYED_FRAGMENT */))], 2 /* CLASS */)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
      'transition-all duration-500 ease-in-out bg-gray-100 z-90': true,
      'w-0': $setup.actionType == 'list',
      'w-full md:w-2/5 px-0 md:px-4': $setup.actionType != 'list'
    })
  }, [$setup.actionType != 'list' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_RexpDetails, {
    key: 0
  })) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)], 2 /* CLASS */)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingSendEmail.vue?vue&type=template&id=23f52dba&scoped=true&ts=true":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingSendEmail.vue?vue&type=template&id=23f52dba&scoped=true&ts=true ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _withScopeId = n => ((0,vue__WEBPACK_IMPORTED_MODULE_0__.pushScopeId)("data-v-23f52dba"), n = n(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.popScopeId)(), n);
const _hoisted_1 = {
  class: "h-full pb-5 overflow-y-auto workspace-content md:pb-0"
};
const _hoisted_2 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row items-center justify-between w-full h-16 workspace-header"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h3", {
  class: "text-xl text-gray-500"
}, " Comptabilité > Envoyer un E-mail"), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "flex flex-row items-center h-full workspace-actions"
})], -1 /* HOISTED */));
const _hoisted_3 = {
  class: "w-full p-8 border border-gray-400 rounded-md border-opacity-40 bg-gray-50",
  style: {
    "height": "calc(100% - 5rem)"
  }
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_SendQuoteEmail = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SendQuoteEmail");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [_hoisted_2, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "translate-subview-right"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SendQuoteEmail)]),
    _: 1 /* STABLE */
  })])]);
}

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingWallets.vue?vue&type=template&id=193aca89&scoped=true&ts=true":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingWallets.vue?vue&type=template&id=193aca89&scoped=true&ts=true ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _withScopeId = n => ((0,vue__WEBPACK_IMPORTED_MODULE_0__.pushScopeId)("data-v-193aca89"), n = n(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.popScopeId)(), n);
const _hoisted_1 = {
  class: "h-full pb-5 overflow-x-hidden overflow-y-auto workspace-content md:h-full md:pb-0"
};
const _hoisted_2 = {
  key: 1,
  class: "flex w-full p-5 mt-4 transition-all duration-500 ease-in-out bg-gray-100 border-2 border-gray-200 rounded-2xl"
};
const _hoisted_3 = {
  key: 2
};
const _hoisted_4 = {
  class: "text-lg font-normal text-gray-400"
};
const _hoisted_5 = {
  class: /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
    'transform transition-all duration-500 ease-in-out w-full': true
  })
};
const _hoisted_6 = {
  key: 1,
  class: "relative flex flex-col w-full h-full mt-4"
};
const _hoisted_7 = {
  key: 2,
  class: "flex flex-col w-full h-full pr-2 md:hidden item-list"
};
const _hoisted_8 = {
  class: "flex flex-col left-infos"
};
const _hoisted_9 = {
  class: "text-xl"
};
const _hoisted_10 = {
  class: "flex flex-col space-y-2"
};
const _hoisted_11 = {
  class: "text-sm text-gray-400"
};
const _hoisted_12 = {
  class: "flex flex-col justify-end text-right right-infos"
};
const _hoisted_13 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm text-gray-400"
}, "Total", -1 /* HOISTED */));
const _hoisted_14 = {
  class: "text-xl"
};
const _hoisted_15 = {
  class: "flex flex-col space-y-3"
};
const _hoisted_16 = {
  class: "flex flex-row items-center justify-between mb-2"
};
const _hoisted_17 = {
  class: "text-black"
};
const _hoisted_18 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Reference système", -1 /* HOISTED */));
const _hoisted_19 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_20 = {
  class: "text-xl"
};
const _hoisted_21 = {
  class: "text-black"
};
const _hoisted_22 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Date", -1 /* HOISTED */));
const _hoisted_23 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_24 = {
  class: "text-xl"
};
const _hoisted_25 = {
  class: "pt-2 text-right text-black border-t border-gray-600 border-opacity-20"
};
const _hoisted_26 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Montant", -1 /* HOISTED */));
const _hoisted_27 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_28 = {
  class: "text-xl"
};
const _hoisted_29 = {
  class: "flex flex-col space-y-3"
};
const _hoisted_30 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "description", -1 /* HOISTED */));
const _hoisted_31 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_32 = {
  class: "text-xl"
};
const _hoisted_33 = {
  class: "pt-2 border-t border-gray-600 border-opacity-20"
};
const _hoisted_34 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "text-sm opacity-50"
}, "Source", -1 /* HOISTED */));
const _hoisted_35 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("br", null, null, -1 /* HOISTED */));
const _hoisted_36 = {
  class: "text-xl"
};
const _hoisted_37 = {
  key: 0,
  class: "flex flex-col pt-2 border-t border-gray-600 border-opacity-20"
};
const _hoisted_38 = /*#__PURE__*/_withScopeId(() => /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", {
  class: "mb-2 text-sm opacity-50"
}, "Pièces jointes", -1 /* HOISTED */));
const _hoisted_39 = {
  key: 0
};
const _hoisted_40 = {
  key: 0,
  class: "w-full p-2 bg-gray-300 rounded-xl"
};
const _hoisted_41 = ["src", "alt", "title", "onClick"];
const _hoisted_42 = ["href", "title"];
const _hoisted_43 = {
  key: 1,
  class: "w-full p-2 italic bg-gray-300 color-gray-500 rounded-xl"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_WorkspaceHeader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WorkspaceHeader");
  const _component_WalletsStatus = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WalletsStatus");
  const _component_WalletsDetails = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WalletsDetails");
  const _component_WalletsDataTable = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("WalletsDataTable");
  const _component_SkeletonDataTable = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SkeletonDataTable");
  const _component_SkeletonLoader = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("SkeletonLoader");
  const _component_iconButton = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("iconButton");
  const _component_ItemCard = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("ItemCard");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [$setup.isFetching == false ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_WorkspaceHeader, {
    key: 0,
    "active-type-view": $setup.actionType,
    "new-item": () => {
      $setup.updateSubView('update');
    },
    "items-list": $setup.activeFilter == $setup.HISTORIQUES || $setup.activeFilter == $setup.OPERATIONS || $setup.activeFilter == $setup.OPERATIONS_IN ? $setup.allData : [],
    "file-name": $setup.activeFilter + '.Wallet.' + $setup.route.params.page + '-',
    "data-to-export": async () => $setup.exportWalletsData($setup.allData, $setup.activeFilter),
    "data-to-p-d-f": {
      data: $setup.allData,
      which: $setup.HISTORY_WALLET,
      filter: $setup.activeFilter,
      wallet: $setup.route.params.page
    },
    "export-all-data": {
      pdf: true,
      excel: true,
      filter: $setup.activeFilter,
      target: $setup.route.params.page
    },
    "breadcums-nav-list": $setup.breadcrumbs,
    "get-filtered": $setup.doGet,
    "date-debut": $setup.dateDebut,
    "date-fin": $setup.dateFin,
    options: {
      titleBar: true,
      actionsBar: $setup.useAccess('read', $setup.permissions),
      fabButtons: true,
      actions: {
        import: false,
        export: true,
        addItem: false,
        filter: $setup.activeFilter == $setup.HISTORIQUES || $setup.activeFilter == $setup.OPERATIONS || $setup.activeFilter == $setup.OPERATIONS_IN
      },
      fabButtonsActions: {
        addItem: false,
        moreMenu: $setup.activeFilter == $setup.HISTORIQUES || $setup.activeFilter == $setup.OPERATIONS || $setup.activeFilter == $setup.OPERATIONS_IN
      }
    }
  }, null, 8 /* PROPS */, ["active-type-view", "new-item", "items-list", "file-name", "data-to-export", "data-to-p-d-f", "export-all-data", "breadcums-nav-list", "date-debut", "date-fin", "options"])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.activeFilter != $setup.HISTORIQUES && $setup.activeFilter != $setup.OPERATIONS ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_2, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
      'transform transition-all duration-500 ease-in-out': true,
      'w-full md:w-3/5': $setup.actionType == 'list',
      'w-0 md:w-3/5 -translate-x-96 md:translate-x-0': $setup.actionType != 'list'
    })
  }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_WalletsStatus)], 2 /* CLASS */), $setup.activePage?.params.page != 'creance' && $setup.activePage?.params.page != 'chifre_affaire' && $setup.activePage?.params.page != 'dette' || $setup.userStats[$setup.activePage?.params.page]?.initial == null ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
    key: 0,
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
      'transform transition-all duration-500 ease-in-out': true,
      'w-0 md:w-2/5 translate-x-96 md:translate-x-0': $setup.actionType == 'list',
      'w-full md:w-2/5': $setup.actionType != 'list'
    })
  }, [$setup.isOwnBranch ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_WalletsDetails, {
    key: 0
  })) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)], 2 /* CLASS */)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])) : $setup.activeFilter == $setup.HISTORIQUES || $setup.activeFilter == $setup.OPERATIONS || $setup.activeFilter == $setup.OPERATIONS_IN ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" <div v-else-if=\"!isFetching\"> "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
    name: "translate-subview-right"
  }, {
    default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [$setup.activeView == 1 && $setup.platform.runtime != 'reactnative' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
      key: 0,
      class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)({
        'flex flex-col w-full duration-500 ease-in-out p-0 md:p-5 transition-all': true,
        'pt-4': $setup.platform.runtime == 'reactnative'
      })
    }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("p", _hoisted_4, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.dynamicTitle()), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_5, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_WalletsDataTable)])], 2 /* CLASS */)) : $setup.activeView == 1 && $setup.platform.runtime == 'reactnative' && $setup.isFetching ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_6, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(vue__WEBPACK_IMPORTED_MODULE_0__.Transition, {
      name: "fade-slow"
    }, {
      default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SkeletonLoader, null, {
        default: (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_SkeletonDataTable)]),
        _: 1 /* STABLE */
      })]),

      _: 1 /* STABLE */
    })])) : $setup.activeView == 1 && $setup.platform.runtime == 'reactnative' && !$setup.isFetching ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_7, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.allData, (item, index) => {
      return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createBlock)(_component_ItemCard, {
        key: index,
        label: item.label,
        "modal-title": `#${item.id} ${item.label}`,
        "modal-title-color": "text-black",
        "modal-header-background": "bg-white",
        "modal-height": "h-auto"
      }, {
        "card-content": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_8, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_9, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.label), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_10, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_11, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.formatDate(item.created_at)), 1 /* TEXT */)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_12, [_hoisted_13, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_14, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.cost) + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.currency), 1 /* TEXT */)])]),

        "modal-header-content": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_15, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_16, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_17, [_hoisted_18, _hoisted_19, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_20, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.transaction?.reference != "" && item.transaction?.reference != null ? $setup.formatRefSystem(item.transaction.reference) : "Non défini"), 1 /* TEXT */)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_21, [_hoisted_22, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(), _hoisted_23, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_24, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.formatDate(item.created_at)), 1 /* TEXT */)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_25, [_hoisted_26, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(), _hoisted_27, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_28, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.numeral(item.cost).format("0,0.00")) + " XAF", 1 /* TEXT */)])])]),

        "modal-content": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(() => [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_29, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", null, [_hoisted_30, _hoisted_31, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_32, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.description == null || item.description == "" ? "Non Défini" : item.description), 1 /* TEXT */)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_33, [_hoisted_34, _hoisted_35, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_36, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(item.source == null || item.source == "" ? "Non Défini" : item.source), 1 /* TEXT */)]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Medias "), $setup.activeFilter == $setup.HISTORIQUES ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_37, [_hoisted_38, item.hasOwnProperty('medias') ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_39, [item.medias.length ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("p", _hoisted_40, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)(item.medias, media => {
          return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
            key: media.id
          }, [$setup.nameReferImage(media.name) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
            key: 0,
            src: $setup.setURL(media),
            alt: media.name,
            title: media.name,
            class: "w-10 h-10 rounded-lg cursor-pointer",
            onClick: $event => $setup.previewMetaData(media, item.medias)
          }, null, 8 /* PROPS */, _hoisted_41)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.nameReferFile(media.name) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("a", {
            key: 1,
            href: $setup.setURL(media),
            title: media.name,
            target: "_blank",
            class: "w-10 h-10 rounded-md",
            onclick: "if(this.href.charAt(this.href.length -1) == '#') return false;",
            rel: "noopener noreferrer"
          }, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_iconButton, {
            icon: "/icons/documents/pdf-icon.svg",
            width: "18px",
            height: "18px"
          })], 8 /* PROPS */, _hoisted_42)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)], 64 /* STABLE_FRAGMENT */);
        }), 128 /* KEYED_FRAGMENT */))])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), !item?.medias.length ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("p", _hoisted_43, " Aucune Pièce Jointe ")) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)])]),
        _: 2 /* DYNAMIC */
      }, 1032 /* PROPS, DYNAMIC_SLOTS */, ["label", "modal-title"]);
    }), 128 /* KEYED_FRAGMENT */))])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]),
    _: 1 /* STABLE */
  }), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" </div> ")])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]);
}

/***/ }),

/***/ "./src/composable/modeles-documents/administrativeLetter.ts":
/*!******************************************************************!*\
  !*** ./src/composable/modeles-documents/administrativeLetter.ts ***!
  \******************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jspdf__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jspdf */ "./node_modules/jspdf/dist/jspdf.es.min.js");
/* harmony import */ var _composable_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/composable/common */ "./src/composable/common/index.ts");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var dayjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! dayjs */ "./node_modules/dayjs/dayjs.min.js");
/* harmony import */ var dayjs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(dayjs__WEBPACK_IMPORTED_MODULE_3__);




const ramdomNumber = dayjs__WEBPACK_IMPORTED_MODULE_3___default()().format("X");
let doc;
const prefix = "Objet : ";
let heightPage;
let pageNumber = 1;
const defaultFont = {
  style: "normal",
  name: "helvetica"
};
// eslint-disable-next-line prefer-const
let currentHeight = 0;
const setOwnerInfo = (data, font) => {
  doc.setTextColor(100);
  doc.setFontSize(12);
  doc.setFont(font.name, font.style);
  doc.text(data.organisationName, 1.4, 4.5 /*{align: "left"}*/);
  doc.text(data.email ?? "Adresse E-mail Non Défini", 1.4, 5);
  doc.text(data.phone ?? "No Tel Non Défini", 1.4, 5.5);
  doc.text(data.address != "0" && data.address != null ? data.address : "Adresse Non Défini", 1.4, 6);
  doc.setFont(defaultFont.name, defaultFont.style);
  return 6 - currentHeight; // height of content
};

const setDate = (date, font) => {
  doc.setFont(font.name, font.style);
  doc.text(date, _composable_common__WEBPACK_IMPORTED_MODULE_1__.Position.getXOffset(date, doc), 4.5);
  doc.setFont(defaultFont.name, defaultFont.style);
};
const setReceiverInfo = (data, doc, font) => {
  // doc.value.text(data., position.getXOffset(date,doc.value), 4.5 /*{align: "left"}*/)
  // doc.value.text(data.address, 1.4, 6)
  return _composable_common__WEBPACK_IMPORTED_MODULE_1__.Spacing.text.justifyCenter(doc, [data.to, data.grade, data.companyName], 8, font, 1); // return height of content
};

const setObjectLetter = (data, doc, font) => {
  if (!lodash__WEBPACK_IMPORTED_MODULE_2__.isEmpty(font)) doc.setFont(font.name, font.style);
  doc.text("Objet : ", 1.4, 10.5);
  doc.setFont(defaultFont.name, defaultFont.style);
  doc.text(data, 1.4 + doc.getTextWidth(prefix), 10.5);
  return 10.5 - currentHeight;
};
const setGrade = (grade, font) => {
  doc.setFont(font.name, font.style);
  doc.text([grade].join(","), 1.4 + 1, 12.5);
  doc.setFont(defaultFont.name, defaultFont.style);
  return 12 - currentHeight;
};
const setGreeting = content => {
  doc.setLineHeightFactor(1.5);
  const result = doc.splitTextToSize(content, 21 - 1.4 - 1.4 - 1); // 21 - 1.4*2 - 1 (1.4 = marge en x, 1 = indentSpacing)
  const lineTextToIndent = result.splice(0, 1);
  content = result.join(" ");
  // content = doc.splitTextToSize(content, 18.2)
  doc.text(lineTextToIndent, 1.4 + 1, 13.2, {
    align: "justify",
    maxWidth: 18.2 /*lineHeightFactor: 1.5*/
  });
  doc.text(content, 1.4, 13.9, {
    align: "justify",
    maxWidth: 18.2 /*lineHeightFactor: 1.5*/
  });
  return doc.splitTextToSize(content, 21 - 1.4 - 1.4).length * 0.7; // last position y
};

const setBody = (content, lastPositionY) => {
  doc.setLineHeightFactor(1.5);
  const result = doc.splitTextToSize(content, 21 - 1.4 - 1.4 - 1); // 21 - 1.4*2 - 1 (1.4 = marge en x, 1 = indentSpacing)
  const lineTextToIndent = result.splice(0, 1);
  content = result.join(" ");
  // text = doc.splitTextToSize(text, 18.2)
  const height = doc.splitTextToSize(content, 21 - 1.4 - 1.4).length * 0.7; // last position y
  // console.log("Body : currentPage -> ", doc.getNumberOfPages());
  if (heightPage - currentHeight - 2 > height) {
    doc.text(lineTextToIndent, 1.4 + 1, lastPositionY, {
      align: "justify",
      maxWidth: 18.2 /*lineHeightFactor: 1.5*/
    });
    doc.text(content, 1.4, lastPositionY + 0.5, {
      align: "justify",
      maxWidth: 18.2 /*lineHeightFactor: 1.5*/
    });
  } else {
    doc.addPage();
    currentHeight = 2;
    // pageNumber = pageNumber + 1
    doc.setPage(doc.getNumberOfPages());
    doc.text(lineTextToIndent, 1.4 + 1, currentHeight, {
      align: "justify",
      maxWidth: 18.2 /*lineHeightFactor: 1.5*/
    });
    doc.text(content, 1.4, currentHeight + 0.5, {
      align: "justify",
      maxWidth: 18.2 /*lineHeightFactor: 1.5*/
    });
  }

  return height;
};
const setConclusion = (content, lastPositionY) => {
  doc.setLineHeightFactor(1.5);
  const result = doc.splitTextToSize(content, 21 - 1.4 - 1.4 - 1); // 21 - 1.4*2 - 1 (1.4 = marge en x, 1 = indentSpacing)
  const lineTextToIndent = result.splice(0, 1);
  content = result.join(" ");
  const height = doc.splitTextToSize(content, 21 - 1.4 - 1.4).length * 0.7; // last position y
  if (heightPage - currentHeight - 2 > height) {
    doc.text(lineTextToIndent, 1.4 + 1, lastPositionY + 0.5, {
      align: "justify",
      maxWidth: 18.2 /*lineHeightFactor: 1.5*/
    });
    doc.text(content, 1.4, lastPositionY + 1, {
      align: "justify",
      maxWidth: 18.2 /*lineHeightFactor: 1.5*/
    });
  } else {
    doc.addPage();
    // pageNumber = pageNumber + 1
    doc.setPage(doc.getNumberOfPages());
    console.log('doc.getNumberOfPages(): ', doc.getNumberOfPages());
    currentHeight = 2;
    console.log('currentHeight  Conclusion: ', currentHeight);
    doc.text(lineTextToIndent, 1.4 + 1, currentHeight, {
      align: "justify",
      maxWidth: 18.2 /*lineHeightFactor: 1.5*/
    });
    doc.text(content, 1.4, currentHeight + 0.5, {
      align: "justify",
      maxWidth: 18.2 /*lineHeightFactor: 1.5*/
    });
  }

  return height; // last position y
};

const setSignature = (signature, drawCachet, font) => {
  // eslint-disable-next-line prefer-const
  const height = 2 * 0.5;
  doc.setPage(doc.getNumberOfPages());
  // console.log('doc.getFontList(): ', doc.getFontList(), doc.getFont());
  // console.log('currentHeight: ', heightPage - currentHeight - 2, height);
  if (heightPage - currentHeight - 2 > height) {
    if (drawCachet.status) {
      doc.addImage(drawCachet.data, "JPEG", 13, currentHeight, 5, 5, "aliass", "MEDIUM", 0);
    }
    return _composable_common__WEBPACK_IMPORTED_MODULE_1__.Spacing.text.justifyCenter(doc, [signature.signatureName, signature.signatureMore], currentHeight, font, 3); // return height of content
  } else {
    currentHeight = 2;
    doc.addPage();
    // pageNumber = pageNumber + 1
    doc.setPage(doc.getNumberOfPages());
    // console.log('currentHeight 2 ', heightPage - currentHeight - 2, height);
    // console.log('doc.getNumberOfPages(): ', doc.getNumberOfPages());
    if (drawCachet.status) {
      doc.addImage(drawCachet.data, "JPEG", 13, currentHeight, 5, 5, "aliass", "MEDIUM", 0);
    }
    return _composable_common__WEBPACK_IMPORTED_MODULE_1__.Spacing.text.justifyCenter(doc, [signature.signatureName, signature.signatureMore], currentHeight, font, 4);
  }
};
const setFooter = () => {
  // eslint-disable-next-line prefer-const
  let text = "Conclusion culpa duis mollit quis occaecat dolor lorum. Ut ullamco nostrud et duis esse incididunt labo";
  const height = 0;
  if (heightPage - currentHeight - 2 > height) {
    //
  } else {
    doc.addPage();
    pageNumber = pageNumber + 1;
    doc.setPage(doc.getNumberOfPages());
    currentHeight = 2;
    //
  }

  return height;
};
const prepareContent = (file, data, font) => {
  doc = new jspdf__WEBPACK_IMPORTED_MODULE_0__.jsPDF('p', 'cm', 'a4');
  heightPage = doc.internal.pageSize.height;
  currentHeight = _composable_common__WEBPACK_IMPORTED_MODULE_1__.Header.setHeader(file, doc, 0); // 3, p1
  currentHeight = setOwnerInfo(data, font) + currentHeight; // 6, p1
  setDate(data.date, font);
  currentHeight = setReceiverInfo(data.receiver, doc, font) + currentHeight; // 13.5, p1
  currentHeight = setObjectLetter(data.object, doc, font) + currentHeight; // 10.5, p1
  currentHeight = setGrade(data.receiver.grade, font) + currentHeight; // 12, p1
  currentHeight = setGreeting(data.content.greeting) + 2 + currentHeight; // 18.2, p1
  currentHeight = setBody(data.content.body, currentHeight) + currentHeight; // 25.6, p1
  currentHeight = setConclusion(data.content.conclusion, currentHeight) + currentHeight + 2; // 35, p2
  currentHeight = setSignature(data.signature, data.cachet, font) + currentHeight; // 36.5, p3
};

const adminLetter = {
  printToPDF: (file, filename, data, font = {}) => {
    prepareContent(file, data, font);
    doc.save(filename + ramdomNumber + '.pdf');
  },
  generateBlob: (file, filename, data, font) => {
    prepareContent(file, data, font);
    return doc.output('blob', {
      filename: filename + ramdomNumber + '.pdf'
    });
  }
};
/* harmony default export */ __webpack_exports__["default"] = (adminLetter);

/***/ }),

/***/ "./src/composable/useDocuments.ts":
/*!****************************************!*\
  !*** ./src/composable/useDocuments.ts ***!
  \****************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   adminLetter: function() { return /* reexport safe */ _modeles_documents_administrativeLetter__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _modeles_documents_administrativeLetter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modeles-documents/administrativeLetter */ "./src/composable/modeles-documents/administrativeLetter.ts");



/***/ }),

/***/ "./src/composable/useEmail.ts":
/*!************************************!*\
  !*** ./src/composable/useEmail.ts ***!
  \************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   responseEmailSended: function() { return /* binding */ responseEmailSended; },
/* harmony export */   sendMail: function() { return /* binding */ sendMail; }
/* harmony export */ });
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/api */ "./src/api/index.ts");
/* harmony import */ var _composable_useNotyf__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/composable/useNotyf */ "./src/composable/useNotyf.ts");


const notify = (0,_composable_useNotyf__WEBPACK_IMPORTED_MODULE_1__["default"])();
let responseEmailSended;
async function sendMail(files = [], sender, receiver, object, message, alert) {
  await (0,_api__WEBPACK_IMPORTED_MODULE_0__["default"])().gadgets.mail.send({
    title: object,
    email: receiver,
    body: message,
    // files: filesToSend.value
    files: files
  }).then(resp => {
    notify.success(alert.successMsg);
    responseEmailSended = true;
  }).catch(err => {
    console.log("Impossible d'envoyer le Mail: ", err);
    notify.error(alert.errorMsg);
    responseEmailSended = false;
  });
}


/***/ }),

/***/ "./src/composable/useValidator.ts":
/*!****************************************!*\
  !*** ./src/composable/useValidator.ts ***!
  \****************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_push_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.push.js */ "./node_modules/core-js/modules/es.array.push.js");
/* harmony import */ var core_js_modules_es_array_push_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_push_js__WEBPACK_IMPORTED_MODULE_0__);

const useValidator = {
  adminLetter: (object, sendingEmail, emailReceiver, errorSignatureFormat, errorReceiverFormat, content, dateCreation) => {
    let globalStateError = 0;
    const errorResult = {
      object: {
        state: false,
        msg: ""
      },
      destination: {
        state: false,
        msg: ""
      },
      date: {
        state: false,
        msg: ""
      },
      greeting: {
        state: false,
        msg: ""
      },
      body: {
        state: false,
        msg: ""
      },
      conclusion: {
        state: false,
        msg: ""
      },
      signature: {
        state: false,
        msg: ""
      },
      emailReceiver: {
        state: false,
        msg: ""
      }
    };
    const error = [];
    if (object == "") {
      globalStateError++;
      errorResult.emailReceiver.state = true;
      errorResult.emailReceiver.msg = "Votre document n'a aucun Objet";
      error.push("Votre document n'a aucun Objet");
    }
    if (sendingEmail == true && emailReceiver == "") {
      globalStateError++;
      errorResult.emailReceiver.state = true;
      errorResult.emailReceiver.msg = "Veuillez saisir une adresse E-mail Valide";
      error.push("Veuillez saisir une adresse E-mail Valide");
    }
    if (errorSignatureFormat == false) {
      globalStateError++;
      errorResult.signature.state = true;
      errorResult.signature.msg = "Veuiller signer littéralement votre document";
      error.push("Veuiller signer littéralement votre document");
    }
    if (content.conclusion == "") {
      globalStateError++;
      errorResult.conclusion.state = true;
      errorResult.conclusion.msg = "Veuillez effectuer une conclusion votre document";
      error.push("Veuillez effectuer une conclusion votre document");
    }
    if (content.body == "") {
      globalStateError++;
      errorResult.conclusion.state = true;
      errorResult.conclusion.msg = "Veuillez saisir le corps du document";
      error.push("Veuillez saisir le corps du document");
    }
    if (content.greeting == "") {
      globalStateError++;
      errorResult.greeting.state = true;
      errorResult.greeting.msg = "Veuillez saisir l'introduction de votre document";
      error.push("Veuillez saisir l'introduction de votre document");
    }
    if (dateCreation.date == "") {
      globalStateError++;
      errorResult.greeting.state = true;
      errorResult.greeting.msg = "Veuillez définir la date de votre document";
      error.push("Veuillez définir la date de votre document");
    }
    if (errorReceiverFormat == false) {
      globalStateError++;
      errorResult.destination.state = true;
      errorResult.destination.msg = "Veuillez saisir le destinataire de votre document";
      error.push("Veuillez saisir le destinataire de votre document");
    }
    if (globalStateError == 0) {
      return {
        error: false,
        content: []
      };
    } else {
      return {
        error: true,
        content: error,
        errorObject: errorResult
      };
    }
  },
  refundLetter: () => {
    // Demande de remboursement
  },
  collectionLetter: () => {
    // Lettre de recouvrement
  },
  refundUndeliveredMerchandise: () => {
    // DEmande de remboursement pour Marchandise non livréés
  },
  contactAndProposalDocument: () => {
    // Demande de contact  et proposition
  },
  RequestForPriceOfGoods: () => {
    // Demande de prix de marchandise
  },
  productRepaiOrrReplacementRequest: () => {
    // Demande de de reparation ou remplacement de marchandise
  }
};
/* harmony default export */ __webpack_exports__["default"] = (useValidator);

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingDisbursement.vue?vue&type=style&index=0&id=7140f842&lang=scss&scoped=true":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingDisbursement.vue?vue&type=style&index=0&id=7140f842&lang=scss&scoped=true ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/noSourceMaps.js */ "./node_modules/css-loader/dist/runtime/noSourceMaps.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, "[data-v-7140f842] .p-paginator .p-paginator-current {\n  margin-left: auto;\n}\n[data-v-7140f842] .p-progressbar {\n  height: 0.5rem;\n  background-color: #d8dadc;\n}\n[data-v-7140f842] .p-progressbar .p-progressbar-value {\n  background-color: #607d8b;\n}\n[data-v-7140f842] .p-datepicker {\n  min-width: 25rem;\n}\n[data-v-7140f842] .p-datepicker td {\n  font-weight: 400;\n}\n[data-v-7140f842] .p-datatable.p-datatable-5nkap .p-datatable-header {\n  padding: 1rem;\n  text-align: left;\n  font-size: 1.5rem;\n}\n[data-v-7140f842] .p-datatable.p-datatable-5nkap .p-paginator {\n  padding: 0.3rem;\n}\n[data-v-7140f842] .p-datatable.p-datatable-5nkap .p-datatable-thead > tr > th {\n  text-align: left;\n}\n[data-v-7140f842] .p-datatable.p-datatable-5nkap .p-datatable-tbody > tr > td {\n  cursor: auto;\n}\n[data-v-7140f842] .p-datatable.p-datatable-5nkap .p-dropdown-label:not(.p-placeholder) {\n  text-transform: uppercase;\n}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingPurchases.vue?vue&type=style&index=0&id=156c84c1&lang=scss&scoped=true":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingPurchases.vue?vue&type=style&index=0&id=156c84c1&lang=scss&scoped=true ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/noSourceMaps.js */ "./node_modules/css-loader/dist/runtime/noSourceMaps.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, "[data-v-156c84c1] .p-paginator .p-paginator-current {\n  margin-left: auto;\n}\n[data-v-156c84c1] .p-progressbar {\n  height: 0.5rem;\n  background-color: #d8dadc;\n}\n[data-v-156c84c1] .p-progressbar .p-progressbar-value {\n  background-color: #607d8b;\n}\n[data-v-156c84c1] .p-datepicker {\n  min-width: 25rem;\n}\n[data-v-156c84c1] .p-datepicker td {\n  font-weight: 400;\n}\n[data-v-156c84c1] .p-datatable.p-datatable-5nkap .p-datatable-header {\n  padding: 1rem;\n  text-align: left;\n  font-size: 1.5rem;\n}\n[data-v-156c84c1] .p-datatable.p-datatable-5nkap .p-paginator {\n  padding: 0.3rem;\n}\n[data-v-156c84c1] .p-datatable.p-datatable-5nkap .p-datatable-thead > tr > th {\n  text-align: left;\n}\n[data-v-156c84c1] .p-datatable.p-datatable-5nkap .p-datatable-tbody > tr > td {\n  cursor: auto;\n}\n[data-v-156c84c1] .p-datatable.p-datatable-5nkap .p-dropdown-label:not(.p-placeholder) {\n  text-transform: uppercase;\n}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingReceipt.vue?vue&type=style&index=0&id=7da6dbb2&lang=scss&scoped=true":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingReceipt.vue?vue&type=style&index=0&id=7da6dbb2&lang=scss&scoped=true ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/noSourceMaps.js */ "./node_modules/css-loader/dist/runtime/noSourceMaps.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, "[data-v-7da6dbb2] .p-paginator .p-paginator-current {\n  margin-left: auto;\n}\n[data-v-7da6dbb2] .p-progressbar {\n  height: 0.5rem;\n  background-color: #d8dadc;\n}\n[data-v-7da6dbb2] .p-progressbar .p-progressbar-value {\n  background-color: #607d8b;\n}\n[data-v-7da6dbb2] .p-datepicker {\n  min-width: 25rem;\n}\n[data-v-7da6dbb2] .p-datepicker td {\n  font-weight: 400;\n}\n[data-v-7da6dbb2] .p-datatable.p-datatable-5nkap .p-datatable-header {\n  padding: 1rem;\n  text-align: left;\n  font-size: 1.5rem;\n}\n[data-v-7da6dbb2] .p-datatable.p-datatable-5nkap .p-paginator {\n  padding: 0.3rem;\n}\n[data-v-7da6dbb2] .p-datatable.p-datatable-5nkap .p-datatable-thead > tr > th {\n  text-align: left;\n}\n[data-v-7da6dbb2] .p-datatable.p-datatable-5nkap .p-datatable-tbody > tr > td {\n  cursor: auto;\n}\n[data-v-7da6dbb2] .p-datatable.p-datatable-5nkap .p-dropdown-label:not(.p-placeholder) {\n  text-transform: uppercase;\n}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingRecordingExpense.vue?vue&type=style&index=0&id=5304de70&lang=scss&scoped=true":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingRecordingExpense.vue?vue&type=style&index=0&id=5304de70&lang=scss&scoped=true ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/noSourceMaps.js */ "./node_modules/css-loader/dist/runtime/noSourceMaps.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, "[data-v-5304de70] .p-paginator .p-paginator-current {\n  margin-left: auto;\n}\n[data-v-5304de70] .p-progressbar {\n  height: 0.5rem;\n  background-color: #d8dadc;\n}\n[data-v-5304de70] .p-progressbar .p-progressbar-value {\n  background-color: #607d8b;\n}\n[data-v-5304de70] .p-datepicker {\n  min-width: 25rem;\n}\n[data-v-5304de70] .p-datepicker td {\n  font-weight: 400;\n}\n[data-v-5304de70] .p-datatable.p-datatable-5nkap .p-datatable-header {\n  padding: 1rem;\n  text-align: left;\n  font-size: 1.5rem;\n}\n[data-v-5304de70] .p-datatable.p-datatable-5nkap .p-paginator {\n  padding: 0.3rem;\n}\n[data-v-5304de70] .p-datatable.p-datatable-5nkap .p-datatable-thead > tr > th {\n  text-align: left;\n}\n[data-v-5304de70] .p-datatable.p-datatable-5nkap .p-datatable-tbody > tr > td {\n  cursor: auto;\n}\n[data-v-5304de70] .p-datatable.p-datatable-5nkap .p-dropdown-label:not(.p-placeholder) {\n  text-transform: uppercase;\n}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingSendEmail.vue?vue&type=style&index=0&id=23f52dba&lang=scss&scoped=true":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingSendEmail.vue?vue&type=style&index=0&id=23f52dba&lang=scss&scoped=true ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/noSourceMaps.js */ "./node_modules/css-loader/dist/runtime/noSourceMaps.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, "[data-v-23f52dba] .p-paginator .p-paginator-current {\n  margin-left: auto;\n}\n[data-v-23f52dba] .p-progressbar {\n  height: 0.5rem;\n  background-color: #d8dadc;\n}\n[data-v-23f52dba] .p-progressbar .p-progressbar-value {\n  background-color: #607d8b;\n}\n[data-v-23f52dba] .p-datepicker {\n  min-width: 25rem;\n}\n[data-v-23f52dba] .p-datepicker td {\n  font-weight: 400;\n}\n[data-v-23f52dba] .p-datatable.p-datatable-5nkap .p-datatable-header {\n  padding: 1rem;\n  text-align: left;\n  font-size: 1.5rem;\n}\n[data-v-23f52dba] .p-datatable.p-datatable-5nkap .p-paginator {\n  padding: 0.3rem;\n}\n[data-v-23f52dba] .p-datatable.p-datatable-5nkap .p-datatable-thead > tr > th {\n  text-align: left;\n}\n[data-v-23f52dba] .p-datatable.p-datatable-5nkap .p-datatable-tbody > tr > td {\n  cursor: auto;\n}\n[data-v-23f52dba] .p-datatable.p-datatable-5nkap .p-dropdown-label:not(.p-placeholder) {\n  text-transform: uppercase;\n}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingWallets.vue?vue&type=style&index=0&id=193aca89&lang=scss&scoped=true":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingWallets.vue?vue&type=style&index=0&id=193aca89&lang=scss&scoped=true ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/noSourceMaps.js */ "./node_modules/css-loader/dist/runtime/noSourceMaps.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_noSourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, "[data-v-193aca89] .p-paginator .p-paginator-current {\n  margin-left: auto;\n}\n[data-v-193aca89] .p-progressbar {\n  height: 0.5rem;\n  background-color: #d8dadc;\n}\n[data-v-193aca89] .p-progressbar .p-progressbar-value {\n  background-color: #607d8b;\n}\n[data-v-193aca89] .p-datepicker {\n  min-width: 25rem;\n}\n[data-v-193aca89] .p-datepicker td {\n  font-weight: 400;\n}\n[data-v-193aca89] .p-datatable.p-datatable-5nkap .p-datatable-header {\n  padding: 1rem;\n  text-align: left;\n  font-size: 1.5rem;\n}\n[data-v-193aca89] .p-datatable.p-datatable-5nkap .p-paginator {\n  padding: 0.3rem;\n}\n[data-v-193aca89] .p-datatable.p-datatable-5nkap .p-datatable-thead > tr > th {\n  text-align: left;\n}\n[data-v-193aca89] .p-datatable.p-datatable-5nkap .p-datatable-tbody > tr > td {\n  cursor: auto;\n}\n[data-v-193aca89] .p-datatable.p-datatable-5nkap .p-dropdown-label:not(.p-placeholder) {\n  text-transform: uppercase;\n}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./src/pages/workspace/accounting/accountingDisbursement.vue":
/*!*******************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingDisbursement.vue ***!
  \*******************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _accountingDisbursement_vue_vue_type_template_id_7140f842_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./accountingDisbursement.vue?vue&type=template&id=7140f842&scoped=true&ts=true */ "./src/pages/workspace/accounting/accountingDisbursement.vue?vue&type=template&id=7140f842&scoped=true&ts=true");
/* harmony import */ var _accountingDisbursement_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./accountingDisbursement.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/accounting/accountingDisbursement.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _accountingDisbursement_vue_vue_type_style_index_0_id_7140f842_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./accountingDisbursement.vue?vue&type=style&index=0&id=7140f842&lang=scss&scoped=true */ "./src/pages/workspace/accounting/accountingDisbursement.vue?vue&type=style&index=0&id=7140f842&lang=scss&scoped=true");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;


const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_3__["default"])(_accountingDisbursement_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_accountingDisbursement_vue_vue_type_template_id_7140f842_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__scopeId',"data-v-7140f842"],['__file',"src/pages/workspace/accounting/accountingDisbursement.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/accounting/accountingGenerateDocument.vue":
/*!***********************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingGenerateDocument.vue ***!
  \***********************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _accountingGenerateDocument_vue_vue_type_template_id_094da8d1_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./accountingGenerateDocument.vue?vue&type=template&id=094da8d1&ts=true */ "./src/pages/workspace/accounting/accountingGenerateDocument.vue?vue&type=template&id=094da8d1&ts=true");
/* harmony import */ var _accountingGenerateDocument_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./accountingGenerateDocument.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/accounting/accountingGenerateDocument.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_accountingGenerateDocument_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_accountingGenerateDocument_vue_vue_type_template_id_094da8d1_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/accounting/accountingGenerateDocument.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/accounting/accountingOverview.vue":
/*!***************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingOverview.vue ***!
  \***************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _accountingOverview_vue_vue_type_template_id_5efc1d1a_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./accountingOverview.vue?vue&type=template&id=5efc1d1a&ts=true */ "./src/pages/workspace/accounting/accountingOverview.vue?vue&type=template&id=5efc1d1a&ts=true");
/* harmony import */ var _accountingOverview_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./accountingOverview.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/accounting/accountingOverview.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_accountingOverview_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_accountingOverview_vue_vue_type_template_id_5efc1d1a_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/accounting/accountingOverview.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/accounting/accountingPurchases.vue":
/*!****************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingPurchases.vue ***!
  \****************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _accountingPurchases_vue_vue_type_template_id_156c84c1_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./accountingPurchases.vue?vue&type=template&id=156c84c1&scoped=true&ts=true */ "./src/pages/workspace/accounting/accountingPurchases.vue?vue&type=template&id=156c84c1&scoped=true&ts=true");
/* harmony import */ var _accountingPurchases_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./accountingPurchases.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/accounting/accountingPurchases.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _accountingPurchases_vue_vue_type_style_index_0_id_156c84c1_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./accountingPurchases.vue?vue&type=style&index=0&id=156c84c1&lang=scss&scoped=true */ "./src/pages/workspace/accounting/accountingPurchases.vue?vue&type=style&index=0&id=156c84c1&lang=scss&scoped=true");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;


const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_3__["default"])(_accountingPurchases_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_accountingPurchases_vue_vue_type_template_id_156c84c1_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__scopeId',"data-v-156c84c1"],['__file',"src/pages/workspace/accounting/accountingPurchases.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/accounting/accountingReceipt.vue":
/*!**************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingReceipt.vue ***!
  \**************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _accountingReceipt_vue_vue_type_template_id_7da6dbb2_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./accountingReceipt.vue?vue&type=template&id=7da6dbb2&scoped=true&ts=true */ "./src/pages/workspace/accounting/accountingReceipt.vue?vue&type=template&id=7da6dbb2&scoped=true&ts=true");
/* harmony import */ var _accountingReceipt_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./accountingReceipt.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/accounting/accountingReceipt.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _accountingReceipt_vue_vue_type_style_index_0_id_7da6dbb2_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./accountingReceipt.vue?vue&type=style&index=0&id=7da6dbb2&lang=scss&scoped=true */ "./src/pages/workspace/accounting/accountingReceipt.vue?vue&type=style&index=0&id=7da6dbb2&lang=scss&scoped=true");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;


const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_3__["default"])(_accountingReceipt_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_accountingReceipt_vue_vue_type_template_id_7da6dbb2_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__scopeId',"data-v-7da6dbb2"],['__file',"src/pages/workspace/accounting/accountingReceipt.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/accounting/accountingRecordingExpense.vue":
/*!***********************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingRecordingExpense.vue ***!
  \***********************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _accountingRecordingExpense_vue_vue_type_template_id_5304de70_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./accountingRecordingExpense.vue?vue&type=template&id=5304de70&scoped=true&ts=true */ "./src/pages/workspace/accounting/accountingRecordingExpense.vue?vue&type=template&id=5304de70&scoped=true&ts=true");
/* harmony import */ var _accountingRecordingExpense_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./accountingRecordingExpense.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/accounting/accountingRecordingExpense.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _accountingRecordingExpense_vue_vue_type_style_index_0_id_5304de70_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./accountingRecordingExpense.vue?vue&type=style&index=0&id=5304de70&lang=scss&scoped=true */ "./src/pages/workspace/accounting/accountingRecordingExpense.vue?vue&type=style&index=0&id=5304de70&lang=scss&scoped=true");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;


const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_3__["default"])(_accountingRecordingExpense_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_accountingRecordingExpense_vue_vue_type_template_id_5304de70_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__scopeId',"data-v-5304de70"],['__file',"src/pages/workspace/accounting/accountingRecordingExpense.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/accounting/accountingSendEmail.vue":
/*!****************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingSendEmail.vue ***!
  \****************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _accountingSendEmail_vue_vue_type_template_id_23f52dba_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./accountingSendEmail.vue?vue&type=template&id=23f52dba&scoped=true&ts=true */ "./src/pages/workspace/accounting/accountingSendEmail.vue?vue&type=template&id=23f52dba&scoped=true&ts=true");
/* harmony import */ var _accountingSendEmail_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./accountingSendEmail.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/accounting/accountingSendEmail.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _accountingSendEmail_vue_vue_type_style_index_0_id_23f52dba_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./accountingSendEmail.vue?vue&type=style&index=0&id=23f52dba&lang=scss&scoped=true */ "./src/pages/workspace/accounting/accountingSendEmail.vue?vue&type=style&index=0&id=23f52dba&lang=scss&scoped=true");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;


const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_3__["default"])(_accountingSendEmail_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_accountingSendEmail_vue_vue_type_template_id_23f52dba_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__scopeId',"data-v-23f52dba"],['__file',"src/pages/workspace/accounting/accountingSendEmail.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/accounting/accountingWallets.vue":
/*!**************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingWallets.vue ***!
  \**************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _accountingWallets_vue_vue_type_template_id_193aca89_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./accountingWallets.vue?vue&type=template&id=193aca89&scoped=true&ts=true */ "./src/pages/workspace/accounting/accountingWallets.vue?vue&type=template&id=193aca89&scoped=true&ts=true");
/* harmony import */ var _accountingWallets_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./accountingWallets.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/accounting/accountingWallets.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _accountingWallets_vue_vue_type_style_index_0_id_193aca89_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./accountingWallets.vue?vue&type=style&index=0&id=193aca89&lang=scss&scoped=true */ "./src/pages/workspace/accounting/accountingWallets.vue?vue&type=style&index=0&id=193aca89&lang=scss&scoped=true");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;


const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_3__["default"])(_accountingWallets_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_accountingWallets_vue_vue_type_template_id_193aca89_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__scopeId',"data-v-193aca89"],['__file',"src/pages/workspace/accounting/accountingWallets.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/accounting/accountingDisbursement.vue?vue&type=script&setup=true&lang=ts":
/*!******************************************************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingDisbursement.vue?vue&type=script&setup=true&lang=ts ***!
  \******************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingDisbursement_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingDisbursement_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingDisbursement.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingDisbursement.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/accounting/accountingGenerateDocument.vue?vue&type=script&setup=true&lang=ts":
/*!**********************************************************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingGenerateDocument.vue?vue&type=script&setup=true&lang=ts ***!
  \**********************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingGenerateDocument_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingGenerateDocument_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingGenerateDocument.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingGenerateDocument.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/accounting/accountingOverview.vue?vue&type=script&setup=true&lang=ts":
/*!**************************************************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingOverview.vue?vue&type=script&setup=true&lang=ts ***!
  \**************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingOverview_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingOverview_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingOverview.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingOverview.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/accounting/accountingPurchases.vue?vue&type=script&setup=true&lang=ts":
/*!***************************************************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingPurchases.vue?vue&type=script&setup=true&lang=ts ***!
  \***************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingPurchases_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingPurchases_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingPurchases.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingPurchases.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/accounting/accountingReceipt.vue?vue&type=script&setup=true&lang=ts":
/*!*************************************************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingReceipt.vue?vue&type=script&setup=true&lang=ts ***!
  \*************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingReceipt_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingReceipt_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingReceipt.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingReceipt.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/accounting/accountingRecordingExpense.vue?vue&type=script&setup=true&lang=ts":
/*!**********************************************************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingRecordingExpense.vue?vue&type=script&setup=true&lang=ts ***!
  \**********************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingRecordingExpense_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingRecordingExpense_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingRecordingExpense.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingRecordingExpense.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/accounting/accountingSendEmail.vue?vue&type=script&setup=true&lang=ts":
/*!***************************************************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingSendEmail.vue?vue&type=script&setup=true&lang=ts ***!
  \***************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingSendEmail_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingSendEmail_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingSendEmail.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingSendEmail.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/accounting/accountingWallets.vue?vue&type=script&setup=true&lang=ts":
/*!*************************************************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingWallets.vue?vue&type=script&setup=true&lang=ts ***!
  \*************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingWallets_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingWallets_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingWallets.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingWallets.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/accounting/accountingDisbursement.vue?vue&type=template&id=7140f842&scoped=true&ts=true":
/*!*********************************************************************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingDisbursement.vue?vue&type=template&id=7140f842&scoped=true&ts=true ***!
  \*********************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingDisbursement_vue_vue_type_template_id_7140f842_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingDisbursement_vue_vue_type_template_id_7140f842_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingDisbursement.vue?vue&type=template&id=7140f842&scoped=true&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingDisbursement.vue?vue&type=template&id=7140f842&scoped=true&ts=true");


/***/ }),

/***/ "./src/pages/workspace/accounting/accountingGenerateDocument.vue?vue&type=template&id=094da8d1&ts=true":
/*!*************************************************************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingGenerateDocument.vue?vue&type=template&id=094da8d1&ts=true ***!
  \*************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingGenerateDocument_vue_vue_type_template_id_094da8d1_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingGenerateDocument_vue_vue_type_template_id_094da8d1_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingGenerateDocument.vue?vue&type=template&id=094da8d1&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingGenerateDocument.vue?vue&type=template&id=094da8d1&ts=true");


/***/ }),

/***/ "./src/pages/workspace/accounting/accountingOverview.vue?vue&type=template&id=5efc1d1a&ts=true":
/*!*****************************************************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingOverview.vue?vue&type=template&id=5efc1d1a&ts=true ***!
  \*****************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingOverview_vue_vue_type_template_id_5efc1d1a_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingOverview_vue_vue_type_template_id_5efc1d1a_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingOverview.vue?vue&type=template&id=5efc1d1a&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingOverview.vue?vue&type=template&id=5efc1d1a&ts=true");


/***/ }),

/***/ "./src/pages/workspace/accounting/accountingPurchases.vue?vue&type=template&id=156c84c1&scoped=true&ts=true":
/*!******************************************************************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingPurchases.vue?vue&type=template&id=156c84c1&scoped=true&ts=true ***!
  \******************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingPurchases_vue_vue_type_template_id_156c84c1_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingPurchases_vue_vue_type_template_id_156c84c1_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingPurchases.vue?vue&type=template&id=156c84c1&scoped=true&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingPurchases.vue?vue&type=template&id=156c84c1&scoped=true&ts=true");


/***/ }),

/***/ "./src/pages/workspace/accounting/accountingReceipt.vue?vue&type=template&id=7da6dbb2&scoped=true&ts=true":
/*!****************************************************************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingReceipt.vue?vue&type=template&id=7da6dbb2&scoped=true&ts=true ***!
  \****************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingReceipt_vue_vue_type_template_id_7da6dbb2_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingReceipt_vue_vue_type_template_id_7da6dbb2_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingReceipt.vue?vue&type=template&id=7da6dbb2&scoped=true&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingReceipt.vue?vue&type=template&id=7da6dbb2&scoped=true&ts=true");


/***/ }),

/***/ "./src/pages/workspace/accounting/accountingRecordingExpense.vue?vue&type=template&id=5304de70&scoped=true&ts=true":
/*!*************************************************************************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingRecordingExpense.vue?vue&type=template&id=5304de70&scoped=true&ts=true ***!
  \*************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingRecordingExpense_vue_vue_type_template_id_5304de70_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingRecordingExpense_vue_vue_type_template_id_5304de70_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingRecordingExpense.vue?vue&type=template&id=5304de70&scoped=true&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingRecordingExpense.vue?vue&type=template&id=5304de70&scoped=true&ts=true");


/***/ }),

/***/ "./src/pages/workspace/accounting/accountingSendEmail.vue?vue&type=template&id=23f52dba&scoped=true&ts=true":
/*!******************************************************************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingSendEmail.vue?vue&type=template&id=23f52dba&scoped=true&ts=true ***!
  \******************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingSendEmail_vue_vue_type_template_id_23f52dba_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingSendEmail_vue_vue_type_template_id_23f52dba_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingSendEmail.vue?vue&type=template&id=23f52dba&scoped=true&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingSendEmail.vue?vue&type=template&id=23f52dba&scoped=true&ts=true");


/***/ }),

/***/ "./src/pages/workspace/accounting/accountingWallets.vue?vue&type=template&id=193aca89&scoped=true&ts=true":
/*!****************************************************************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingWallets.vue?vue&type=template&id=193aca89&scoped=true&ts=true ***!
  \****************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingWallets_vue_vue_type_template_id_193aca89_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingWallets_vue_vue_type_template_id_193aca89_scoped_true_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js!../../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingWallets.vue?vue&type=template&id=193aca89&scoped=true&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingWallets.vue?vue&type=template&id=193aca89&scoped=true&ts=true");


/***/ }),

/***/ "./src/pages/workspace/accounting/accountingDisbursement.vue?vue&type=style&index=0&id=7140f842&lang=scss&scoped=true":
/*!****************************************************************************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingDisbursement.vue?vue&type=style&index=0&id=7140f842&lang=scss&scoped=true ***!
  \****************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingDisbursement_vue_vue_type_style_index_0_id_7140f842_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!../../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../../node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingDisbursement.vue?vue&type=style&index=0&id=7140f842&lang=scss&scoped=true */ "./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingDisbursement.vue?vue&type=style&index=0&id=7140f842&lang=scss&scoped=true");
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingDisbursement_vue_vue_type_style_index_0_id_7140f842_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingDisbursement_vue_vue_type_style_index_0_id_7140f842_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingDisbursement_vue_vue_type_style_index_0_id_7140f842_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = function(key) { return _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingDisbursement_vue_vue_type_style_index_0_id_7140f842_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__[key]; }.bind(0, __WEBPACK_IMPORT_KEY__)
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);


/***/ }),

/***/ "./src/pages/workspace/accounting/accountingPurchases.vue?vue&type=style&index=0&id=156c84c1&lang=scss&scoped=true":
/*!*************************************************************************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingPurchases.vue?vue&type=style&index=0&id=156c84c1&lang=scss&scoped=true ***!
  \*************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingPurchases_vue_vue_type_style_index_0_id_156c84c1_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!../../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../../node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingPurchases.vue?vue&type=style&index=0&id=156c84c1&lang=scss&scoped=true */ "./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingPurchases.vue?vue&type=style&index=0&id=156c84c1&lang=scss&scoped=true");
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingPurchases_vue_vue_type_style_index_0_id_156c84c1_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingPurchases_vue_vue_type_style_index_0_id_156c84c1_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingPurchases_vue_vue_type_style_index_0_id_156c84c1_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = function(key) { return _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingPurchases_vue_vue_type_style_index_0_id_156c84c1_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__[key]; }.bind(0, __WEBPACK_IMPORT_KEY__)
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);


/***/ }),

/***/ "./src/pages/workspace/accounting/accountingReceipt.vue?vue&type=style&index=0&id=7da6dbb2&lang=scss&scoped=true":
/*!***********************************************************************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingReceipt.vue?vue&type=style&index=0&id=7da6dbb2&lang=scss&scoped=true ***!
  \***********************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingReceipt_vue_vue_type_style_index_0_id_7da6dbb2_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!../../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../../node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingReceipt.vue?vue&type=style&index=0&id=7da6dbb2&lang=scss&scoped=true */ "./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingReceipt.vue?vue&type=style&index=0&id=7da6dbb2&lang=scss&scoped=true");
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingReceipt_vue_vue_type_style_index_0_id_7da6dbb2_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingReceipt_vue_vue_type_style_index_0_id_7da6dbb2_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingReceipt_vue_vue_type_style_index_0_id_7da6dbb2_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = function(key) { return _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingReceipt_vue_vue_type_style_index_0_id_7da6dbb2_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__[key]; }.bind(0, __WEBPACK_IMPORT_KEY__)
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);


/***/ }),

/***/ "./src/pages/workspace/accounting/accountingRecordingExpense.vue?vue&type=style&index=0&id=5304de70&lang=scss&scoped=true":
/*!********************************************************************************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingRecordingExpense.vue?vue&type=style&index=0&id=5304de70&lang=scss&scoped=true ***!
  \********************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingRecordingExpense_vue_vue_type_style_index_0_id_5304de70_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!../../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../../node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingRecordingExpense.vue?vue&type=style&index=0&id=5304de70&lang=scss&scoped=true */ "./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingRecordingExpense.vue?vue&type=style&index=0&id=5304de70&lang=scss&scoped=true");
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingRecordingExpense_vue_vue_type_style_index_0_id_5304de70_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingRecordingExpense_vue_vue_type_style_index_0_id_5304de70_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingRecordingExpense_vue_vue_type_style_index_0_id_5304de70_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = function(key) { return _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingRecordingExpense_vue_vue_type_style_index_0_id_5304de70_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__[key]; }.bind(0, __WEBPACK_IMPORT_KEY__)
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);


/***/ }),

/***/ "./src/pages/workspace/accounting/accountingSendEmail.vue?vue&type=style&index=0&id=23f52dba&lang=scss&scoped=true":
/*!*************************************************************************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingSendEmail.vue?vue&type=style&index=0&id=23f52dba&lang=scss&scoped=true ***!
  \*************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingSendEmail_vue_vue_type_style_index_0_id_23f52dba_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!../../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../../node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingSendEmail.vue?vue&type=style&index=0&id=23f52dba&lang=scss&scoped=true */ "./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingSendEmail.vue?vue&type=style&index=0&id=23f52dba&lang=scss&scoped=true");
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingSendEmail_vue_vue_type_style_index_0_id_23f52dba_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingSendEmail_vue_vue_type_style_index_0_id_23f52dba_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingSendEmail_vue_vue_type_style_index_0_id_23f52dba_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = function(key) { return _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingSendEmail_vue_vue_type_style_index_0_id_23f52dba_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__[key]; }.bind(0, __WEBPACK_IMPORT_KEY__)
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);


/***/ }),

/***/ "./src/pages/workspace/accounting/accountingWallets.vue?vue&type=style&index=0&id=193aca89&lang=scss&scoped=true":
/*!***********************************************************************************************************************!*\
  !*** ./src/pages/workspace/accounting/accountingWallets.vue?vue&type=style&index=0&id=193aca89&lang=scss&scoped=true ***!
  \***********************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingWallets_vue_vue_type_style_index_0_id_193aca89_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!../../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../../node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingWallets.vue?vue&type=style&index=0&id=193aca89&lang=scss&scoped=true */ "./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingWallets.vue?vue&type=style&index=0&id=193aca89&lang=scss&scoped=true");
/* harmony import */ var _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingWallets_vue_vue_type_style_index_0_id_193aca89_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingWallets_vue_vue_type_style_index_0_id_193aca89_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingWallets_vue_vue_type_style_index_0_id_193aca89_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = function(key) { return _node_modules_vue_style_loader_index_js_clonedRuleSet_22_use_0_node_modules_css_loader_dist_cjs_js_clonedRuleSet_22_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_vue_cli_service_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_22_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_22_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_accountingWallets_vue_vue_type_style_index_0_id_193aca89_lang_scss_scoped_true__WEBPACK_IMPORTED_MODULE_0__[key]; }.bind(0, __WEBPACK_IMPORT_KEY__)
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);


/***/ }),

/***/ "./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingDisbursement.vue?vue&type=style&index=0&id=7140f842&lang=scss&scoped=true":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingDisbursement.vue?vue&type=style&index=0&id=7140f842&lang=scss&scoped=true ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(/*! !!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!../../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../../node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingDisbursement.vue?vue&type=style&index=0&id=7140f842&lang=scss&scoped=true */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingDisbursement.vue?vue&type=style&index=0&id=7140f842&lang=scss&scoped=true");
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(/*! !../../../../node_modules/vue-style-loader/lib/addStylesClient.js */ "./node_modules/vue-style-loader/lib/addStylesClient.js")["default"])
var update = add("f90fcb9a", content, false, {"sourceMap":false,"shadowMode":false});
// Hot Module Replacement
if(false) {}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingPurchases.vue?vue&type=style&index=0&id=156c84c1&lang=scss&scoped=true":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingPurchases.vue?vue&type=style&index=0&id=156c84c1&lang=scss&scoped=true ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(/*! !!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!../../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../../node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingPurchases.vue?vue&type=style&index=0&id=156c84c1&lang=scss&scoped=true */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingPurchases.vue?vue&type=style&index=0&id=156c84c1&lang=scss&scoped=true");
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(/*! !../../../../node_modules/vue-style-loader/lib/addStylesClient.js */ "./node_modules/vue-style-loader/lib/addStylesClient.js")["default"])
var update = add("923a1740", content, false, {"sourceMap":false,"shadowMode":false});
// Hot Module Replacement
if(false) {}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingReceipt.vue?vue&type=style&index=0&id=7da6dbb2&lang=scss&scoped=true":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingReceipt.vue?vue&type=style&index=0&id=7da6dbb2&lang=scss&scoped=true ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(/*! !!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!../../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../../node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingReceipt.vue?vue&type=style&index=0&id=7da6dbb2&lang=scss&scoped=true */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingReceipt.vue?vue&type=style&index=0&id=7da6dbb2&lang=scss&scoped=true");
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(/*! !../../../../node_modules/vue-style-loader/lib/addStylesClient.js */ "./node_modules/vue-style-loader/lib/addStylesClient.js")["default"])
var update = add("60d96d92", content, false, {"sourceMap":false,"shadowMode":false});
// Hot Module Replacement
if(false) {}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingRecordingExpense.vue?vue&type=style&index=0&id=5304de70&lang=scss&scoped=true":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingRecordingExpense.vue?vue&type=style&index=0&id=5304de70&lang=scss&scoped=true ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(/*! !!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!../../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../../node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingRecordingExpense.vue?vue&type=style&index=0&id=5304de70&lang=scss&scoped=true */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingRecordingExpense.vue?vue&type=style&index=0&id=5304de70&lang=scss&scoped=true");
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(/*! !../../../../node_modules/vue-style-loader/lib/addStylesClient.js */ "./node_modules/vue-style-loader/lib/addStylesClient.js")["default"])
var update = add("3e09516e", content, false, {"sourceMap":false,"shadowMode":false});
// Hot Module Replacement
if(false) {}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingSendEmail.vue?vue&type=style&index=0&id=23f52dba&lang=scss&scoped=true":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingSendEmail.vue?vue&type=style&index=0&id=23f52dba&lang=scss&scoped=true ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(/*! !!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!../../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../../node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingSendEmail.vue?vue&type=style&index=0&id=23f52dba&lang=scss&scoped=true */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingSendEmail.vue?vue&type=style&index=0&id=23f52dba&lang=scss&scoped=true");
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(/*! !../../../../node_modules/vue-style-loader/lib/addStylesClient.js */ "./node_modules/vue-style-loader/lib/addStylesClient.js")["default"])
var update = add("0aed5e44", content, false, {"sourceMap":false,"shadowMode":false});
// Hot Module Replacement
if(false) {}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingWallets.vue?vue&type=style&index=0&id=193aca89&lang=scss&scoped=true":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-style-loader/index.js??clonedRuleSet-22.use[0]!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingWallets.vue?vue&type=style&index=0&id=193aca89&lang=scss&scoped=true ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(/*! !!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!../../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../../node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./accountingWallets.vue?vue&type=style&index=0&id=193aca89&lang=scss&scoped=true */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-22.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/@vue/cli-service/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-22.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-22.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/accounting/accountingWallets.vue?vue&type=style&index=0&id=193aca89&lang=scss&scoped=true");
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = (__webpack_require__(/*! !../../../../node_modules/vue-style-loader/lib/addStylesClient.js */ "./node_modules/vue-style-loader/lib/addStylesClient.js")["default"])
var update = add("413cc054", content, false, {"sourceMap":false,"shadowMode":false});
// Hot Module Replacement
if(false) {}

/***/ })

}]);
//# sourceMappingURL=workspace-accounting.js.map