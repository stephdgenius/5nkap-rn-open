"use strict";
(self["webpackChunk_5nkap"] = self["webpackChunk_5nkap"] || []).push([["workspace-overview"],{

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/overview.vue?vue&type=script&setup=true&lang=ts":
/*!************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/overview.vue?vue&type=script&setup=true&lang=ts ***!
  \************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");
/* harmony import */ var _vueuse_head__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @vueuse/head */ "./node_modules/@unhead/vue/dist/shared/vue.f36acd1f.mjs");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/api */ "./src/api/index.ts");
/* harmony import */ var vue_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! vue-router */ "./node_modules/vue-router/dist/vue-router.mjs");
/* harmony import */ var _state_api_userState__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/state/api/userState */ "./src/state/api/userState.ts");
/* harmony import */ var _composable_useFormatMoney__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/composable/useFormatMoney */ "./src/composable/useFormatMoney.ts");
/* harmony import */ var _composable_useReports__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/composable/useReports */ "./src/composable/useReports.ts");
/* harmony import */ var _composable_useFeature__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/composable/useFeature */ "./src/composable/useFeature.ts");
/* harmony import */ var _composable_useAccess__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @/composable/useAccess */ "./src/composable/useAccess.ts");












/**
 * Injects
 */
/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.defineComponent)({
  __name: 'overview',
  setup(__props, {
    expose: __expose
  }) {
    __expose();
    const activePage = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("activePage");
    const userStats = (0,vue__WEBPACK_IMPORTED_MODULE_0__.inject)("userStats");
    const userStats2 = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)([]);
    /**
     * Variables
     */
    const route = (0,vue_router__WEBPACK_IMPORTED_MODULE_8__.useRoute)();
    const incomingTransactionsChart = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    const incomingTransactionsInstance = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)(null);
    const outgoingTransactionsChart = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)();
    const outgoingTransactionsInstance = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)();
    const incomingTransactionsPeriod = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("3");
    const outgoingTransactionsPeriod = (0,vue__WEBPACK_IMPORTED_MODULE_0__.ref)("3");
    /**
     * Watcher
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(outgoingTransactionsPeriod, newValue => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_7__["default"])("read", permissions_graph)) {
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_5__.refreshChart)("transactions_sortantes", newValue, outgoingTransactionsChart.value, outgoingTransactionsInstance);
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(incomingTransactionsPeriod, newValue => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_7__["default"])("read", permissions_graph)) {
        (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_5__.refreshChart)("transactions_sortantes", newValue, incomingTransactionsChart.value, incomingTransactionsInstance);
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => route.params.page, newValue => {
      if (newValue == "home") {
        setTimeout(() => {
          setAllChart();
        }, 300);
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.watch)(() => _state_api_userState__WEBPACK_IMPORTED_MODULE_3__.userCurrentBranch.value.id, newValue => {
      // incomingTransactionsPeriod.value.toString(),
      if (route.name == "workspace-overview" && route.params.page == "home") {
        // setAllChart();
        // if (useAccess('read', permissions_graph)) {
        (0,_state_api_userState__WEBPACK_IMPORTED_MODULE_3__.refreshUserInfos)();
        // }
      }
    });

    function setAllChart() {
      (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_5__.setCharts)("transactions_entrantes", incomingTransactionsPeriod.value, incomingTransactionsChart.value, "Transactions entrantes", incomingTransactionsInstance);
      (0,_composable_useReports__WEBPACK_IMPORTED_MODULE_5__.setCharts)("transactions_sortantes", outgoingTransactionsPeriod.value, outgoingTransactionsChart.value, "Transactions sortantes", outgoingTransactionsInstance);
    }
    async function getWalletState() {
      await (0,_api__WEBPACK_IMPORTED_MODULE_2__["default"])().comptability.WalletState().then(response => {
        console.log("response walet infos :", response.data);
        userStats2.value = response.data;
      });
    }
    // User permissions
    const permissions_graph = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_6__["default"])("graphes").map(element => {
      return element.action;
    });
    // User permissions
    const permissions_wallet = (0,_composable_useFeature__WEBPACK_IMPORTED_MODULE_6__["default"])("etatPortefeuilles").map(element => {
      return element.action;
    });
    /**
     * Page Lifecycle
     */
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onBeforeMount)(async () => {
      if ((0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_7__["default"])("read", permissions_wallet)) {
        await getWalletState();
      }
    });
    (0,vue__WEBPACK_IMPORTED_MODULE_0__.onMounted)(async () => {
      // incomingTransactionsPeriod.value.toString(),
      if (route.name == "workspace-overview" && route.params.page == "home" && (0,_composable_useAccess__WEBPACK_IMPORTED_MODULE_7__["default"])("read", permissions_graph)) {
        setAllChart();
      }
      // console.log("orderByModule", orderByModule());
    });

    (0,_vueuse_head__WEBPACK_IMPORTED_MODULE_9__.u)({
      title: "Accueil - Tableau de bord | " + lodash__WEBPACK_IMPORTED_MODULE_1__.upperCase(_state_api_userState__WEBPACK_IMPORTED_MODULE_3__.userInfos.value.enterprise ? _state_api_userState__WEBPACK_IMPORTED_MODULE_3__.userInfos.value.enterprise.nom_entreprise : _state_api_userState__WEBPACK_IMPORTED_MODULE_3__.userInfos.value.staff ? _state_api_userState__WEBPACK_IMPORTED_MODULE_3__.userInfos.value.staff.label : _state_api_userState__WEBPACK_IMPORTED_MODULE_3__.userInfos.value.particular.nom)
    });
    const __returned__ = {
      activePage,
      userStats,
      userStats2,
      route,
      incomingTransactionsChart,
      incomingTransactionsInstance,
      outgoingTransactionsChart,
      outgoingTransactionsInstance,
      incomingTransactionsPeriod,
      outgoingTransactionsPeriod,
      setAllChart,
      getWalletState,
      permissions_graph,
      permissions_wallet,
      get orignalMoney() {
        return _composable_useFormatMoney__WEBPACK_IMPORTED_MODULE_4__["default"];
      },
      get month_options() {
        return _composable_useReports__WEBPACK_IMPORTED_MODULE_5__.month_options;
      },
      get useAccess() {
        return _composable_useAccess__WEBPACK_IMPORTED_MODULE_7__["default"];
      }
    };
    Object.defineProperty(__returned__, '__isScriptSetup', {
      enumerable: false,
      value: true
    });
    return __returned__;
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/overview.vue?vue&type=template&id=4dae75b7&ts=true":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/overview.vue?vue&type=template&id=4dae75b7&ts=true ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");

const _hoisted_1 = {
  class: "w-full h-full pb-5 overflow-x-hidden workspace-content md:pb-0"
};
const _hoisted_2 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
  class: "hidden w-10 h-10 bg-blue-600"
}, null, -1 /* HOISTED */);
const _hoisted_3 = {
  key: 0,
  class: "w-full"
};
const _hoisted_4 = {
  class: "grid w-full h-auto gap-6 mb-6 md:grid-cols-1 lg:grid-cols-2"
};
const _hoisted_5 = {
  class: "w-full h-56"
};
const _hoisted_6 = {
  class: "relative flex flex-col justify-between w-full h-full p-6 bg-white rounded-2xl"
};
const _hoisted_7 = {
  class: "flex items-center justify-between card-header"
};
const _hoisted_8 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "items-center hidden text-lg md:flex"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/trend-down-icon.svg",
  alt: "",
  class: "mr-4"
}), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Transactions entrantes ")], -1 /* HOISTED */);
const _hoisted_9 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "flex items-center text-lg md:hidden"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/trend-down-icon.svg",
  alt: "",
  class: "mr-4"
}), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Entrée ")], -1 /* HOISTED */);
const _hoisted_10 = ["value"];
const _hoisted_11 = {
  key: 0,
  id: "incomingTransactionsChart",
  ref: "incomingTransactionsChart",
  width: "100%",
  height: "90%"
};
const _hoisted_12 = ["src"];
const _hoisted_13 = {
  class: "w-full h-56"
};
const _hoisted_14 = {
  class: "relative flex flex-col justify-between w-full h-full p-6 bg-white rounded-2xl"
};
const _hoisted_15 = {
  class: "flex items-center justify-between card-header"
};
const _hoisted_16 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "items-center hidden text-lg md:flex"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/trend-up-icon.svg",
  alt: "",
  class: "mr-4"
}), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Transactions sortantes ")], -1 /* HOISTED */);
const _hoisted_17 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "flex items-center text-lg md:hidden"
}, [/*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/trend-up-icon.svg",
  alt: "",
  class: "mr-4"
}), /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createTextVNode)(" Sorties ")], -1 /* HOISTED */);
const _hoisted_18 = ["value"];
const _hoisted_19 = {
  key: 0,
  id: "outgoingTransactionsChart",
  ref: "outgoingTransactionsChart",
  width: "100%",
  height: "90%"
};
const _hoisted_20 = ["src"];
const _hoisted_21 = {
  class: "grid w-full gap-x-0 md:gap-x-6 gap-y-8 md:gap-y-6 md:h-4/5 xl:h-1/3 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-3 2xl:grid-cols-3"
};
const _hoisted_22 = {
  class: "w-full"
};
const _hoisted_23 = {
  class: "relative flex flex-col justify-between w-full h-full p-6 bg-white rounded-2xl"
};
const _hoisted_24 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "text-lg opacity-90"
}, "Chiffre d'affaires", -1 /* HOISTED */);
const _hoisted_25 = {
  class: "flex flex-col"
};
const _hoisted_26 = {
  class: "mb-3 text-xl text-green-700"
};
const _hoisted_27 = {
  class: "flex flex-row items-end space-x-1"
};
const _hoisted_28 = {
  key: 0,
  class: "text-4xl font-medium"
};
const _hoisted_29 = ["src"];
const _hoisted_30 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/dollar-icon-color.svg",
  class: "absolute bottom-0 right-0 rounded-2xl"
}, null, -1 /* HOISTED */);
const _hoisted_31 = {
  class: "w-full"
};
const _hoisted_32 = {
  class: "relative flex flex-col justify-between w-full h-full p-6 bg-white rounded-2xl"
};
const _hoisted_33 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "text-lg opacity-90"
}, "Banque", -1 /* HOISTED */);
const _hoisted_34 = {
  class: "flex flex-col"
};
const _hoisted_35 = {
  class: "mb-3 text-xl text-yellow-600"
};
const _hoisted_36 = {
  class: "flex flex-row items-end space-x-1"
};
const _hoisted_37 = {
  key: 0,
  class: "text-4xl font-medium"
};
const _hoisted_38 = ["src"];
const _hoisted_39 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/money-pill-icon-color.svg",
  class: "absolute bottom-0 right-0 md:h-24 md:w-24 lg:h-32 lg:w-32 rounded-2xl"
}, null, -1 /* HOISTED */);
const _hoisted_40 = {
  class: "w-full"
};
const _hoisted_41 = {
  class: "relative flex flex-col justify-between w-full h-full p-6 bg-white rounded-2xl"
};
const _hoisted_42 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "text-lg opacity-90"
}, "Caisse", -1 /* HOISTED */);
const _hoisted_43 = {
  class: "flex flex-col"
};
const _hoisted_44 = {
  class: "mb-3 text-xl text-green-700"
};
const _hoisted_45 = {
  class: "flex flex-row items-end space-x-1"
};
const _hoisted_46 = {
  key: 0,
  class: "text-4xl font-medium"
};
const _hoisted_47 = ["src"];
const _hoisted_48 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/money-icon-color.svg",
  class: "absolute bottom-0 right-0 md:h-24 md:w-24 lg:h-32 lg:w-32 rounded-2xl"
}, null, -1 /* HOISTED */);
const _hoisted_49 = {
  class: "w-full"
};
const _hoisted_50 = {
  class: "relative flex flex-col justify-between w-full h-full p-6 bg-white rounded-2xl"
};
const _hoisted_51 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "text-lg opacity-90"
}, "Créances", -1 /* HOISTED */);
const _hoisted_52 = {
  class: "flex flex-col"
};
const _hoisted_53 = {
  class: "mb-3 text-xl text-yellow-600"
};
const _hoisted_54 = {
  class: "flex flex-row items-end space-x-1"
};
const _hoisted_55 = {
  key: 0,
  class: "text-4xl font-medium"
};
const _hoisted_56 = ["src"];
const _hoisted_57 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/money-bag-icon-color.svg",
  class: "absolute bottom-0 right-0 md:h-24 md:w-24 lg:h-32 lg:w-32 rounded-2xl"
}, null, -1 /* HOISTED */);
const _hoisted_58 = {
  class: "w-full"
};
const _hoisted_59 = {
  class: "relative flex flex-col justify-between w-full h-full p-6 bg-white rounded-2xl"
};
const _hoisted_60 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "text-lg opacity-90"
}, "Dettes", -1 /* HOISTED */);
const _hoisted_61 = {
  class: "flex flex-col"
};
const _hoisted_62 = {
  class: "mb-3 text-xl text-yellow-600"
};
const _hoisted_63 = {
  class: "flex flex-row items-end space-x-1"
};
const _hoisted_64 = {
  key: 0,
  class: "text-4xl font-medium"
};
const _hoisted_65 = ["src"];
const _hoisted_66 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/money-bag-icon-color.svg",
  class: "absolute bottom-0 right-0 lg:h-32 lg:w-32 md:h-24 md:w-24 rounded-2xl"
}, null, -1 /* HOISTED */);
const _hoisted_67 = {
  class: "w-full"
};
const _hoisted_68 = {
  class: "relative flex flex-col justify-between w-full h-full p-6 bg-white rounded-2xl"
};
const _hoisted_69 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("h4", {
  class: "text-lg opacity-90"
}, "Mobile Money", -1 /* HOISTED */);
const _hoisted_70 = {
  class: "flex flex-col"
};
const _hoisted_71 = {
  class: "mb-3 text-xl text-yellow-600"
};
const _hoisted_72 = {
  class: "flex flex-row items-end space-x-1"
};
const _hoisted_73 = {
  key: 0,
  class: "text-4xl font-medium"
};
const _hoisted_74 = ["src"];
const _hoisted_75 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
  src: "/icons/money-bag-icon-color.svg",
  class: "absolute bottom-0 right-0 lg:h-32 lg:w-32 md:h-24 md:w-24 rounded-2xl"
}, null, -1 /* HOISTED */);
const _hoisted_76 = {
  key: 1
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_Notifications = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("Notifications");
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_1, [_hoisted_2, $setup.route.params.page === 'home' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_3, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_4, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_5, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_6, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_7, [_hoisted_8, _hoisted_9, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("select", {
    "onUpdate:modelValue": _cache[0] || (_cache[0] = $event => $setup.incomingTransactionsPeriod = $event),
    class: "w-auto pr-4 text-xl font-light text-right custom-select active:overflow-none focus:overflow-none"
  }, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.month_options, option => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("option", {
      key: option.key,
      value: option.key
    }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(option.value), 9 /* TEXT, PROPS */, _hoisted_10);
  }), 128 /* KEYED_FRAGMENT */))], 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelSelect, $setup.incomingTransactionsPeriod]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)(`${!$setup.useAccess('read', $setup.permissions_wallet) ? 'flex justify-center items-center' : ''} w-full mt-10 card-graph h-5/6`)
  }, [$setup.useAccess('read', $setup.permissions_graph) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("canvas", _hoisted_11, null, 512 /* NEED_PATCH */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "100px"
    }
  }, null, 8 /* PROPS */, _hoisted_12))], 2 /* CLASS */)])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_13, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_14, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_15, [_hoisted_16, _hoisted_17, (0,vue__WEBPACK_IMPORTED_MODULE_0__.withDirectives)((0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("select", {
    "onUpdate:modelValue": _cache[1] || (_cache[1] = $event => $setup.outgoingTransactionsPeriod = $event),
    class: "w-auto pr-4 text-xl font-light text-right custom-select active:overflow-none focus:overflow-none"
  }, [((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,vue__WEBPACK_IMPORTED_MODULE_0__.renderList)($setup.month_options, option => {
    return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("option", {
      key: option.key,
      value: option.key
    }, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(option.value), 9 /* TEXT, PROPS */, _hoisted_18);
  }), 128 /* KEYED_FRAGMENT */))], 512 /* NEED_PATCH */), [[vue__WEBPACK_IMPORTED_MODULE_0__.vModelSelect, $setup.outgoingTransactionsPeriod]])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)(`${!$setup.useAccess('read', $setup.permissions_wallet) ? 'flex justify-center items-center' : ''} w-full mt-10 card-graph h-5/6`)
  }, [$setup.useAccess('read', $setup.permissions_graph) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("canvas", _hoisted_19, null, 512 /* NEED_PATCH */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "100px"
    }
  }, null, 8 /* PROPS */, _hoisted_20))], 2 /* CLASS */)])])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_21, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_22, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_23, [_hoisted_24, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_25, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_26, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.useAccess("read", $setup.permissions_wallet) ? $setup.orignalMoney($setup.userStats2[5] != undefined ? $setup.userStats2[5].value : 0) : "") + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.userStats2[5]?.curency), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_27, [$setup.useAccess('read', $setup.permissions_wallet) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("h1", _hoisted_28, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(_ctx.$filters.moneyFormat($setup.userStats2[5] != undefined ? $setup.userStats2[5].value : 0).toString()), 1 /* TEXT */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "36px"
    }
  }, null, 8 /* PROPS */, _hoisted_29))])]), _hoisted_30])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_31, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_32, [_hoisted_33, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_34, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_35, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.useAccess("read", $setup.permissions_wallet) ? $setup.orignalMoney($setup.userStats2[1] != undefined ? $setup.userStats2[1].value : 0) : "") + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.userStats2[1]?.curency), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_36, [$setup.useAccess('read', $setup.permissions_wallet) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("h1", _hoisted_37, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(_ctx.$filters.moneyFormat($setup.userStats2[1] != undefined ? $setup.userStats2[1].value : 0).toString()), 1 /* TEXT */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "36px"
    }
  }, null, 8 /* PROPS */, _hoisted_38))])]), _hoisted_39])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_40, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_41, [_hoisted_42, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_43, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_44, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.useAccess("read", $setup.permissions_wallet) ? $setup.orignalMoney($setup.userStats2[0] != undefined ? $setup.userStats2[0].value : 0) : "") + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.userStats2[0]?.curency), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_45, [$setup.useAccess('read', $setup.permissions_wallet) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("h1", _hoisted_46, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(_ctx.$filters.moneyFormat($setup.userStats2[0] != undefined ? $setup.userStats2[0].value : 0).toString()), 1 /* TEXT */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "36px"
    }
  }, null, 8 /* PROPS */, _hoisted_47))])]), _hoisted_48])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_49, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_50, [_hoisted_51, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_52, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_53, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.useAccess("read", $setup.permissions_wallet) ? $setup.orignalMoney($setup.userStats2[3] != undefined ? $setup.userStats2[3].value : 0) : "") + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.userStats2[3]?.curency), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_54, [$setup.useAccess('read', $setup.permissions_wallet) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("h1", _hoisted_55, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(_ctx.$filters.moneyFormat($setup.userStats2[3] != undefined ? $setup.userStats2[3].value : 0).toString()), 1 /* TEXT */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "36px"
    }
  }, null, 8 /* PROPS */, _hoisted_56))])]), _hoisted_57])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_58, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_59, [_hoisted_60, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_61, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_62, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.useAccess("read", $setup.permissions_wallet) ? $setup.orignalMoney($setup.userStats2[4] != undefined ? $setup.userStats2[4].value : 0) : "") + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.userStats2[4]?.curency), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_63, [$setup.useAccess('read', $setup.permissions_wallet) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("h1", _hoisted_64, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(_ctx.$filters.moneyFormat($setup.userStats2[4] != undefined ? $setup.userStats2[4].value : 0).toString()), 1 /* TEXT */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "36px"
    }
  }, null, 8 /* PROPS */, _hoisted_65))])]), _hoisted_66])]), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)(" Mobile Money "), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_67, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_68, [_hoisted_69, (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_70, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("span", _hoisted_71, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.useAccess("read", $setup.permissions_wallet) ? $setup.orignalMoney($setup.userStats2[2] != undefined ? $setup.userStats2[2].value : 0) : "") + " " + (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)($setup.userStats2[2]?.curency), 1 /* TEXT */), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("div", _hoisted_72, [$setup.useAccess('read', $setup.permissions_wallet) ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("h1", _hoisted_73, (0,vue__WEBPACK_IMPORTED_MODULE_0__.toDisplayString)(_ctx.$filters.moneyFormat($setup.userStats2[2] != undefined ? $setup.userStats2[2].value : 0).toString()), 1 /* TEXT */)) : ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 1,
    src: '/icons/no-access-icon-c.svg',
    style: {
      "width": "36px"
    }
  }, null, 8 /* PROPS */, _hoisted_74))])]), _hoisted_75])])])])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), $setup.route.params.page === 'notifications' ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", _hoisted_76, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_Notifications)])) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true)]);
}

/***/ }),

/***/ "./src/pages/workspace/overview.vue":
/*!******************************************!*\
  !*** ./src/pages/workspace/overview.vue ***!
  \******************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _overview_vue_vue_type_template_id_4dae75b7_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./overview.vue?vue&type=template&id=4dae75b7&ts=true */ "./src/pages/workspace/overview.vue?vue&type=template&id=4dae75b7&ts=true");
/* harmony import */ var _overview_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./overview.vue?vue&type=script&setup=true&lang=ts */ "./src/pages/workspace/overview.vue?vue&type=script&setup=true&lang=ts");
/* harmony import */ var _node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_overview_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_overview_vue_vue_type_template_id_4dae75b7_ts_true__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"src/pages/workspace/overview.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./src/pages/workspace/overview.vue?vue&type=script&setup=true&lang=ts":
/*!*****************************************************************************!*\
  !*** ./src/pages/workspace/overview.vue?vue&type=script&setup=true&lang=ts ***!
  \*****************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_overview_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_overview_vue_vue_type_script_setup_true_lang_ts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js!../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./overview.vue?vue&type=script&setup=true&lang=ts */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/overview.vue?vue&type=script&setup=true&lang=ts");
 

/***/ }),

/***/ "./src/pages/workspace/overview.vue?vue&type=template&id=4dae75b7&ts=true":
/*!********************************************************************************!*\
  !*** ./src/pages/workspace/overview.vue?vue&type=template&id=4dae75b7&ts=true ***!
  \********************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   render: function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_overview_vue_vue_type_template_id_4dae75b7_ts_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_cli_plugin_typescript_node_modules_ts_loader_index_js_clonedRuleSet_41_use_1_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_4_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_overview_vue_vue_type_template_id_4dae75b7_ts_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js!../../../node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./overview.vue?vue&type=template&id=4dae75b7&ts=true */ "./node_modules/babel-loader/lib/index.js!./node_modules/@vue/cli-plugin-typescript/node_modules/ts-loader/index.js??clonedRuleSet-41.use[1]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[4]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./src/pages/workspace/overview.vue?vue&type=template&id=4dae75b7&ts=true");


/***/ })

}]);
//# sourceMappingURL=workspace-overview.js.map